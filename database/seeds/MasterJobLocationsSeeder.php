<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobLocations;

class MasterJobLocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobLocations::insert([
            'master_job_location_name' => 'Cavite',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Parañaque City',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Manila',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Makati City',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Quezon City',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Pasig',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Boni',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Taguig City',
            'master_job_country_id' => '1',
        ]);

        MasterJobLocations::insert([
            'master_job_location_name' => 'Pasay City',
            'master_job_country_id' => '1',
        ]);
        

    }
}
