<?php

use Illuminate\Database\Seeder;
use App\Models\MasterMailTemplate;

class MasterMailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterMailTemplate::insert([
            'master_mail_subject' => 'Welcome message from LEAPWORK',
            'master_mail_body' => ' <p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 25px;"><strong>Welcome to Leapwork!</strong></p>
                                    <p style="font-size: 15px;">Thank you for choosing LEAPWORK! Please wait for us to validate</p>
                                    <p style="font-size: 15px;">your documents. We will contact you as soon as possible if we are</p>
                                    <p style="font-size: 15px;">able to verify the documents that we have received.</p>
                                    <p style="font-size: 15px;">&nbsp;</p>
                                    <p><br /><span style="color: #ff0000; font-size: 15px;"><em>*NOTE: If the LEAPWORK ADMINISTRATOR has already created your Company </em></span></p>
                                    <p><span style="color: #ff0000; font-size: 15px;"><em>Profile, kindly check your email for payment instructions.</em></span></p>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'NEW COMPANY',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Company Verification from Leapwork',
            'master_mail_body' => '<p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 15px;"><strong>Congratulations! You are now verified!</strong></p>
                                    <p><strong>Your current subscription is:</strong></p>
                                    <p>PLAN NAME: {{planName}}</p>
                                    <p>AMOUNT: {{planAmount}}</p>
                                    <p><br /><span style="color: #ff0000; font-size: 15px;"><em>NOTE: If you already made payment, please wait within 24hrs to activate </em></span></p>
                                    <p><span style="color: #ff0000; font-size: 15px;"><em>your subscription plan. Kindly contact us if there are any concerns.</em></span></p>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin</p>
                                    <p>&nbsp;</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'COMPANY VERIFIED',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Company Verification from Leapwork',
            'master_mail_body' => '<p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 13px;"><strong>We would like to inform you that your registration for Leapwork </strong></p>
                                    <p style="font-size: 15px;"><strong>has been <span style="color: #ff0000;">DENIED</span> for some reasons:</strong></p>
                                    <p>&nbsp;</p>
                                    <ol>
                                    <li>Incomplete Documents.</li>
                                    <li>Documents does not match.</li>
                                    <li>Illegal Documents.</li>
                                    </ol>
                                    <p>&nbsp;</p>
                                    <p><br /><span style="color: #ff0000; font-size: 15px;"><em>*You may register again to use Leapwork. Please make sure that the </em></span></p>
                                    <p><span style="color: #ff0000; font-size: 15px;"><em>documents submitted are LEGAL and COMPLETED. </em></span></p>
                                    <p><span style="color: #ff0000; font-size: 15px;"><em>Please make a call on us if there are any concerns.</em></span></p>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin</p>
                                    <p>&nbsp;</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'COMPANY DECLINED',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Your Application status',
            'master_mail_body' => '<p>Congratulations! you have are now Accepted!</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'ACCEPTED',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Your Application status',
            'master_mail_body' => '<p>Sorry! you have been Declined</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'DECLINED',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Your Application status',
            'master_mail_body' => '<p>Sorry! you have been Rejected</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'REJECTED',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Your Application status',
            'master_mail_body' => '<p>Good Day! You are now for scheduled interview!</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'FOR INTERVIEW',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Your Application status',
            'master_mail_body' => '<p>Good Day! you are currently reviewed by our HR!</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'UNDER REVIEW',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Payment Verification from LEAPWORK',
            'master_mail_body' => '<p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 25px;"><strong>Thanks for choosing Leapwork.</strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 15px;">{{!!invoicePortalLink!!}} to check your INVOICE and kindly</p>
                                    <p style="font-size: 15px;">proceed to payment to enjoy your plan subscription.</p>
                                    <p>&nbsp;</p>
                                    <p><strong>Your current purchase is:</strong></p>
                                    <p>Payment Method: {{claimPaymentMethod}}</p>
                                    <p>Plan: {{claimPlan}}</p>
                                    <p>Amount: {{claimPlanAmount}}</p>
                                    <p>&nbsp;</p>
                                    <div>{{!!claimPortal!!}} to proceed in your payment to begin with your plan.</div>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'CREDIT CARD',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Payment Verification from LEAPWORK',
            'master_mail_body' => '<p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 25px;"><strong>Thanks for choosing Leapwork.</strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 15px;">{{!!invoicePortalLink!!}} to check your INVOICE and kindly proceed</p>
                                    <p style="font-size: 15px;">to payment to enjoy your plan subscription.</p>
                                    <p>&nbsp;</p>
                                    <p>Please check the instructions below to pay the invoice:</p>
                                    <p><strong>Bank Name</strong>: <strong><span style="color: #ff0000;">BDO</span></strong></p>
                                    <p><strong>Account Number</strong>: <strong><span style="color: #ff0000;">123-000032-2</span></strong></p>
                                    <p><strong>Account Name</strong>: <strong><span style="color: #ff0000;">COMMUDE PHILIPPINES, INC.</span></strong></p>
                                    <p><strong>Amount</strong>: <span style="color: #ff0000;"><strong>PHP&nbsp;{{claimPlanAmount}}</strong></span></p>
                                    <div>&nbsp;</div>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'BANK TRANSFER',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'INVOICE statement from LEAPWORK',
            'master_mail_body' => ' <p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 25px;"><strong>Thanks for choosing Leapwork.</strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 15px;">{{!!invoicePortalLink!!}} to check your INVOICE and kindly proceed</p>
                                    <p style="font-size: 15px;">to payment to enjoy your plan subscription.</p>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'ISSUED',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Payment Successful from LEAPWORK',
            'master_mail_body' => ' <p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{companyName}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 18px;"><strong>Thank you for purchasing!</strong></p>
                                    <p style="font-size: 18px;">&nbsp;</p>
                                    <p style="font-size: 14px;">Please {{!!loginLink!!}} to start your <strong>{{currentPlan}}</strong> plan</p>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin.</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'PAID',
        ]);

        MasterMailTemplate::insert([
            'master_mail_subject' => 'Welcome to LEAPWORK!',
            'master_mail_body' => '<p>{{leapworkLogo}}</p>
                                    <hr style="width: 500px; float: left;" />
                                    <p>&nbsp;</p>
                                    <p><strong>Good Day</strong> <strong><span style="font-size: 15px;">{{name}}!</span></strong></p>
                                    <p>&nbsp;</p>
                                    <p style="font-size: 25px;"><strong>Welcome to Leapwork!</strong></p>
                                    <p style="font-size: 13px;">Use this credential upon Login:</p>
                                    <p>Username: {{email}}</p>
                                    <p>&nbsp;</p>
                                    <div>{{!!activationLink!!}} to activate your account</div>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p>Cheers,</p>
                                    <p>Leapwork Admin&nbsp;</p>',
            'master_mail_date_created' => '2018-06-21 00:00:00',
            'master_mail_status' => 'NEW USER',
        ]);


        //
    }
}
