<?php

use Illuminate\Database\Seeder;
use App\Models\CompanyUserRights;

class CompanyUserRightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CompanyUserRights::insert([
            'company_user_right_id' => '1000036',
            'company_user_right_key' => 'JOB POST',
            'company_user_right_value' => 'b\'1111111111111111111111111111111\'',
            'company_user_right_company_user_id' => '100013',
        ]);

        CompanyUserRights::insert([
            'company_user_right_id' => '1000037',
            'company_user_right_key' => 'SCOUT',
            'company_user_right_value' => 'bl\'1111111111111111111111111111111\'',
            'company_user_right_company_user_id' => '100013',
        ]);

        CompanyUserRights::insert([
            'company_user_right_id' => '1000038',
            'company_user_right_key' => 'COMPANY INFORMATION',
            'company_user_right_value' => 'b\'1111111111111111111111111111111\'',
            'company_user_right_company_user_id' => '100013',
        ]);


        
    }
}
