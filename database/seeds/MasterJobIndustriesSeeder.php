<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobIndustries;

class MasterJobIndustriesSeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Accountants',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Advertising/Public Relations',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Agriculture',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Alcoholic Beverages',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Architectural Services',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Attorneys/Law Firms',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Auto Dealers',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Automotive',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Banks, Savings & Loans',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Books, Magazines & Newspapers',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Casinos / Gambling',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Computer Hardware',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Computer Software',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Dairy',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Dentists',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Doctors & Other Health Professionals',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Education',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Environment ',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Farming',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Finance, Insurance & Real Estate',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Food & Beverage',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Gas & Oil',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Health Services/HMOs',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Labor',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Lobbyists',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Mining',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Nurses',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Nutritional & Dietary Supplements',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Public Sector Unions',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Publishing & Printing',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Real Estate',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Record Companies/Singers',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Restaurants & Drinking Establishments',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Retail Sales',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Retired',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Savings & Loans',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Sea Transport',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Sports, Professional',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Telecom Services & Equipment',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Tobacco',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Transportation',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'TV Production',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Unions, Building Trades',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Vegetables & Fruits',
            'master_job_industry_status' => 'ACTIVE',
        ]);
        
        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Waste Management',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Women\'s Issues',
            'master_job_industry_status' => 'ACTIVE',
        ]);

        MasterJobIndustries::insert([
            'master_job_industry_name' => 'Other',
            'master_job_industry_status' => 'ACTIVE',
        ]);
    }
}
