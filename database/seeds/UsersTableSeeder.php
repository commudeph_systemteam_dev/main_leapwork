<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //seed the admin account ot the database
        DB::table('users')->insert([
            'email'             => 'admin@leapwork.com',
            'password'          => bcrypt('adminpassword'),
            'user_accounttype'  => 'ADMIN',
            'user_status'       => 'ACTIVE',
        ]);

        // DB::table('users')->insert([
        //     'email'             => 'corporate@leapwork.com',
        //     'password'          => bcrypt('adminpassword'),
        //     'user_accounttype'  => 'CORPORATE ADMIN',
        //     'user_status'       => 'ACTIVE',
        // ]);
    }
}
