<?php

use Illuminate\Database\Seeder;
use App\Models\MasterQualification;

class MasterQualificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterQualification::insert([
            'master_qualification_name' => 'Bachelors/ Degree',
        ]);

        MasterQualification::insert([
            'master_qualification_name' => 'Diploma/ Non Degree Tertiary',
        ]);
        
        MasterQualification::insert([
            'master_qualification_name' => 'Under Graduate',
        ]);
        
        MasterQualification::insert([
            'master_qualification_name' => 'Vocational Degree',
        ]);
        
        MasterQualification::insert([
            'master_qualification_name' => 'Masteral Degree',
        ]);
        
        MasterQualification::insert([
            'master_qualification_name' => 'Doctorate',
        ]);
    }
}
