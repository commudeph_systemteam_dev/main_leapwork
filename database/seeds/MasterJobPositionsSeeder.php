<?php

use Illuminate\Database\Seeder;
use App\Models\MasterJobPositions;

class MasterJobPositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterJobPositions::insert([
            'master_job_position_name' => 'Administrative Assistant',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '1',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Clerical',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Receptionist',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '99',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Graphic Designer',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Graphic Designer',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '99',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Director',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Sales Manager',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '99',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Software Engineer',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Business Analyst',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '99',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Manager',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Attorney',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Customer Service Representative',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Office Assistant',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);

        MasterJobPositions::insert([
            'master_job_position_name' => 'Call Center Representative',
            'master_job_position_status' => 'ACTIVE',
            'master_job_position_datecreated' => '17-10-2017',
            'master_job_position_industry_id' => '2',
        ]);
    }
}
