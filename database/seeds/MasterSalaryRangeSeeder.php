<?php

use Illuminate\Database\Seeder;
use App\Models\MasterSalaryRange;

class MasterSalaryRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterSalaryRange::insert([
            'master_salary_range_min' => '10000',
            'master_salary_range_max' => '20000',
            'master_salary_range_datecreated' => '2017-11-28 03:14:44',
        ]);
        
        MasterSalaryRange::insert([
            'master_salary_range_min' => '50000',
            'master_salary_range_max' => '10000',
            'master_salary_range_datecreated' => '2017-11-28 03:14:44',
        ]);

        MasterSalaryRange::insert([
            'master_salary_range_min' => '100000',
            'master_salary_range_max' => '200000',
            'master_salary_range_datecreated' => '2017-11-28 03:14:44',
        ]);

        MasterSalaryRange::insert([
            'master_salary_range_min' => '200000',
            'master_salary_range_max' => '500000',
            'master_salary_range_datecreated' => '2017-11-28 03:14:44',
        ]);

        MasterSalaryRange::insert([
            'master_salary_range_min' => '500000',
            'master_salary_range_max' => '1000000',
            'master_salary_range_datecreated' => '2017-11-28 03:14:44',
        ]);
        
    }
}
