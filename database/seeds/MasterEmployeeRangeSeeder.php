<?php

use Illuminate\Database\Seeder;
use App\Models\MasterEmployeeRange;

class MasterEmployeeRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterEmployeeRange::insert([
            'master_employee_range_min' => '1',
            'master_employee_range_max' => '100',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);
        
        MasterEmployeeRange::insert([
            'master_employee_range_min' => '100',
            'master_employee_range_max' => '1000',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);
        
        MasterEmployeeRange::insert([
            'master_employee_range_min' => '1000',
            'master_employee_range_max' => '10000',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);

        MasterEmployeeRange::insert([
            'master_employee_range_min' => '10000',
            'master_employee_range_max' => '50000',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);

        MasterEmployeeRange::insert([
            'master_employee_range_min' => '50000',
            'master_employee_range_max' => '500000',
            'master_employee_range_datecreated' => '2017-11-28 03:14:44',
        ]);
    }
}
