<?php

use Illuminate\Database\Seeder;
use App\Models\MasterCountry;

class MasterCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MasterCountry::insert([
            'master_country_name' => 'Philippines',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'Japan',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'Singapore',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'Malaysia',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'India',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'Russia',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'Indonesia',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'Hong Kong',
        ]);

        MasterCountry::insert([
            'master_country_name' => 'China',
        ]);

        MasterCountry::insert([
            'master_country_name' => 'Vietnam',
        ]);
        
        MasterCountry::insert([
            'master_country_name' => 'North Korea',
        ]);

        MasterCountry::insert([
            'master_country_name' => 'South Korea',
        ]);
        
    }
}
