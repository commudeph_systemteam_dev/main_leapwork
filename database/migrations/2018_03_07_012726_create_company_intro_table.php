<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyIntroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_intro', function (Blueprint $table) {
            $table->increments('company_intro_id');
            $table->text('company_intro_content')->nullable();
            $table->text('company_intro_pr')->nullable();
            $table->string('company_intro_image', 150)->nullable();
            $table->integer('company_intro_company_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_intro');
    }
}
