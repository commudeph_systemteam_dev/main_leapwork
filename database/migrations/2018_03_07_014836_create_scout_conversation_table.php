<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoutConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scout_conversation', function (Blueprint $table) {
            $table->increments('scout_conversation_id');
            $table->text('scout_conversation_message');
            $table->datetime('scout_conversation_date_added');
            $table->mediumInteger('scout_conversation_user_id')->unsigned()->nullable();
            $table->integer('scout_conversation_thread_id')->unsigned();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scout_conversation');
    }
}
