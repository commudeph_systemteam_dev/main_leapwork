<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scouts', function (Blueprint $table) {
            $table->increments('scout_id');
            $table->integer('scout_applicant_id')->unsigned()->nullable();
            $table->mediumInteger('scout_company_id')->unsigned()->nullable();
            $table->integer('scout_job_post_id')->unsigned()->nullable();
            $table->enum('scout_status', [
                'UNDER REVIEW',
                'INTERVIEW',
                'PENDING',
                'JOB OFFER',
                'ACCEPTED',
                'NOT ACCEPTED',
                'WITHDRAW',
                'FAVORITE'
            ]);
            $table->datetime('scout_date_scouted')->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scouts');
    }
}
