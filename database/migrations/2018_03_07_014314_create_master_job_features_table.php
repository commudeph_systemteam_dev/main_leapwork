<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterJobFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_job_features', function (Blueprint $table) {
            $table->increments('job_feature_id');
            $table->datetime('job_feature_datefrom');
            $table->datetime('job_feature_dateto');
            $table->enum('job_feature_visibility', [
                'PUBLIC',
                'PRIVATE',
            ])->nullable();
            $table->datetime('job_feature_datecreated')->nullable();
            $table->integer('job_feature_job_post_id')->unsigned();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_job_features');
    }
}
