<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIndustryMasterJobIndustryIdForeignKey extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::table('company_industry', function (Blueprint $table) {
            $table->foreign('company_industry_master_job_industry_id')->references('master_job_industry_id')->on('master_job_industries');
        });            //
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        //
    }
}