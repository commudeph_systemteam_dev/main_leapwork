<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostFavorites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_favorites', function (Blueprint $table) {
            $table->increments('job_post_favorite_id');
            $table->datetime('job_post_favorite_datecreated')->nullable();
            $table->integer('job_post_favorite_post_id')->unsigned()->nullable();
            $table->integer('job_post_favorite_user_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_favorites');
    }
}
