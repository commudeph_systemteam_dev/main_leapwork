<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_users', function (Blueprint $table) {
            $table->increments('company_user_id');
            $table->string('company_user_firstname', 15)->nullable();
            $table->string('company_user_lastname', 15)->nullable();
            $table->string('company_user_contact_no', 15)->nullable();
            $table->string('company_user_designation', 45)->nullable();
            $table->enum('company_user_gender', [
                'MALE',
                'FEMALE'
            ])->nullable();
            $table->date('company_user_birthdate')->nullable();
            $table->char('company_user_postalcode', 8)->nullable();
            $table->string('company_user_address1', 45)->nullable();
            $table->string('company_user_address2', 45)->nullable();
            $table->string('company_user_imagepath', 150)->nullable();
            $table->string('company_user_access', 45)->nullable();
            $table->integer('company_user_user_id')->unsigned();
            $table->integer('company_user_company_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_users');
    }
}
