<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPrivateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_private_info', function (Blueprint $table) {
            $table->increments('company_private_id');
            $table->string('company_private_creditcard_name', 45)->nullable();
            $table->string('company_private_credittype', 45)->nullable();
            $table->string('company_private_creditcard_number', 45)->nullable();
            $table->datetime('company_private_datecreated');
            $table->datetime('company_private_dateudated')->nullable();
            $table->integer('company_private_company_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_private_info');
    }
}
