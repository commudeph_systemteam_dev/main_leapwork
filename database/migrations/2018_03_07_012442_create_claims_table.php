<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('claim_id');
            $table->string('claim_name', 45)->nullable();
            $table->mediumInteger('claim_amount')->nullable();
            $table->enum('claim_payment_method', [
                'BANK TRANSFER',
                'CREDIT CARD',
                'PAYPAL',
            ]);
            $table->enum('claim_invoice_status', [
                'NOT ISSUED',
                'ISSUED',
                'REQUESTED'
            ])->nullable();
            $table->enum('claim_payment_status', [
                'PAID',
                'UNPAID'
            ]);
            $table->enum('claim_status', [
                'OPENED',
                'CLOSED',
                'CANCELLED'
            ])->nullable();
            $table->text('claim_transaction_no')->nullable();
            $table->text('claim_token')->nullable();
            $table->datetime('claim_start_date')->nullable();
            $table->datetime('claim_end_date')->nullable();
            $table->datetime('claim_datecreated');
            $table->datetime('claim_dateupdated')->nullable();
            $table->integer('claim_master_plan_id')->unsigned()->nullable();
            $table->integer('claim_company_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
