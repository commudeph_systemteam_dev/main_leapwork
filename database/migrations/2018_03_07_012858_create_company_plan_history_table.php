<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPlanHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_plan_history', function (Blueprint $table) {
            $table->increments('company_plan_history_id');
            $table->enum('company_plan_type', [
                'TRIAL',
                'STANDARD',
                'ENTERPRISE',
            ])->nullable();
            $table->enum('company_plan_status', [
                'ACTIVE',
                'INACTIVE',
                'EXPIRED'
            ])->nullable();
            $table->datetime('company_plan_datestarted')->nullable();
            $table->datetime('company_plan_dateexpiry')->nullable();
            $table->integer('company_plan_company_id')->unsigned()->nullable();
            $table->integer('company_plan_history_claim_id')->unsigned()->nullable();

            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_plan_history');
    }
}
