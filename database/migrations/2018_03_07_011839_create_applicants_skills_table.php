<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants_skills', function (Blueprint $table) {
            $table->increments('applicant_skill_id');
            $table->string('applicant_skill_name', 15)->nullable();
            $table->enum('applicant_skill_type', [
                'COMMUNICATION',
                'TECHNICAL'
            ])->nullable();
            $table->string('applicant_skill_level', 45)->nullable();
            $table->integer('applicant_skill_profile_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants_skills');
    }
}
