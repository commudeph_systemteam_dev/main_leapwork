<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_views', function (Blueprint $table) {
            $table->increments('job_post_view_id');
            $table->integer('job_post_view_job_post_id')->unsigned();
            $table->boolean('job_post_view_archive_flag')->default(0);
            $table->integer('job_post_view_user_id')->unsigned()->nullable();
            $table->datetime('job_post_view_date_created')->useCurrent();

            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_views');
    }
}
