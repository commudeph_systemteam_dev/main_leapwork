<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCompanyConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_conversations', function (Blueprint $table) {
            $table->increments('admin_company_conversation_id');

            $table->text('admin_company_conversation_message');
            $table->datetime('admin_company_conversation_datecreated');
            $table->integer('admin_company_conversation_user_id')->unsigned();
            $table->integer('admin_company_conversation_thread_id')->unsigned();


            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_conversations');
    }
}
