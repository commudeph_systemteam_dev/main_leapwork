<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('announcement_id');
            $table->string('announcement_title', 100)->nullable();
            $table->text('announcement_details')->nullable();
            $table->datetime('announcement_deliverydate')->nullable();
            $table->enum('announcement_target', [
                'ALL',
                'COMPANY',
                'USER',
            ])->nullable();
            $table->string('announcement_to', 50)->nullable();
            $table->string('announcement_to_user', 50)->nullable();
            //$table->datetime('announcement_datecreated')->nullable();
            $table->enum('announcement_status', [
                'ACTIVE',
                'INACTIVE'
            ])->nullable();
            $table->integer('announcement_user_id')->nullable()->unsigned();
            $table->boolean('announcement_delivered')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
