<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterEmployeeRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_employee_range', function (Blueprint $table) {
            $table->increments('master_employee_range_id');
            $table->mediumInteger('master_employee_range_min')->nullable();
            $table->mediumInteger('master_employee_range_max')->nullable();
            $table->datetime('master_employee_range_datecreated');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_employee_range');
    }
}
