<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_plans', function (Blueprint $table) {
            $table->increments('master_plan_id');
            $table->string('master_plan_name', 45)->default('TRIAL');
            $table->integer('master_plan_price')->default(0)->nullable();
            $table->smallInteger('master_plan_post_limit')->default(1);
            $table->mediumInteger('master_plan_expiry_days')->default(7);
            $table->enum('master_plan_status', [
                'ACTIVE',
                'INACTIVE'
            ]);
            $table->datetime('master_plan_datecreated')->nullable();
            $table->datetime('master_plan_dateupdated')->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_plans');
    }
}
