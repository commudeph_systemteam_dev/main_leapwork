<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants_profile', function (Blueprint $table) {
            $table->increments('applicant_profile_id');
            $table->string('applicant_profile_firstname', 15)->nullable();
            $table->string('applicant_profile_middlename', 15)->nullable();
            $table->string('applicant_profile_lastname', 15)->nullable();
            $table->string('applicant_profile_preferred_email', 50)->nullable();
            $table->enum('applicant_profile_gender', [
                'MALE',
                'FEMALE'
            ])->nullable();
            $table->date('applicant_profile_birthdate')->nullable();
            $table->char('applicant_profile_postalcode', 8)->nullable();
            $table->string('applicant_profile_address1', 45)->nullable();
            $table->string('applicant_profile_address2', 45)->nullable();
            $table->string('applicant_profile_contact_no', 15)->nullable();
            $table->string('applicant_profile_imagepath', 150)->nullable();
            $table->string('applicant_profile_nationality', 15)->nullable();
            $table->text('applicant_profile_objective')->nullable();
            $table->text('applicant_profile_public_relation')->nullable();
            $table->integer('applicant_profile_expected_salary')->nullable()->default(0);
            $table->string('applicant_profile_desiredjob1', 45)->nullable();
            $table->string('applicant_profile_desiredjob2', 45)->nullable();
            $table->string('applicant_profile_desiredjob3', 45)->nullable();
            $table->string('applicant_profile_resumepath', 150)->nullable();
            $table->string('applicant_profile_token', 255)->nullable();
            $table->datetime('applicant_profile_dateregistered');
            $table->datetime('applicant_profile_dateupdated')->nullable();
            $table->integer('applicant_profile_user_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants_profile');
    }
}
