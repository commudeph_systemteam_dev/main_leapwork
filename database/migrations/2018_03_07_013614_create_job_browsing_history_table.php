<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobBrowsingHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_browsing_history', function (Blueprint $table) {
            $table->increments('job_browsing_history_id');
            $table->datetime('job_browsing_history_datebrowsed');
            $table->integer('job_browsing_history_job_post_id')->unsigned()->nullable();
            $table->integer('job_browsing_history_applicant_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_browsing_history');
    }
}
