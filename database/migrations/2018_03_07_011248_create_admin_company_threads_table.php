<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCompanyThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_company_threads', function (Blueprint $table) {
            $table->increments('admin_company_thread_id');
            $table->string('admin_company_thread_title', 150)->nullable();
            $table->datetime('admin_company_thread_datecreated');
            $table->integer('admin_company_thread_user_id')->unsigned();
            $table->integer('admin_company_thread_company_id')->unsigned();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_company_threads');
    }
}
