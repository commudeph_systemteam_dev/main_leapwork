<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsWorkexpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants_workexp', function (Blueprint $table) {
            $table->increments('applicant_workexp_id');
            $table->string('applicant_workexp_company_name', 45)->nullable();
            $table->date('applicant_workexp_datefrom')->nullable();
            $table->date('applicant_workexp_dateto')->nullable();
            $table->text('applicant_workexp_past_job_description')->nullable();
            $table->integer('applicant_workexp_profile_id')->unsigned()->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants_workexp');
    }
}
