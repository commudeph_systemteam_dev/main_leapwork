<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error messages
    |--------------------------------------------------------------------------
    |
    | Error messages should be placed here with the context of view access
    |
    */

    'COMPANY_NAME_REQUIRED' => 'Company Name is required',
    'CONTACT_NUMBER_REQUIRED' => 'Contact Number is required',
    'EMAIL_REQUIRED' => 'Email is required',    

];
