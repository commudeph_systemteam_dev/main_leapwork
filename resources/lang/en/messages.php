<?php                                                    

return [
     
     //************ GENERAL ************/
    'failed'         => 'Something went wrong',
    'empty-list'     => 'No data found',
    'no-conversation' => 'No conversation history',
    'no-messages'     => 'No messages',
    'message-sent'    => 'Message Sent',

    //************ MODULE ************/
    'new-job-posted'          => 'New Job Posted!',
    'job-post-saved'          => 'Job Post Saved!',
    'no-scout-conversation'   => 'No Scout Conversation History',
    'no-users'                => 'No users associated to this account.',
    'no-subscribed-plan'      => 'Please pay your current plan to proceed.',
    'plan-expired'            => 'Your plan has expired, please avail a new one to access this feature',
    'scout-no-users'          => 'No users found.',
    'scout-no-data'           => 'Not Available',
    
    'applicant' => [
        'profile-account'         => 'Profile Account Information is now updated',
        'profile-password'        => 'Profile Account Password is now updated',
        'application-update'      => 'Application updated',
        'profile-history'         => 'Profile Resume updated',
        'browsing-history'        => [
                                      'addToFavorte' => 'Job Post Added To Favorites',
                                      'remove'       => 'Job post Removed From Favorites'
                                      ],
        'application-history'        => [
                                      'messageSent' => 'Message sent'
                                      ]
    ],

    'job-post' => [
        'apply' => 'Job application sent!'
    ],

    //************ Company ************/
    'company' => [
        'mail-template' => 'Mail Template saved',
        'applicant'     => [
                            'job-application-conversation-empty' => 'No conversation history'
                           ],
        'users'         => [
                            'new'           => 'New user successfully saved',
                            'archived'      => 'User deleted',
                            'statusUpdated' => 'User status updated'
                          ],
        'job-application'     => [
             'memo-saved' => 'Memo saved'
         ],
         'recruitments'     => [
            'job-post-repost-confirmation' => 'Are you sure you want to repost this job?'
        ],
    ],

    //************ Notifications on Header ************/
    
    'notifications-none'           => 'No new notifications',
    'messages-none'                => 'No new messages',

    //************ ADMIN ************/
    'admin' => [
        'notice' => [
                      'save' => 'Announcement saved',

                      'delete' => 'Announcement deleted' 
                    ],
        'company' => [
                      'create' => 'Company successfully registered. </br> Please double check the claim.',
                      'createTrial' => 'Company successfully registered.',
                      'update' => 'Company Information successfully updated.',
                      'updatePassword' => 'Company password is now updated.'
                    ],
        'master-settings-job-applications' => [
            'job-industry'               => 'Job Industry successfully deleted!',
            'job-classification'         => 'Job Classification successfully deleted!',
            'job-position'               => 'Job Position successfully deleted!',
            'country'                    => 'Country successfully deleted!',
            'salary-range'               => 'Salary Range successfully deleted!',
            'employee-range'             => 'Employee Range successfully deleted!',
            'skill'                      => 'Skill successfully deleted!',
            'success-add'                => 'The entry successfully Added!',
            'forbidden-delete-has-child' => 'Deletion forbidden as there children node attached to this option.',
            'forbidden-range'            => 'Range is invalid.',
            'forbidden-existing'         => 'The entry is already exist.',
            ],

        'master-settings-users' => [
            'user-applicant-change-success'               => 'Applicant User details change successfully.',
            'user-corporate-change-success'               => 'Corporate User details change successfully.',
            'user-applicant-change-fail'                  => 'No changes applied in Applicant User details.',
            'user-corporate-change-fail'                  => 'No changes applied in Corporate User details.',
            'user-applicant-change-password-success'      => 'Applicant User Password is now updated.',
            'user-corporate-change-password-success'      => 'Corporate User Password is now updated.',
            ],

    ],


    'company-inquiry-sent' => 'Inquiry Sent',

    'job-application-status-saved'    => 'Job Application Status updated',
];