@include('admin.common.html-head')
<!-- js -->
<script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" />

<!-- title -->
<title>Member Registration | LEAP Work</title>

</head>
<body style="
background: url(../images/login_page_bg.jpg) center center no-repeat / cover;
background-repeat: no-repeat;
background-attachment: fixed;
">

<main id="register-view">
  <figure class="logo">
    <img src="{{url('images/common/icon/header-logo.svg')}}" alt="">
  </figure>
  <a href="{{url('/')}}" id="main__pageBtn">Back to Main Page</a>

      <div class="panel">
        <div class="inner">
            <div class="registration__container">
                <div class="registration__complete">
                    <h2>Thank you for choosing Leap Work</h2>
                    <p>Congratulations! your account has been created. Email Verification has been sent out in your email.
                        <br><br>Please let us know if you have any concerns thru email: <a href="mailto:admin@leapwork.com?Subject=[INQUIRY]" target="_top">admin@leapwork.com</a>. </p>
                    <a href="{{url('/')}}"><button class="btnRegister"> HOME </button></a>
                </div>
                <!-- .registration__complete -->
            </div>
            <!-- .registration__container -->
        </div> <!-- .inner -->
      </div><!-- .panel -->

</main>
</body>

</html>