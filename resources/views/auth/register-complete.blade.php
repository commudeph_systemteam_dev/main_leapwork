@include('admin.common.html-head')
<!-- js -->
<script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" />

<!-- title -->
<title>Member Registration | LEAP Work</title>

</head>
<body style="
background: url(../public/images/login_page_bg.jpg) center center no-repeat / cover;
background-repeat: no-repeat;
background-attachment: fixed;
">

<main id="register-view">
  <a href="{{url('/')}}">
    <figure class="logo">
      <img src="{{url('images/common/icon/header-logo.svg')}}" alt="">
    </figure>
  </a>

    <ul class="registration__steps complete cf">
        <li>Corporate Registration</li>
        <li class="active">Complete</li>
    </ul>

  <div class="panel">
    <div class="inner">
        <div class="registration__container">
            <div class="registration__complete">
                <h2>Thank you for choosing Leap Work</h2>
                <p>Please wait for the confirmation of your registration. <br><br>
                We will contact you as soon as possible. Thank you. </p>
                <a href="{{url('/')}}"><button class="btnRegister"> HOME </button></a>
            </div>
            <!-- .registration__complete -->
        </div>
        <!-- .registration__container -->
    </div> <!-- .inner -->
  </div><!-- .panel -->
</main>
</body>

</html>