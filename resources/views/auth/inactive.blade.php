@include('admin.common.html-head')
<!-- js -->
<script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" />

<!-- title -->
<title>Member Registration | LEAP Work</title>

</head>
<body>

<main id="register-view">
  <figure class="logo">
    <img src="{{url('images/common/icon/header-logo.svg')}}" alt="">
  </figure>
  <a href="{{url('/')}}" id="main__pageBtn">Back to Main Page</a>

      <div class="panel">
        <div class="inner">
            <div class="registration__container">
                <div class="registration__complete">
                    <h2>Thank you for choosing Leap Work</h2>
                    <p>Please be advice that your account is currently inactive<br><br>
                    Please contact the admin for the activation of your account. <br><br>
                    Thank you. </p>
                    <a href="{{url('/')}}"><button class="btnRegister"> HOME </button></a>
                </div>
                <!-- .registration__complete -->
            </div>
            <!-- .registration__container -->
        </div> <!-- .inner -->
      </div><!-- .panel -->

</main>
</body>

</html>