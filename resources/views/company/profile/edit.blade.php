@extends('company.layouts.app')

@section('title')
  {{ __('labels.company-information') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/company/profile.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/config/constants.js')}}"></script>
@endpush

@section('content')

@if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
      </div>
    </div>
@endif

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      {{ __('labels.company-information') }}
    </p>
  </header>

  <form method="POST" action="" id="frmCompanyProfile" enctype='multipart/form-data'>
   {{ csrf_field() }}
   <input type="hidden" id="company_id" name="company_id" value="{{ $company->company_id }}" >
   <input type="hidden" id="company_intro_id" name="company_intro_id" value="{{ $companyIntro->company_intro_id }}" >

    <div class="content-inner">
          <aside class="search-menu">
          <ul class="list">
              <li class="item">
                  <a onclick="document.getElementById('how-to').style.display='block'" class="btn green">
                      {{ __('labels.how-to-write-company-info') }}
                  </a>
              </li>
          </ul>
          </aside>
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">
              <fieldset id="myFieldset" disabled>
            <div class="tr">
               
              <div class="td">
                {{ __('labels.company-name') }} <span class="required"> {{ __('labels.required') }}  </span>
              </div>
              <div class="td">
                 <input type="text" id="txt_company_name" name="txt_company_name" value="{{ $company->company_name}}" >
                 <span id="spCompanyName" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.company-website') }}
              </div>
              <div class="td">
                <input type="text" id="txt_company_website" name="txt_company_website" value="{{ $company->company_website}}" >
              </div>
            </div>

             <div class="tr">
              <div class="td">
                {{ __('labels.address') }}  <span class="required"> {{ __('labels.required') }}  </span>
              </div>
              <div class="td">
                 <input list="codes" placeholder="Postal Code" id="txt_company_postalcode" name="txt_company_postalcode" value="{{ $company->company_postalcode }} ">
                <datalist id="codes">
                    @foreach(array_keys($arrPhilippines) as $key)
                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                    @endforeach
                </datalist>

                <!-- <input type="text" id="txt_company_postalcode" name="txt_company_postalcode" value=" {{ $company->company_postalcode}}" > -->
                <br>
                <input type="text" id="txt_company_address1" name="txt_company_address1" value=" {{ $company->company_address1}}" >
                <br>
                <input type="text" id="txt_company_address2" name="txt_company_address2" value=" {{ $company->company_address2}}" >

                <span id="spPostalCode" class="help-block required--text">
                    <strong class="text"></strong>
                </span>
                <span id="spAddress1" class="help-block required--text">
                    <strong class="text"></strong>
                </span>

              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.ceo') }}
              </div>
              <div class="td">
                <input type="text" id="txt_company_ceo" name="txt_company_ceo" value="{{ $company->company_ceo}}" >
                <span id="cpCompanCeo" class="help-block required--text">
                  <strong class="text">CEO is required</strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.no-of-employees') }}
              </div>
              <div class="td">
                 {{ Form::select('so_employee_range_id', 
                                  $employeeRanges,
                                  $company->company_employee_range_id,
                                  ['placeholder' => '- Choose One -'])
                 }}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.date-founded') }}
              </div>
              <div class="td">
              <input type="text" id="txt_company_date_founded" name="txt_company_date_founded" class="datepickerPast" value="@if(!is_null($company->company_date_founded)){{\Carbon\Carbon::parse($company->company_date_founded)->format('m/d/Y')}}@endif" required> 
            </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.industry') }}
              </div>
              <div class="td" >
                <div>
                <div id="companyIndustry">
                @for ($i = 0; $i < count($company->company_industries); $i++)
                  {{ Form::select('so_company_industry_id_' . $i, 
                                  $jobIndustries, 
                                  $company->company_industries[$i]->company_industry_master_job_industry_id,
                                   ['placeholder' => '- Choose One -'])
                  }}
                  <br>
                @endfor

                @for ($i = count($company->company_industries); $i < 3; $i++)
                  {{ Form::select('so_company_industry_id_' . $i, 
                                   $jobIndustries,
                                   null,
                                   ['placeholder' => '- Choose One -'])
                  }}
                  <br>
                @endfor
                </div>
               </div>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.twitter') }}
              </div>
              <div class="td">
               <input type="text" id="txt_company_twitter" name="txt_company_twitter" value=" {{ $company->company_twitter}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.facebook') }}
              </div>
              <div class="td">
              <input type="text" id="txt_company_facebook" name="txt_company_facebook" value=" {{ $company->company_facebook}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.company-logo') }}
                <p class="help-block required--text red" id="image-size-label">(Please upload 1:1)</p>
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <span class="js-company_logo-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                  @if(!empty($company->company_logo))
                    <div id="dv_company_logo">
                      <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                      <a href="#" id="btnRemoveCompanyLogo"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                    </div>
                  @endif
                <input type="file" id="file_company_logo" name="file_company_logo">
                <span id="spCompanyLogo" class="help-block required--text js-company_logo-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.company-banner') }}
                <p class="help-block required--text red" id="image-size-label">(Please upload 16:4)</p>
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag" >
                <span class="js-company_banner-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                  @if(!empty($company->company_banner))
                    <div id="dv_company_banner">
                      <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                      <a href="#" id="btnRemoveCompanyBanner"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                    </div>
                  @endif
                <input type="file" id="file_company_banner" name="file_company_banner">
                <span id="spCompanyBanner" class="help-block required--text js-company_banner-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    {{ __('labels.introduction') }}
                  </div>
                  <div class="td">
                    <textarea maxlength="1000" id="txt_company_intro_content" name="txt_company_intro_content" value="{{ $companyIntro->company_intro_content }}" >
                        {{ $companyIntro->company_intro_content }}
                    </textarea>
                  </div>              
             </div>
             <div class="tr">
                  <div class="td">
                      {{ __('labels.public-relations') }}
                    </div>
                    <div class="td">
                      <textarea maxlength="1000" id="txt_company_intro_pr" name="txt_company_intro_pr" value="{{ $companyIntro->company_intro_pr }}">
                          {{ $companyIntro->company_intro_pr }}
                      </textarea>
                    </div>              
              </div>
               <div class="tr">
                  <div class="td">
                      {{ __('labels.public-relations-image') }}
                    </div>
                    <div class="td">                   
                       <input type="hidden" id="txt_company_intro_image_remove_flag" name="txt_company_intro_image_remove_flag" >
                       <span class="js-public_relation-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                       @if(!empty($companyIntro->company_intro_image))
                          <div id="dv_company_intro_image">
                                <img src='{{url("/storage/uploads/company/company_$company->company_id/$companyIntro->company_intro_image")}}'  id="img_company_logo">
                                <a href="#" id="btnRemoveCompanyIntroImage"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                          </div>
                        @endif
                      <input type="file" id="file_company_intro_image" name="file_company_intro_image">
                      <span id="spPublicRelation" class="help-block required--text js-public_relation-error">
                        <strong class="text"></strong>
                      </span>
                  </div>              
               </div> 

            
          </div>
        </div>
      </fieldset>
       <!--  <div class="table form panel">
              <div class="tbody">

                  <div>
                        <div class="tr">
                            <div class="td">
                                Introduction
                              </div>
                              <div class="td">
                                <textarea maxlength="1000" id="txt_company_intro[]" name="txt_company_intro[]"  disabled>skipped</textarea>
                              </div>              
                        </div>
                         <div class="tr">
                            <div class="td">
                                Public Relations
                              </div>
                              <div class="td">
                                <textarea maxlength="1000" id="txt_company_intro[]" name="txt_company_intro[]"  disabled>skipped</textarea>
                              </div>              
                        </div>
                         <div class="tr">
                            <div class="td">
                                Public Relations (image)
                              </div>
                              <div class="td">                   
                                <input type="file" id="file_company_pr_img[]" name="file_company_pr_img[]">
                            </div>              
                        </div>
                </div>
              </div>
          </div>
      </div> -->
      <div class="btn-wrapper">
        <a href="{{ url('company/profile') }}" class="btn alt-btn">
          {{ __('labels.back') }}
        </a>
        <button type="button" id="btnEditCompany" class="btn">
            {{__('labels.edit')}}
          </button>
        <button type="button" id="btnEditProfile" class="btn">
          {{ __('labels.save') }}
        </button>
      </div>
    </div>
  </form>
</main>
<div id="how-to" class="modal">
    <form class="modal-content animate">
      <div class="imgcontainer">

      </div>
  
      <div class="container">
            <h2>{{ __('labels.how-to-write-company-info') }}</h2>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum fringilla magna, in semper sapien consequat nec. Cras varius sollicitudin lobortis. Aliquam elementum purus nec fermentum accumsan. Nam vel sem ut tortor porta lacinia at eget arcu. Suspendisse ornare accumsan neque at placerat. Quisque rhoncus lacus eu erat maximus tempus. Fusce feugiat neque eu ultrices luctus. Phasellus a purus tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum maximus, ex at molestie luctus, mauris ex ornare nisl, sed mollis nisl nisi tincidunt ipsum. Pellentesque gravida orci sed nibh interdum, quis egestas est tristique.
            <br><br>
            Proin dapibus ante felis, quis congue lacus auctor sodales. Duis ultricies quam a dapibus vehicula. Nulla auctor porta mauris, nec tempus eros maximus sit amet. Ut volutpat tempor felis, vel volutpat nisi accumsan at. Suspendisse risus enim, suscipit eget feugiat ac, ultricies ullamcorper eros. Aliquam semper lorem mauris, vel aliquam est efficitur quis. Sed faucibus iaculis tristique. Pellentesque eget purus vel urna ornare egestas. Sed ornare purus in scelerisque semper.
            <br><br>
            Curabitur venenatis augue vitae porttitor sodales. Nam non pharetra dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Quisque maximus ligula ac nisi fermentum, sed consequat nibh mattis. Vivamus volutpat scelerisque finibus. Donec quis elit non ligula iaculis eleifend.
            <br><br>
            Donec gravida libero at dolor rhoncus vehicula. Fusce sem nisl, faucibus sed massa vel, ultricies porta est. Ut quis lacinia ante. Proin commodo aliquam pretium. Pellentesque nec scelerisque ante. Nunc at tortor laoreet, pellentesque ex eget, suscipit elit. Nulla feugiat diam pharetra mi lobortis, eget vulputate erat congue. Cras ipsum neque, facilisis in volutpat vel, cursus et purus. Nulla varius ac sapien eget convallis.
            <br><br>
            Aenean vel ornare tortor, at sollicitudin nisl. Sed elementum turpis velit, et efficitur dui lacinia ullamcorper. Praesent scelerisque, orci sed luctus sagittis, urna orci efficitur sapien, vel convallis massa est eu mauris. Nam ipsum ante, cursus sit amet elit vel, ultrices ultrices nisl. Duis congue varius justo quis rutrum. Cras ut porta dolor. Morbi quis sollicitudin est. Curabitur quis ex at justo pharetra semper.
            </p>
      </div>
  
      <div class="btn-wrapper">
        <button type="button" onclick="document.getElementById('how-to').style.display='none'" class="btn green">OK</button>
      </div>
    </form>
</div>
<script>
// Get the modal
var modal = document.getElementById('how-to');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
@endsection