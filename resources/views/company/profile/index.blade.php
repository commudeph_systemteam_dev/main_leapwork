@extends('company.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/profile.js')}}"></script>
@endpush

@section('content')

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company Information
    </p>
  </header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
  @endif

  <form method="GET" action="" id="frmCorporateProfile">
   {{ csrf_field() }}
   <input type="hidden" id="company_id" name="company_id" value="{{ $company->company_id }}" >

    <div class="content-inner">
      <aside class="search-menu">
          <ul class="list">
              <li class="item" style="float: right;">
                  <a href="{{url('company/profile/howto/write/company-info')}}" class="btn green">
                      How to write company info
                  </a>
              </li>
          </ul>
      </aside>

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">

            <div class="tr">
              <div class="td">
                Company Name
              </div>
              <div class="td">
                {{ $company->company_name}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Website
              </div>
              <div class="td">
                {{ $company->company_website}}
              </div>
            </div>

             <div class="tr">
              <div class="td">
                Address
              </div>
              <div class="td">
                {{ $company->company_postalcode}}
                <br>
                {{ $company->company_address1}}
                <br>
                {{ $company->company_address2}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                CEO
              </div>
              <div class="td">
                {{ $company->company_ceo}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                No. of employees
              </div>
              <div class="td">
                 {{ $company->employee_range->master_employee_range_min}}
                 ~
                 {{ $company->employee_range->master_employee_range_max}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Date Founded
              </div>
              <div class="td">
                {{ date('F d, Y', strtotime($company->company_date_founded)) }}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Industry
              </div>
              <div class="td">
                @foreach($company->company_industries as $companyIndustry)
                 * 
                 {{ $companyIndustry->job_industry->master_job_industry_name}} 
                 <br>
                @endforeach
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Twitter
              </div>
              <div class="td">
               {{ $company->company_twitter}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Facebook
              </div>
              <div class="td">
               {{ $company->company_facebook}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Logo
              </div>
              <div class="td">
                <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}' />
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Banner
              </div>
              <div class="td">
                <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' />
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Introduction
              </div>
              <div class="td">
                TODO
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Public Relations
              </div>
              <div class="td">
                TODO
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Public Relations Images
              </div>
              <div class="td">
                TODO
              </div>
            </div>


          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <a href="{{ url('company/profile/edit?company_id=') }}{{ $company->company_id }}" class="btn">
          Update
        </a>
      </div>
    </div>
    
  </form>

</main>
@endsection