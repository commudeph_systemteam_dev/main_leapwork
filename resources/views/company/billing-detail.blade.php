@include('company.common.html-head')

  <!-- js -->
  @include('company.scripts.company-script')
  
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/billing.css')}}" />

  <!-- title -->
  <title>Billing | LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <form method="POST" action="{{ url('company/billing/detail') }} ">
      
      {{ csrf_field() }}
      <main id="billing-view" class="content-main">
        <header class="pan-area">
          <p class="text">
            Billing Address
          </p>
        </header>
        {{-- @foreach($companyInfo as $key => $value) --}}
          <div class="content-inner">
            <nav class="local-nav">
              <ul class="menu">
                <li class="item">
                  <a href="{{url('company/billing')}}">Plan</a>
                </li>

                <li class="item">
                  <a href="{{url('company/billing/invoice')}}">Invoice</a>
                </li>
                <li class="item">
                  <a href="{{url('company/billing/payment')}}">Paid by</a>
                </li>
              </ul>
            </nav>
            <div class="section billing-detail">
              <div class="table form panel">
                <div class="tbody">
                  <div class="tr">
                    <div class="td">
                      Billing
                    </div>
                    <div class="td">
                      <label class="radio-wrapper">
                        <input type="radio" name="billingTo" value="toCompAddress"> Same as Company Address
                      </label>
                      <label class="radio-wrapper">
                        <input type="radio" name="billingTo" value="toEmail"> Send through Email
                      </label>
                      <label class="radio-wrapper">
                        <input type="radio" name="billingTo" value="toDiffAdd"> Different billing address
                      </label>
                    </div>
                  </div>
                  <div class="tr">
                    <div class="td">
                      Company Name
                    </div>
                    <div class="td">
                      <input type="text" value="{{isset($value->company_name) ? $value->company_name : ''}}" name="compName">
                    </div>
                  </div>
                  <div class="tr">
                    <div class="td">
                      Contact Person
                    </div>
                    <div class="td">
                      <input type="text" value="{{isset($value->company_contact_person) ? $value->company_contact_person : ''}}" name="compContact">
                    </div>
                  </div>
                  <div class="tr">
                    <div class="td">
                      Billing Address
                    </div>
                    <div class="td">
                      <input type="text"  value="{{isset($value->company_postalcode) ? $value->company_postalcode : ''}}" name="postal"><br><br>
                      <input type="text"  value="{{isset($value->company_address1) ? $value->company_address1 : ''}}" name="addOne"><br><br>
                      <input type="text"  value="{{isset($value->company_address2) ? $value->company_address2 : ''}}" name="addTwo">
                    </div>
                  </div>
                  <div class="tr">
                    <div class="td">
                      E-mail Address
                    </div>
                    <div class="td">
                      <input type="text" value="{{isset($value->company_email) ? $value->company_email : ''}}" name="email">
                    </div>
                  </div>
                  <div class="tr">
                    <div class="td">
                      Payment Method
                    </div>
                    <div class="td">
                      <select name="method" id="method" onchange="checkMethod()">
                        @foreach(config('constants.paymentMethod') as $key => $value)
                          <option value="{{ $value }}"> {{ $value }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
<!--                   <div class="tr cc hidden">
                    <div class="td">
                      Credit Card Name
                    </div>
                    <div class="td">
                      <input type="text" name="creditName">
                    </div>
                  </div>

                  <div class="tr cc hidden">
                    <div class="td">
                      Credit Card No.
                    </div>
                    <div class="td">
                      <input type="text" name="creditNo">
                    </div>
                  </div> -->
               
                </div>
              </div>
              <div class="btn-wrapper">
                <!-- <a href="#" class="btn">
                  Save
                </a> -->
                <input type="submit" name="submit" value="Save">
              </div>
            </div>
          </div>
          <input type="hidden" name="newPlan" value="{{ $planID }} ">
          {{-- @endforeach --}}

      </main>
    </form>
  </div>
</body>
</html>
