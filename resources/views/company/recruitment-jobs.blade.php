@include('company.common.html-head')
  <!-- js -->
  <script type="text/javascript" src=" {{url('/js/company/recruitment-jobs.js')}}"></script>
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/recruitment-jobs.css')}}" />

  <!-- title -->
  <title>{{__('labels.recruitment')}}| LEAP Work</title>

</head>
<body>
   <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">

    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <form class="form-horizontal" method="POST" action="{{ url('/company/recruitment-jobs') }}" id="recruitmentForm">
        {{ csrf_field() }}
    <main id="recruitment-jobs-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{ __('labels.recruitment') }}
        </p>
      </header>
      
      <div class="content-inner">
        <aside class="search-menu">
          <ul class="list">
            @if(Auth::user()->hasRight('JOB POST'))
            <li class="item">
                  <a id="{{ $isLimitReach  ? 'limitReachedModalBtn' : 'btnDirectJobPost'}}" class="btn">
                  {{__('labels.new')}}
              </a>
            </li>
            @endif
            <li class="item">
              <span class="title">{{__('labels.status')}}</span>
              <div class="select-wrapper">
              {{Form::select('select_job_post_visibility', 
                                config('constants.'.Session::get('locale').'_select_job_post_visibility')
                                ,$selectedVisibility
                                ,['id' => 'select_job_post_visibility'
                                ,'class' => 'select__status']
              )}}
              </div>
            </li>
            @if(Auth::user()->hasRight('JOB POST'))
            <li class="item credits">
                <div class="select-wrapper">
                  @if($company->company_credits != 0 && $currentPlanHistory->company_plan_status != "EXPIRED")
                    <span class="green credit">{{ $company->company_credits }}</span> <span class="credit">{{__('labels.credits-left')}}</span>
                  @elseif($company->company_credits == 0 && $currentPlanHistory->company_plan_status != "EXPIRED")
                    <span class="red credit">{{ $company->company_credits }}</span> <span class="credit">{{__('labels.credits-left')}}</span>
                  @endif
                </div>
            </li>
            @endif
          </ul>
        </aside>
        <div class="section">
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{__('labels.job-description')}}
                </div>
                <div class="th">
                    {{__('labels.post-visibility')}}
                </div>
                <div class="th">
                  </div>
                {{-- <div class="th">
                  {{__('labels.comments')}}
                </div> --}}
              </div>
            </div>
            <div class="tbody">
            @if(count($activeJobPosts) > 0)
            @foreach($activeJobPosts as $activeJobPost)

            <div class="tr">
                <div class="td">
                  <div class="text-area">
                    <p class="title">
                        @if(Auth::user()->hasRight('JOB POST'))
                          @if($activeJobPost->job_posts_status == "EXPIRED")
                          <a href="{{ url('company/recruitment-jobs/viewJobDetails/recruitment')}}/{{$activeJobPost->job_post_id }}" class="recruitment-job-post">{{ $activeJobPost->job_post_title }}</a>
                          <span style="color: red; font-size: 12px;">
                              (Expired last {{ date('M. d Y', strtotime($activeJobPost->job_post_expiration_date)) }})
                          </span>
                          @else
                          <a href="{{ url('company/job-posting/edit')}}/{{$activeJobPost->job_post_id }}" class="recruitment-job-post">{{ $activeJobPost->job_post_title }}</a>
                          <span style="color: green; font-size: 12px;">
                              (Will be expired on {{ date('M. d Y', strtotime($activeJobPost->job_post_expiration_date)) }})
                          </span>
                          @endif
                        @else
                          <a href="{{ url('company/recruitment-jobs/viewJobDetails/recruitment')}}/{{$activeJobPost->job_post_id }}">    
                            {{ $activeJobPost->job_post_title }}
                          </a>
                        @endif
                    </p>
                    <dl class="info">
                      <dt>
                          {{ mb_strimwidth($activeJobPost->job_post_description, 0, 300, "...")}}
                      </dt>
                      <dd>
                      @if(isset($activeJobPost->location->master_job_location_name))
                        {{ __('labels.location') }}
                        :
                        {{$activeJobPost->location->master_job_location_name}}
                      @endif
                      </dd>
                      <dd>
                        {{ __('labels.salary') }}
                        ：
                        PHP {{number_format($activeJobPost->job_post_salary_min)}} ~ PHP {{number_format($activeJobPost->job_post_salary_max)}}
                      </dd>
                    </dl>
                  </div>
                </div>
                <div class="td">
                    @if((!$isLimitReach && $activeJobPost->job_posts_status == "EXPIRED") && Auth::user()->hasRight('JOB POST'))
                    <a onclick="return repostJob(this.getAttribute('data-href'))" class="btn">
                        <span>REPOST</span>
                    </a>
                    <div class="confirm__modal_container">
                        <div class="confirm__modal_content">
                            <a class="modal__close" id="btn_close_conf" onclick="return closeModalIcon()"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                            <div class="confirm__modal_dialog">
                                <p>{{ Lang::get('messages.company.recruitments.job-post-repost-confirmation') }}</p>
                                <div class="confirm__modal_btns">
                                    <button class="spare_user confirm__btn" id="spare_user" onclick="return closeModal()">NO</button>
                                    <a href="{{ url('/company/recruitment-jobs')}}/{{$activeJobPost->job_post_id}}" class="save_user confirm__btn" id="btnSavePassword">YES</a>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .confirm__modal_container -->
                    @else
                    <span class="{{strtolower($activeJobPost->job_posts_status)}}">
                        {{$activeJobPost->job_post_visibility}}
                    </span>
                    @endif
                </div>
                <div class="td">
                  {{-- @if((!$isLimitReach && $activeJobPost->job_posts_status == "EXPIRED") && Auth::user()->hasRight('JOB POST'))
                    <a href="{{ url('/company/recruitment-jobs')}}/{{$activeJobPost->job_post_id}}" class="btn">
                        <span>REPOST</span>
                    </a>
                  @endif --}}
                {{-- @else
                @if(!$isLimitAndExpired)

                          @if((($activeJobPost->job_posts_status == "EXPIRED") || $currentPlanHistory == "EXPIRED"))
                            <a href="{{ url('/company/recruitment-jobs')}}/{{$activeJobPost->job_post_id}}">
                              <span class="green">REPOST</span>
                            </a>
                          @endif
                      @endif
                @else
                     @if($isLimitAndExpired && $activeJobPost->job_posts_status == "EXPIRED")
                        <a class="limitandExpiredModalBtnRepost" class="btn">
                            <span class="green">REPOST</span>
                        </a>
                     @else
                        @if($activeJobPost->job_posts_status == "EXPIRED" || $currentPlanHistory == "EXPIRED")
                            <a href="{{ url('/company/recruitment-jobs')}}/{{$activeJobPost->job_post_id}}">
                              <span class="green">REPOST</span>
                            </a>
                        @endif
                      @endif
                @endif --}}
                </div>
                {{-- <div class="td">
                  <span class="num">
                     <a href="{{url('company/recruitment-jobs/viewJobDetails/recruitment')}}/{{$activeJobPost->job_post_id}}">{{$activeJobPost->countInquiries($activeJobPost->job_post_id)}}</a>
                    <input type="hidden" name="recruitment">
                  </span>
                </div> --}}
            </div>
            @endforeach
            @else
              </div>
              </div>
              <div id="noRecordFound">
                <p>No Records Found</p>
              </div>
             @endif
            </div>  <!-- tbody -->
          </div>
        </div>
      </div>
      <center>{{  $activeJobPosts->render() }}</center>
    </main>
  </form>
  <div id="checkLimit" class="w3-modal">
    <div class="w3-modal-content">
        <div class="creditMessageContainer">
            <span onclick="$('#checkLimit').fadeOut(3000);" class="w3-button w3-display-topright"><img id="closeIconRecruit" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
            <p></p>
            <p id="creditMessage">You have insufficient credits for posting a job. Please buy a credit first before posting.</p>
        </div>
    </div>
  </div>

  </div>
</body>
</html>