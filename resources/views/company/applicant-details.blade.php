@include('company.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/applicant.css')}}" />

<!-- title -->
<title> {{ __('labels.applicant-details') }} | LEAP Work</title>
</head>

<body>
   <header id="global-header">@include('company.share.header')</header>
    <div class="content-wrapper">
        <form method="POST" action="{{url('company/applicant/edit')}}" enctype="multipart/form-data">
            <aside id="sidebar">@include('company.share.sidebar')</aside>
            {{ csrf_field() }}
            <main id="applicant-details" class="content-main memo-view">
                <header class="pan-area">
                    <input type="text" name="job_application_id" value="{{$applicant->job_application_id}}" hidden />
                    <p class="text">
                        {{ $applicant->job_post->job_post_title }} - {{ date('Y/m/d', strtotime($applicant->job_post->job_post_created)) }}
                    </p>
                </header>
                <div class="content-inner">
                    <div class="section posting-edit">
                        <div class="table form panel">
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.application-status') }}
                                    </div>
                                    <div class="td">
                                        {{Form::select('applicant_status', 
                                            config('constants.mailStatus'),
                                             $applicant->job_application_status)
                                        }} 
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.memo') }}
                                    </div>
                                    <div class="td">
                                        <p align="center">
                                            <textarea name="applicant_memo" rows="10" cols="70">{{$applicant->job_application_memo}}</textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-wrapper memo">
                                <a href ="{{ url('/company/applicant/') }}" clasS ="btn"> {{ __('labels.back') }} </a>
                                <button type="submit" id="btnMemo">{{ __('labels.memo') }}</button>
                        </div>
                    </div>
                </div>
            </main>
        </form>
    </div>
</body>

</html>