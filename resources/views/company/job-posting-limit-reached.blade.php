@include('company.common.html-head')
<script type="text/javascript" src=" {{url('/js/company/job-posting.js')}}"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/job-posting.css')}}" />
<!-- title -->
<title>Limit Reached-Job Post | LEAP Work</title>
</head>
<body>
 <header id="global-header">@include('company.share.header')</header>
<div class="content-wrapper">
  <aside id="sidebar">@include('company.share.sidebar')</aside>
  <form method="POST" action="{{url('company/job-posting')}}" enctype="multipart/form-data" id="companyForm">
    {{ csrf_field() }}
    <main id="job-posting-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Limit Reached 
        </p>
        <p class="text">
          Current Plan : {{ $company_plan_history->company_plan_type}}
        </p>
      </header>
    </main>
  </form>
</div>
</body>
</html>