@extends('company.layouts.app')

@section('title')
  {{ __('labels.billing') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/modal.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/billing.css')}}" />
@endpush

 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script>
@endpush

@section('content')

<header class="pan-area">
  <p class="text">
    {{ __('labels.billing') }}
  </p>
</header>

 <main id="billing-view" class="content-main">

  <header class="pan-area">
      <p class="text">
        {{ __('labels.billing') }}
      </p>
    </header>

    {{ csrf_field() }}
    <div class="content-inner">
      <nav class="local-nav">
        <ul class="menu">
          <li class="item">
            <span>{{ trans_choice('labels.plan',2) }}</span>
          </li>
<!--           <li class="item">
            <a href="{{url('company/billing/detail')}}">Billing</a>
          </li> -->
          <li class="item">
            <a href="{{url('company/billing/invoice')}}">{{ __('labels.invoice') }}</a>
          </li>
          <li class="item">
            <a href="{{url('company/billing/payment')}}">{{ __('labels.paidby') }}</a>
          </li>
        </ul>
      </nav>
      <div class="section current-plan">
        <header class="header">
          <h2 class="title">
            {{ __('labels.current-plan') }}
          </h2>
        </header>
        <div class="panel">
          <div class="inner">
            @if(!is_null($currentPlan))
              @if($currentPlan->company_plan_status == "ACTIVE")
              <h3 class="plan">{{ $currentPlan->company_plan_type }}</h3>
              <br><center>valid until <span class="red"><b>{{ date("F d, Y",strtotime($currentPlan->company_plan_dateexpiry)) }} </b></span></center>
              @elseif($currentPlan->company_plan_status == "EXPIRED" && !is_null($currentPlan->company_plan_dateexpiry))
              <h3 class="plan"><span class="red">{{ $currentPlan->company_plan_type }} - {{ $currentPlan->company_plan_status }}</span></h3>
              <br><center>Expired Last <span class="red"><b>{{ date("F d, Y",strtotime($currentPlan->company_plan_dateexpiry)) }} </b></span></center>
              {{-- @elseif($currentPlan->company_plan_status == "INACTIVE")
                @if(!is_null($currentPlan->company_plan_dateexpiry))
                  <h3 class="plan"><span class="blue">{{ $currentPlan->company_plan_type }} - {{ $currentPlan->company_plan_status }}</span></h3>
                  <br><center>valid until <span class="red"><b>{{ date("F d, Y",strtotime($currentPlan->company_plan_dateexpiry)) }} </b></span></center>
                @else
                  <h3 class="plan"><span class="blue">{{ $currentPlan->company_plan_type }} - {{ $currentPlan->company_plan_status }}</span></h3>
                @endif --}}
              @else
                <h3 class="plan">NO PLAN SELECTED</h3>
              @endif
            @else
              {{--  <h3 class="plan"><span class="red">TRIAL</span></h3>
              <br><center>valid until <span class="red"><b>{{ date("F d, Y",strtotime($currentPlan->company_plan_dateexpiry)) }} </b></span></center>  --}}
              <h3 class="plan"><span class="red">PLAN NOT AVAILABLE</span></h3>
            @endif
          </div>
        </div>
      </div>

      <div class="section plans">
        <header class="header">
          <h2 class="title">
            {{ __('labels.plan-upgrade') }}

          </h2>
         
          @if(isset($message))
          <br>
            <p class="change_error">
            {!! $message !!}
            </p>
          @endif
        </header>
        <div class="panel">
          <ul class="plan-list">

            @foreach($masterPlans as $masterPlan)
              <form method="POST" action="{{ url('company/billing')}}">
              {{ csrf_field() }}
             
              <li class="plan {{ $currentPlan->company_plan_type == $masterPlan->master_plan_name ? 'current' : ''}}">
                <h3 class="title"> {{ $masterPlan->master_plan_name }} {{ trans_choice('labels.plan',0) }}</h3>
                <p class="price">
                  {{ $masterPlan->master_plan_price == 0 ? 'FREE' : 'PHP ' . number_format($masterPlan->master_plan_price,0) }}
                </p>
                <p class="text">
                  {{ __('labels.billing-plan-definition', ['master_plan_post_limit' => $masterPlan->master_plan_post_limit
                                                         ,'master_plan_expiry_days' => $masterPlan->master_plan_expiry_days]) }}
                </p>

                @if($currentPlan->company_plan_type == $masterPlan->master_plan_name && 
                  $currentPlan->company_plan_type != 'TRIAL')
                    <input type="submit" name="" value="{{ __('labels.renew-plan') }}">
                @else
                  @if( $masterPlan->master_plan_price != 0 )
                    <!-- <a href='{{ url("company/billing/plan/change?plan_id=") . $masterPlan->master_plan_id }}' class="btn">
                      Change Plan
                    </a> -->
                    @if($masterPlan->master_plan_name != "TRIAL")
                      <input type="submit" name="" value="{{ __('labels.change-plan') }}">
                    @endif
                  @endif
                @endif
              </li>
              <input type="hidden" name="newPlan" value="{{ $masterPlan->master_plan_id }}">
            </form>
            @endforeach

          </ul>
        </div>
      </div> <!-- section plans -->

    </div>  <!-- content-inner -->

</main>
  
<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection