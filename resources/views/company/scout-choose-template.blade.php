@extends('company.layouts.app')


@section('title')
  Scout Email |
@stop

@push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/applicant/home.css')}}" />
@endpush
 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/company-scout.js')}}"></script>

@endpush

@section('content')

<main id="scout-view" class="content-main scoutchoose-view">
    
     @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <form method="POST" id="frmScoutMail" >
        {{ csrf_field() }}
        <input type="hidden" id="applicant_profile_id" name="applicant_profile_id" value="{{ $applicant->applicant_profile_id }}" >
        <input type="hidden" id="scout_id" name="scout_id" value="{{ $scout->scout_id }}" >
        <input type="hidden" id="company_id" name="company_id" value="{{$company->company_id}}"  >
        <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $jobPost['job_post_id'] ?? null }}" />
        <input type="hidden" id="scout_thread_id" name="scout_thread_id" >
        <input type="hidden" id="scout_conversation_message" name="scout_conversation_message" >
        <input type="hidden" id="companyIcon" value="{{$viewSettings['companyImgSrc']}}">
        <input type="hidden" id="userIcon" value="{{$viewSettings['userImgSrc']}}">
        <input type="hidden" id="userName" value="{{$userName}}">

        <header class="pan-area">
            <p class="text">
              Scout - Favorites (User)
            </p>
        </header>
            
        <div class="content-inner">
            <ul class="comment_list">
                <li class="list_title_wrap cf">
                  
                    <h1 class="list_title">
                        Scout Email To: 
                        {{ $applicant['userName'] }}
                        < {{ $applicant['email'] }} >
                    </h1>

                     <div style="float:right;">
                         @if($scout->scout_status == "ACCEPTED" || $scout->scout_status == "WITHDRAW")
                            {{ Form::select('scout_status', 
                                config('constants.scoutStatus'),
                                $scout->scout_status,
                                ['id' => 'scout_status', 'disabled' => 'true'])
                            }} 
                        
                         @else
                            {{ Form::select('scout_status', 
                                    config('constants.scoutStatus'),
                                           $scout->scout_status,
                                           ['id' => 'scout_status'])
                            }} 
                        @endif
                     </div>
                </li>

                <li class="list_info">
                    <div class="link_post db cf">
                        <h2 class="list_info_title"> {{ $jobPost['job_post_title'] }} </h2>

                        <div class="inner">
                            <h3 class="office_name"> {{ $jobPost['job_post_company_name'] }} </h3>

                            <div class="info">
                                <p class="info_01 ib"> Location: {{ $jobPost['job_post_location'] }} </p>
                                <p class="info_02 ib"> Salary: {{ $jobPost['job_post_salary'] }} </p>
                            </div>
                        </div>
                    </div><!-- link_post -->
                     
                    <div id="dvInbox" class="detail_contents detail_contents_chat">
                        <div class="tab_area clearfix">
                            <p class="tab tab_recieve_mail active left">Conversation</p>
                        </div>

                        <ul class="chat_list recieve_mail_area">
                            @foreach($scoutThreads as $scoutThread)
                                <li data-t_id="{{ $scoutThread->scout_thread_id }}" id="tb_{{ $scoutThread->scout_thread_id }}" class="cloesed_chat">
                                    <p class="clearfix">
                                        <time class="left"> 
                                            {{ date('Y.m.d', strtotime($scoutThread['lastMessage']->scout_conversation_date_added)) }} 
                                        </time>
                                        <span class="text left">
                                           {{ $scoutThread->scout_thread_title }} 
                                           : 
                                           {{  substr($scoutThread['lastMessage']->scout_conversation_message,0,70) }}
                                        </span>
                                    </p>
                                    <figure class="btn_pulldown">
                                        <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                        <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                    </figure>
                                </li>

                                <li id="message_area_{{ $scoutThread->scout_thread_id }}" class="opened_chat">
                                    <div class="messages_container">
                                        @php
                                            $day="";
                                        @endphp
                                        @foreach($scoutThread->scout_conversations as $scoutConversation)
                                            @if(is_null($day) 
                                            || (\Carbon\Carbon::parse($scoutConversation->scout_conversation_date_added)->format('m-d-Y')
                                            != $day ))
                                            @php
                                                $day = \Carbon\Carbon::parse($scoutConversation->scout_conversation_date_added)->format('m-d-Y');
                                            @endphp
                                            <hr>
                                            <span style="font-size:12px; text-align:center;display:block;">
                                                @if($day == \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m-d-Y'))
                                                    Today
                                                @elseif($day == \Carbon\Carbon::parse(\Carbon\Carbon::yesterday())->format('m-d-Y'))
                                                    Yesterday
                                                @else
                                                    {{ \Carbon\Carbon::parse($scoutConversation->scout_conversation_date_added)->format('M j, Y') }}
                                                @endif
                                            </span>
                                        @endif
                                            <div class="opened_chat_inner opened_chat_recieve">
                                                <div class="chat_area layout_2clm clearfix">
                                                    <figure class="icon {{ ($scoutConversation->scout_conversation_user_id != $applicant->applicant_profile_user_id)  
                                                    ? 'right'
                                                    : 'left'}}">
                                                        <img src="{{ ($scoutConversation->scout_conversation_user_id != $applicant->applicant_profile_user_id) 
                                                        ? $viewSettings['companyImgSrc']
                                                        : $viewSettings['userImgSrc'] }}" alt="">
                                                    </figure>
                                                    <div class="icon__username">
                                                      {{ $scoutConversation->userProperties($scoutConversation->scout_conversation_user_id)["userName"] }}
                                                        <span class="message__date"> {{ date('M j, Y h:i A', strtotime($scoutConversation->scout_conversation_date_added)) }}</span>
                                                    </div>
                                                    <p class="message_contents {{ ($scoutConversation->scout_conversation_user_id != $applicant->applicant_profile_user_id) 
                                                    ? 'right'
                                                    : 'left'}}">
                                                       {{$scoutConversation->scout_conversation_message}} 
                                                    </p>
                                                </div>                                             
                                            </div>
                                        @endforeach    
                                    </div>
                                    <div class="reply_container">
                                        <div class="opened_chat_inner opened_chat_send">
                                            <div class="form_area">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <th>
                                                                <h3 class="sub_title"><!-- {{ __('labels.reply') }} --></h3>
                                                            </th>
                                                            <td>
                                                                <textarea class="ctxtInput" placeholder="Type a Message..."></textarea>
                                                                 <span class="help-block required--text spErrorMessage hidden">
                                                                     <strong class="text">Please enter a message</strong>
                                                                </span><br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <div class="layout_1clm ta_c">
                                                    <div class="dvMainButtons">
                                                        <a class="btn ib alt-btn" href="{{ url('/company/scout/done') }} ">Back</a> 
                                                        <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $scoutThread->scout_thread_id }}">{{ __('labels.reply') }}</a>
                                                    </div>
                                                    <div class="dvConfirmButtons hidden">
                                                        <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                        <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $scoutThread->scout_thread_id }}">{{ __('labels.confirm') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>

 
                    </div><!-- detail_contents -->

                </li> <!-- list_info -->

            </ul>
        </div>

    </form>
</main>
@endsection