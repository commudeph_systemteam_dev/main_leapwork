<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="{{url('/js/company/billing.js')}}"></script>
<script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
<script src="{{url('/js/common/lib/pace.min.js')}}"></script>
<script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>
<script>
  $(function() {
      $('.matchHeight').matchHeight();
      $('main#billing-view div.content-inner > div.section.plans ul.plan-list li.plan').matchHeight();
  });
</script>