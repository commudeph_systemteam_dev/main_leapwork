<!DOCTYPE html>
<html lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <!-- meta -->
    <meta charset="UTF-8" />
    <meta name="format-detection" content="telephone=no,address=no,email=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- FOR AJAX REQUESTS -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

    <!-- favicon -->
    <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
    <meta name="msapplication-TileColor" content="#000" />
    <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />

    <!-- js -->
    <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/validator.js')}}"></script>

    <script type="text/javascript" src="{{url('/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/moment-timezone.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/moment-timezone-with-data.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/common/global.js')}}"></script>
    <!-- Notification.js bundle -->
    <script type="text/javascript" src="{{url('/js/pusher.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/notification.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/messages.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/jquery.cookie.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/applicant/header.js')}}"></script>


    <link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
    <link href="{{url('css/app.css')}}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{url('css/admin/modal.css')}}"     media="screen" rel="stylesheet" type="text/css">
    <link href="{{url('css/company/common.css')}}"  media="screen" rel="stylesheet" type="text/css">

    @include('company.scripts.company-script')



  <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!}
      window.userId    =  {!! json_encode(Auth::user()['id']) !!};
      window.pusherKey =  {!! json_encode(config('app.PUSHER_APP_KEY')) !!};
      window.notificationsLimit =  {!! json_encode(config('constants.notifications.limit')) !!};

      //setup the ajax headers
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  </script>

  <header>
      <!-- Notification modal -->
      <div id="noti-container" class="alert__modal" style="display:none;">
        <div class="alert__modal--container">
           <p id="noti-content"></p><span>x</span>
        </div>
      </div>

      <input type=hidden value="{{ $isPaid = Auth::user()->isPaid(Auth::user()->user_company_id) }}"/>
      <input type=hidden value="{{ $planHistory = Auth::user()->getCurrentPlanHistory(Auth::user()->user_company_id) }}"/>


      @if($isPaid)
      <div id="activatePlanModal" class="w3-modal">
          <div class="w3-modal-content w3-animate-top w3-card-4">
              <header class="billing_modal_header">
                  {{-- <span onclick="document.getElementById('activatePlanModal').style.display='none'"
                    class="w3-button w3-display-topright">&times;</span> --}}
            <span onclick="document.getElementById('activatePlanModal').style.display='none';window.location.reload()"  class="w3-button w3-display-topright"><img src="{{ url('images/ico_remove.svg') }}" alt=""></span>
                  <p><br></p>
                  <h2> Success! </h2>
              </header>
              <div class="w3-container billing_modal_body">
                  <p>Congratulations!
                    @if($planHistory->company_plan_type != "TRIAL")
                      Your payment has been acknowledged.
                    @endif
                    <br>You can now start your <span class="red">{{ $planHistory->company_plan_type }}</span> plan!<br></p>
                  <p><input type="button" onclick="window.location.reload()" name="activate" value="OK"><br><br></p>
              </div>
          </div>
      </div>
      @endif
  </header>

  <!-- HIDDENT FIELDS FOR NOTIFICATIONS -->
  <input type="hidden" id="userId" value="{{Auth::user()->id}}">
  <input type="hidden" id="companyId" value="{{Auth::user()->user_company_id}}">