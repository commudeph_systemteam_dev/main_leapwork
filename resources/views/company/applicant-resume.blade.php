@include('company.common.html-head')
<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/applicant.css')}}" />
<script type="text/javascript" src="{{url('/js/company/resume.js')}}"></script>
<!-- title -->
<title>{{ __('labels.applicant-resume') }} | LEAP Work</title>
</head>
    <body>
        <header id="global-header">@include('company.share.header')</header>
        <div class="content-wrapper">
            <form method="GET" action="{{url('/company/applicant')}}" enctype="multipart/form-data">
                <aside id="sidebar">@include('company.share.sidebar')</aside>
                <main id="job-posting-view" class="content-main resume-view">
                    <header class="pan-area">
                        <p class="text">
                            {{ __('labels.resume') }}: {{$applicant->applicant_profile_firstname}} {{$applicant->applicant_profile_middlename}} {{$applicant->applicant_profile_lastname}}
                            < <a href="mailto:{{$applicant->user_applicant->email}}">{{$applicant->user_applicant->email}}</a> >
                        </p>
                    </header>
                    <div class="content-inner">
                        <div class="section resume">
                            <div class="resume__imgcontainer">
                                <img src="{{ url('/storage/uploads/applicantProfile/profile_') . 
                                                        $applicant->applicant_profile_id . 
                                                        '/' .
                                                        $applicant->applicant_profile_imagepath }}" alt="">
                            </div>
                            <table class="table__resume">
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.full-name') }}</th>
                                        <td>{{$applicant->applicant_profile_firstname}} {{$applicant->applicant_profile_middlename}} {{$applicant->applicant_profile_lastname}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.birthdate') }}</th>
                                        <td>{{date('m/d/Y', strtotime($applicant->applicant_profile_birthdate))}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.gender') }}</th>
                                        <td>{{$applicant->applicant_profile_gender}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.address') }}</th>
                                        <td>
                                            {{$applicant->applicant_profile_postalcode}} <br/>
                                            {{$applicant->applicant_profile_address1}} <br/>
                                            {{$applicant->applicant_profile_address2}} 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.contact') }}</th>
                                        <td>{{$applicant->applicant_profile_contact_no}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.desired-jobs') }}</th>
                                        <td>
                                            <ul>
                                                <li>{{$applicant->applicant_profile_desiredjob1}}</li>
                                                <li>{{$applicant->applicant_profile_desiredjob2}}</li>
                                                <li>{{$applicant->applicant_profile_desiredjob3}}</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.salary') }}</th>
                                        <td>PHP {{number_format($applicant->applicant_profile_expected_salary)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.public-relation') }}</th>
                                        <td>{{$applicant->applicant_profile_public_relation}}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">{{ __('labels.skills') }}</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="clear">
                                            <table class="resume__innertbl">
                                                <tbody>
                                                    <tr>
                                                        <th>{{ __('labels.name') }}</th>
                                                        <th>{{ __('labels.type') }}</th>
                                                        <th>{{ __('labels.level') }}</th>
                                                    </tr>
                                                    @foreach($applicant->applicant_skills as $skill)
                                                    <tr>
                                                        <td>{{$skill->applicant_skill_name}}</td>
                                                        <td>{{$skill->applicant_skill_type}}</td>
                                                        <td>{{$skill->applicant_skill_level}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">{{ __('labels.work-experience') }}</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="clear">
                                            <table class="resume__innertbl" >
                                                <tbody>
                                                    <tr>
                                                        <th>{{ __('labels.company-name') }}</th>
                                                        <th>{{ __('labels.date-from') }}</th>
                                                        <th>{{ __('labels.date-to') }}</th>
                                                        <th>{{ __('labels.job-description') }}</th>
                                                    </tr>
                                                    @foreach($applicant->applicant_workexperiences as $workExperience)
                                                    <tr>
                                                        <td>{{$workExperience->applicant_workexp_company_name}}</td>
                                                        <td>{{date('m/d/Y', strtotime($workExperience->applicant_workexp_datefrom))}}</td>
                                                        <td>{{date('m/d/Y', strtotime($workExperience->applicant_workexp_dateto))}}</td>
                                                        @if(strlen($workExperience->applicant_workexp_past_job_description) >= 200)
                                                        <td class="jobDesc"><span id="jobDescContents_{{$workExperience->applicant_workexp_id}}"> {{substr($workExperience->applicant_workexp_past_job_description, 0, 200)}} <a href="#" class="jobDescSeeMore" data-id="{{$workExperience->applicant_workexp_id}}"> see more</a></span>
                                                            <span id="seeMoreContents_{{$workExperience->applicant_workexp_id}}"  hidden>{{substr($workExperience->applicant_workexp_past_job_description, 201)}}<a href="#" class="jobDescSeeLess" data-id="{{$workExperience->applicant_workexp_id}}"> see less</a></span>
                                                        </td>
                                                        @else
                                                        <td class="jobDesc">{{$workExperience->applicant_workexp_past_job_description}}</td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="btn-wrapper">
                                <button type="submit" id="btnMemo">{{ __('labels.back') }}</button>
                            </div>
                        </div>
                    </div>
                </main>
            </form>
        </div>
    </body>
</html>

