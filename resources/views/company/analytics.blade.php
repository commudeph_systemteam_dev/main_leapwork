@extends('company.layouts.app')
@section('title')  {{ __('labels.analytics') }} | @stop 

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/analytics.css')}}" />
@endpush
 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/analytics.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/chart.js')}}"></script>
@endpush

@section('content')
    <main id="analytics-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{ __('labels.analytics') }}
        </p>
      </header>
      <form id="analyticsForm" method="GET">
      @if(isset($jobPostInitial))
      <div class="content-inner">
        <aside class="search-menu">
          <ul class="list">
            <li class="item">
              <span class="title">Job Content</span>
              <input type="text" name="job_post_id" value="{{$jobPostInitial->job_post_id}}" hidden/>
              <div class="select-wrapper">
                {{Form::select('job_post_title', 
                        $jobPostTitles)
                }}
              </div>
            </li>
            <li class="item">
              <span class="title">{{ __('labels.month') }}</span>
              <div class="select-wrapper">
                  {{Form::select('job_created_month', 
                        config('constants.timeframe'))
                  }}
              </div>
            </li>
          </ul>
        </aside>
        <div class="section panel graph">
          <canvas id="analyticsChart"></canvas>
        </div>
        <div class="section panel">
          <div class="table">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{ __('labels.job-content') }}
                </div>
                <div class="th">
                  {{ __('labels.total-pv-points') }}
                </div>
                <div class="th">
                  {{ __('labels.entry') }}
                </div>
                <div class="th">
                  {{ __('labels.total-votes') }}
                </div>
                <div class="th" style="font-size: 16px;">
                  <strong>{{ __('labels.ranking') }}</strong>
                </div>
              </div>
            </div>
            <div class="tbody">
              <div class="tr">
                <div class="td" style="text-align: left;">
                  <p style="font-size: 1.8rem; font-weight: bold;" id="jobPostTitle"></p>
                  <span  id="jobPostDescription"></span>
                </div>
                <div class="td" id="jobPostTotalPV"></div>
                <div class="td" id="jobPostApplicants"></div>
                <div class="td" id="jobPostTotalVotes"></div>
                <div class="td" id="jobPostRanking"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @else
      <div class="no_job_display">
        <p>
        {{ __('messages.empty-list') }}
        </p>
      </div>
      @endif
      </form>
    </main>
    <script>
      
    </script>
@endsection