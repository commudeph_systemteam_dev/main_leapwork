@include('company.common.html-head')

  <!-- js -->
  <script type="text/javascript" src=" {{url('/js/company/applicant.js')}}"></script>

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/applicant.css')}}" />

  <!-- title -->
  <title>{{ __('labels.inquiry-of-applicants') }} | LEAP Work</title>

</head>
<body>
  <header id="global-header">@include('company.share.header')</header>
  <div class="content-wrapper">
    <aside id="sidebar">@include('company.share.sidebar')</aside>
    <main id="applicant-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{ __('labels.inquiry-of-applicants') }}
        </p>
      </header>
      <div class="content-inner">
        <nav class="local-nav">
          <ul class="menu">
            <li class="item">
              <a href="{{url('company/applicant')}}">{{ __('labels.applicants') }}</a>
            </li>
            <li class="item">
              <span>{{ __('labels.inquiry-of-applicants') }}</span>
            </li>
          </ul>
        </nav>
        <div class="section panel questioner-index">
          <div class="table">
            <div class="tbody">
              <!-- tr -->
            @if(count($companyJobPosts) > 0)
              @foreach($companyJobPosts as $companyJobPost)
                @foreach($companyJobPost->job_inquiries as $inquiry)
                <div class="tr">
                  <div class="td">
                    <a href="{{url('company/applicant/questionaire/detail')}}/{{$inquiry->job_inquiry_id}}">
                      {{$companyJobPost->job_post_title}} - {{$inquiry->user_inquiring->email}}
                    </a>
                  </div>
                  <div class="td">
                  
                    <span class="num">
                    
                      <a href="{{url('job/details')}}/{{$companyJobPost->job_post_id}}">{{$inquiry->job_inquiry_replies->count()+1}}</a>
                    </span>
                  </div>
                  <div class="td">
                    <a href="{{url('company/applicant/inquiry/delete')}}/{{$inquiry->job_inquiry_id}}" class="btn red">
                     {{ __('labels.delete') }}
                    </a>
                  </div>
                </div>
                @endforeach
              @endforeach
                    @else
                </div>
                </div>
                <div id="noRecordFound">
                  <p>No Records Found</p>
                </div>
                @endif
              <!-- tr -->
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
  <script>
    $(function(){
        $(".tab-menu li a").on("click", function() {
            $(".tab-menu li a").removeClass("active");
            $(this).addClass("active");
            $(".tab-boxes>div.box").hide();
            $($(this).attr("href")).fadeToggle();
        });
        return false;
    });
  </script>
</body>
</html>