@extends('company.layouts.app')

@section('title') {{__('labels.scout')}} {{__('labels.search')}} | @stop @push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />

@endpush @push('scripts')
<script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
@endpush @section('content')
<form action="{{url('/company/scout/search')}}" method="GET">
  {{csrf_field()}}
  <main id="scout-view" class="content-main">
      <header class="pan-area">
          <p class="text">
              {{__('labels.scout')}} - {{__('labels.search')}}
          </p>
      </header>

      <div class="content-inner">

          <nav class="local-nav">
             <ul class="menu">
                <li class="item">
                    <a href="{{url('company/scout')}}">{{__('labels.list')}}</a>
                </li>
                <li class="item">
                    <span>{{__('labels.search')}}</span>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-user')}}">{{__('labels.favorites-user')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-company')}}">{{__('labels.favorites-company')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/done')}}">{{__('labels.scouted')}}</a>
                </li>
            </ul>
          </nav>
          
          <div class="section posting-edit">
              <div class="table form panel">
                <div class="tbody">
                    <div class="tr">
                        <div class="td">
                           {{__('labels.gender')}}
                        </div>
                        <div class="td">
                         {{ Form::select('gender', 
                              [ 
                                "" => "ALL", 
                                "FEMALE" => "FEMALE",
                                "MALE" => "MALE"
                              ]
                              ,['id' => 'gender'],
           
                              array('class' => 'select_box')
                          )}}
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td">
                            {{__('labels.age')}}
                        </div>
                        <div class="td">
                          {{ Form::select('age', 
                              [ 
                                "" => "ALL", 
                                "20-25" => "20-25",
                                "25-30" => "25-30",
                                "30-35" => "30-35",
                                "35-40" => "35-40",
                              ]
                              ,['id' => 'age'],
                              array('class' => 'select_box')
                          )}}
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td">
                            {{__('labels.skills')}}
                        </div>
                        <div class="td">
                            <select class="select_box" name="skills_name" placeholder="{{__('labels.skill-name')}}" value="{{$selectedSkillName}}">
                                <option value="">ALL</option>
                                @foreach($skill_names as $skills_name)
                                    <option value="{{$skills_name->applicant_skill_name}}"> {{$skills_name->applicant_skill_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td">
                           {{__('labels.expected-salary-php')}}
                        </div>
                        <div class="td">
                            <select name="salary_min" class="select_box">
                            <option value=""></option>
                              @foreach($salaryMin as $salarymin)
                                @if($salarymin == $selectedSalaryMin)
                                  <option value="{{$salarymin}}" selected>{{number_format($salarymin)}}</option>
                                @else
                                  <option value="{{$salarymin}}">{{number_format($salarymin)}}</option>
                                @endif
                              @endforeach
                            </select>
                            
                            ~ 

                            <select name="salary_max" class="select_box">
                              <option value=""></option>
                              @foreach($salaryMax as $salarymax)
                                @if($salarymax == $selectedSalaryMax)
                                  <option value="{{$salarymax}}" selected>{{number_format($salarymax)}}</option>
                                @else
                                  <option value="{{$salarymax}}">{{number_format($salarymax)}}</option>
                                @endif
                              @endforeach
                            </select>
                        </div>
                    </div>
                </div>
              </div>  
              <div class="btn-wrapper">
                  <button type="submit" class="btn">
                    {{__('labels.search')}}
                  </button>
              </div>
          </div>
          <div class="section panel scout-index">
              <div class="table">
                @if($scoutApplicants->count())
                    <div class="thead">
                        <div class="tr">
                            <div class="th">
                            </div>
                            <div class="th">
                                {{__('labels.user')}}
                            </div>
                            <div class="th">
                                {{__('labels.education')}}
                            </div>
                            <div class="th">
                                {{__('labels.skills')}}
                            </div>
                            <div class="th">
                            </div>
                        </div>
                    </div>
                    <div class="tbody">
                        @foreach($scoutApplicants as $scoutApplicant)
                        <div class="tr">
                            <div class="td">
                                <div class="checkbox-wrapper">
                                    <label>
                                        <label>
                                        @if(empty($scoutApplicant->scout_profile($scoutApplicant->applicant_profile_id)))
                                            <input type="checkbox" name="addFavorites[]" class="checkbox-input" value="{{$scoutApplicant->applicant_profile_id}}">
                                            <span for="" class="checkbox-parts"></span>
                                        @endif
                                        </label>
                                    </label>
                                </div>
                            </div>
                            <div class="td">
                                <span class="icon"></span>  
                                <a href="{{url('company/scout/search/resume/')}}/{{$scoutApplicant->applicant_profile_id}}">
                                {{$scoutApplicant->applicant_profile_firstname}} 
                                {{$scoutApplicant->applicant_profile_middlename}} 
                                {{$scoutApplicant->applicant_profile_lastname}}
                                </a>
                            </div>
                            <div class="td">
                            {{(count($scoutApplicant->applicant_education) > 0 ) ? '' : __('messages.scout-no-data')}}
                            @foreach($scoutApplicant->applicant_education as $school)
                                <span>
                                    {{$school->applicant_education_school}} 
                                    <!-- <br>- {{$school->qualification->master_qualification_name}} -->
                                </span><br>
                            @endforeach
                            </div>
                            <div class="td">
                            {{(count($scoutApplicant->applicant_skills) > 0 ) ? '' : __('messages.scout-no-data')}}
                                @foreach($scoutApplicant->applicant_skills as $key => $value)
                                <span>{{$value->applicant_skill_name}}</span>
                          
                                

                                  @if($key % 3 == 2)
                                    @if($key + 1 != count($scoutApplicant->applicant_skills))
                                        <p></p>
                                    @endif
                                  @endif
                                @endforeach

                            </div>
                            <div class="td">
                                @if(empty($scoutApplicant->scout_profile($scoutApplicant->applicant_profile_id)))
                                
                                <a href="{{url('/company/scout/favorite/add')}}/{{$scoutApplicant->applicant_profile_id}}"  class="btn">
                                <!-- <button> -->
                                    {{__('labels.add-to-favorites')}}
                                <!-- </button> -->
                                </a>
                                
                                @else
                                <a href="{{url('/company/scout/favorite-user/choosTemplate')}}/{{$scoutApplicant->applicant_profile_id}}" class="btn {{strtolower(str_replace(' ', '-' , $scoutApplicant->scout_profile($scoutApplicant->applicant_profile_id)->scout_status))}}">
                                    <!-- <button> -->
                                    {{$scoutApplicant->scout_profile($scoutApplicant->applicant_profile_id)->scout_status}}
                                    <!-- </button> -->
                                </a>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                @else
                    <div class="tbody">
                        <div class="tr">
                            <div class="td">
                                {{__('messages.scout-no-users')}}
                            </div>
                        </div>
                    </div>
                @endif
              </div>
          </div>
      </div>
      <center>{{ $scoutApplicants->render()}} </center>
  </main>

</form>
@endsection