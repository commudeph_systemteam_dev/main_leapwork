@include('company.common.html-head')
<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/scout-resume.css')}}" />
<script type="text/javascript" src="{{url('/js/company/resume.js')}}"></script>

<!-- title -->
<title>{{ __('labels.scout-resume') }} | LEAP Work</title>
</head>
    <body>
        <header id="global-header">@include('company.share.header')</header>
        <div class="content-wrapper">
            <form method="GET" action="{{$isSearchView ? url('/company/scout/search') : url('/company/scout')}}" enctype="multipart/form-data">
                <aside id="sidebar">@include('company.share.sidebar')</aside>
                <main id="job-posting-view" class="content-main resume-view">
                    <header class="pan-area">
                        <p class="text">
                            {{ __('labels.resume') }}: {{$scout->applicant_profile_firstname}} {{$scout->applicant_profile_middlename}} {{$scout->applicant_profile_lastname}}
                            < <a href="mailto:{{$scout->user_applicant->email}}">{{$scout->user_applicant->email}}</a> >
                        </p>
                    </header>
                    <div class="content-inner">
                        <div class="section resume">
                            <div class="resume__imgcontainer">
                                <img src="{{ url('/storage/uploads/applicantProfile/profile_') . 
                                                        $scout->applicant_profile_id . 
                                                        '/' .
                                                        $scout->applicant_profile_imagepath }}" alt="">
                            </div>
                            <table class="table__resume">
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.full-name') }}</th>
                                        <td>{{$scout->applicant_profile_firstname}} {{$scout->applicant_profile_middlename}} {{$scout->applicant_profile_lastname}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.birthdate') }}</th>
                                        <td>{{date('m/d/Y', strtotime($scout->applicant_profile_birthdate))}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.gender') }}</th>
                                        <td>{{$scout->applicant_profile_gender}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.address') }}</th>
                                        <td>
                                            {{$scout->applicant_profile_postalcode}} <br/>
                                            {{$scout->applicant_profile_address1}} <br/>
                                            {{$scout->applicant_profile_address2}} 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.contact') }}</th>
                                        <td>{{$scout->applicant_profile_contact_no}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.desired-jobs') }}</th>
                                        <td>
                                            <ul>
                                                <li>{{$scout->applicant_profile_desiredjob1}}</li>
                                                <li>{{$scout->applicant_profile_desiredjob2}}</li>
                                                <li>{{$scout->applicant_profile_desiredjob3}}</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.salary') }}</th>
                                        <td>PHP {{number_format($scout->applicant_profile_expected_salary)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.public-relation') }}</th>
                                        <td>{{$scout->applicant_profile_public_relation}}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">{{ __('labels.skills') }}</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="clear">
                                            <table class="resume__innertbl">
                                                <tbody>
                                                    <tr>
                                                        <th>{{ __('labels.name') }}</th>
                                                        <th>{{ __('labels.type') }}</th>
                                                        <th>{{ __('labels.level') }}</th>
                                                    </tr>
                                                    @foreach($scout->applicant_skills as $skill)
                                                    <tr>
                                                        <td>{{$skill->applicant_skill_name}}</td>
                                                        <td>{{$skill->applicant_skill_type}}</td>
                                                        <td>{{$skill->applicant_skill_level}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">{{ __('labels.work-experience') }}</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="clear">
                                            <table class="resume__innertbl">
                                                <tbody>
                                                    <tr>
                                                        <th>{{ __('labels.company-name') }}</th>
                                                        <th>{{ __('labels.date-from') }}</th>
                                                        <th>{{ __('labels.date-to') }}</th>
                                                        <th>{{ __('labels.job-description') }}</th>
                                                    </tr>
                                                    @foreach($scout->applicant_workexperiences as $workExperience)
                                                    <tr>
                                                        <td>{{$workExperience->applicant_workexp_company_name}}</td>
                                                        <td>{{date('m/d/Y', strtotime($workExperience->applicant_workexp_datefrom))}}</td>
                                                        <td>{{date('m/d/Y', strtotime($workExperience->applicant_workexp_dateto))}}</td>
                                                        @if(strlen($workExperience->applicant_workexp_past_job_description) >= 200)
                                                        <td class="jobDesc"><span id="jobDescContents_{{$workExperience->applicant_workexp_id}}"> {{substr($workExperience->applicant_workexp_past_job_description, 0, 200)}} <a href="#" class="jobDescSeeMore" data-id="{{$workExperience->applicant_workexp_id}}"> see more</a></span>
                                                            <span id="seeMoreContents_{{$workExperience->applicant_workexp_id}}"  hidden>{{substr($workExperience->applicant_workexp_past_job_description, 201)}}<a href="#" class="jobDescSeeLess" data-id="{{$workExperience->applicant_workexp_id}}"> see less</a></span>
                                                        </td>
                                                        @else
                                                        <td class="jobDesc">{{$workExperience->applicant_workexp_past_job_description}}</td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="btn-wrapper">
                                <button type="submit" id="btnMemo">{{ __('labels.back') }}</button>
                            </div>
                        </div>
                    </div>
                </main>
            </form>
        </div>
    </body>
</html>

