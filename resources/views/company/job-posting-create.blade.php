@include('company.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" />
<link rel="stylesheet" href="{{url('css/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{url('css/app.css')}}">

<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/job-posting.css')}}" />

<!-- js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="{{url('js/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
</script>
<script type="text/javascript" src="{{url('/js/company/home.js')}}"></script>
<script type="text/javascript" src=" {{url('/js/company/job-posting.js')}}"></script>

<!-- title -->
<title> {{ __('labels.job-post') }} | LEAP Work</title>


</head>



<body>

    <header id="global-header">@include('company.share.header')</header>
      <div class="content-wrapper">

        <div id="limitReachedModal"></div>


        <aside id="sidebar">@include('company.share.sidebar')</aside>
        
        <form method="POST" action="{{url('company/job-posting')}}" enctype="multipart/form-data" id="companyForm">
            {{ csrf_field() }}
            <main id="job-posting-view" class="content-main">
                @if( !is_null($company_plan_history) && $company_plan_history->company_plan_status == "ACTIVE" )
                <header class="pan-area">
                    <p class="text">
                        {{ __('labels.job-post') }}
                    </p>
                </header>
        <div id="formJobPost">
                    <div class="content-inner">
                        <aside class="search-menu">
                            <ul class="list">
                                <li class="item">
                                    <a onclick="document.getElementById('how-to').style.display='block'" class="btn green">
                                      {{__('labels.how-to-write-a-job-post')}}
                                    </a>
                                </li>
                            </ul>
                        </aside>
                        <div class="section posting-edit">
                            <div class="table form panel">
                                <div class="tbody">
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.job-title')}}
                                        </div>
                                        <div class="td">
                                            <input type="text" id="job_post_title" name="job_post_title">
                                            @if ($errors->first('jobPostTitleErrorMessage'))
                                                <div class="alert alert-danger">
                                                        {{ $errors->first('jobPostTitleErrorMessage') }}
                                                </div>
                                            @endif
                                            @if ($errors->first('job_post_title'))
                                                <div class="alert alert-danger">
                                                        {{ $errors->first('job_post_title') }}
                                                </div>
                                            @endif
                                            <span class="help-block required--text hide red" id="titleErr">
                                                <strong>Job Title is required</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.job-position')}}
                                        </div>
                                        <div class="td">
                                            <input list="job_positions" id="job_post_job_position_id" name="job_post_job_position_id" placeholder="Job Position">
                                            <datalist id="job_positions">
                                                @foreach($jobPositions as $jobPosition)
                                                <option value="{{$jobPosition->master_job_position_name}}"></option>
                                                 @endforeach
                                            </datalist>
                                            <span class="help-block required--text hide red" id="positionErr">
                                              <strong>Job Position is required</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            {{__('labels.key-visual-pc')}}
                                            <br/>
                                            <p class="help-block required--text red" id="image-size-label">(Please upload 1100 x 287)</p>
                                        </div>
                                        <div class="td">
                                            <input type="file" name="file_job_post_key_pc" id="file_job_post_key_pc">
                                            <div class="image__container">
                                                <img id="img_header_container" alt=""/>
                                            </div>
                                             <span class="help-block required--text hide red" id="imageErr">
                                                <strong>Image size should be 1100 X 287</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            {{__('labels.key-visual-smartphone')}}
                                        </div>
                                        <div class="td">
                                            <input type="file" name="file_job_post_key_sp" id="file_job_post_key_sp">
                                            <div class="image__container">
                                                <img id="img_header_sp_container" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                           {{__('labels.business-contents')}}
                                        </div>
                                        <div class="td">
                                            <textarea name="job_post_business_contents" id="job_post_business_contents" value="{{  old('job_post_business_contents') }}" maxlength="1000"></textarea>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.job-description')}}
                                        </div>
                                        <div class="td">
                                            <textarea name="job_post_description" id="job_post_description" maxlength="1000"></textarea>
                                            <span class="help-block required--text hide red" id="descriptionErr">
                                              <strong>Job Description is required</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span>{{__('labels.job-skills')}}
                                        </div>
                                        <div class="td">
                                            <div class="dvTag bootstrap-label-adjust">
                                            <!-- styles to override is below. Note that the global input mas be overriden since a js auto generated one is being used  -->
                                                <input type="text" id="tag_job_post_skills" name="tag_job_post_skills" data-role="tagsinput" maxlength="1000" />
                                            </div>
                                            <span class="help-block required--text hide red" id="skillErr">
                                              <strong>Job Skill is required</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                           {{__('labels.image')}}
                                        </div>
                                        <div class="td file-upload">
                                            {{Form::select('numOfImages', config('constants.'.Session::get('locale').'_numberOfImagesToUpload')) }}
                                            <input type="file" name="file_job_post_image1" id="file_job_post_image1">
                                            <div class="image__container">
                                                <img id="img_job1_container" alt=""/>
                                            </div>
                                             <span class="help-block required--text hide red" id="jobPostImage1Err">
                                                <strong>Image size should be 539 X 290</strong>
                                            </span>
                                            <input type="file" name="file_job_post_image2" id="file_job_post_image2" class="hide">
                                            <div class="image__container">
                                                <img id="img_job2_container" alt=""/>
                                            </div>
                                             <span class="help-block required--text hide red" id="jobPostImage2Err">
                                                <strong>Image size should be 539 X 290</strong>
                                            </span>
                                            {{--  <input type="file" name="file_job_post_image3" class="hide">  --}}
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.job-industry')}}
                                        </div>
                                        <div class="td">
                                            <select name="job_post_industry_id" id="job_post_industry_id" onchange="showfield(this.options[this.selectedIndex].value)">
                                                <option value=""></option>
                                                @foreach($jobIndustries as $jobIndustry)
                                                <option value="{{$jobIndustry->master_job_industry_name}}">{{$jobIndustry->master_job_industry_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block required--text hide red" id="industryErr">
                                                <strong>Job Industry is required</strong>
                                            </span>
                                            <div id="div1"></div>
                                            <span class="help-block required--text hide red" id="industryOtherErr">
                                                <strong>Please specify Other Industry.</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            {{__('labels.job-overview')}}
                                        </div>
                                        <div class="td">
                                            <textarea id="job_post_overview" name="job_post_overview" maxlength="1000"></textarea>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            {{__('labels.qualifications-requirements')}}
                                        </div>
                                        <div class="td">
                                            <textarea id="job_post_qualifications" name="job_post_qualifications" maxlength="1000"></textarea>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.employment-classification')}}
                                        </div>
                                        <div class="td">
                                            <select id="job_post_classification_id" name="job_post_classification_id">
                                                <option value="" selected hidden>Select Option</option><!-- Karen(11-21-2017) : Do not disabled the input as we will not get the value. -->
                                                @foreach($jobClassifications as $jobClassification)
                                                <option value="{{$jobClassification->master_job_classification_id}}">{{$jobClassification->master_job_classification_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block required--text hide red" id="classificationErr">
                                                <strong>Employment Classification is required</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span>{{__('labels.salary-range')}}
                                        </div>
                                        <div class="td">
                                            <input type="number" id="job_post_salary_min" min=1 name="job_post_salary_min" lang="en-150"> ~
                                            <input type="number" id="job_post_salary_max" min=1 name="job_post_salary_max" lang="en-150">
                                            @if ($errors->first('job_post_salary_min'))
                                                <div class="alert alert-danger">
                                                        {{ $errors->first('job_post_salary_min') }}
                                                </div>
                                            @endif
                                            @if ($errors->first('job_post_salary_max'))
                                                <div class="alert alert-danger">
                                                        {{ $errors->first('job_post_salary_max') }}
                                                </div>
                                            @endif
                                            <span class="help-block required--text hide red" id="salaryRangeErr">
                                                <strong>Salary Minimum is required</strong>
                                            </span>
                                            <span class="help-block required--text hide red" id="salaryMaxErr">
                                                <strong >Salary Max should be greater than or equal Salary Minimum</strong>
                                            </span>
    
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            {{__('labels.supplement')}}
                                        </div>
                                        <div class="td">
                                            <textarea id="job_post_supplement" name="job_post_supplement" maxlength="1000"></textarea>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.number-of-employees-needed')}}
                                        </div>
                                        <div class="td">
                                            <input type="number" id="job_post_no_of_positions" min="1" name="job_post_no_of_positions">
                                            @if ($errors->first('job_post_no_of_positions'))
                                                <div class="alert alert-danger">
                                                        {{ $errors->first('job_post_no_of_positions') }}
                                                </div>
                                            @endif
                                            <span class="help-block required--text hide red" id="numEmployeesErr">
                                              <strong>Number of Employees is required</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            {{__('labels.application-process')}}
                                        </div>
                                        <div class="td">
                                            <textarea name="job_post_process" id="job_post_process" maxlength="1000"></textarea>
                                        </div>
                                    </div>
                                    <div class="tr">
                                        <div class="td">
                                            <span class="required">{{__('labels.required')}}</span> {{__('labels.work-location')}}
                                        </div>
                                        <div class="td">
                                            <select name="job_post_location_id" id="job_post_location_id">
                                                <option value=""></option>
                                                @foreach($jobLocations as $jobLocation)
                                                    <option value="{{$jobLocation->master_job_location_id}}">{{$jobLocation->master_job_location_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block required--text hide red" id="workLocation">
                                              <strong>Work location is required</strong>
                                            </span>
                                        </div>
                                    </div>
                             
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.work-hours')}}
                                    </div>
                                    <div class="td">
                                        <select name="job_post_work_timestart" id="job_post_work_timestart">
                                            @foreach($workingHoursStart as $start)
                                            <option value="{{$start}}">{{$start}}</option>
                                            @endforeach
                                        </select> ~
                                        <select name="job_post_work_timeend" id="job_post_work_timeend">
                                            @foreach($workingHoursEnd as $end)
                                            <option value="{{$end}}">{{$end}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{__('labels.benefits')}}
                                    </div>
                                    <div class="td">
                                        <textarea type="text" id="job_post_benefits" name="job_post_benefits" maxlength="1000"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                       {{__('labels.restday-holiday-vacation')}}
                                    </div>
                                    <div class="td">
                                        <textarea type="text" id="job_post_holiday" name="job_post_holiday" maxlength="1000"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                       {{__('labels.others')}}
                                    </div>
                                    <div class="td">
                                        <textarea id="job_post_otherinfo" name="job_post_otherinfo" maxlength="1000"></textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                       {{__('labels.post-visibility')}}
                                    </div>
                                    <div class="td">
                                        <select name="job_post_visibility">
                                            <option value="PUBLIC">{{ __('labels.public') }}</option>
                                            <option value="PRIVATE">{{ __('labels.private') }}</option>
                                        </select>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-wrapper">
                        <a class="btn green" id="job-create-preview">Preview</a>
                    </div>
                    
                    @if(isset($message))
                    <div id="activateModal" class="w3-modal" style="display: block !important;">
                        <div class="w3-modal-content w3-animate-top w3-card-4" id="jobPostModal">
                            <header class="billing_modal_header"> 
                                <span onclick="document.getElementById('activateModal').style.display='none'" 
                                  class="w3-button w3-display-topright"><img class="jobPostCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
                                <p><br></p>
                                <h4> {{$message}} </h4>
                            </header>
                            <div class="w3-container billing_modal_body" id="modalOkButton">
                            <p><input type="button" onclick="window.location='{{url('/company/recruitment-jobs')}}'" id="activate" name="activate" value="OK"><br><br></p>
                            </div>
                        </div>
                    </div>
                    @endif
              </div> 
              @elseif (Auth::user()->user_accounttype == "CORPORATE USER")
              <div class="tbody">
                <div class="tr">
                  <div class="td">
                        <div id="activateModal" class="w3-modal">
                            <div class="w3-modal-content w3-animate-top w3-card-4">
                                <header class="billing_modal_header"> 
                                    <p><br></p>
                                    <h4><span class="red"> Please contact your company administrator to pay or activate the plan.<span class="red"></h4>
                                    <p><input type="button" onclick="window.location='{{url('company/home')}}'" name="activate" value="OK"><br><br></p>
                                </header>
                            </div>
                        </div>
                  </div>
                </div>
              </div> 
              @else
              <div class="tbody">
                    <div class="tr">
                      <div class="td">
                            <div id="activateModal" class="w3-modal">
                                <div class="w3-modal-content w3-animate-top w3-card-4">
                                    <header class="billing_modal_header"> 
                                        {{-- <span onclick="document.getElementById('activateModal').style.display='none'" 
                                          class="w3-button w3-display-topright">&times;</span> --}}
                                        <p><br></p>
                                        <h4><span class="red"> {{$message}} <span class="red"></h4>
                                        <p><br></p>
                                        <p><input type="button" onclick="window.location='{{url('company/billing')}}'" name="activate" value="OK"><br><br></p>
                                    </header>
                                    {{-- <div class="w3-container billing_modal_body">
                                        <p><input type="button" onclick="document.getElementById('activateModal').style.display='none'" name="activate" value="OK"><br><br></p>
                                    </div> --}}
                                </div>
                            </div>
                            {{-- {{$message}}  --}}
                      </div>
                    </div>
                  </div> 
              @endif
        </div>

        <div class="preview_job" id="previewPost">
            <input id="header-image-default" type="hidden" value="{{ asset('images/top_noimage_1200x400.jpg') }}">
            <input id="job-image-default" type="hidden" value="{{ asset('images/top_blankimage.jpg') }}">

            <div class="content jobdetails">
                <div class="job__details">                    
                    <div class="feature__image clearfix">
                        <div>
                            <img id="header_image" src="{{ asset('images/top_noimage_1200x400.jpg') }}" alt="">
                        </div>
                    </div>
    
                    <div class="jobdetails__content">
                        <h1 id="job_title"></h1>
                        <div class="jobdetails__information">
                            <h3>Business Contents</h3>
                            <p id="job_business"></p>
                            <h3>Job Description</h3>
                            <p id="job_description"></p>
                                <div class="image__container clearfix" style="max-width: 100%;">
                                <div>
                                    <img id="job_image1" src="{{ asset('images/top_blankimage.jpg') }}" alt="">
                                </div>
                                <div>
                                    <img id="job_image2" src="{{ asset('images/top_blankimage.jpg') }}" alt="">
                                </div>
                                </div>
    
                            <h3>Work Description</h3>
                            <table class="jobdetails__table">
                                <tr>
                                    <td>Job Position</td>
                                    <td><span id="job_position"></span></td>
                                </tr>
                                <tr>
                                    <td>Overview</td>
                                    <td><span id="job_overview"></span></td>
                                </tr>
                                <tr>
                                    <td>Qualifications Requirements</td>
                                    <td><span id="job_requirements"></span></td>
                                </tr>
                                <tr>
                                    <td>Employment Category</td>
                                    <td><span id="job_classification"></span></td>
                                </tr>
                                <tr>
                                    <td>Salary</td>
                                    <td>
                                        <span id="job_min" name="job_min"></span> - <span id="job_max" name="job_max"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Vacancy</td>
                                    <td><span id="job_vacancy"></span>
                                </tr>
                                <tr>
                                    <td>Selection Process</td>
                                    <td><span id="job_process"></span>
                                </tr>
                                <tr>
                                    <td>Work Location</td>
    
                                    <td>{{ is_null($company->company_address1) ? '' : $company->company_address1}}
                                        <br>{{ is_null($company->company_address2) ? '' : $company->company_address2 }}
                                        <span id="job_location"></span>, {{ is_null($company->master_country_name) ? '' : $company->master_country_name}}</td>
                                </tr>
                                <tr>
                                    <td>Working Hours</td>
                                    <td><span id = "job_start" name="job_start" readonly></span> - <span id = "job_end" name="job_end"></span></td>
                                </tr>
                                <tr>
                                    <td>Benefits</td>
                                    <td><span id="job_benefits"></span></td>
                                </tr>
                                <tr>
                                    <td>Holiday / Vacation</td>
                                    <td><span id="job_holiday"></span></td>
                                </tr>
                                <tr>
                                    <td>Characteristics</td>
                                    <td><span id="job_characteristics"></span></td>
                                </tr>
                            </table>
                        </div><!-- .jobdetails__information -->
                    </div><!-- .jobdetails__content -->
                      <div class="btn-wrapper">
                            <a id="job-create-back" class="btn alt-btn">
                                      Back
                            </a>
                            <a data-modalId="limitReachedModal" class="btn" id="{{$isLimitReach ? 'limitReachedModalBtn' : 'btnJobPost'}}">{{ __('labels.save') }}</a>
                        </div>
    
                </div><!-- .job__details -->
                
        </div>
        </main>
    </form>
    </div>

    <div id="how-to" class="modal">
        <form class="modal-content animate">
          <div class="imgcontainer">

          </div>
      
          <div class="container">
            <span><a href="#" onclick="document.getElementById('how-to').style.display='none'"><img class="howToCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></a></span>

                <h2>{{ __('labels.how-to-write-a-job-post') }}</h2>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum fringilla magna, in semper sapien consequat nec. Cras varius sollicitudin lobortis. Aliquam elementum purus nec fermentum accumsan. Nam vel sem ut tortor porta lacinia at eget arcu. Suspendisse ornare accumsan neque at placerat. Quisque rhoncus lacus eu erat maximus tempus. Fusce feugiat neque eu ultrices luctus. Phasellus a purus tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum maximus, ex at molestie luctus, mauris ex ornare nisl, sed mollis nisl nisi tincidunt ipsum. Pellentesque gravida orci sed nibh interdum, quis egestas est tristique.
                <br><br>
                Proin dapibus ante felis, quis congue lacus auctor sodales. Duis ultricies quam a dapibus vehicula. Nulla auctor porta mauris, nec tempus eros maximus sit amet. Ut volutpat tempor felis, vel volutpat nisi accumsan at. Suspendisse risus enim, suscipit eget feugiat ac, ultricies ullamcorper eros. Aliquam semper lorem mauris, vel aliquam est efficitur quis. Sed faucibus iaculis tristique. Pellentesque eget purus vel urna ornare egestas. Sed ornare purus in scelerisque semper.
                <br><br>
                Curabitur venenatis augue vitae porttitor sodales. Nam non pharetra dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Quisque maximus ligula ac nisi fermentum, sed consequat nibh mattis. Vivamus volutpat scelerisque finibus. Donec quis elit non ligula iaculis eleifend.
                <br><br>
                Donec gravida libero at dolor rhoncus vehicula. Fusce sem nisl, faucibus sed massa vel, ultricies porta est. Ut quis lacinia ante. Proin commodo aliquam pretium. Pellentesque nec scelerisque ante. Nunc at tortor laoreet, pellentesque ex eget, suscipit elit. Nulla feugiat diam pharetra mi lobortis, eget vulputate erat congue. Cras ipsum neque, facilisis in volutpat vel, cursus et purus. Nulla varius ac sapien eget convallis.
                <br><br>
                Aenean vel ornare tortor, at sollicitudin nisl. Sed elementum turpis velit, et efficitur dui lacinia ullamcorper. Praesent scelerisque, orci sed luctus sagittis, urna orci efficitur sapien, vel convallis massa est eu mauris. Nam ipsum ante, cursus sit amet elit vel, ultrices ultrices nisl. Duis congue varius justo quis rutrum. Cras ut porta dolor. Morbi quis sollicitudin est. Curabitur quis ex at justo pharetra semper.
                </p>
          </div>
      
          <div class="btn-wrapper">
            <button type="button" onclick="document.getElementById('how-to').style.display='none'" class="btn green">OK</button>
          </div>
        </form>
    </div>
                
</body>
</html>