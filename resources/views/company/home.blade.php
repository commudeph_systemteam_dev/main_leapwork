@include('company.common.html-head')
l
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/home.css')}}" />

  <script type="text/javascript" src="{{url('/js/company/home.js')}}"></script>
  <!-- title -->
  <title>{{__('labels.dashboard')}} | LEAP Work</title>
</head>
<body>
  <header id="global-header">
    @include('company.share.header')
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
      @include('company.share.sidebar')
    </aside>
    <main id="home-view" class="content-main">
      <div class="content-inner">
        <div class="summary">
          <ul class="list cf">
            <li class="panel ranking">
              <p class="text">
                {{__('labels.ranking')}}<br>
                @if(!is_null($ranking))
                  <span class="num">{{$ranking}}<span>{{ str_ordinal($ranking) }}</span></span>
                @else
                  <span class="num">N/A</span>
                @endif
              </p>
            </li>
            <li class="panel pv">
              <p class="text">
                {{__('labels.current-total-pv')}}<br>
                <span class="num">{{$currentTotalPv}}<span> PV</span></span>
              </p>
              @if(Auth::user()->user_accounttype == 'CORPORATE ADMIN')
                <a href="{{url('company/analytics')}}">
                  {{__('labels.view-analytics')}}
                </a>
              @endif
            </li>
            <li class="panel plan">
              <p class="text">
                @if(!is_null($planHistory))
                  @if($planHistory->company_plan_status == "ACTIVE")
                  {{__('labels.current-plan')}}<br>
                  <span class="plan-name">
                    @if($planHistory->company_plan_type == "TRIAL")
                      {{$planHistory->company_plan_type}}
                    @else
                      {{$planHistory->company_plan_type}}
                      <br>
                      <span>
                        PHP {{ number_format(Auth::user()["current_plan"]["plan_price"])}}
                      </span>
                    @endif
                    
                    </span>
                  @elseif($planHistory->company_plan_status == "EXPIRED" && !is_null($planHistory->company_plan_dateexpiry))
                  {{__('labels.current-plan')}}<br>
                  <span class="plan-name">
                    {{$planHistory->company_plan_type}}<br>
                    <span class="red">
                      {{$planHistory->company_plan_status}}
                      </span>
                    </span>
                  @elseif($planHistory->company_plan_status == "INACTIVE")
                  {{__('labels.current-plan')}}<br>
                  <span class="plan-name">
                    {{$planHistory->company_plan_type}}<br>
                    <span class="blue">
                      {{$planHistory->company_plan_status}}
                      </span>
                    </span>
                @endif
                @else
                <span class="plan-name">
                    <br> - <br>
                <span>
                @endif
                
                <br>
              </p>
              @if(Auth::user()->user_accounttype == 'CORPORATE ADMIN' && !is_null($planHistory))
                  <a href="{{url('company/billing')}}">
                    {{__('labels.change-in-account-settings')}}
                  </a>
              @endif
            </li>
          </ul>
        </div>
        <div class="section recruitment-jobs">
          <header class="header">
            <h2 class="title">
              {{__('labels.job-posts')}}
            </h2>
            <a href="{{url('/company/recruitment-jobs')}}" class="link">
              {{__('labels.recruitment')}}
            </a>
          </header>
          @if (Session::has('message'))
        
            <div class="alert__modal">
            <div class="alert__modal--container">
               <p>{{ Session::get('message') }}</p><span>x</span>
            </div>
            </div>
          @endif
          <div class="table panel">
            <div class="thead">
              <div class="tr">
                <div class="th">
                  {{__('labels.job-title')}}
                </div>
                <div class="th">
                  {{__('labels.job-description')}}
                </div>
                <div class="th">
                  {{__('labels.job-status')}}
                </div>
              </div>
            </div>
            <div class="tbody">
              @foreach($activeJobPosts as $activeJobPost)
                <div class="tr">
                    @if(Auth::user()->hasRight('JOB POST'))
                    <div class="td">
                        <a href="{{ url('company/job-posting/edit')}}/{{$activeJobPost->job_post_id }}">{{$activeJobPost->job_post_title}}</a>
                      </div>
                    @else
                    <div class="td">
                        <p>{{$activeJobPost->job_post_title}}</p>
                      </div>
                    @endif
                  <div class="td">
                      {{ mb_strimwidth($activeJobPost->job_post_description, 0, 200, " ...")}}
                  </div>
                  <div class="td">
                    <span class="{{strtolower(str_replace(' ', '-' ,$activeJobPost->job_post_visibility))}}">
                      {{$activeJobPost->job_post_visibility}}
                    </span>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
</body>
</html>