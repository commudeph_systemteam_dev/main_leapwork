<div class="menu-btn">
  <img src="{{url('/images/common/icon/nav-menu.svg')}}" alt="">
</div>
<nav class="global-nav">
  <ul class="list">
    <li class="item home">
      <a href="{{url('company/home')}}">
        <span>{{__('labels.dashboard')}}</span>
      </a>
    </li>
    <li class="item applicant" id="applicantSidebar">
      <a href="{{url('company/applicant')}}">
        <span>{{__('labels.applicants')}}</span>
      </a>
    </li>
    <li class="item recruitment-jobs">
      <a href="{{url('company/recruitment-jobs')}}">
        <span>{{__('labels.recruitment')}}</span>
      </a>
    </li>

    @if(Auth::user()->hasRight('JOB POST'))
        <li class="item job-posting">
          <a href="{{url('company/job-posting')}}">
            <span>{{__('labels.job-posting')}}</span>
          </a>
        </li>
    @endif

    @if(Auth::user()->hasRight('SCOUT'))
        <li class="item scout" id="scoutSidebar">
          <a href="{{url('company/scout')}}">
            <span>{{__('labels.scout')}}</span>
          </a>
        </li>
    @endif

    @if(Auth::user()->user_accounttype == 'CORPORATE ADMIN')
        <li class="item analytics">
          <a href="{{url('company/analytics')}}">
            <span>{{__('labels.analytics')}}</span>
          </a>
        </li>
    @endif

    @if(Auth::user()->hasRight('COMPANY INFORMATION'))
        <li class="item companies">
          <a href="{{url('company/profile')}}">
            <span>{{__('labels.company-information')}}</span>
          </a>
        </li>
    @endif

    <li class="item account">
      <a href="{{url('company/account')}}">
        <span>{{__('labels.account')}}</span>
      </a>
    </li>

    @if(Auth::user()->user_accounttype == 'CORPORATE ADMIN')
        <li class="item billing">
          <a href="{{url('company/billing')}}">
            <span>{{__('labels.billing')}}</span>
          </a>
        </li>
    @endif

    <li class="item notice">
      <a href="{{url('company/notice')}}">
        <span>{{__('labels.announcements')}}</span>
      </a>
    </li>
    <li class="item contact">
      <a href="{{url('company/inquiry')}}">
        <span>{{__('labels.inquiry')}}</span>
      </a>
    </li>
  </ul>
</nav>

<input type=hidden value="{{ $planHistory = Auth::user()->getCurrentPlanHistory(Auth::user()->user_company_id) }}"/>
<input type=hidden value="{{ $planChecker = is_null($planHistory = Auth::user()->getCurrentPlanHistory(Auth::user()->user_company_id)) }}"/>

@if(Auth::user()->user_accounttype == 'CORPORATE ADMIN')
<div class="usage-plan">

      @if(!$planChecker)
      <a href="{{url('company/billing')}}">
        @if($planHistory->company_plan_status == "ACTIVE")
          {{__('labels.current-plan')}}<br>
          <span class="plan-name" id="currentPlan">
              {{$planHistory->company_plan_type}}
          </span>
        @elseif($planHistory->company_plan_status == "EXPIRED" && !is_null($planHistory->company_plan_dateexpiry))
          {{__('labels.current-plan')}}<br>
          <span class="plan-name" id="currentPlan">
            {{$planHistory->company_plan_type}}
          </span><br>
          <span class="red" id="currentPlan">
              {{$planHistory->company_plan_status}}
            </span>
        @elseif($planHistory->company_plan_status == "INACTIVE")
          {{__('labels.current-plan')}}<br>
          <span class="plan-name" id="currentPlan">
            {{$planHistory->company_plan_type}}
          </span>
          <br>
          <span class="blue" id="currentPlan">
              {{$planHistory->company_plan_status}}
            </span>
        @endif
        <br>
        <span class="btn">{{__('labels.change-plan')}}</span>
      </a>
      @else
      <a style="pointer-events: none; cursor: default;">
        <span class="plan-name red" id="currentPlan">
            NO SUBSCRIBED PLAN
        </span>
      </a>
      @endif


  </div>
@else
<div class="usage-plan">
    <a href="{{url('company/billing')}}" style="pointer-events: none; cursor: default;">
      @if(!$planChecker)
        @if($planHistory->company_plan_status == "ACTIVE")
          {{__('labels.current-plan')}}<br>
          <span class="plan-name" id="currentPlan">
              {{$planHistory->company_plan_type}}
          </span>
        @elseif($planHistory->company_plan_status == "EXPIRED" && !is_null($planHistory->company_plan_dateexpiry))
          {{__('labels.current-plan')}}<br>
          <span class="plan-name" id="currentPlan">
            {{$planHistory->company_plan_type}}
          </span><br>
          <span class="red" id="currentPlan">
              {{$planHistory->company_plan_status}}
            </span>
        @elseif($planHistory->company_plan_status == "INACTIVE")
          {{__('labels.current-plan')}}<br>
          <span class="plan-name" id="currentPlan">
            {{$planHistory->company_plan_type}}
          </span>
          <br>
          <span class="blue" id="currentPlan">
              {{$planHistory->company_plan_status}}
            </span>
        @endif
        @else
          <span class="plan-name red" id="currentPlan">
              NO SUBSCRIBED PLAN
          </span>
        @endif
      <br>
    </a>
  </div>
@endif

<script type="text/javascript">
  $(function(){
    // if user has cookie close default
    var key = $.cookie('sidemenu');
    // alert(key);
    if ( key == 'close' ) {
      // alert('sidemenu key is close');
      sideMenuClose();
    } else {
      // alert('sidemenu key is open or empty');
      sideMenuOpen();
    }
    // side menu open & close click event
    $("aside#sidebar div.menu-btn").on("click", function() {
      if ( $(this).hasClass('active') ) {
        sideMenuOpen();
      } else {
        sideMenuClose();
      }
    });
    // side menu open & close function
    function sideMenuOpen() {
      $("aside#sidebar div.menu-btn").removeClass("active");
      $("aside#sidebar").removeClass("side-open");
      $("main.content-main").removeClass("side-open");
      $.cookie('sidemenu', 'open'); // change cookie
    }
    function sideMenuClose() {
      $("aside#sidebar div.menu-btn").addClass("active");
      $("aside#sidebar").addClass("side-open");
      $("main.content-main").addClass("side-open");
      $.cookie('sidemenu', 'close', { expires: 7 }); // change cookie
    }
  });
</script>
