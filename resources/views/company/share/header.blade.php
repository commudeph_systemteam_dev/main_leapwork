
<script type="text/javascript" src="{{url('/js/company/header.js')}}"></script>

<script type="text/javascript">
  var seenAnnouncements = {{json_encode($seenAnnouncements)}};
</script>


<div class="inner cf">
  <figure class="logo">
    <a href="{{url('company/home')}}"><img src="{{url('/images/common/icon/header-logo.svg')}}" alt=""></a>
  </figure>
  <ul class="function-menu cf">
    <li class="function applicant">
      <span class="icon {{ (count($messageIconNotification)) > 0 ? 'unread': ''}}"></span>
      <ul class="tooltip">
        <div class="tooltip__container">

          @if(count($messageNotifications) == 0)
          <li id="noMessageItem" class="item">
            <p class="text item-empty">
              {{__('messages.messages-none')}}
            </p>
          </li>
          @endif
          @foreach($messageNotifications as $message)
            @php 
              $timezone = 'Asia/Manila';
              $now = \Carbon\Carbon::now($timezone);
              $deliveryDate = \Carbon\Carbon::parse($message->notification_created_at ,$timezone);
            @endphp
            <li class="item">
              <span class="user-icon">
                  @if($message->notification_type_id == 1)
                  <img src="{{ asset('/storage/uploads/master-admin/leapwork_logo.png') }}"></span>
                  @else 
                  <img src="{{ is_null($message->company_logo) 
                    ? url('/images/common/icon/header-user.png') 
                    :url('/storage/uploads/company/company_') .
                    $message->company_id . 
                      '/' . $message->company_logo }}">
                  @endif 
                  
                
              </span>
              <p style="text-align: right;">
              @if($now->diffInHours($deliveryDate) <= 1)
                {{$deliveryDate->diffForHumans()}}
              @elseif($now->diffInDays($deliveryDate) < 1)
                {{$deliveryDate->format('h:i A')}}
              @else
                {{$deliveryDate->format('D (m/d)')}}
              @endif
              </p>
              <a href="{{ url($message->notification_url)}}">
              <p class="text">
                <b>{{json_decode($message->notification_data)->title}}</b><br>
                {{ mb_strimwidth(json_decode($message->notification_data)->content, 0, 100, " ...")}}
              </p>
              </a>
            </li>

          @endforeach
          <li class="read-more">
            <a href="{{url('/company/notice')}}">
              {{ __('labels.view-more') }}
            </a>
          </li>
        </div>
      </ul>
    </li>
    <li class="function notice">
      <span class="icon {{(count(json_decode($bellNotifications)) > 0) || $newAnnouncementsCount > 0 ? 'unread' : ''}}"></span>
      <ul class="tooltip">
        <div class="tooltip__container">
        <!-- <temporary> 
            <li class="item">
              <span class="user-icon"></span>
              <a href="#">
                Fri (03/02)
                <p class="text">
                  <b>User Name</b>
                  <br>
                  Lorem ipsum dolor sit amet
                </p>
              </a>
            </li>
            <li class="item">
              <span class="user-icon"></span>
              <a href="#">
                Fri (03/02)
                <p class="text">
                  <b>User Name</b>
                  <br>
                  Lorem ipsum dolor sit amet
                </p>
              </a>
            </li>
        </temporary> -->
        @php
          $timezone = 'Asia/Manila';
        @endphp
        @foreach($bellListNotifications as $notification)
        @php
            $now = \Carbon\Carbon::now($timezone);
            $deliveryDate = \Carbon\Carbon::parse($notification->announcement_deliverydate ,$timezone);
        @endphp
        <li data-id="{{$notification->id}}" data-type="notification" class="item">
          @if($notification['notification_url'] == "/company/applicant")
          <a href="{{ url($notification['notification_url'])}}">
          @else
          <a href="{{ url('company/notice/announcements/details/'. $notification['notification_url'])}}"><p style="text-align: right;">
          @endif
             @if($now->diffInHours($deliveryDate) <= 1)
                {{$deliveryDate->diffForHumans()}}
              @elseif($now->diffInDays($deliveryDate) < 1)
                {{$deliveryDate->format('h:i A')}}
              @else
                {{$deliveryDate->format('D (m/d)')}}
              @endif
          </p>
            <p class="text">
              <b>{{json_decode($notification->notification_data)->title}}</b>
              <br>
              {{ mb_strimwidth(json_decode($notification->notification_data)->content, 0, 100, " ...")}}
            </p>
          </a>
        </li>
        @endforeach
    
        <!-- Adjust this depending on the requirements -->
       

        @foreach($announcements as $announcement)

          @php
            $now = \Carbon\Carbon::now($timezone);
            $deliveryDate = \Carbon\Carbon::parse($announcement->announcement_deliverydate ,$timezone);
          @endphp

          <li data-id="{{$announcement->announcement_id}}" data-type="announcement" class="item {{(!in_array($announcement->announcement_id, $seenAnnouncements)) ? 'unseen' : ''}}">
            <a href="{{ url('company/notice/announcements/details/'. $announcement->announcement_id)}}">
            <p style="text-align: right;">
              @if($now->diffInHours($deliveryDate) <= 1)
                {{$deliveryDate->diffForHumans()}}
              @elseif($now->diffInDays($deliveryDate) < 1)
                {{$deliveryDate->format('h:i A')}}
              @else
                {{$deliveryDate->format('D (m/d)')}}
              @endif
            </p>
              <p class="text">
                <b>Announcement: {{$announcement->announcement_title}}</b>
                <br>
                {{ mb_strimwidth($announcement->announcement_details, 0, 100, " ...")}}
              </p>
            </a>
          </li>
        @endforeach

        @if(count($bellListNotifications) == 0 && count($announcements) == 0)
        <li id="noNotif" class="item">
          <p class="text">
            {{__('messages.notifications-none')}}
          </p>
        </li>
        @endif
        <li class="read-more">
          <a href="{{url('/company/notice/announcements')}}">
            {{ __('labels.view-more') }}
          </a>
        </li>
      </div>
      </ul>
    </a>
    @if(is_null(Auth::user()->company_profile))
    <li class="function user">
      <span class="icon"></span>
      <ul class="tooltip">
      <div class="tooltip__container">
        <li class="item profile">
          <a href="{{ url('applicant/profile/account') }}">
            {{__('labels.profile')}}
          </a>
        </li>
        <li class="item logout">
          <a href="{{url('/logoutUser')}}">
            {{__('labels.logout')}}
          </a>
        </li>
      </ul>
    </li>
    @else
    <li class="function user">
      <span class="icon">       
        @if(!is_null(Auth::user()->company_profile->company_logo))
        <img src="{{ url('/storage/uploads/company/company_') . Auth::user()->company_profile->company_id . '/' . Auth::user()->company_profile->company_logo }}" alt="" srcset="">
        @else
        <img src="{{ url('/images/common/icon/header-user.png') }}"style="
            margin-left: -3%;
            margin-top: 6px;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            overflow: hidden;">
        @endif 
      </span>
      <ul class="tooltip">
        <div class="tooltip__container">
            @if(Auth::user()->hasRight('COMPANY INFORMATION'))
            <li class="item profile">
                <a href="{{ url('company/profile') }}">
                  {{ __('labels.profile') }}
                </a>
            </li>
        @endif
        <li class="item logout">
          <a href="{{url('/logoutUser')}}">
            {{__('labels.logout')}}
          </a>
        </li>
      </div>
      </ul>
    </li>
    @endif
    
  </ul>
</div>

<input type="hidden" id="detailsUrl" value="/company/notice/announcements/details/">
