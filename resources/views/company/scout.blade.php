@extends('company.layouts.app') @section('title') Scout List | @stop @push('styles')
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
<link href="{{url('css/admin/modal.css')}}"     media="screen" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />

@endpush @push('scripts')
<script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
<script type="text/javascript" src="{{url('/js/company/home.js')}}"></script>

@endpush @section('content')

<main id="scout-view" class="content-main">
    <header class="pan-area">
        <p class="text">
            {{__('labels.scout')}}
        </p>
    </header>
    @if(!isset($message))
    <div class="content-inner">
        <nav class="local-nav">
            <ul class="menu">
                <li class="item">
                    <span>{{__('labels.list')}}</span>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/search')}}">{{__('labels.search')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-user')}}">{{__('labels.favorites-user')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/favorite-company')}}">{{__('labels.favorites-company')}}</a>
                </li>
                <li class="item">
                    <a href="{{url('company/scout/done')}}">{{__('labels.scouted')}}</a>
                </li>
            </ul>
        </nav>
        <aside class="search-menu">
            <form action="{{url('/company/scout/favorite/addMany')}}" method="POST">
                {{csrf_field()}}
                <ul class="list">
                    <li class="item">
                        <div class="hide" id="divCheckboxWrapper">
                            <div class="checkbox-wrapper">
                                <label>
                                    <input type="checkbox" name="checkAll" id="checkAll" class="checkbox-input">
                                    <span for="checkAll" class="checkbox-parts">Check All</span>
                                    <button type="submit" id="allFavorite" class="hide"> {{__('labels.add-to-favorites')}} </button>
                                </label>
                            </div>
                        </div>
                    </li>
                </ul>
        </aside>
        <div class="section panel scout-index">
            <div class="table">
                @if($allWithApplicantProfile->count())
                <div class="thead">
                    <div class="tr">
                        <div class="th">
                        </div>
                        <div class="th">
                            {{__('labels.user')}}
                        </div>
                        <div class="th">
                            {{__('labels.education')}}
                        </div>
                        <div class="th">
                            {{__('labels.skills')}}
                        </div>
                        <div class="th">
                            {{__('labels.status')}}
                        </div>
                    </div>
                </div>
                <div class="tbody">
                    <!-- tr -->
                    @foreach($allWithApplicantProfile as $key => $value)
                   <div class="user__modal_container" id="show_user_modal_{{$value->applicant_profile_id}}">
                    <div class="user__modal_content">
                        <div class="user__modal_dialog">
                            <a href="#" class="userInfoClose"><img id="userInfoCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                        <h2 class="page__header">{{__('labels.account-information')}}</h2>
                         <div class="table panel">
                         <div class="tbody">                
                            <div class="tr create__fullname">
                                <div class="td"><strong>{{__('labels.full-name')}}</strong></div>
                                <div class="td">
                                    <p>{{$value->applicant_profile_firstname}} {{$value->applicant_profile_lastname}}</p>
                                </div>
                            </div>
                      
                        <div class="tr">
                            <div class="td"><strong>{{__('labels.email-address')}}</strong></div>
                            <div class="td">
                                <p>{{$value->applicant_profile_preferred_email}}</p>
                            </div>
                        </div>
                        <div class="tr">
                            <div class="td"><strong>{{__('labels.contact-number')}}</strong></div>
                            <div class="td">
                                <p>{{$value->applicant_profile_contact_no}}</p>
                            </div>
                        </div>
                        <div class="tr">
                            <div class="td"><strong>{{__('labels.birthdate')}}</strong></div>
                            <div class="td">
                                <p>{{$value->applicant_profile_birthdate}}</p>
                            </div>
                        </div>
                         <div class="tr">
                            <div class="td"><strong>{{__('labels.gender')}}</strong></div>
                            <div class="td">
                                <p>{{$value->applicant_profile_gender}}</p>
                            </div>
                        </div>
                        <div class="tr">
                            <div class="td"><strong>{{__('labels.address')}}</strong></div>
                            <div class="td">
                                @if(!$value->applicant_profile_postalcode)
                                <p></p>
                                @else
                                <p>{{$value->applicant_profile_postalcode}}, {{$value->applicant_profile_address1}}, {{$value->applicant_profile_address2}}</p>
                                @endif
                            </div>
                        </div>
                        
                        </div>
                        </div>
                         <div class="user__modal_btns">
                            <button class="spare_user confirm__btn" id="close_modal_applicant">Back</button>
                        </div>
                        </div>
                         </div>
                        </div>
                    @endforeach
                    @foreach($allWithApplicantProfile as $applicant)
                    <div class="tr">
                        <div class="td">
                            <div class="checkbox-wrapper">
                                <label>
                                    @if(empty($applicant->scout_profile($applicant->applicant_profile_id)))
                                    <input type="checkbox" name="addFavorites[]" class="checkbox-input" value="{{$applicant->applicant_profile_id}}">
                                    <span for="" class="checkbox-parts"></span> @endif
                                </label>
                            </div>
                        </div>
                        <div class="td">
                        <span class="icon"></span><a href="{{url('company/scout/resume/')}}/{{$applicant->applicant_profile_id}}"> {{$applicant->applicant_profile_firstname}} {{$applicant->applicant_profile_middlename}} {{$applicant->applicant_profile_lastname}}</a>
                            {{-- <span class="icon"></span><a onclick="return displayUserModal(this.getAttribute('data-id'))" data-id="{{$applicant->applicant_profile_id}}"> {{$applicant->applicant_profile_firstname}} {{$applicant->applicant_profile_middlename}} {{$applicant->applicant_profile_lastname}}</a> --}}
                        </div>
                        <div class="td">
                            {{(count($applicant->applicant_education) > 0 ) ? '' : __('messages.scout-no-data')}}
                            @foreach($applicant->applicant_education as $school)
                            <span>
                              {{(empty($school->applicant_education_school)) ? __('messages.scout-no-data') : $school->applicant_education_school}} 
                              <!-- <br>- {{$school->qualification->master_qualification_name}} -->
                            </span>
                            <br>
                           
                            @endforeach
                        </div>
                        <div class="td">
                            {{(count($applicant->applicant_skills) > 0 ) ? '' : __('messages.scout-no-data')}}
                            @foreach($applicant->applicant_skills as $key => $value)
                              <span>{{$value->applicant_skill_name}}</span> 
                              @if(($key % 3 == 2) && count($applicant->applicant_skills) < $key)
                                  <p><br></p>
                              @endif
                            @endforeach
                        </div>
                        <div class="td">
                            @if(empty($applicant->scout_profile($applicant->applicant_profile_id)))
                              <a href="{{url('/company/scout/favorite/add')}}/{{$applicant->applicant_profile_id}}" class="btn" name="btnAddToFavorites">
                                {{__('labels.add-to-favorites')}}
                              </a> 
                            @else
                              <a href="{{url('/company/scout/favorite-user/choosTemplate')}}/{{$applicant->applicant_profile_id}}" class="btn {{strtolower(str_replace(' ', '-' , $applicant->scout_profile($applicant->applicant_profile_id)->scout_status))}}">
                                {{$applicant->scout_profile($applicant->applicant_profile_id)->scout_status}}
                              </a> 
                            @endif
                        </div>
                    </div>
                    @endforeach
                    <!-- tr -->
                </div>
                @else
                <div class="tbody">
                    <div class="tr">
                        <div class="td">
                            {{__('messages.scout-no-users')}}
                        </div>
                    </div>
                </div>
                
                @endif
            </div>
          </div>
        </div>
        <center>{{ $allWithApplicantProfile->render() }}</center>
        </form>
    
    
    
    
    </div>
    @else
    <div id="activateModal" class="w3-modal" style="display: block !important;">
        <div class="w3-modal-content w3-animate-top w3-card-4">
            <header class="billing_modal_header"> 
                <p><br></p>
                <h4><span class="red"> {{$message}} <span class="red"></h4>

            </header>
            <div class="w3-container billing_modal_body">
            <p><input type="button" onclick="window.location='{{url('/company/job-posting')}}'" id="activate" name="activate" value="OK"><br><br></p>
            </div>
        </div>
    </div>
    @endif

</main>
@endsection