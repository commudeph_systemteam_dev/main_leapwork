@extends('company.layouts.app')

@section('title')
  {{ __('labels.account') }} - {{ __('labels.mail-templates') }} |
@stop

@push('styles')
  <!-- Remove this and create own css for email-tempaltes -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

@endpush

@push('scripts')
   <script src="{{url('/js/company/account-mail-template.js')}}"></script>
@endpush

@section('content')

<!-- Create own id and css for email-templates -->
<main id="mail-template-view" class="content-main">
  <header class="pan-area">
  <p class="text">
    {{ __('labels.account') }}
  </p>
</header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
  @endif
    <form method="GET" action="" id="frmMailTemplate">
    {{ csrf_field() }}
    <input type="hidden" id="company_id" name="company_id" value="{{ $companyId  }}" >

        <div class="content-inner">

           <nav class="local-nav">
                <ul class="menu">
                    <li class="item">
                        <a href="{{url('company/account')}}">{{ __('labels.users') }}</a>
                    </li>
                    <li class="item">
                      <span>{{ __('labels.mail-templates') }}</span>
                    </li>
                </ul>
            </nav>

            <!-- Static  -->
            <div class="section mail-template">
              <div class="table panel">
                <div class="tbody">
                  
                    @foreach($mailTemplates as $mailTemplate)
                    <div class="tr">
                         <input type="hidden" class="ctxtMailTemplateId" value="{{ $mailTemplate->company_mail_type }}">
                        <div class="td">
                          @if(!empty($mailTemplate->company_mail_template_id))
                              <a href="{{ url('/company/account/mail-templates/display/') . '/' . $mailTemplate->company_mail_template_id }}"> 
                              {{ $mailTemplate->company_mail_type }}
                              </a>
                          @else
                            {{ $mailTemplate->company_mail_type }}
                          @endif
                          <input type="hidden" class="ctxtMailType" value="{{ $mailTemplate->company_mail_type }}">
                        </div>
                        <div class="td">
                          <a href="{{ url('/company/account/mail-templates/edit/') . '/' .
                                          $mailTemplate->company_mail_type . '/' .
                                          $mailTemplate->company_mail_template_id }}"
                           id="btnEditMailTemplate" class="btn blue">
                            {{ __('labels.edit') }}
                          </a>
                        </div>
                      </div>
                    @endforeach

                </div> <!-- tbody -->
              </div> <!-- table panel -->
            </div> <!-- section mail-template -->
          
        </div><!-- content-inner -->

      </form>
  </main>

<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection