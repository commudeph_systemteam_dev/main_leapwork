@extends('company.layouts.app')

@section('title')
  {{ __('labels.account') }} - {{ __('labels.users') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script> -->
  <script type="text/javascript" src="{{url('/js/company/account.js')}}"></script>
@endpush

@section('content')

<main id="account-view" class="content-main">
    <header class="pan-area">
      <p class="text">
        {{ __('labels.account') }}
      </p>
    </header>

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p class="update__message">{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
        </div>
      </div>
    @endif

  <div class="content-inner">

       <nav class="local-nav">
            <ul class="menu">
                <li class="item">
                    <span>{{ __('labels.users') }}</span>
                </li>
                <li class="item">
                    <a href="{{url('company/account/mail-templates')}}">{{ __('labels.mail-templates') }}</a>
                </li>
            </ul>
        </nav>

        <aside class="search-menu">
            <ul class="list">
                <li class="item">
                    <a href="{{url('company/account/user/create')}}" class="btn">
                      {{ __('labels.new') }}
                    </a>
                </li>
            </ul>
        </aside>

             
        <div class="section panel">
            @foreach($companyUsers as $value)
            <div class="user__modal_container" id="show_user_modal_{{$value->company_user_id}}">
            <div class="user__modal_content">
                <div class="user__modal_dialog">
            <span><a href="#"><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt="" style="float: right; margin-right: 5px;"></a></span>
                  <h2 class="page__header">{{__('labels.account-information')}}</h2>
                <div class="table panel">
                <div class="tbody">                
                    <div class="tr create__fullname">
                        <div class="td"><strong>{{__('labels.full-name')}}</strong></div>
                        <div class="td">
                            <p>{{$value->company_user_firstname}} {{$value->company_user_lastname}}</p>
                        </div>
                    </div>
               
                    <div class="tr">
                        <div class="td"><strong>{{__('labels.email-address')}}</strong></div>
                        <div class="td">
                            <p>{{$value->email}}</p>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td"><strong>{{__('labels.contact-number')}}</strong></div>
                        <div class="td">
                            <p>{{$value->company_user_contact_no}}</p>
                        </div>
                    </div>
                    <div class="tr">
                        <div class="td"><strong>{{__('labels.birthdate')}}</strong></div>
                        <div class="td">
                            <p>{{$value->company_user_birthdate}}</p>
                        </div>
                    </div>
                    <div class="tr">
                      <div class="td"><strong>{{__('labels.gender')}}</strong></div>
                      <div class="td">
                          <p>{{$value->company_user_gender}}</p>
                      </div>
                    </div>
                    <div class="tr">
                        <div class="td"><strong>{{__('labels.address')}}</strong></div>
                        <div class="td">
                            <p>{{$value->company_user_postalcode}}, {{$value->company_user_address1}}, {{$value->company_user_address2}}</p>
                        </div>
                    </div>
                    
                </div>
                </div>
                <div class="user__modal_btns">
                        <button class="spare_user confirm__btn" id="close_modal_corporate">Back</button>
                </div>
                </div>
                </div>
                </div>
            @endforeach





          <form method="POST" id="frmCompanyUser">
              {{ csrf_field() }}
              <input type="hidden" id="selected_user_id" name="selected_user_id">
              <input type="hidden" id="status_flag" name="status_flag">

              <d//iv class="table">
                @if( $companyUsers->isNotEmpty() )
                <div class="thead">
                  <div class="tr">
                    <div class="th">
                    </div>
                    <div class="th">
                      {{ __('labels.name') }}
                    </div>
                    <div class="th">
                      {{ __('labels.designation') }}
                    </div>
                    <div class="th">
                      {{ __('labels.email') }}
                    </div>
                    <div class="th">
                    </div>
                  </div>  <!-- tr -->
                </div><!-- thead -->
                <div class="tbody">
                  @foreach($companyUsers as $companyUser)
                    <div class="tr">
                      <div class="td">
                        <span class="icon"></span>
                         <input type="hidden" id="current_company_user_user_id" name="current_company_user_user_id" value="{{ $companyUser->company_user_id }}">
                      </div>
                      <div class="td">
                        <a onclick="return displayUserModal(this.getAttribute('data-id'))" data-id="{{$companyUser->company_user_id}}">{{ $companyUser->company_user_firstname }} {{ $companyUser->company_user_lastname }}</a>
                      </div>
                      <div class="td">
                        {{ $companyUser->company_user_designation }}
                      </div>
                       <div class="td">
                        {{ $companyUser->user->email }}
                      </div>
                      <div class="td">
                        <a class="btn" href="{{url('company/account/user/edit')}}/{{$companyUser->company_user_id}}">
                                      {{ __('labels.edit') }}
                        </a>
                        <a href="#" class="{{ $companyUser['updateActionCSS'] }} btn" data-id="{{$companyUser->company_user_user_id}}">
                          {{ $companyUser['updateLabel'] }}
                        </a>
                        <a class="btn red" href="#" onclick="return confirmDelete()">
                          {{__('labels.delete')}}
                        </a>
                        <div class="confirm__modal_container">
                            <div class="confirm__modal_content">
                                <a class="modal__close btn_close_delete" id="btn_close_delete"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                                <div class="confirm__modal_dialog">
                                    Are you sure you want to delete this user?
                                    <div class="confirm__modal_btns">
                                        <button class="spare_user spare_user_delete confirm__btn">No</button>
                                        <a id="delete_user" data-href=" {{ url('/company/account/user/archiveUser') }}" data-id="{{ $companyUser->company_user_user_id }}" class="delete_user confirm__btn btnArchiveUser" >Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .confirm__modal_container -->
                      </div>
                    </div><!-- tr -->
                  @endforeach
                </div> <!-- tbody -->
              
                @else 
                <div class="tbody">
                  <div class="tr">
                    <div class="td">
                      {{ $message }}
                    </div>
                  </div>
                </div> 
                @endif
              </div> <!-- table -->

            </form> <!-- frmCompanyUser -->

        </div><!-- section panel -->
 
  </div><!-- content-inner -->
</main>



<!-- Post Scripts -->
    
<!-- Scripts -->

@endsection