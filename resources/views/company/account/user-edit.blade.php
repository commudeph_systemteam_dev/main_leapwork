@extends('company.layouts.app')

@section('title')
  {{ __('labels.account') }} - {{ __('labels.users') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/company/account.js')}}"></script>
@endpush

@section('content')


@foreach($errors->all() as $error)
{{$error}}
@endforeach
<form method="POST" action="{{url('company/account/user/saveUser')}}" id="frmCompanyUser">
<main id="account-view" class="content-main company-create-view">
    <header class="pan-area">
      <p class="text">
        {{ __('labels.account') }}
      </p>
    </header>
    
    <div class="content-inner">
    @if($currentUser->user_accounttype == config('constants.accountType.2'))
   <nav class="local-nav">
        <ul class="menu">
            <li class="item">
                <span>{{ __('labels.users') }}</span>
            </li>
            <li class="item">
                <a href="{{url('company/account/mail-templates')}}">{{ __('labels.mail-templates') }}</a>
            </li>
        </ul>
    </nav>
    @endif
    <div id="divAccountMessage"></div>
         @if (Session::has('message'))
      <!-- add css detail in here -->
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p>
           <span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
        </div>
      </div>
    @endif

    <div class="section panel">
            {{ csrf_field() }}
            <input type="hidden" id="user_id" name="user_id" value="{{$user->id}}">
            <input type="hidden" id="company_user_id" name="company_user_id" value="{{$companyUser->company_user_id}}">
            <input type="hidden" id="change_password_flag" name="change_password_flag" >

            <div class="confirm__modal_container">
              <div class="confirm__modal_content">
                  <a class="modal__close" id="btn_close"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                  <div class="confirm__modal_dialog">
                      <p><input placeholder="Enter Password" type="password" id="txt_password" name="txt_password"></p>
                      <p><input placeholder="Confirm Password" type="password" id="txt_password_confirmation" name="txt_password_confirmation"></p>
                      <span id="spPassword" class="help-block required--text">
                          <strong class="text"></strong>
                      </span>
                      <div class="confirm__modal_btns">
                          <button class="spare_user confirm__btn" id="spare_user">BACK</button>
                          <a class="save_user confirm__btn" id="btnSavePassword">SAVE</a>
                      </div>
                  </div>
              </div>
            </div> <!-- .confirm__modal_container -->
            
             <div class="table form panel">
                  <div class="tbody">

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.full-name') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="text" id="txt_company_user_fname" name="txt_company_user_firstname" value="{{$companyUser->company_user_firstname}}">
                        <input type="text" id="txt_company_user_lname" name="txt_company_user_lastname" value="{{$companyUser->company_user_lastname}}">
                         <span class="help-block required--text hide" id="firstNameErr">
                                            <strong>First Name is required</strong>
                         </span>
                         <span class="help-block required--text hide" id="lastNameErr">
                                            <strong>Last Name is required</strong>
                         </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.email-address') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                         <input type="text" id="txt_user_email" name="txt_user_email" value="{{$user->email}}"> 
                         <span id="spUseremailAjax" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                           <span id="spUseremail" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                         <span class="help-block required--text hide" id="emailErr">
                           <strong>Email is required</strong>
                        </span>
                      </div>
                    </div>

                    <div class="tr">
                       <div class="td">
                         {{ __('labels.password') }} <span class="required hidden"> {{ __('labels.required') }} </span>
                       </div>
                       <div class="td">
                         <a onclick="return changePassword(this.getAttribute('data-href'))" class="changepassword">{{__('labels.change-password')}}</a>
                         {{--  <a href="#" id="aChangePassword" class="toggleChangePassword"> Change Password </a>
                         <div id="dvChangePassword" class="hidden">
                             <input type="password" id="txt_password" name="txt_password" style="width: 20%" placeholder="Enter new password">
                             <input type="password" id="txt_password_confirmation" name="txt_password_confirmation" style="width: 20%" placeholder="Enter password again">
                             <span id="btnCancelChangePassword"> X </span>
                         </div>
                          <span class="help-block required--text hide" id="passwordErr">
                                   <strong>Password is required</strong>
                                 </span>  --}}
                       </div>
                   </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.contact') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="text" id="txt_user_contact" name="txt_user_contact" value="{{$companyUser->company_user_contact_no}}"> 
                        <span id="spContact" class="help-block required--text">
                            <strong class="text"></strong>
                        </span>
                        <span id="spTeleContact" class="help-block required--text">
                            <strong class="text"></strong>
                        </span>
                        <span class="help-block required--text hide" id="contactErr">
                            <strong>Contact number is required</strong>
                        </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.birthdate') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="text" id="txt_user_birthdate" name="txt_user_birthdate" class="datepickerPast" value="{{ $companyUser->company_user_birthdate ? date('m/d/Y', strtotime($companyUser->company_user_birthdate)) :'' }}"> 
                        <span id="spBirthdate" class="help-block required--text">
                            <strong class="text"></strong>
                        </span>
                        <span class="help-block required--text hide" id="birthdayErr">
                            <strong>Birthday is required</strong>
                        </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.gender') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        @if($companyUser->company_user_gender == "MALE")
                        <input type="radio" name="gender" value="MALE" checked> Male
                        <input type="radio" name="gender" value="FEMALE" > Female
                        @else
                        <input type="radio" name="gender" value="MALE" > Male
                        <input type="radio" name="gender" value="FEMALE" checked> Female
                        @endif
                      </div>
                    </div>

                    <div class="tr create__address">
                      <div class="td">
                        {{ __('labels.address') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div> 
                      <div class="td">
                        <input list="codes" id="txt_user_postalCode" name="txt_user_postalCode" class="short_input ib" value="{{$companyUser->company_user_postalcode}}">
                          <datalist id="codes">
                              @foreach(array_keys($arrPhilippines) as $key)
                                  <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                              @endforeach
                          </datalist>
                        <input type="text" name="txt_user_address1" value="{{$companyUser->company_user_address1}}"> 
                        <input type="text" name="txt_user_address2" value="{{$companyUser->company_user_address2}}"> 
                        <span class="help-block required--text hide" id="postalCodeErr">
                            <strong>Postal Code is required</strong>
                        </span>
                        <span class="help-block required--text hide" id="address1Err">
                            <strong>Address 1 is required</strong>
                        </span>
                        <span class="help-block required--text hide" id="address2Err">
                            <strong>Address 2 is required</strong>
                        </span>
                      </div>
                    </div>
                    @if($currentUser->user_accounttype == config('constants.accountType.2'))
                    <div class="tr">
                      <div class="td">
                        {{ __('labels.access-rights') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div> 
                      <div class="td">
                        
                            <div class="access__rights">
                                <span> {{ __('labels.job-posting') }} </span>
                                @if($userRightsJobPost->company_user_right_value == 1)
                                <div class="ar--item">
                                    <input type="radio" value="1" checked name="job-posting" id="job-posting-on">
                                    <label for="job-posting-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" name="job-posting" id="job-posting-off">
                                    <label for="job-posting-off">{{ __('labels.off') }}</label>
                                </div>
                                  @else
                                  <div class="ar--item">
                                    <input type="radio" value="1" name="job-posting" id="job-posting-on">
                                    <label for="job-posting-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" checked name="job-posting" id="job-posting-off">
                                    <label for="job-posting-off">{{ __('labels.off') }}</label>
                                </div>
                                @endif
                            </div>
                            <div class="access__rights">
                                <span> Scout </span>
                                @if($userRightsScout->company_user_right_value == 1)
                                <div class="ar--item">
                                    <input type="radio" value="1" checked name="scout" id="scout-on">
                                    <label for="scout-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" name="scout" id="scout-off">
                                    <label for="scout-off">{{ __('labels.off') }}</label>
                                </div>
                                @else
                                <div class="ar--item">
                                    <input type="radio" value="1" name="scout" id="scout-on">
                                    <label for="scout-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" checked name="scout" id="scout-off">
                                    <label for="scout-off">{{ __('labels.off') }}</label>
                                </div>
                                @endif
                            </div>
                            <div class="access__rights">
                                <span> Company Information </span>
                                @if($userRightsAccountInfo->company_user_right_value == 1)
                                <div class="ar--item">
                                    <input type="radio" value="1" checked name="company_information" id="company_information-on">
                                    <label for="company_information-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" name="company_information" id="company_information-off">
                                    <label for="company_information-off">{{ __('labels.off') }}</label>
                                </div>
                                @else
                                <div class="ar--item">
                                    <input type="radio" value="1" name="company_information" id="company_information-on">
                                    <label for="company_information-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" checked name="company_information" id="company_information-off">
                                    <label for="company_information-off">{{ __('labels.off') }}</label>
                                </div>
                                @endif
                            </div>
                      </div>
                    </div>
                    @endif
                    <div class="tr">
                      <div class="td">
                        {{ __('labels.designation') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div> 
                      <div class="td">
                        <input type="text" name="txt_user_designation" value="{{$companyUser->company_user_designation}}">
                        <span class="help-block required--text hide" id="designationErr">
                            <strong>Designation is required</strong>
                        </span>
                      </div>
                    </div>

                  </div>
                </div>
            
        </div>

        <div class="btn-wrapper cf">
          <a href="{{url('company/account')}}" class="btn">{{ __('labels.back') }}</a>
          <button type="button" id="btnCreateUser" class="btn">
              {{ __('labels.save') }}
          </button>
        </div>



    </div><!-- section panel -->
  </div><!-- content-inner -->
</main>
</form>
<!-- Post Scripts -->
    
<!-- Scripts -->

@endsection