@extends('company.layouts.app')

@section('title')
  {{ __('labels.account') }} - {{ __('labels.users') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/company/account.js')}}"></script>
@endpush

@section('content')

<form method="POST" action="{{url('company/account/user/saveUser')}}" id="frmCompanyUser">
<main id="account-create" class="content-main company-create-view">
    <header class="pan-area">
      <p class="text">
        {{ __('labels.account') }}
      </p>
    </header>
    
    <div class="content-inner">

   <nav class="local-nav">
        <ul class="menu">
            <li class="item">
                <span> {{ __('labels.users') }}</span>
            </li>
            <li class="item">
                <a href="{{url('company/account/mail-templates')}}"> {{ __('labels.mail-templates') }}</a>
            </li>
        </ul>
    </nav>
    <div id="divAccountMessage"></div>
         @if (Session::has('message'))
      <!-- add css detail in here -->
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p>
        </div>
      </div>
    @endif
    <div class="section panel">
            {{ csrf_field() }}
            <input type="hidden" id="user_id" name="user_id" value="{{ $userId }}">
            <input type="hidden" id="user_id_edit" name="user_id">
            
          
             <div class="table form panel">
                  <div class="tbody">

                    <div class="tr">
                      <div class="td">
                         {{ __('labels.full-name') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="text" id="txt_company_user_name" name="txt_company_user_firstname" placeholder="First Name" value="{{ old('txt_company_user_firstname') }}">
                        <input type="text" id="txt_company_user_name" name="txt_company_user_lastname" placeholder="Last Name" value="{{ old('txt_company_user_lastname') }}">
                         <span class="help-block required--text hide" id="firstNameErr">
                                            <strong>First Name is required</strong>
                         </span>
                         <span class="help-block required--text hide" id="lastNameErr">
                                            <strong>Last Name is required</strong>
                         </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.email-address') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                         <input type="text" id="txt_user_email" name="txt_user_email" value="{{ old('txt_user_email') }}"> 
                         @if ($errors->has('txt_user_email'))
                            <div class="alert alert-danger">
                                <ul>
                                    {{-- @if ($errors->first('txt_user_email')) --}}
                                        <li>The email address is already taken.</li>
                                    {{-- @endif --}}
                                </ul>
                            </div>
                        @endif
                         <span id="spUseremailAjax" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                           <span id="spUseremail" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                         <span class="help-block required--text hide" id="emailErr">
                           <strong>Email is required</strong>
                        </span>
                      </div>
                    </div>

                     <div class="tr">
                      <div class="td">
                        {{ __('labels.password') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                          <p><input type="password" id="txt_user_password" name="txt_password" ></p>
                          <p><input type="password" id="txt_user_password" name="txt_password_confirmation" placeholder="Confirm Password"></p>
                          @if($errors->has('txt_password'))
                          <div class="alert alert-danger">
                              <ul>
                                  {{-- @if ($errors->first('txt_user_email')) --}}
                                      <li>Passwords do not match.</li>
                                  {{-- @endif --}}
                              </ul>
                          </div>
                          @endif
                           <span id="spUserpassword" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                         <span class="help-block required--text hide" id="passwordErr">
                           <strong>Password is required</strong>
                        </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.contact') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="text" id="txt_user_contact" name="txt_user_contact" value="{{ old('txt_user_contact') }}"> 
                        <span id="spContact" class="help-block required--text">
                            <strong class="text"></strong>
                        </span>
                        <span id="spTeleContact" class="help-block required--text">
                            <strong class="text"></strong>
                        </span>
                        <span class="help-block required--text hide" id="contactErr">
                            <strong>Contact number is required</strong>
                        </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.birthdate') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="text" id="txt_user_birthdate" name="txt_user_birthdate" class="datepickerPast" value="{{ old('txt_user_birthdate') }}">
                         <span id="spBirthdate" class="help-block required--text">
                            <strong class="text"></strong>
                        </span>
                        <span class="help-block required--text hide" id="birthdayErr">
                            <strong>Birthday is required</strong>
                        </span>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.gender') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div>
                      <div class="td">
                        <input type="radio" name="gender" value="MALE" checked> {{ __('labels.male') }} <input type="radio" name="gender" value="FEMALE"> {{ __('labels.female') }}
                      </div>
                    </div>

                    <div class="tr create__address">
                      <div class="td">
                        {{ __('labels.address') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div> 
                      <div class="td">
                        <input list="codes" placeholder="Postal Code" id="txt_user_postalCode" name="txt_user_postalCode" class="short_input ib" value="{{ old('txt_user_postalCode') }}">
                          <datalist id="codes">
                              @foreach(array_keys($arrPhilippines) as $key)
                                  <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                              @endforeach
                          </datalist>
                        <input type="text" name="txt_user_address1" placeholder="Address 1" value="{{ old('txt_user_address1') }}"> 
                        <input type="text" name="txt_user_address2" placeholder="Address 2" value="{{ old('txt_user_address2') }}"> 
                        <span class="help-block required--text hide" id="postalCodeErr">
                            <strong>Postal Code is required</strong>
                        </span>
                        <span class="help-block required--text hide" id="address1Err">
                            <strong>Address 1 is required</strong>
                        </span>
                        <span class="help-block required--text hide" id="address2Err">
                            <strong>Address 2 is required</strong>
                        </span>
                      </div>
                    </div>
                    <div class="tr">
                      <div class="td">
                        {{ __('labels.access-rights') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div> 
                      <div class="td">
                        
                            <div class="access__rights">
                                <span> {{ __('labels.job-posting') }} </span>
                                <div class="ar--item">
                                    <input type="radio" value="1" checked name="job-posting" id="job-posting-on">
                                    <label for="job-posting-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" name="job-posting" id="job-posting-off">
                                    <label for="job-posting-off">{{ __('labels.off') }}</label>
                                </div>
                            </div>
                            <div class="access__rights">
                                <span> {{ __('labels.scout') }} </span>
                                <div class="ar--item">
                                    <input type="radio" value="1" checked name="scout" id="scout-on">
                                    <label for="scout-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" name="scout" id="scout-off">
                                    <label for="scout-off">{{ __('labels.off') }}</label>
                                </div>
                            </div>
                            <div class="access__rights">
                                <span> {{ __('labels.company-information') }} </span>
                                <div class="ar--item">
                                    <input type="radio" value="1" checked name="company_information" id="company_information-on">
                                    <label for="company_information-on">{{ __('labels.on') }}</label>
                                </div>
                                <div class="ar--item">
                                    <input type="radio" value="0" name="company_information" id="company_information-off">
                                    <label for="company_information-off">{{ __('labels.off') }}</label>
                                </div>
                            </div>
                      </div>
                    </div>

                    <div class="tr">
                      <div class="td">
                        {{ __('labels.designation') }} <span class="required"> {{ __('labels.required') }} </span>
                      </div> 
                      <div class="td">
                        <input type="text" name="txt_user_designation" value="{{ old('txt_user_designation') }}">
                        <span class="help-block required--text hide" id="designationErr">
                            <strong>Designation is required</strong>
                        </span>
                      </div>
                    </div>

                  </div>
                </div>
            
        </div>

        <div class="btn-wrapper cf">
          <a href="{{url('company/account')}}" class="btn alt-btn">{{ __('labels.back') }}</a>
          <button type="button" id="btnCreateUser" class="btn">
              {{ __('labels.save') }}
          </button>
        </div>
      

    </div><!-- section panel -->
  </div><!-- content-inner -->
</main>
</form>
<!-- Post Scripts -->
    
<!-- Scripts -->

@endsection