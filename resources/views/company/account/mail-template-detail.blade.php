@extends('company.layouts.app')

@section('title')
   {{ __('labels.account') }} - {{ __('labels.mail-templates') }} |
@stop

@push('styles')
  <!-- Remove this and create own css for email-tempaltes -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/account.css')}}" />

@endpush

@push('scripts')
  <script src="{{url('/js/company/account-mail-template.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/tinyMCE/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/tinyMCE/getcompanydata.js')}}"></script>
  <script type="text/javascript" src="{{url('/plugin/tinymce/tinymce.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/plugin/tinymce/init-tinymce.js')}}"></script>
@endpush

@section('content')

<!-- Create own id and css for email-templates -->
<main id="mail-template-detail" class="content-main">
  <header class="pan-area">
  <p class="text">
     {{ __('labels.mail-templates') }} - {{ $companyMailTemplate->company_mail_type }}
  </p>
</header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
  @endif

  <form method="POST" action="" id="frmMailTemplate">
    {{ csrf_field() }}
    <input type="hidden" id="company_mail_template_id" name="company_mail_template_id" value="{{ $companyMailTemplate->company_mail_template_id }}">

    <div class="content-inner">
      <div class="section">
        <div class="table form panel">
          <div class="tbody">
            
            <div class="tr">
              <div class="td">
                {{ __('labels.subject') }}
              </div>
              <div class="td">
                {{ $companyMailTemplate->company_mail_subject }}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                {{ __('labels.body') }}
              </div>
              <div class="td" style="">
                 
                 <textarea id="content">{{ $companyMailTemplate->company_mail_body }}</textarea>

              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <a href="{{ url('company/account/mail-templates') }}"  class="btn">
          {{ __('labels.back') }}
        </a>
        <a href="{{ url('/company/account/mail-templates/edit/') . '/' .
                        $companyMailTemplate->company_mail_type . '/' .
                        $companyMailTemplate->company_mail_template_id }}"
         id="btnEditMailTemplate" class="btn blue">
          {{ __('labels.edit') }}
        </a>
      </div>

   
     
    </div><!-- content-inner -->
  </main>

<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection