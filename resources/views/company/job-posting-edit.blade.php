@include('company.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" />
<link rel="stylesheet" href="{{url('css/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{url('css/app.css')}}">

<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/job-posting.css')}}" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="{{url('js/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
</script>
<script type="text/javascript" src=" {{url('/js/company/job-posting.js')}}"></script>

<!-- title -->
<title> {{ __('labels.job-post') }}  | LEAP Work</title>
</head>

<body>
    <header id="global-header">@include('company.share.header')</header>
      <div class="content-wrapper">
        <aside id="sidebar">@include('company.share.sidebar')</aside>
        <form method="POST" action="{{url('company/job-posting/save')}}/{{$jobPosting->job_post_id}}" enctype="multipart/form-data" id="companyForm">
            {{ csrf_field() }}
            <main id="job-posting-view" class="content-main">
            <div id="formJobPost">
                <header class="pan-area">
                    <p class="text">
                        {{ __('labels.job-post') }} - {{ $jobPosting->job_post_title }}
                    </p>
                </header>
                <div class="content-inner">
                    <aside class="search-menu">
                        <ul class="list">
                            <li class="item">
                                <a  onclick="document.getElementById('how-to').style.display='block'" class="btn green">
                                  {{ __('labels.how-to-edit-a-job-post') }}
                                </a>
                            </li>
                        </ul>
                    </aside>
                 
                    <div class="section posting-edit">
                        <div class="table form panel">
                            <div class="tbody">
                                <div class="tr">
                                    <div class="td">
                                        Job Title
                                    </div>
                                    <div class="td">
                                        <h2 class="title">{{ $jobPosting->job_post_title }}</h2>
                                        <input type="hidden" id="job_post_title" value="{{ $jobPosting->job_post_title }}">
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Position
                                    </div>
                                    <div class="td">
                                        <input list="job_positions" id="job_post_job_position_id" name="job_post_job_position_id" value="{{$jobPos->master_job_position_name}}">
                                        <datalist id="job_positions">
                                            @foreach($jobPositions as $jobPosition)
                                            <option value="{{$jobPosition->master_job_position_name}}"></option>
                                            @endforeach
                                        </datalist>
                                        <span class="help-block required--text hide" id="positionErr">
                                          <strong>Job Position is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.key-visual-pc') }}
                                        <br/>
                                        <p class="help-block required--text red" id="image-size-label">(Please upload 1100 x 287)</p>
                                    </div>
                                    <div class="td">
                                        <input type="file" name="file_job_post_key_pc" id="file_job_post_key_pc">
                                        @if($jobPosting->job_post_key_pc)
                                        <div class="image__container">
                                            <img id="img_header_container" src="{{url('/storage/uploads/job-posts/' . $jobPosting->job_post_key_pc)}}" alt=""/>
                                        </div>
                                        @endif
                                         <span class="help-block required--text hide red" id="imageErr">
                                            <strong>Image size should be 1100 X 287</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.key-visual-smartphone') }}
                                    </div>
                                    <div class="td">
                                        <input type="file" name="file_job_post_key_sp" id="file_job_post_key_sp">
                                        <div class="image__container">
                                            <img src="{{url('/storage/uploads/job-posts/' .
                                        $jobPosting->job_post_key_sp)}}" id="img_header_sp_container" alt=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.business-contents') }}
                                    </div>
                                    <div class="td">
                                        <textarea id="job_post_business_contents" name="job_post_business_contents" maxlength="1000">{{$jobPosting->job_post_business_contents}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> {{ __('labels.job-description') }}
                                    </div>
                                    <div class="td">
                                        <textarea id="job_post_description" name="job_post_description" maxlength="1000">{{$jobPosting->job_post_description}}</textarea>
                                        <span class="help-block required--text hide" id="descriptionErr">
                                          <strong>Job Description is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Skills
                                    </div>
                                    <div class="td">
                                        <div class="dvTag bootstrap-label-adjust">
                                        <!-- styles to override is below. Note that the global input mas be overriden since a js auto generated one is being used  -->
                                            <input type="text" maxlength="1000" value="{{$jobPosting->job_post_skills}}" id="tag_job_post_skills" name="tag_job_post_skills" data-role="tagsinput" 
                                            style=" width: auto;
                                                    height: inherit;
                                                    padding: inherit;
                                                    border: none;" />
                                        </div>
                                        <span class="help-block required--text hide" id="skillErr">
                                          <strong>Job Skill is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.image') }}
                                    </div>
                                    <div class="td file__uploads">
                                        <!-- 2017-12-21 Louie: Not workng or something -->
                                        <?php /* -- {{Form::select('numOfImages', config('constants.numberOfImagesToUpload'), array('selected' => $numOfImages), array('id' => 'numImages'), 
                                                        [1=>['id' => 'oneImage'], 2=>['id' => 'twoImages'], 3=>['id' => 'threeImages']]) }} --> */ ?>

                                        {{Form::select('numOfImages', config('constants.'.Session::get('locale').'_numberOfImagesToUpload'), $numOfImages) }}
                                        <!-- end -->

                                        <input type="file" name="file_job_post_image1" id="file_job_post_image1">
                                         <span class="help-block required--text hide red" id="jobPostImage1Err">
                                            <strong>Image size should be 539 X 290</strong>
                                        </span>

                                        {{--  <input type="file" name="file_job_post_image3" class="hide">  --}}

                                        <div class="image__container" name="image1">
                                            <img id="img_job1_container" src="{{url('/storage/uploads/job-posts/' .
                                                $jobPosting->job_post_image1)}}" alt=""/>
                                        </div>

                                        <input type="file" name="file_job_post_image2" id="file_job_post_image2" class="hide">
                                        <span class="help-block required--text hide red" id="jobPostImage2Err">
                                           <strong>Image size should be 539 X 290</strong>
                                       </span>
                                        <div class="image__container" name="image2">
                                            <img id="img_job2_container" src="{{url('/storage/uploads/job-posts/' .
                                                $jobPosting->job_post_image2)}}" alt=""/>
                                        </div>
                                        </div>
                                    </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Job Industry
                                    </div>
                                    <div class="td">
                                        <select id="job_post_industry_id" name="job_post_industry_id" value="{{ $jobInd->master_job_industry_name }}"  onchange="showfield(this.options[this.selectedIndex].value)">
                                            <option selected="selected">{{ $jobInd->master_job_industry_name }}</option>
                                            <option disabled>────────────────────</option>
                                            @foreach($jobIndustries as $jobIndustry)
                                                @if($jobPosting->job_post_industry_id != count($jobIndustries))
                                                    @if($jobPosting->job_post_industry_id == $jobIndustry->master_job_industry_id)
                                                        <option value="{{$jobIndustry->master_job_industry_name}}" selected="selected">{{$jobIndustry->master_job_industry_name}}</option>
                                                    @else
                                                        <option value="{{$jobIndustry->master_job_industry_name}}">{{$jobIndustry->master_job_industry_name}}</option>
                                                    @endif
                                                @elseif($jobPosting->job_post_industry_id == $jobIndustry->master_job_industry_id)
                                                    <option value="{{$jobIndustry->master_job_industry_name}}" selected="selected">{{$jobIndustry->master_job_industry_name}}</option>
                                                @else
                                                    <option value="{{$jobIndustry->master_job_industry_name}}" selected="selected">{{$jobIndustry->master_job_industry_name}}</option>
                                                @endif

                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide red" id="industryErr">
                                            <strong>Job Industry is required</strong>
                                        </span>
                                        <div id="div1">
                                            @if($jobPosting->job_post_industry_id == count($jobIndustries))
                                                <br>Please specify: <input type="text" name="job_post_others" value="{{$jobPosting->job_post_others}}"/>
                                            @endif
                                        </div>
                                        <span class="help-block required--text hide red" id="industryOtherErr">
                                            <strong>Please specify Other Industry.</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.job-overview') }}
                                    </div>
                                    <div class="td">
                                        <textarea id="job_post_overview" name="job_post_overview" maxlength="1000">{{$jobPosting->job_post_overview}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.qualifications-requirements') }}
                                    </div>
                                    <div class="td">
                                        <textarea id="job_post_qualifications" name="job_post_qualifications" maxlength="1000">{{$jobPosting->job_post_qualifications}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span>  {{ __('labels.employment-classification') }}
                                    </div>
                                    <div class="td">
                                        <select id="job_post_classification_id" name="job_post_classification_id" value="{{$jobClass->master_job_classification_name}}">
                                            @foreach($jobClassifications as $jobClassification)
                                            <option value="{{$jobClassification->master_job_classification_id}}">{{$jobClassification->master_job_classification_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide" id="classificationErr">
                                            <strong>Employment Classification is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Salary Range
                                    </div>
                                    <div class="td">
                                        <div class="salary__container">
                                            <input type="number" min=1 id="job_post_salary_min" name="job_post_salary_min" lang="en-150" value="{{$jobPosting->job_post_salary_min}}">
                                            ~
                                            <input type="number" min=1 id="job_post_salary_max" name="job_post_salary_max" lang="en-150" value="{{$jobPosting->job_post_salary_max}}">
                                        </div>
                                        <span class="help-block required--text hide red" id="salaryRangeErr">
                                            <strong>Salary Minimum is required</strong>
                                        </span> 
                                        <span class="help-block required--text hide red" id="salaryMaxErr2">
                                            <strong >Salary Max should be greater than or equal Salary Minimum</strong>
                                        </span>

                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.supplement') }}
                                    </div>
                                    <div class="td">
                                        <textarea name="job_post_supplement" maxlength="1000">{{$jobPosting->job_post_suppliment}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> {{ __('labels.number-of-employees-needed') }}
                                    </div>
                                    <div class="td">
                                        <input type="number" min="1" id="job_post_no_of_positions" name="job_post_no_of_positions" value="{{$jobPosting->job_post_no_of_positions}}">
                                        <span class="help-block required--text hide" id="numEmployeesErr">
                                          <strong>Number of Employees is required</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        {{ __('labels.application-process') }}
                                    </div>
                                    <div class="td">
                                        <textarea id="job_post_process" name="job_post_process" maxlength="1000">{{$jobPosting->job_post_process}}</textarea>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td">
                                        <span class="required"> {{ __('labels.required') }} </span> Work Location
                                    </div>
                                    <div class="td">
                                        <select name="job_post_location_id" id="job_post_location_id" value="{{$jobLoc->master_job_location_id}}">
                                            @foreach($jobLocations as $jobLocation)
                                                @if($jobLocation->master_job_location_id == $jobLoc->master_job_location_id)
                                                <option value="{{$jobLoc->master_job_location_id}}" selected>{{$jobLoc->master_job_location_name}}</option>
                                                @else
                                                <option value="{{$jobLocation->master_job_location_id}}">{{$jobLocation->master_job_location_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span class="help-block required--text hide" id="workLocation">
                                          <strong>Work location is required</strong>
                                        </span>
                                    </div>
                                </div>
                         
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.work-hours') }}
                                </div>
                                <div class="td">
                                    <select name="job_post_work_timestart" id="job_post_work_timestart">
                                        <option selected="{{$formattedTimeStart}}">{{$formattedTimeStart}}</option>
                                        @foreach($workingHoursStart as $start)
                                        <option value="{{$start}}">{{$start}}</option>
                                        @endforeach
                                    </select> ~
                                    <select name="job_post_work_timeend" id="job_post_work_timeend">
                                        <option selected="{{$formattedTimeEnd}}">{{$formattedTimeEnd}}</option>
                                        @foreach($workingHoursEnd as $end)
                                        <option value="{{$end}}">{{$end}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.benefits') }}
                                </div>
                                <div class="td">
                                    <textarea type="text" id="job_post_benefits" name="job_post_benefits" maxlength="1000">{{$jobPosting->job_post_benefits}}</textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.restday-holiday-vacation') }}
                                </div>
                                <div class="td">
                                    <textarea type="text" id="job_post_holiday" name="job_post_holiday" maxlength="1000">{{$jobPosting->job_post_holiday}}</textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.others') }}
                                </div>
                                <div class="td">
                                    <textarea id="job_post_otherinfo" name="job_post_otherinfo" maxlength="1000">{{$jobPosting->job_post_otherinfo}}</textarea>
                                </div>
                            </div>
                            <div class="tr">
                                <div class="td">
                                    {{ __('labels.post-visibility') }}
                                </div>
                                <div class="td">
                                    <select name="job_post_visibility" value="{{$jobPosting->job_post_visibility}}">
                                    @if($jobPosting->job_post_visibility == "PRIVATE")
                                        <option value="PUBLIC">{{ __('labels.public') }}</option>
                                        <option value="PRIVATE" selected>{{ __('labels.private') }}</option>
                                    @else
                                        <option value="PUBLIC">{{ __('labels.public') }}</option>
                                        <option value="PRIVATE">{{ __('labels.private') }}</option>
                                    @endif
                                    </select>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-wrapper">
                    <!-- <button class="btn" id="btnJobPost">Save</button> -->
                    <a class="btn green" id="job-create-preview">Preview</a>
                </div>
            </div>
        </div>
            <div id="previewPost" class="preview_job" >
                <input id="header-image-default" type="hidden" value="{{ asset('images/top_noimage_1200x400.jpg') }}">
                <input id="job-image-default" type="hidden" value="{{ asset('images/top_blankimage.jpg') }}">
    
                <div class="content jobdetails">
                        <div class="job__details">                    
                            <div class="feature__image clearfix">
                                <div>
                                    <img id="header_image" src="{{ is_null($jobPosting->job_post_key_pc) 
                                        ? asset('images/top_noimage_1200x400.jpg')
                                        : url('/storage/uploads/job-posts/' . $jobPosting->job_post_key_pc) }}" alt="">
                                </div>
                            </div>
            
                            <div class="jobdetails__content">
                                <h1 id="job_title"></h1>
                                <div class="jobdetails__information">
                                    <h3>Business Contents</h3>
                                    <p id="job_business"></p>
                                    <h3>Job Description</h3>
                                    <p id="job_description"></p>
                                        <div class="image__container clearfix" style="max-width: 100%;">
                                        <div>
                                            <img id="job_image1" src="{{ is_null($jobPosting->job_post_image1) 
                                                ? asset('images/top_blankimage.jpg')
                                                : url('/storage/uploads/job-posts/' . $jobPosting->job_post_image1) }}" alt="">
                                        </div>
                                        <div>
                                            <img id="job_image2" src="{{ is_null($jobPosting->job_post_image2)
                                            ? asset('images/top_blankimage.jpg')
                                            : url('/storage/uploads/job-posts/' . $jobPosting->job_post_image2) }}" alt="">
                                        </div>
                                        </div>
            
                                    <h3>Work Description</h3>
                                    <table class="jobdetails__table">
                                        <tr>
                                            <td>Job Position</td>
                                            <td><span id="job_position"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Overview</td>
                                            <td><span id="job_overview"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Qualifications Requirements</td>
                                            <td><span id="job_requirements"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Employment Category</td>
                                            <td><span id="job_classification"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Salary</td>
                                            <td>
                                                <span id="job_min" name="job_min"></span> - <span id="job_max" name="job_max"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vacancy</td>
                                            <td><span id="job_vacancy"></span>
                                        </tr>
                                        <tr>
                                            <td>Selection Process</td>
                                            <td><span id="job_process"></span>
                                        </tr>
                                        <tr>
                                            <td>Work Location</td>
            g
                                            <td>{{ is_null($company->company_address1) ? '' : $company->company_address1}}
                                                    <br>{{ is_null($company->company_address2) ? '' : $company->company_address2 }}
                                                    <span id="job_location"></span>, {{ is_null($company->master_country_name) ? '' : $company->master_country_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Working Hours</td>
                                            <td><span id = "job_start" name="job_start" readonly></span> - <span id = "job_end" name="job_end"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Benefits</td>
                                            <td><span id="job_benefits"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Holiday / Vacation</td>
                                            <td><span id="job_holiday"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Characteristics</td>
                                            <td><span id="job_characteristics"></span></td>
                                        </tr>
                                    </table>
                                </div><!-- .jobdetails__information -->
                            </div><!-- .jobdetails__content -->
                              <div class="btn-wrapper">
                                    <a id="job-create-back" class="btn alt-btn">
                                              Back
                                    </a>
                                    <button type="button" id="btnJobPost">
                                        {{ $viewSettings['btnSaveLabel'] }}
                                    </button>
                                </div>
            
                        </div><!-- .job__details -->
                        
            </div>
        </div>
        </main>
    </form>
  </div>

  <div id="how-to" class="modal">
    <form class="modal-content animate">
      <div class="imgcontainer">

      </div>
  
      <div class="container">
        <span><a href="#" onclick="document.getElementById('how-to').style.display='none'"><img class="howToCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></a></span>
            <h2>{{ __('labels.how-to-edit-a-job-post') }}</h2>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum fringilla magna, in semper sapien consequat nec. Cras varius sollicitudin lobortis. Aliquam elementum purus nec fermentum accumsan. Nam vel sem ut tortor porta lacinia at eget arcu. Suspendisse ornare accumsan neque at placerat. Quisque rhoncus lacus eu erat maximus tempus. Fusce feugiat neque eu ultrices luctus. Phasellus a purus tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum maximus, ex at molestie luctus, mauris ex ornare nisl, sed mollis nisl nisi tincidunt ipsum. Pellentesque gravida orci sed nibh interdum, quis egestas est tristique.
            <br><br>
            Proin dapibus ante felis, quis congue lacus auctor sodales. Duis ultricies quam a dapibus vehicula. Nulla auctor porta mauris, nec tempus eros maximus sit amet. Ut volutpat tempor felis, vel volutpat nisi accumsan at. Suspendisse risus enim, suscipit eget feugiat ac, ultricies ullamcorper eros. Aliquam semper lorem mauris, vel aliquam est efficitur quis. Sed faucibus iaculis tristique. Pellentesque eget purus vel urna ornare egestas. Sed ornare purus in scelerisque semper.
            <br><br>
            Curabitur venenatis augue vitae porttitor sodales. Nam non pharetra dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Quisque maximus ligula ac nisi fermentum, sed consequat nibh mattis. Vivamus volutpat scelerisque finibus. Donec quis elit non ligula iaculis eleifend.
            <br><br>
            Donec gravida libero at dolor rhoncus vehicula. Fusce sem nisl, faucibus sed massa vel, ultricies porta est. Ut quis lacinia ante. Proin commodo aliquam pretium. Pellentesque nec scelerisque ante. Nunc at tortor laoreet, pellentesque ex eget, suscipit elit. Nulla feugiat diam pharetra mi lobortis, eget vulputate erat congue. Cras ipsum neque, facilisis in volutpat vel, cursus et purus. Nulla varius ac sapien eget convallis.
            <br><br>
            Aenean vel ornare tortor, at sollicitudin nisl. Sed elementum turpis velit, et efficitur dui lacinia ullamcorper. Praesent scelerisque, orci sed luctus sagittis, urna orci efficitur sapien, vel convallis massa est eu mauris. Nam ipsum ante, cursus sit amet elit vel, ultrices ultrices nisl. Duis congue varius justo quis rutrum. Cras ut porta dolor. Morbi quis sollicitudin est. Curabitur quis ex at justo pharetra semper.
            </p>
      </div>
  
      <div class="btn-wrapper">
        <button type="button" onclick="document.getElementById('how-to').style.display='none'" class="btn green">OK</button>
      </div>
    </form>
</div>

</body>
</html>