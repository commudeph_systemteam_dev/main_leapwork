@extends('company.layouts.app')

@section('title')
  {{ __('labels.notifications') }} |
@stop


@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/notice.css')}}" />
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script> -->
@endpush

@section('content')

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      {{ __('labels.notifications') }}
    </p>
  </header>
  <div class="content-inner">
    <nav class="local-nav">
      <ul class="menu">
        <li class="item">
          <span>{{ __('labels.messages') }}</span>
        </li>
        <li class="item">
          <a href="{{url('company/notice/announcements')}}">{{ __('labels.announcements') }}</a>
        </li>
      </ul>
    </nav>
    <div class="section">
      <div class="table panel">
        <div class="tbody">


          @if($companyUserType == "CORPORATE ADMIN")
          @if(count(json_decode($adminCompanyMessage)) > 0)
          @foreach(json_decode($adminCompanyMessage) as $message)
               
            <div class="tr">
              <div class="td">
                {{ date('Y.m.d', strtotime($message->admin_company_thread_datecreated)) }}
              </div>
              <div class="td">
                <span class="cate notice">
                    {{ __('labels.admin') }}
                </span>
              </div>
              <div class="td">
                <b>{{$message->admin_company_thread_title}}</b><br>
                  
              </div>
              <div class="td">
                  <a href="{{url('/company/notice/details')}}/{{$message->admin_company_thread_company_id}}" class="btn"> 
                      {{ __('labels.view') }} 
                  </a>
              </div>
            </div>
          @endforeach 
          @else 
        </div>
                </div>
                <div id="noAnnouncementFound">
                  <p>No Messages Found</p>
                </div>
                @endif
          @else
            <div class="tr">
              <div class="td">{{ __('labels.for-admin-only') }}</div>
            </div>
          @endif

         
          </div><!--tbody -->
      </div>
    </div>
  </div>
  
</main>
@endsection