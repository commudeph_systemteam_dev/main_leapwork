@extends('company.layouts.app')

@section('title')
  Announcement |
@stop


@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/notice.css')}}" />
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script> -->
@endpush

@section('content')

    <main class="content-main right">
      <ul class="comment_list">
        <li class="list_title_wrap cf">
            <h1 class="list_title">
                <a href="{{url('/company/notice/announcements')}}" class="link">
                    {{ __('labels.news-announcements') }}
                </a> 
                 
                > {{$announcement->announcement_title}}
            </h1>
        </li>
        <li class="list_info">
                    
                </li><!-- list_info -->
        </ul><!-- comment_list -->

        <div class="announcement_container">
                <div class="link_post">
                    <div class="inner clearfix">
                        <div class="left_box">
                            <time class="time" id="announcement_time">{{date('Y.m.d', strtotime($announcement->announcement_datecreated))}}</time>
                            <p class="label label_long green ta_c">{{ __('labels.admin') }}</p>
                        </div>
                        <div class="right_box">
                            <h2 class="sub_title" id="announcement_subtitle">{{$announcement->announcement_title}}</h2>
                            <hr/>
                            <p class="text" id="announcement_text">
                            {{$announcement->announcement_details}}
                            </p>
                            <div class="btn-wrapper">
                                <a href="{{ url('company/notice/announcements') }}" class="btn alt-btn">
                                    {{__('labels.back')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
                    

    </main><!-- content-main -->

@endsection