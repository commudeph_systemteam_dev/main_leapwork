@extends('company.layouts.app')

@section('title')
  {{ __('labels.notifications') }} |
@stop


@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/notice.css')}}" />
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script> -->
@endpush

@section('content')

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      {{ __('labels.notifications') }}
    </p>
  </header>
  <div class="content-inner">
    <nav class="local-nav">
      <ul class="menu">
        <li class="item">
          <a href="{{url('/company/notice')}}">{{ __('labels.messages') }}</a>
        </li>
        <li class="item">
          <span>Announcements</span>
        </li>
      </ul>
    </nav>
    <div class="section">
      <div class="table panel">
          <div class="thead">
              <div class="tr">
                  <div class="th title">
                      {{__('labels.title')}}
                  </div>
                  <div class="th date">
                      {{__('labels.posted-at')}}
                  </div>
                  <div class="th details">
                      {{__('labels.announcements-details')}}
                  </div>
                  {{-- <div class="th">
                     {{__('labels.created-by')}}
                  </div> --}}
                  <div class="th"></div>
              </div>
          </div>
        <div class="tbody">
          @if(count($data["announcements"]) <= 0)
              </div>
              </div>
              <div id="noAnnouncementFound" style="margin-top: 2px;">
                <p>No Announcements Found</p>
              </div>
          @else
          @foreach($data["announcements"] as $announcement)
              <div class="tr">
                  <div class="td">
                      <a href="{{ url( 'company/notice/announcements/details/'. $announcement->announcement_id) }} }}">
                        {{ $announcement->announcement_title }}
                      </a>
                  </div>
                  <div class="td">
                    <label title="{{ \Carbon\Carbon::parse($announcement->announcement_deliverydate)->format('m-d-Y g:i A') }}">{{ \Carbon\Carbon::parse($announcement->announcement_deliverydate)->diffForHumans()}}</label>
                  </div>
                  <div class="td">
                      <p style="text-align: justify; word-wrap: break-word;width: 450px;">{{ mb_strimwidth($announcement->announcement_details, 0, 150, "...")}}</p>
                  </div>
                  <div class="td">
                    <a href="{{ url( 'company/notice/announcements/details/'. $announcement->announcement_id) }}" class="btn"> View </a>
                  </div>
            </div>
          @endforeach 
          @endif
          </div><!--tbody -->
        </div>
        <center>{{$data["announcements"]->render()}}</center>
    </div>
  </div>
  
</main>
@endsection