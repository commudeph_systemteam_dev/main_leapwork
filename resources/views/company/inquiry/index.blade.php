@extends('company.layouts.app')

@section('title')
  {{ __('labels.inquiry') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/inquiry.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/applicant.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/applicant/home.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/company/inquiry.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/company-inquiry.js')}}"></script>
@endpush

@section('content')

<main id="contact-view" class="content-main">

<header class="pan-area">
  <p class="text">
    {{ __('labels.inquiries') }}
  </p>
</header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
      </div>
    </div>
  @endif

  <div class="content-inner">      

     <form method="POST" id="frmCompanyInquiry" >
        {{ csrf_field() }}
        <input type="hidden" id="company_inquiry_thread_id" name="company_inquiry_thread_id" >
        <input type="hidden" id="company_inquiry_conversation_message" name="company_inquiry_conversation_message" >
        <input type="hidden" id="adminIcon" value="{{$adminImgSrc}}" >
        <input type="hidden" id="userIcon" value="{{$companyImgSrc}}" >
        <input type="hidden" id="userName" value="{{$userProfileName}}" >

        <ul class="comment_list">
           <!--  <li class="list_title_wrap cf">
                
            </li> -->
            <li class="list_info">
                <div class="detail_contents detail_contents_chat">
                  
                    <div class="tab_area clearfix">
                        <p class="tab tab_recieve_mail active left">{{ __('labels.conversations') }}</p>
                        <p class="tab tab_compose_mail left" style="display: none;">{{ __('labels.new') }}</p>
                        <button type="button" id="btnNewInquiry" class="btn--right" style="line-height: 28px;
                                                                                            padding-left: 10px;
                                                                                            padding-right: 10px;
                                                                                            min-width: 100px;
                                                                                            height: 28px;
                                                                                            float: right;">
                            {{ __('labels.new') }}
                        </button>
                    </div>

                    <div id="dvInbox" class="dvTab">
                        @if(count($inboxMessages) < 1 )
                            <ul class="chat_list recieve_mail_area">
                                <li  class="cloesed_chat" style="display: list-item;background: white;">
                                   
                                    <div class="opened_chat_inner opened_chat_send">
                                         {{ Lang::get('messages.no-messages') }}
                                    </div>

                                </li>
                            </ul>                                
                        @endif


                        <ul class="chat_list recieve_mail_area">
                            @foreach($inboxMessages as $k => $inboxMessage)
                                <li data-t_id="{{ $inboxMessage->company_inquiry_thread_id }}" id="tb_{{ $inboxMessage->company_inquiry_thread_id }}" class="cloesed_chat">
                                    <p class="clearfix">
                                        <span class="text left"><b>{{$inboxMessage->company_inquiry_thread_contact_person}} </b></span>
                                        <time class="left"> 
                                            {{ date('Y.m.d', strtotime($inboxMessage['lastMessage']->company_inquiry_conversation_datecreated)) }} 
                                        </time>
                                        <span class="text left">
                                            {{ mb_strimwidth($inboxMessage->company_inquiry_thread_title, 0, 50, "") }}
                                            :
                                            {{ mb_strimwidth($inboxMessage['lastMessage']->company_inquiry_conversation_message, 0, 100, "...") }}
                                        </span>
                                    </p>
                                    <figure class="btn_pulldown">
                                        <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                        <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                    </figure>
                                </li>

                                <li id="message_area_{{ $inboxMessage->company_inquiry_thread_id }}" class="opened_chat">
                                    <div class="messages_container">
                                        @php
                                            $day="";
                                        @endphp
                                        @foreach($inboxMessage->company_inquiry_conversations as $key => $companyInquiryConversation)
                                        @if(is_null($day) 
                                            || (\Carbon\Carbon::parse($companyInquiryConversation->company_inquiry_conversation_datecreated)->format('m-d-Y')
                                            != $day ))
                                            @php
                                                $day = \Carbon\Carbon::parse($companyInquiryConversation->company_inquiry_conversation_datecreated)->format('m-d-Y');
                                            @endphp
                                            <hr>
                                            <span style="font-size:12px; text-align:center;display:block;">
                                                @if($day == \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m-d-Y'))
                                                    Today
                                                @elseif($day == \Carbon\Carbon::parse(\Carbon\Carbon::yesterday())->format('m-d-Y'))
                                                    Yesterday
                                                @else
                                                    {{ \Carbon\Carbon::parse($companyInquiryConversation->company_inquiry_conversation_datecreated)->format('M j, Y') }}
                                                @endif
                                            </span>
                                        @endif
                                            <div class="opened_chat_inner opened_chat_recieve">
                                                <div class="chat_area layout_2clm clearfix">
                                                    <figure class="icon {{ ($companyInquiryConversation->company_inquiry_conversation_user_id != $userId) 
                                                    ? 'left'
                                                    : 'right'}}">
                                                        <img src="{{ ($companyInquiryConversation->company_inquiry_conversation_user_id != $userId) 
                                                        ? $adminImgSrc
                                                        : $companyImgSrc}}" alt="">
                                                    </figure>
                                                    <div class="icon__username">
                                                      {{ $completeUserNameArray[$k][$key] }}
                                                        <span class="message__date"> {{ date('M j, Y h:i A', strtotime($companyInquiryConversation->company_inquiry_conversation_datecreated)) }}</span>
                                                    </div>
                                                    <div>
                                                            <p class="message_contents {{ ($companyInquiryConversation->company_inquiry_conversation_user_id != $userId) 
                                                                    ? 'left'
                                                                    : 'right'}}">
                                                                        {{$companyInquiryConversation->company_inquiry_conversation_message}}
                                                            </p>
                                                    </div>
                                                </div>                                             
                                            </div>
                                        @endforeach    
                                    </div>
                                    <div class="reply_container">
                                        <div class="opened_chat_inner opened_chat_send">
                                            <div class="form_area">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <th>
                                                                <h3 class="sub_title"><!-- {{ __('labels.reply') }} --></h3>
                                                            </th>
                                                            <td>
                                                                <textarea class="ctxtInput" placeholder="Type a Message..."></textarea>
                                                                 <span class="help-block required--text spErrorMessage hidden">
                                                                     <strong class="text">Please enter a message</strong>
                                                                </span><br>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <div class="layout_1clm ta_c">
                                                    <div class="dvMainButtons">
                                                        <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->company_inquiry_thread_id }}">{{ __('labels.reply') }}</a>
                                                    </div>
                                                    <div class="dvConfirmButtons hidden">
                                                        <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                        <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->company_inquiry_thread_id }}">{{ __('labels.confirm') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div> <!-- dvInbox -->

                    <!-- default hidden -->
                    <div id="dvCompose" style="display:none;" class="dvTab">

                        <ul class="chat_list recieve_mail_area">                           
                            <li class="opened_chat" style="display: list-item; background: white;">
                                <div class="opened_chat_inner opened_chat_send"> 
                                    <span style="float:right; cursor: pointer;" id="btnCloseCompose">x</span>
                                    <div class="form_area">
                                        <table>
                                            <tbody>
                                                 <tr>
                                                    <td>
                                                        <h3 class="sub_title">{{ __('labels.company-name') }}</h3>
                                                    </td>
                                                    <td>
                                                        {{ $company->company_name}}
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        <h3 class="sub_title">
                                                            {{ __('labels.email-address') }} 
                                                        </h3>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" id="txt_email" name="txt_email"  value="{{ $company->company_email }}">
                                                        {{ $company->company_email }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h3 class="sub_title">
                                                          {{ __('labels.contact-person') }} <span class="required" style="float:none;">{{ __('labels.required') }}</span>
                                                        </h3>
                                                    </td>
                                                    <td>
                                                       <input type="text" id="txt_contact_person" name="txt_contact_person" value="{{ $company->company_contact_person }}">
                                                        <span id="spContactPerson" class="help-block required--text hidden">
                                                          <strong class="text">Contact Person is required</strong>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h3 class="sub_title">
                                                           {{ __('labels.title') }} <span class="required" style="float:none;">{{ __('labels.required') }}</span>
                                                        </h3>
                                                    </td>
                                                    <td>
                                                       <input type="text" id="txt_title" name="txt_title" style="width: 100%;">
                                                        <span id="spTitle" class="help-block required--text hidden">
                                                          <strong class="text">Title is required</strong>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <h3 class="sub_title">
                                                              {{ __('labels.inquiry') }} <span class="required" style="float:none;">{{ __('labels.required') }}</span>
                                                        </h3>
                                                    </th>
                                                    <td>
                                                        <textarea id="txt_content" name="txt_content"></textarea>
                                                        <span id="spInquiry" class="help-block required--text hidden">
                                                            <strong class="text">Message is required</strong>
                                                       </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                    </table>
                                </div>

                                <div class="layout_1clm ta_c">
                                    <div class="dvMainButtons">
                                        <a class="btnConfirmSend btn_submit btn ib"> {{ __('labels.send') }}</a>
                                    </div>
                                    <div class="dvConfirmButtons hidden">
                                        <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                        <a id="btnComposeThread" class="btn_submit btn ib"  >{{ __('labels.send') }}</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>  <!-- dvCompose -->
            </div><!-- detail_contents --> 
        </li> <!-- list_info -->
      </ul>
    </form>

</div>
    

</main>

<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection