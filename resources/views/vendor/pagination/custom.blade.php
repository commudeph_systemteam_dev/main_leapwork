@if ($paginator->hasPages())
<div class="top">  <!-- Louie: Remove this top div and ensure custom css is applied - if applicable. Make sure top like css is copied -->
    <div class="pc">
        <ul class="pagination clearfix">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <!-- <li class="disabled"><span>&laquo;</span></li> --> <!-- default -->
                <!-- <li class="disabled"><a>&laquo;</a></li> -->
            @else
                <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active">
                                <a href="{{ $paginator->currentPage() }}" rel="prev">{{ $page }}</a>
                            </li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}" rel="next"><sup class="more">&raquo;</sup></a></li>
            @else
                <!-- <li class="disabled"><span>&raquo;</span></li> --> <!-- default -->
                <!-- <li class="disabled"><a>&raquo;</a></li> -->
            @endif
        </ul>
    </div>
</div>
@endif
