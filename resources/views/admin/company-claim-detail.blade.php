@include('admin.common.html-head')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/home.css')}}" />
  <!-- title -->
  <title>Dashboard | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
    
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Claim Information
    </p>
  </header>

  <form method="POST" action="" id="frmAdminCompanyProfile" enctype='multipart/form-data'>
   {{ csrf_field() }}

    <div class="content-inner">

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                Company Name <span class="required"> required </span>
              </div>
              <div class="td">
                 Test
              </div>
            </div>
          </div>
        </div>
        </div>
      <div class="btn-wrapper">
        <a href="{{ url('admin/companies') }}" class="btn">
          Back
        </a>
      </div>
    </div>
    
  </form>

</main>
@endsection