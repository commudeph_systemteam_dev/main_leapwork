@include('admin.common.html-head')
@extends('admin.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/companies.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  {{--  <script type="text/javascript" src="{{url('/js/company/profile.js')}}"></script>  --}}
@endpush

@section('content')

@if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
@endif

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company Information 
    </p>
  </header>

  <form method="POST" action="{{url('admin/companies/companyStatusChange')}}" id="frmAdminCompanyProfile" enctype='multipart/form-data'>
   {{ csrf_field() }}
   <input type="hidden" id="company_id" name="company_id" value="{{ $company->company_id }}" >

    <div class="content-inner">

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">

            <div class="tr">
              <div class="td">
                Company Name
              </div>
              <div class="td">
                 {{ $company->company_name}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Website
              </div>
              <div class="td">
                {{ $company->company_website}}
              </div>
            </div>

             <div class="tr">
              <div class="td">
                Address 
              </div>
              <div class="td">
                {{ $company->company_postalcode }}
                <br>
                {{ $company->company_address1}}
                <br>
                {{ $company->company_address2}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                CEO
              </div>
              <div class="td">
              {{ $company->company_ceo}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                No. of employees
              </div>
              <div class="td">
              @if(isset($company->employee_range))
              {{ number_format($company->employee_range->master_employee_range_min) }} ~ {{ number_format($company->employee_range->master_employee_range_max) }}
              @endif
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Date Founded
              </div>
              <div class="td">
                {{$company->company_date_founded}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Industry
              </div>
              <div class="td">
                @for ($i = 0; $i < count($company->company_industries); $i++)
                    {{$company->company_industries[$i]->job_industry->master_job_industry_name}}
                  <br>
                @endfor
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Twitter
              </div>
              <div class="td">
              {{ $company->company_twitter}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Facebook
              </div>
              <div class="td">
              {{ $company->company_facebook}}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Logo
                <p class="help-block required--text red" id="image-size-label">(Please upload 1:1)</p>
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <div id="dv_company_logo">
                     @if(!empty($company->company_logo))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                        <a href="#" id="btnRemoveCompanyLogo"></a>
                    @endif
                    
                </div>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Banner
                <p class="help-block required--text red" id="image-size-label">(Please upload 16:4)</p>
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag" >
                <div id="dv_company_banner">
                    @if(!empty($company->company_banner))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                        <a href="#" id="btnRemoveCompanyBanner"></a>
                    @endif
                </div>
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    Introduction
                  </div>
                  <div class="td">
                  {{ $companyIntro->company_intro_content }}
                  </div>              
             </div>
             <div class="tr">
                  <div class="td">
                      Public Relations
                    </div>
                    <div class="td">
                        {{ $companyIntro->company_intro_pr }}
                    </div>              
              </div>
               <div class="tr">
                  <div class="td">
                      Public Relations (image)
                    </div>
                    <div class="td">                   
                       <input type="hidden" id="txt_company_intro_image_remove_flag" name="txt_company_intro_image_remove_flag" >
                       <div id="dv_company_intro_image">
                             @if(!empty($companyIntro->company_intro_image))
                                <img src='{{url("/storage/uploads/company/company_$company->company_id/$companyIntro->company_intro_image")}}'  id="img_company_logo">
                                <a href="#" id="btnRemoveCompanyIntroImage"></a>
                            @endif
                        </div>
                  </div>              
               </div> 
               <div class="tr">
                    <div class="td">
                            File Attachments 
                    </div>
                    <div class="td">
                      @for($i =1 ; $i <= 3; $i++)
                        @if($company["company_file".$i] != '')
                            <div class="tr">
                                <div class="td">
                                    <a target="_blank" href="https://docs.google.com/viewerng/viewer?url={{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file'.$i]}}">File {{$i}}</a>
                                </div>
                            </div>
                        @endif
                     @endfor
                    </div>
                </div>
                @foreach($unissuedClaims as $claim)
                <div class="tr">
                    <div class="td">
                       Unissued/Unpaid Claim
                    </div>
                    <div class="td">
                        <a href="{{url('/admin/billing/detail')}}/{{ $company->company_id}}">{{$claim->claim_token}}</a>
                    </div>
                </div>
                @endforeach

                <div class="tr">
                    <div class="td">
                        Company Status
                    </div>
                    <div class="td">
                         {{ Form::select('company_status', 
                                              config('constants.companyStatus'), 
                                              $company->company_status)  
                          }}
                    </div>
                </div>
          </div>
        </div>
      <div class="btn-wrapper">
        <a href="{{ url('admin/companies') }}" class="btn alt-btn">
          Back
        </a>
        <button type="submit">SAVE</button>
      </div>
    </div>
    
  </form>

</main>
@endsection