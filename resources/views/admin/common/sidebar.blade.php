<div class="menu-btn">
  <img src="{{url('images/common/icon/nav-menu.svg')}}" alt="">
</div>
<nav class="global-nav">
  <ul class="list">
    <li class="item dashboard">
      <a href="{{url('admin/dashboard')}}">
        <span>{{__('labels.dashboard')}}</span>
      </a>
    </li>
    <li class="item companies">
      <a href="{{url('admin/companies')}}">
        <span>{{__('labels.company-list')}}</span>
      </a>
    </li>
    <li class="item billing">
      <a href="{{url('admin/billing')}}">
        <span>{{__('labels.claim')}}</span>
      </a>
    </li>
    <li class="item ranking">
      <a href="{{url('admin/ranking')}}">
        <span>{{__('labels.popular-jobs')}}</span>
      </a>
    </li>
    <li class="item notice">
      <a href="{{url('admin/notice')}}">
        <span>{{__('labels.announcements')}}</span>
      </a>
    </li>
    <li class="item contact">
      <a href="{{url('admin/inquiries')}}">
        <span>{{__('labels.inquiry-details')}}</span>
      </a>
    </li>
    <li class="item master-admin">
      <a href="{{url('admin/master-admin')}}">
        <span>{{__('labels.master-settings')}}</span>
      </a>
    </li>
  </ul>
</nav>

<script type="text/javascript">
  $(function(){

    // if user has cookie close default
    var key = $.cookie('sidemenu');
    // alert(key);
    if ( key == 'close' ) {
      // alert('sidemenu key is close');
      sideMenuClose();
    } else {
      // alert('sidemenu key is open or empty');
      sideMenuOpen();
    }
    // side menu open & close click event
    $("aside#sidebar div.menu-btn").on("click", function() {
      if ( $(this).hasClass('active') ) {
        sideMenuOpen();
      } else {
        sideMenuClose();
      }
    });
    // side menu open & close function
    function sideMenuOpen() {
      $("aside#sidebar div.menu-btn").removeClass("active");
      $("aside#sidebar").removeClass("side-open");
      $("main.content-main").removeClass("side-open");
      $.cookie('sidemenu', 'open'); // change cookie
    }
    function sideMenuClose() {
      $("aside#sidebar div.menu-btn").addClass("active");
      $("aside#sidebar").addClass("side-open");
      $("main.content-main").addClass("side-open");
      $.cookie('sidemenu', 'close', { expires: 7 }); // change cookie
    }
  });
</script>