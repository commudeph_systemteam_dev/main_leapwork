<div class="inner cf">
  <figure class="logo">
        <a href="{{ url('/admin/home') }}"><img src="{{url('images/common/icon/header-logo.svg')}}" alt=""></a>
  </figure>
  <ul class="function-menu cf">
      <li class="function applicant">
          <span class="icon {{ (count($messageIconNotification)) == 0 ? 'read': 'unread'}}"></span>
          <ul class="tooltip" style="opacity: 1;">
            <div class="tooltip__container">
    
              @if(count($messageNotifications) == 0)
              <li id="noMessageItem" class="item">
                <p class="text item-empty">
                  {{__('messages.messages-none')}}
                </p>
              </li>
              @endif

              @foreach($messageNotifications as $message)
                @php
                  $timezone = 'Asia/Manila';
                  $now = \Carbon\Carbon::now($timezone);
                  $deliveryDate = \Carbon\Carbon::parse($message->notification_created_at ,$timezone);
                @endphp
                <li class="item">
                  <span class="user-icon">  
                      <img src="{{ is_null($message->company_logo) 
                                      ? url('/images/common/icon/header-user.png') 
                                      :url('/storage/uploads/company/company_') .
                                      $message->company_id . 
                                        '/' . $message->company_logo }}" alt="" srcset="">
                    
                  </span>
                  <a href="{{ url($message->notification_url)}}">
                    <p style="text-align: right;">
                    @if($now->diffInHours($deliveryDate) <= 1)
                      {{$deliveryDate->diffForHumans()}}
                    @elseif($now->diffInDays($deliveryDate) < 1)
                      {{$deliveryDate->format('h:i A')}}
                    @else
                      {{$deliveryDate->format('D (m/d)')}}
                    </p>
                    @endif
                  <p class="text">
                      <b>{{ $message->company_name }}</b><br> 
                      <b>{{json_decode($message->notification_data)->title}}</b><br>
                      {{ mb_strimwidth(json_decode($message->notification_data)->content, 0, 100, " ...")}}
                  </p>
                  </a>
                </li>
    
              @endforeach
              <li class="read-more">
                <a href="{{url('/admin/inquiries')}}">
                  {{ __('labels.view-more') }}
                </a>
              </li>
            </div>
          </ul>
        </li>
    <li class="function notice">
      <input type="hidden" name="hiddenID" id="hiddenID" value="{{ Auth::user()['id'] }}"/>
      <span id="adminBellNotification" class="icon {{(sizeof(json_decode($adminNotifications)) == 0)?'read':'unread'}}"></span>
      <ul class="tooltip" id="tooltipAlert">
        <div id="notificationContainer" class="tooltip__container">
        @if(sizeof(json_decode($adminNotifications)) == 0)
            <div class="tr">
              <div id="noNotificationsMsg" class="td" style="padding: 10px; text-align: center;">
                {{Lang::get('messages.notifications-none')}}
              </div>
            </div>
        @endif
        @foreach($bellListNotifications as $notification)
        @php 
            $timezone = 'Asia/Manila';
            $now = \Carbon\Carbon::now($timezone);
            $deliveryDate = \Carbon\Carbon::parse($notification->notification_created_at ,$timezone);
        @endphp
        <li class="item">
          <p style="text-align: right;">
            @if($now->diffInHours($deliveryDate) <= 1)
              {{$deliveryDate->diffForHumans()}}
            @elseif($now->diffInDays($deliveryDate) < 1)
              {{$deliveryDate->format('h:i A')}}
            @else
              {{$deliveryDate->format('D (m/d)')}}
            @endif
          </p>
          @if($notification->notification_type != "NEW_COMPANY")
            <a href="{{ url('admin/notice/edit?announcement_id=') }}{{ json_decode($notification->notification_type_id) }}">{{json_decode($notification->notification_data)->title}}</a>
          @else
            <a style="text-align: left; color:black" href="{{ url('admin/companies') }}/{{ json_decode($notification->notification_type_id) }}">
                  <strong>{{json_decode($notification->notification_data)->title}}</strong><br>{{ mb_strimwidth(json_decode($notification->notification_data)->content, 0, 100, "...")}}
            </a>
          @endif
        </li>
        @endforeach
        <li class="read-more">
          <a href="{{url('admin/notice')}}" align="center">
            View More
          </a>
        </li>
      </div>
      </ul>
    </li>
    <li class="function user">
      <span class="icon">
          <img src="{{ asset('/storage/uploads/master-admin/leapwork_logo.png') }}"></span>
      <ul class="tooltip" id="tooltipUser">
        <div class="tooltip__container">
        <li class="item logout">
          <a href="{{url('/logoutUser')}}">
             {{ __('labels.logout') }}
          </a>
        </li>
        </div>
      </ul>
    </li>
    
    </li>
  </ul>
</div>

<input type="hidden" id="userId" value="{{Auth::user()->id}}">

<!--<script type="text/javascript">
// toggle content
$(function(){
  $("body").click(function(e){
      if(e.target.className == "tooltip")
      {
        $(".tooltip").show();
      }
    }
  );
});
</script>  
-->