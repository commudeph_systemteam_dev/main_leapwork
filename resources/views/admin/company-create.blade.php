@extends('admin.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <!-- create own css to display plans cleanly -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/registration.css')}}" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/companies.css')}}" />
  <link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
  <link rel="stylesheet" media="screen and (max-width: 767px)" href="{{url('css/sp.css')}}">

@endpush

@push('scripts')
  <script>
  var view = {
              validations: <?php echo json_encode( Lang::get('validation')['custom'] ) ?>
             };

  </script>
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companies.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/admin/master-admin/common.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/config/constants.js')}}"></script>

@endpush

@section('content')


@if($errors->any())
    <div class="alert__modal">
      <div class="alert__modal--container">
         @foreach($errors->all() as $error)
            <p>{{$error}}</p>
         @endforeach
         <span>x</span>
      </div>
    </div>
@endif

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company - Create 
    </p>
  </header>

  <form method="POST" id="frmAdminCompanyProfile" enctype='multipart/form-data'>
      {{ csrf_field() }}
      <input type="hidden" id="company_status" name="company_status" value="VERIFIED">

    <div class="content-inner edit-view">

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">

            <div class="tr">
              <div class="td">
                Company Name <span class="required"> required </span>
              </div>
              <div class="td">
                 <input type="text" id="txt_company_name" name="txt_company_name"  value="{{ old('txt_company_name') }}">
                 <span id="spCompanyName" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Website
              </div>
              <div class="td">
                <input type="text" id="txt_company_website" name="txt_company_website"  value="{{ old('txt_company_website') }}">
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Contact Name
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_person" name="txt_company_contact_person" value="{{ old('txt_company_contact_person') }}">
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Contact Number <span class="required"> required </span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_no" name="txt_company_contact_no"  value="{{ old('txt_company_contact_no') }}">
                <span id="spContactNo" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Email Address <span class="required"> required </span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_email" name="txt_company_email"  value="{{ old('txt_company_email') }}">
                <span id="spEmail" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Password <span class="required"> required </span>
              </div>
              <div class="td password_panel">
                <input type="password" id="txt_password" name="txt_password" placeholder="New Password" style="width: 350px;">
                <input type="password" id="txt_password_confirmation" name="txt_password_confirmation" placeholder="Confirm New Password" style="width: 350px;">
                <span id="spPassword" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
              <div class="td">
                Address
              </div>
              <div class="td">
                 <input type="text" list="codes" placeholder="Postal Code" id="txt_company_postalcode" name="txt_company_postalcode" value="{{ old('txt_company_postalcode') }}">
                <datalist id="codes">
                    @foreach(array_keys($arrPhilippines) as $key)
                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                    @endforeach
                </datalist>
                <br>
                <br>
                <input type="text" id="txt_company_address1" name="txt_company_address1" value="{{ old('txt_company_address1') }}">
                <br><br>
                <input type="text" id="txt_company_address2" name="txt_company_address2" value="{{ old('txt_company_address2') }}">

                <span id="spPostalCode" class="help-block required--text">
                    <strong class="text"></strong>
                </span>
                <span id="spAddress1" class="help-block required--text">
                    <strong class="text"></strong>
                </span>

              </div>
            </div>

            <div class="tr">
              <div class="td">
                CEO
              </div>
              <div class="td">
                <input type="text" id="txt_company_ceo" name="txt_company_ceo" value="{{ old('txt_company_ceo') }}">
                <span id="cpCompanCeo" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                No. of employees
              </div>
              <div class="td">
                 {{ Form::select('so_employee_range_id', 
                                  $employeeRanges,
                                  null,
                                  ['placeholder' => '- Choose One -'])
                 }}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Date Founded
              </div>
              <div class="td">
                <input type="text" id="txt_company_date_founded" name="txt_company_date_founded" class="datepickerPast" value="{{ old('txt_company_date_founded') }}"> 
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Industry
              </div>
              <div class="td">

                @for ($i = 0; $i < 3; $i++)
                  {{ Form::select('so_company_industry_id[]', 
                                   $jobIndustries,
                                   null,
                                   ['placeholder' => '- Choose One -'])
                  }}
                  <br><br>
                @endfor

              </div>
            </div>

            <div class="tr">
              <div class="td">
                Twitter
              </div>
              <div class="td">
               <input type="text" id="txt_company_twitter" name="txt_company_twitter" value="{{ old('txt_company_twitter') }}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Facebook
              </div>
              <div class="td">
              <input type="text" id="txt_company_facebook" name="txt_company_facebook" value="{{ old('txt_company_facebook') }}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Logo
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <span class="js-company_logo-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_logo">
                     @if(!empty($company->company_logo))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                        <a href="#" id="btnRemoveCompanyLogo"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                    @endif
                </div>
                <input type="file" id="file_company_logo" name="file_company_logo" value="{{ old('file_company_logo') }}">
                <span id="spCompanyLogo" class="help-block required--text js-company_logo-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Banner
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag">
                <span class="js-company_banner-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_banner">
                    @if(!empty($company->company_banner))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                        <a href="#" id="btnRemoveCompanyBanner"></a>
                    @endif
                </div>
                <input type="file" id="file_company_banner" name="file_company_banner" value="{{ old('file_company_banner') }}">
                <span id="spCompanyBanner" class="help-block required--text js-company_banner-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    Introduction
                  </div>
                  <div class="td">
                    <textarea id="txt_company_intro_content" name="txt_company_intro_content" value="{{ old('txt_company_intro_content') }}"></textarea>
                  </div>              
             </div>

             <div class="tr">
                  <div class="td">
                      Public Relations
                    </div>
                    <div class="td">
                      <textarea id="txt_company_intro_pr" name="txt_company_intro_pr" value="{{ old('txt_company_intro_pr') }}"></textarea>
                    </div>              
              </div>

              <div class="tr" id="payment_method" style="display:none;">
                    <div class="td">
                       Payment Method <span class="required"> required </span>
                    </div>
                    <div class="td">
                      {{ Form::select('so_payment_method', 
                                        config('constants.paymentMethod'),
                                        null,
                                        array('id' => 'so_payment_method',
                                              'placeholder' => '- Select One -') 
                                      )   
                      }}
                      <span id="spPaymentMethod" class="help-block required--text">
                        <strong class="text"></strong>
                      </span>
                  </div>            
               </div> 

               <div class="tr">
                    <div class="td">
                      Select Plan  <span class="required"> required </span>
                    </div>
                    <div class="td">
                       <div class="panel__plan">
                          <ul class="plan-list js-plan-list">

                            @foreach($masterPlans as $masterPlan)
                              <li class="plan">
                                <h3 class="title"> {{ $masterPlan->master_plan_name }} PLAN</h3>
                                <p class="price">
                                  {{ $masterPlan->master_plan_price == 0 ? 'FREE' : 'PHP ' . number_format($masterPlan->master_plan_price,0) }}
                                </p>
                                <p class="text">
                                  Can only post up to {{ $masterPlan->master_plan_post_limit }} 
                                  {{ $masterPlan->master_plan_post_limit > 1 ? 'Jobs.' : 'Job.'}}
                                  <br>
                                  The recruitment period is limited to ({{ floor(($masterPlan->master_plan_expiry_days)/7) }}) 
                                  {{ $masterPlan->master_plan_expiry_days > 1 ? 'weeks.' : 'week.'}}
                                  per publication.
                                </p>

                                <p class="plan__button">
                                    <input type="radio" name="rb_plan_type" value="{{ $masterPlan->master_plan_id }}" value="{{ old('rb_plan_type') }}">
                                    <label for="trial_plan">SELECT</label>
                                </p>
                              </li>
                            @endforeach

                          </ul>
                        </div>
                         <span id="spPlanType" class="help-block required--text">
                            <strong class="text"></strong>
                          </span>
                    </div>
               </div> 

               <div class="tr">
                    <div class="td">
                        Business Permits <span class="required"> required </span>
                        <br>
                        <span class="sub__text">Ex. Mayor's Permit, BIR Registration</span>
                        <br><span class="sub__text">(png, jpeg, jpg, pdf)</span>
                    </div>
                    <div class="td">
                      <input type="hidden" id="txt_company_file_remove_flag" name="txt_company_file_remove_flag" >
                      <input type="file" id="companyFile1" name="file_business_permit[]" class="company__file" />
                      <br/>
                      <input type="file" id="companyFile2" name="file_business_permit[]" class="company__file" />
                      <br/>
                      <input type="file" id="companyFile3" name="file_business_permit[]" class="company__file" />
                      <br>

                      <!-- <input type="file" id="file_business_permit" name="file_business_permit[]" multiple="multiple"> -->
                      <span id="spBusinessPermitMain" class="sub__text">* 3 maximum upload</span>
                      <span id="spBusinessPermit" class="help-block required--text">
                        <strong class="text"></strong>
                      </span>
                  </div>            
               </div> 

             </div>  

          </div>
          <div class="btn-wrapper cf">
        <a href="{{ url('admin/companies') }}" class="btn alt-btn">
          Back
        </a>
        <a href="#" class="btn" id="btnCreateCompany">
          SAVE
        </a>
      </div>
      </div>
      
    </div>
    
    <div class="content-inner" id="confirm-company-view" hidden>
    <div class="panel" id="confirmCompanyContainer">
    <div class="inner" >
    <div class="registration__content registration__confirmation">
                    <h2>Company Information - Confirmation</h2>
                    <table>
                        <tr>
                            <td>Company Name</td>
                            <td>
                                <input type="label" id = "companyNameConfirm" name="companyNameConfirm" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>Company Website</td>
                            <td>
                                <input type="label" id = "companyWebsiteConfirm" name="companyWebsiteConfirm" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>Contact Name</td>
                            <td>
                                <input type="label" id = "contactNameConfirm" name="contactNameConfirm" readonly>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>Contact Number</td>
                            <td>
                                <input type="label" id = "contactNumberConfirm" name="contactNumberConfirm" readonly>
                            </td>
                        </tr>
                            <td>Email Address</td>
                            <td>
                                <input type="label" id = "emailConfirm" name="emailConfirm">
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <input type="label" id = "codesValueConfirm" name="codesValueConfirm" readonly>
                                <br>
                                <input type="label" id = "address1Confirm" name="address1Confirm" readonly>
                                <br>
                                <input type="label" id = "address2Confirm" name="address2Confirm" readonly>
                                <br>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                            <span>Selected Plan</span>
                            <div class="panel__plan">
                                <ul class="plan-list selected">
                                  @foreach($masterPlans as $masterPlan)
                                    <li class="plan">
                                      <h3 class="title"> {{ $masterPlan->master_plan_name }} PLAN</h3>
                                      <p class="price">
                                        {{ $masterPlan->master_plan_price == 0 ? 'FREE' : 'PHP ' . number_format($masterPlan->master_plan_price,0) }}
                                      </p>
                                      <p class="text">
                                        Can only post up to {{ $masterPlan->master_plan_post_limit }} 
                                        {{ $masterPlan->master_plan_post_limit > 1 ? 'Jobs.' : 'Job.'}}
                                        <br>
                                        The recruitment period is limited to ({{  floor(($masterPlan->master_plan_expiry_days)/7) }}) 
                                        {{ $masterPlan->master_plan_expiry_days > 1 ? 'weeks.' : 'week.'}}
                                        per publication.
                                      </p>

                                      <p class="plan__button">
                                          <input type="radio" name="rb_plan_type_selected" value="{{ $masterPlan->master_plan_id }}" disabled>
                                          
                                      </p>
                                    </li>
                            @endforeach
                                </ul>
                            </div>
                        </tr>
                        <tr class="js-table__tr-payment_confirm">
                            <td>Payment Method</td>
                            <td>
                                 <input type="label" id = "payment_methodConfirm" name="payment_methodConfirm" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>Upload Business Permit<br><span class="sub__text">Ex. Mayor's Permit, BIR Registration</span></td>
                            <td>
                                <ul id="ul-files_confirm">

                                </ul>

                            </td>
                        </tr>
                        
                        <tr class="submit__button">
                            <td colspan="2">
                                <input type="button" class="btnRegister btnBack" value="Back" id="btnBackToInput">
                                <input type="submit" class="btnRegister" id="btnRegisterCompany" value="Confirm">
          
                            </td>
                        </tr>
                        
                    </table>
            </div>
    </div>
    </div> 
    </div>
 </form>

</main>
@endsection
