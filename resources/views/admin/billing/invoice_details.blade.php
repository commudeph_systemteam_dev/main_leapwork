<!DOCTYPE html>
<html lang="ja">

<head>
	<link rel="stylesheet" type="text/css" media="print,screen" href="{{url('css/company/invoice.css')}}" />
</head>

<body>
	
	<div class="invoice__wrapper">

		<header class="cf">
			<figure>
				<img src="{{url('images/logo.svg')}}" alt="">
			</figure>
			<span>
				{{ __('labels.invoice') }}
			</span>
		</header>

		<table class="invoice__table" style="margin-top: 0px;">
			<tbody>
				<tr>
					<td>
						{{ __('labels.bill-to') }} 
					</td>
					<td>
						{{ $claimDetails->company_name }}
					<td>
						{{ __('labels.invoice-no') }} :
					</td>
					<td>
						{{ $claimDetails->claim_id }}
					</td>
				</tr>
				<tr>
					<td></td>
					<td>{{ $claimDetails->company_address1 }}</td>
					<td> 
						{{ __('labels.date') }} :
					</td>
					<td>{{ date('d/m/Y', strtotime($claimDetails->claim_datecreated)) }}</td>
				</tr>
				<tr>
					<td></td>
					<td>{{ $claimDetails->company_address2 }}</td>
					<td> {{ __('labels.due-date') }} : </td>
					<td>{{ date('d/m/Y', strtotime($claimDetails->claim_end_date)) }}</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td> {{ __('labels.payment-method') }} : </td>
					<td>{{ $claimDetails->claim_payment_method }}</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td> {{ __('labels.payment-status') }} : </td>
					<td>{{ $claimDetails->claim_payment_status }}</td>
				</tr>
			</tbody>
		</table> <!-- .invoice__table -->

		
		<table class="invoice__table02">
			<thead>
				<tr>
					<th>{{ __('labels.item') }}</th>
					<th>{{ __('labels.quantity') }}</th>
					<th>{{ __('labels.unit-price') }}</th>
					<th>{{ __('labels.total-price') }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $claimDetails->company_plan_type }}</td>
					<td>1</td>
					<td>Php {{ number_format($claimDetails->master_plan_price , 2, '.', ',') }}</td>
					<td>Php {{ number_format($claimDetails->master_plan_price , 2, '.', ',') }}</td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					<td>* {{ __('labels.powered-by') }} Commude Philippines</td>
					<td></td>
					<td></td>
					<td style="padding-left: 90px;">{{ __('labels.total') }}: Php {{ number_format($claimDetails->master_plan_price , 2, '.', ',') }}</td>
				</tr>
				<tr>
					<td>Visit <a href="https://www.commude.ph">https://www.commude.ph</a></td>
					<td></td>
				</tr>
				<tr>
					<td>{{ __('labels.thank-you-leapwork') }}</td>
					<td></td>
				</tr>
			</tbody>
		</table> <!-- .invoice__table02 -->
		<div class="sub_total">
			<span>{{ __('labels.balance-due') }}</span>
			<span>Php {{ number_format($claimDetails->master_plan_price , 2, '.', ',') }}</span>
		</div>

	</div> <!-- .invoice__wrapper -->

</body>

</html>
