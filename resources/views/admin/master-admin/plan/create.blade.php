@extends('admin.layouts.app')

@section('title')
  Plans |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/admin/master-admin/common.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/master-admin/plan.js')}}"></script>
@endpush

@section('content')

<main id="notice-view" class="content-main">

   <header class="pan-area">
    <p class="text">
      Plans - New
    </p>
  </header>

  <form method="POST" action="" id="frmMasterPlan">
    {{ csrf_field() }}
    <input type="hidden" id="txt_status" name="txt_status" value="ACTIVE">

    <div class="content-inner">
      <div class="section notice-edit">
        <div class="table form panel">
          <div class="tbody">
            <div class="tr">
              <div class="td">
                Name
              </div>
              <div class="td">
                <input type="text" id="txt_name" name="txt_name" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                Price
              </div>
              <div class="td">
                <input type="number" min="0" id="txt_price" name="txt_price" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                Post Limit
              </div>
              <div class="td">
                <input type="number" min="0" max="3" id="txt_post_limit" name="txt_post_limit" required>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                Expiry Days
              </div>
              <div class="td">
                <input type="number" min="0" max="90" id="txt_expiry_days" name="txt_expiry_days" required>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-wrapper">
        <button id="btnCreate" class="btn">
          Save
        </button>
      </div>
    </div>
  </form>

</main>

<!-- Post Scripts -->
    
<!-- Scripts -->
      
@endsection