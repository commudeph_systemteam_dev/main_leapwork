    @include('admin.common.html-head')
    <!-- css -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />
    <script src="{{url('/js/admin/master-admin/user.js')}}"></script>
    <!-- title -->
    <title>{{__('labels.master-settings')}} - {{__('labels.user-management')}} | LEAP Work</title>
</head>

<body>
    @foreach($activeUsers as $key => $value)
        @if(isset($value->applicant_profile_firstname))
            <div class="user__modal_container" id="show_user_modal_{{$value->id}}">
                <div class="user__modal_content">
                    <div class="user__modal_dialog">
                        <a href="#" class="userInfoClose"><img class="userInfoCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                        <h2 class="page__header">{{__('labels.account-information')}}</h2>
                        <div class="table panel">
                            <div class="tbody">
                                <div class="tr create__fullname">
                                    <div class="td"><strong>{{__('labels.full-name')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->applicant_profile_firstname}} {{ $value->applicant_profile_middlename }} {{$value->applicant_profile_lastname}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.email-address')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->applicant_profile_preferred_email}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.contact-number')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->applicant_profile_contact_no}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.birthdate')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->applicant_profile_birthdate}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.gender')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->applicant_profile_gender}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.address')}}</strong></div>
                                    <div class="td">
                                        @if(!$value->applicant_profile_postalcode)
                                        <p></p>
                                        @else
                                        <p>{{$value->applicant_profile_postalcode}}, {{$value->applicant_profile_address1}}, {{$value->applicant_profile_address2}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="user__modal_btns">
                            <button class="spare_user confirm__btn" id="close_modal_applicant">Back</button>
                        </div>
                    </div>
                </div>
            </div> <!-- .user__modal_container -->
        @elseif(isset($value->company_user_firstname))
            <div class="user__modal_container" id="show_user_modal_{{$value->id}}">
                <div class="user__modal_content">
                    <div class="user__modal_dialog">
                        <a href="#" class="userInfoClose"><img class="userInfoCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                        <h2 class="page__header">{{__('labels.account-information')}}</h2>
                        <div class="table panel">
                            <div class="tbody">
                                <div class="tr create__fullname">
                                    <div class="td"><strong>{{__('labels.full-name')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->company_user_firstname}} {{$value->company_user_lastname}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.email-address')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->email}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.contact-number')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->company_user_contact_no}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.birthdate')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->company_user_birthdate}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.gender')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->company_user_gender}}</p>
                                    </div>
                                </div>
                                <div class="tr">
                                    <div class="td"><strong>{{__('labels.address')}}</strong></div>
                                    <div class="td">
                                        @if(!$value->company_user_postalcode)
                                        <p></p>
                                        @else
                                        <p>{{$value->company_user_postalcode}}, {{$value->company_user_address1}}, {{$value->company_user_address2}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .table -->
                        <div class="user__modal_btns">
                            <button class="spare_user confirm__btn" id="close_modal_corporate">Back</button>
                        </div>
                    </div>
                </div>
            </div> <!-- .user__modal_container -->
        @else
            <div class="user__modal_container" id="show_user_modal_{{$value->id}}">
                <div class="user__modal_content">
                    <div class="user__modal_dialog">
                        <h2 class="page__header">{{__('labels.account-information')}}</h2>
                        <div class="table panel">
                            <div class="tbody">
                                <div class="tr create__fullname">
                                    <div class="td"><strong>{{__('labels.email-address')}}</strong></div>
                                    <div class="td">
                                        <p>{{$value->email}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="user__modal_btns">
                                <button class="spare_user confirm__btn" id="close_modal_other">Back</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- .user__modal_container -->
        @endif
    @endforeach

    </div>
    </div>
    </div>
    <header id="global-header">
        @include('admin.common.header')
    </header>
    <div class="content-wrapper">
        <aside id="sidebar">
            @include('admin.common.sidebar')
        </aside>
        <main id="master-admin-view" class="content-main">
            <header class="pan-area">
                <p class="text">
                    {{__('labels.master-settings')}} - {{__('labels.user-management')}}
                </p>
            </header>
            <div class="content-inner">
                <nav class="local-nav">
                    <ul class="menu">
                        <li class="item">
                            <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
                        </li>
                        <li class="item">
                            <span>{{__('labels.user-management')}}</span>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/mail')}}">{{__('labels.email-template')}}</a>
                        </li>
                        <li class="item">
                            <a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
                        </li>
                    </ul>
                </nav>
                <div id="divAccountMessage"></div>
                @if (Session::has('message'))
                <!-- add css detail in here -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="alert alert-success">
                        <strong><center>{{ Session::get('message') }}.</center></strong>
                    </div>
                </div>
                @endif
                <div class="section">
                    {{--
                    <div class="tab panel">
                        <ul class="tab-menu" class="cf">
                            <li><a href="#tab-box1" class="active">Corporate Users</a></li>
                            <li><a href="#tab-box2">Applicant Users</a></li>
                        </ul>
                        <div class="tab-boxes">
                            <div id="tab-box1" class="box job-category">
                            </div>
                        </div>
                        <div class="tab-boxes">
                            <div id="tab-box2" class="box job-category">
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="section setting">
                    <div class="table panel">
                        <div class="thead">
                            <div class="tr">
                                <div class="th">
                                </div>
                                <div class="th" >
                                    {{__('labels.member')}}
                                </div>
                                <div class="th">
                                    {{__('labels.company-name')}}
                                </div>
                                <div class="th">
                                    {{__('labels.account-type')}}
                                </div>
                                <div class="th">
                                </div>
                            </div>
                        </div>
                        <div class="tbody">
                            <!-- start of each tr -->
                            @foreach($activeUsers as $key => $value)
                            <div class="tr">
                                <div class="td">
                                    <span class="icon">
                                            @if($value->user_accounttype = "USER")
                                                @if( !empty($value->applicant_profile_imagepath) )
                                                <img src="{{ url('/storage/uploads/applicantProfile/profile_') .
                                                            $value->applicant_profile_id . 
                                                             '/' .
                                                             $value->applicant_profile_imagepath }}" alt="">
                                                @elseif(!empty($value->company_logo))
                                                <img src="{{ url('/storage/uploads/company/company_') .
                                                            $value->company_id . 
                                                             '/' .
                                                             $value->company_logo }}" alt="">
                                                @endif
                                            @endif
                                    </span>
                                </div>
                                <div class="td" >
                                    @if(isset($value->applicant_profile_firstname))
                                    <a style="float:left; margin-left: 50px;"  onclick="return displayUserModal(this.getAttribute('data-id'))" data-id="{{$value->id}}">{{$value->applicant_profile_firstname }} {{ $value->applicant_profile_middlename }} {{ $value->applicant_profile_lastname }}</a> @elseif(isset($value->company_user_firstname))
                                    <a style="float:left; margin-left: 50px;" onclick="return displayUserModal(this.getAttribute('data-id'))" data-id="{{$value->id}}">{{ $value->company_user_firstname }} {{ $value->company_user_lastname }}</a> @else
                                    <a style="float:left; margin-left: 50px;" href="#"> {{$value->email}}</a>
                                    @endif
                                </div>
                                @php $urlServer = "'".url('')."'"; @endphp
                                <div class="td">
                                    @if(isset($value->company_name)) {{ $value->company_name }} @else - @endif
                                </div>
                                <div class="td">
                                    <!--  {{ Form::select('accountType', array( 'ADMIN' => 'Administrator', 'CORPORATE' => 'Corporate', 'USER' => 'User' ), $value->user_accounttype , array('onchange' => "updateAccountStatus(this.value, $value->id, $urlServer);")) }} -->
                                 
                                    <!-- {{ $value->user_accounttype }} -->
                                    @if(isset($value->company_name)) CORPORATE USER  @else {{ $value->user_accounttype }} @endif
                                </div>
                                <div class="td">
                                    @if(isset($value->applicant_profile_firstname))
                                    <a class="btn" href="{{url('admin/master-admin/user/edit')}}/{{$value->id}}/{{$value->user_accounttype}}" data-link="{{url('admin/master-admin/user/edit')}}/{{$value->id}}/{{$value->user_accounttype}}">
                                          {{__('labels.edit')}}
                                        </a> @elseif(isset($value->company_user_firstname))
                                    <a class="btn" href="{{url('admin/master-admin/user/edit')}}/{{$value->company_user_user_id}}/CORPORATE USER">
                                          {{__('labels.edit')}}
                                        </a> @endif
                                    <a class="btn del-btn" onclick="return confirmDelete(this)" data-id="{{$value->id}}" data-href="{{url('admin/master-admin/user/delete')}}/{{$value->id}}">
                                      {{__('labels.delete')}}
                                    </a>
                                    
                                </div>
                            </div>
                            <!-- end of each tr -->
                            @endforeach
                        </div>
                        <div class="confirm__modal_container">
                            <div class="confirm__modal_content">
                                <a class="modal__close btn_close_delete" id="btn_close_delete"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                                <div class="confirm__modal_dialog">
                                    Are you sure you want to delete this user?
                                    <div class="confirm__modal_btns">
                                        <button class="spare_user spare_user_delete confirm__btn">No</button>
                                        <a id="delete_user"  class="remove_user delete_user confirm__btn" >Yes</a>
                                        {{-- href="{{url('admin/master-admin/user/delete')}}/{{ $value->id }}" --}}
                                        <input type="hidden" id="txt_user_id" name="txt_user_id">
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .confirm__modal_container -->
                        <!-- </div>endof section setting -->
                    </div>
                    <center>{{$activeUsers->render()}}</center>
                    <div class="btn-wrapper">
                        <a href="{{url('admin/master-admin/user/create')}}" class="btn">
                        {{__('labels.add-user')}}
                        </a>
                    </div>
        </main>
        </div>
        <script>
        $(function() {
            $(".tab-menu li a").on("click", function() {
                $(".tab-menu li a").removeClass("active");
                $(this).addClass("active");
                $(".tab-boxes>div.box").hide();
                $($(this).attr("href")).fadeToggle();
            });
            return false;
        });
        </script>
        @include('admin.common.footer')