@extends('admin.layouts.app') @section('title') {{__('labels.master-settings')}} - {{__('labels.job-features')}} | @stop @push('styles')
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" /> @endpush @push('scripts')
<script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
<script type="text/javascript" src="{{url('/js/admin/master-admin/feature.js')}}"></script>
@endpush @section('content') @php $urlServer = "'".url('')."'"; @endphp
<main id="master-admin-view" class="content-main">
	<form method="POST" action="{{ url('admin/master-admin/saveJobFeature')}}" id="frmMasterFeature" enctype='multipart/form-data'>
		{{ csrf_field() }}
		<header class="pan-area">
				<p class="text">
						{{__('labels.master-settings')}} - {{__('labels.job-features')}}
				</p>
		</header>
		<div class="content-inner">
			<nav class="local-nav">
					<ul class="menu">
							<li class="item">
									<span>{{__('labels.job-features')}}</span>
							</li>
							<li class="item">
									<a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
							</li>
							<li class="item">
									<a href="{{url('admin/master-admin/user')}}">{{__('labels.user-management')}}</a>
							</li>
							<li class="item">
									<a href="{{url('admin/master-admin/mail')}}">{{__('labels.email-template')}}</a>
							</li>
							<li class="item">
									<a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
							</li>
					</ul>
			</nav>
			<div class="section">
				<div class="tab panel">
					<ul class="tab-menu" class="cf">
							<li><a href="#tab-box1" class="active" id="tab-box1-link">{{__('labels.add-features')}}</a></li>
							<li><a href="#tab-box2">{{__('labels.feature-lists')}}</a></li>
					</ul>
					<div class="tab-boxes">
							<!-- Add Features tab -->
							<div class="box create" id="tab-box1">
									<div class="table form">
											<div class="tbody">
													<div class="tr">
															<div class="td">
																	{{__('labels.feature-title')}}
															</div>
															@if(isset($error))
															<div class="td">
															</div>
															<div class="td">
																	{{__('labels.feature-invalid-entries-check-period')}}
															</div>
													</div>
													@endif
													<div class="td title">
															<select name="job_title" id="job_title" onchange="getDetails(APP_URL)">
																	<option value="0" disabled selected> {{__('labels.select-job-title')}}</option>
																	@foreach($jobs as $key => $value)
																	<option value="{{$value->job_post_id}}">{{$value->job_post_title}}</option>
																	@endforeach
															</select>
															<span class="help-block required--text" id="spTitle"><strong class="text"></strong></span>
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.overview')}}
													</div>
													<div id="overview" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.qualifications-requirements')}}
													</div>
													<div id="qualifications" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.employment-category')}}
													</div>
													<div id="category" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.salary')}}
													</div>
													<div id="salary" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.vacancy')}}
													</div>
													<div id="vacancy" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.selection-process')}}
													</div>
													<div id="process" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.selection-process')}}
													</div>
													<div id="location" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.work-hours')}}
													</div>
													<div id="hours" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.benefits')}}
													</div>
													<div id="benefits" class="td">
													</div>
											</div>
											<div class="tr hidden">
													<div class="td">
															{{__('labels.holiday-vacation')}}
													</div>
													<div id="holiday" class="td">
													</div>
											</div>
											<div class="tr period">
													<div class="td">
															{{__('labels.period')}}
													</div>
													<div class="td">
															<input type="text" id="txt_job_feature_datefrom" name="txt_job_feature_datefrom" class="datepickerFuture" required> 〜
															<input type="text" id="txt_job_feature_dateto" name="txt_job_feature_dateto" class="datepickerFuture" required> <span class="help-block required--text" id="spDateRange"><strong class="text"></strong></span>
													</div>
											</div>
											<div class="tr">
													<div class="td">
															{{__('labels.publish-settings')}}
													</div>
													<div class="td">
															<select id="so_job_feature_visibility" name="so_job_feature_visibility">
																	<option value="Public">
																			{{__('labels.public')}}
																	</option>
																	<option value="Private">
																			{{__('labels.private')}}
																	</option>
															</select>
													</div>
											</div>
									</div>
							</div>
							<div class="btn-wrapper">
									<button id="btnSaveJobFeature" type="submit" name="save"> {{__('labels.save')}}</button>
									<!--  <a href="#" class="btn red">Delete</a> -->
							</div>
					</div>
			</div>
		</div>
	</form>
		<!-- Feature Lists tab -->
	<div class="box index" id="tab-box2">
			<div class="table">
					<div class="tbody">
							@foreach($masterJobFeatures as $masterJobFeature)
							<div class="tr">
							<!-- <div class="td">
																			<figure class="thumb">
																								<img src="/images/admin/dummy-pic.jpg">
																							 {{-- <img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image1) }}" alt=""> --}}
																						</figure>
																				</div> -->
										<div class="td">
												<h2 class="title">{{ $masterJobFeature->job_post_title }}</h2>
												<!-- <p class="term">{{ date('d/m/Y', strtotime($masterJobFeature->job_feature_datefrom)) }} - {{ date('d/m/Y', strtotime($masterJobFeature->job_feature_dateto)) }}</p> -->
												<form method="POST" action="{{ url('admin/master-admin/editJobFeature')}}" enctype='multipart/form-data'>
														{{ csrf_field() }}
														<input type="hidden" readonly name="job_feature_id" value="{{ $masterJobFeature->job_feature_id }}">
														<div class="tr">
																<div class="td">
																		<input class="datepickerFuture" id="txt_job_feature_datefrom_featuretab_{{ $masterJobFeature->job_feature_id }}" name="txt_job_feature_datefrom_{{ $masterJobFeature->job_feature_id }}" type="text" value="{{ date('m/d/Y', strtotime($masterJobFeature->job_feature_datefrom)) }}">
																</div>
																<div class="td">
																		〜
																</div>
																<div class="td">
																		<input class="datepickerFuture" id="txt_job_feature_dateto_featuretab_{{ $masterJobFeature->job_feature_id }}" name="txt_job_feature_dateto_{{ $masterJobFeature->job_feature_id }}" type="text" value="{{ date('m/d/Y', strtotime($masterJobFeature->job_feature_dateto)) }}">
																</div>
																<div class="td">
																		<input type="submit" name="Save" value=" {{__('labels.update')}}" class="saveButton">
																</div>
																<div class="td">
																		<input type="submit" name="Remove" value="Remove" class="removeButton">
																</div>
														</div>
												</form>
										</div>
								</div>
								@endforeach
						</div>
				</div>
		</div>
		</div>
		</div>
		</div>
		</div>
</main>
<!-- Post Scripts -->
<!-- Scripts -->
@include('admin.common.footer') @endsection