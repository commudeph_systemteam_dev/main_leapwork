@include('admin.common.html-head')
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
   <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/common.css')}}" /> 
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/master-admin.css')}}" />

  <!-- title -->
  <title>{{__('labels.master-settings')}} - {{__('labels.email-template')}} | LEAP Work</title>

</head>
<body>
  <header id="global-header">
      @include('admin.common.header')
    
  </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>
    <main id="master-admin-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          {{__('labels.master-settings')}} - {{__('labels.email-template')}}
        </p>
      </header>
      
      @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
      </div>
    </div>
      @endif

      <div class="content-inner">
        <nav class="local-nav">
          <ul class="menu">
            <li class="item">
              <a href="{{url('admin/master-admin')}}">{{__('labels.job-features')}}</a>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/job')}}">{{__('labels.job-applications')}}</a>
            </li>
            <li class="item">
              <a href="{{url('admin/master-admin/user')}}">{{__('labels.user-management')}}</a>
            </li>
            <li class="item">
              <span>{{__('labels.email-template')}}</span>
            </li>
             <li class="item">
              <a href="{{url('admin/master-admin/plan')}}">{{__('labels.plans')}}</a>
            </li>
          </ul>
        </nav>
        <div class="section mail-template">
          <div class="table panel">
            <div class="tbody">
              @foreach ($mailTemplates as $key => $value)  
              <div class="tr">
                <div class="td">
                  <!-- <a href="{{url('admin/master-admin/user-edit-mail/' . $value->master_mail_template_id) }}">{{$value->master_mail_subject}}</a> -->
                  <!-- <a href="{{url('admin/master-admin/user-edit-mail/' . $value->master_mail_template_id) }}"></a> -->
                  {{ $value->master_mail_status }}
                </div>
                <div class="td">
                  <a class="btn blue" href="{{url('admin/master-admin/user-edit-mail/' . $value->master_mail_template_id) }}">
                    {{__('labels.edit')}}
                  </a>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
  <script>
    $(function(){
        $(".tab-menu li a").on("click", function() {
            $(".tab-menu li a").removeClass("active");
            $(this).addClass("active");
            $(".tab-boxes>div.box").hide();
            $($(this).attr("href")).fadeToggle();
        });
        return false;
    });
  </script>
@include('admin.common.footer') 