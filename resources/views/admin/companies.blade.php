@extends('admin.layouts.app')

@section('title')
  {{ __('labels.title') }} |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/companies.css')}}" />
   <style>
      .required-empty {
        border-color: red !important;
    }
  </style>
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companies.js')}}"></script>
@endpush

@section('content')

<main id="companies-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      {{ __('labels.title') }}
    </p>
  </header>


  <form method="POST" action="" id="frmCompanies">
    {{ csrf_field() }}

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <div class="content-inner">
      <aside class="search-menu">
        <ul class="list">
          <li class="item">
            <span class="title">{{ __('labels.date-registered') }}</span>
            <div class="select-wrapper">
              {{ Form::text('txt_date_registered', $args['date_registered'] ?? null, array('class' => 'datepickerPast',
                                                                                              'id' => 'txt_date_registered')) }}

            </div>
          </li>
          <li class="item">
            <span class="title">{{ __('labels.last-login-date') }}</span>
            <div class="select-wrapper">
              {{ Form::text('txt_date_lastlogin', $args['date_lastlogin'] ?? null, array('class' => 'datepickerPast',
                                                                                            'id' => 'txt_date_lastlogin')) }}
            </div>
          </li>
          <li class="item">
            <div class="select-wrapper">
              <span class="title">{{ __('labels.keyword') }}:</span>
              <span class="input-area">
                <input type="text" id="txt_keyword" name="txt_keyword" value='{{ $args["company_name"]  }}'>
              </span>
            </div>
          </li>
          <li class="item">
            <button type="button" id="btnSearch" name="btnSearch">
              {{ __('labels.search') }}
            </button>
          </li>
          <li class="item item--company">
            <a href="{{ url('/admin/companies/create') }}" class="btn">
              {{ __('labels.new-company') }}
            </a>
          </li>
        </ul>
      </aside>
      <div class="section companies" style="margin-bottom: 0px;">
        <div class="table panel">
          <div class="thead">
            <div class="tr">
              <div class="th">
                {{ __('labels.company-name') }}
              </div>
              <div class="th">
                {{ __('labels.date-registered') }}
              </div>
              <div class="th">
                {{ __('labels.current-plan') }}
              </div>
              <div class="th">
                {{ __('labels.last-activity') }}
              </div>
              <div class="th"></div>
              <div class="th"></div>
              <div class="th">
                {{ __('labels.status') }}
              </div>
            </div>
          </div>
          <div class="tbody">
            @if($corpDataCount > 0)
            @foreach ($corpData as $key => $value)
            <div class="tr">
              <div class="td">
                {{ $value->company_name }}
              </div>
              <div class="td">
                {{ date('M. d Y', strtotime($value->company_date_registered)) }}
              </div>
              <div class="td">
                {{ $value->company_current_plan }}
              </div>
              <div class="td">
                  <label title="{{ \Carbon\Carbon::parse($value->updated_at)->format('m-d-Y g:i A') }}">{{ \Carbon\Carbon::parse($value->updated_at)->diffForHumans() }}</label>
              </div>
              <div class="td">
                 <a href="{{url('admin/companies')}}/{{$value->company_id}}" class="btn">
                  {{ __('labels.view-company-details') }}
                </a>
              </div>
              <div class="td">
                <a class="btn alt-btn" href="{{url('admin/companies/contact')}}/{{$value->company_id}}">
                  Contact
                </a>
              </div>
              <div class="td">
                @if($value->company_status == 'ACTIVE')
                  <a href="javascript:void(0)" class="btn green status">
                @elseif($value->company_status == 'VERIFIED')
                  <a href="javascript:void(0)" class="btn blue status">
                @elseif($value->company_status == 'INACTIVE')
                  <a href="javascript:void(0)" class="btn red status">
                @elseif($value->company_status == 'UNVERIFIED')
                  <a href="javascript:void(0)" class="btn yellow status">
                @elseif($value->company_status == 'DISABLED')
                  <a href="javascript:void(0)" class="btn black status">
                @else
                  <a href="javascript:void(0)" class="btn red status">
                @endif
                  {{$value->company_status}}</a>
              </div>
            </div>
            @endforeach
             @else
                </div>
                </div>
                <div id="noRecordFound">
                  <p>No Records Found</p>
                </div>
                @endif
          </div>
        </div>
      </div>
  <center>{{  $corpData->render() }}</center>
    </div>
  </form>
</main>

<!-- Post Scripts -->
    
<!-- Scripts -->
@include('admin.common.footer')   
@endsection
