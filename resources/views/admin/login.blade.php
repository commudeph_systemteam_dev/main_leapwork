@include('admin.common.html-head')

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/login.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/new-login.css')}}" />

  <!-- title -->
  <title>Job Search Login | LEAP Work</title>

</head>
<body>
  <a href="{{url('/')}}" id="main__pageBtn">Back to Main Page</a>
  <main id="login-view">
    <figure class="logo">
      <img src="{{url('images/common/icon/header-logo.svg')}}" alt="">
    </figure>
    <div class="panel">
      <div class="inner">
        <p class="service-title">
          Job Search
        </p>
        <form method="POST" action="{{url('/loginUser')}}">
        {{ csrf_field() }}
          <ul class="list">
            <li class="item">
              <input type="email" name="email" id="email" placeholder="Username">
            </li>
            <li class="item">
              <input type="password" name="password" id="password" placeholder="Password">
            </li>
            @if ($errors->has('msg'))
                <span class="help-block">
                    <strong>{{ $errors->first('msg') }}</strong>
                </span>
            @endif>
          </ul>
          <div class="checkbox-wrapper">
            <label>
              <input type="checkbox" name="remember" class="checkbox-input">
              <span for="autoLogin" class="checkbox-parts">Remember Me</span>
            </label>
          </div>
          <div class="menu">
            <button type="submit" class="btn">Login</button>
          </div>

          <div class="regfor">
            <span class="reg">Register</span>
            <span class="for">Forgot Password</span>
          </div>
          <div class="facebook_btn">
            <p class="fb_or">OR</p>
            <a href="{{url('/auth/facebook')}}" class="fb__Btn">Login with Facebook</a>
          </div>

        </form>
      </div>
    </div>
  </main>
</body>
</html>