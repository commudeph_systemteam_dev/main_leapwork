@extends('company.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" />
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/company/profile.js')}}"></script>
@endpush

@section('content')

@if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
@endif

<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company Information 
    </p>
  </header>

  <form method="POST" action="" id="frmCompanyProfile" enctype='multipart/form-data'>
   {{ csrf_field() }}
   <input type="hidden" id="company_id" name="company_id" value="{{ $company->company_id }}" >
   <input type="hidden" id="company_intro_id" name="company_intro_id" value="{{ $companyIntro->company_intro_id }}" >

    <div class="content-inner">
          <aside class="search-menu">
          <ul class="list">
              <li class="item" style="float: right;">
                  <a href="{{url('company/profile/howto/write/company-info')}}" class="btn green">
                      How to write company info
                  </a>
              </li>
          </ul>
          </aside>

      <div class="section contact">
        <div class="table form panel">
          <div class="tbody">

            <div class="tr">
              <div class="td">
                Company Name <span class="required"> required </span>
              </div>
              <div class="td">
                 <input type="text" id="txt_company_name" name="txt_company_name" value="{{ $company->company_name}}" >
                 <span id="spCompanyName" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Website
              </div>
              <div class="td">
                <input type="text" id="txt_company_website" name="txt_company_website" value="{{ $company->company_website}}" >
              </div>
            </div>

             <div class="tr">
              <div class="td">
                Address  <span class="required"> required </span>
              </div>
              <div class="td">
                 <input list="codes" placeholder="Postal Code" id="txt_company_postalcode" name="txt_company_postalcode" value="{{ $company->company_postalcode }} ">
                <datalist id="codes">
                    @foreach(array_keys($arrPhilippines) as $key)
                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                    @endforeach
                </datalist>

                <!-- <input type="text" id="txt_company_postalcode" name="txt_company_postalcode" value=" {{ $company->company_postalcode}}" > -->
                <br>
                <input type="text" id="txt_company_address1" name="txt_company_address1" value=" {{ $company->company_address1}}" >
                <br>
                <input type="text" id="txt_company_address2" name="txt_company_address2" value=" {{ $company->company_address2}}" >

                <span id="spPostalCode" class="help-block required--text">
                    <strong class="text"></strong>
                </span>
                <span id="spAddress1" class="help-block required--text">
                    <strong class="text"></strong>
                </span>

              </div>
            </div>

            <div class="tr">
              <div class="td">
                CEO
              </div>
              <div class="td">
                <input type="text" id="txt_company_ceo" name="txt_company_ceo" value="{{ $company->company_ceo}}" >
                <span id="cpCompanCeo" class="help-block required--text">
                  <strong class="text">CEO is required</strong>
                </span>
              </div>
            </div>

            <div class="tr">
              <div class="td">
                No. of employees
              </div>
              <div class="td">
                 {{ Form::select('so_employee_range_id', 
                                  $employeeRanges,
                                  $company->company_employee_range_id,
                                  ['placeholder' => '- Choose One -'])
                 }}
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Date Founded
              </div>
              <div class="td">
                <input type="text" id="txt_company_date_founded" name="txt_company_date_founded" class="datepicker" required> 
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Industry
              </div>
              <div class="td">
                @for ($i = 0; $i < count($company->company_industries); $i++)
                  {{ Form::select('so_company_industry_id_' . $i, 
                                  $jobIndustries, 
                                  $company->company_industries[$i]->company_industry_id,
                                   ['placeholder' => '- Choose One -'])
                  }}
                  <br>
                @endfor

                @for ($i = count($company->company_industries); $i < 3; $i++)
                  {{ Form::select('so_company_industry_id_' . $i, 
                                   $jobIndustries,
                                   null,
                                   ['placeholder' => '- Choose One -'])
                  }}
                  <br>
                @endfor

               
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Twitter
              </div>
              <div class="td">
               <input type="text" id="txt_company_twitter" name="txt_company_twitter" value=" {{ $company->company_twitter}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Facebook
              </div>
              <div class="td">
              <input type="text" id="txt_company_facebook" name="txt_company_facebook" value=" {{ $company->company_facebook}}" >
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Logo
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <div id="dv_company_logo">
                     @if(!empty($company->company_logo))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                        <a href="#" id="btnRemoveCompanyLogo"> Remove X </a>
                    @endif
                </div>
                <input type="file" id="file_company_logo" name="file_company_logo">
              </div>
            </div>

            <div class="tr">
              <div class="td">
                Company Banner
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag" >
                <div id="dv_company_banner">
                    @if(!empty($company->company_banner))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                        <a href="#" id="btnRemoveCompanyBanner"> Remove X </a>
                    @endif
                </div>
                <input type="file" id="file_company_banner" name="file_company_banner">
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    Introduction
                  </div>
                  <div class="td">
                    <textarea id="txt_company_intro_content" name="txt_company_intro_content" value="{{ $companyIntro->company_intro_content }}" >
                        {{ $companyIntro->company_intro_content }}
                    </textarea>
                  </div>              
             </div>
             <div class="tr">
                  <div class="td">
                      Public Relations
                    </div>
                    <div class="td">
                      <textarea id="txt_company_intro_pr" name="txt_company_intro_pr" value="{{ $companyIntro->company_intro_pr }}">
                          {{ $companyIntro->company_intro_pr }}
                      </textarea>
                    </div>              
              </div>
               <div class="tr">
                  <div class="td">
                      Public Relations (image)
                    </div>
                    <div class="td">                   
                       <input type="hidden" id="txt_company_intro_image_remove_flag" name="txt_company_intro_image_remove_flag" >
                       <div id="dv_company_intro_image">
                             @if(!empty($companyIntro->company_intro_image))
                                <img src='{{url("/storage/uploads/company/company_$company->company_id/$companyIntro->company_intro_image")}}'  id="img_company_logo">
                                <a href="#" id="btnRemoveCompanyIntroImage"> Remove X </a>
                            @endif
                        </div>
                      <input type="file" id="file_company_intro_image" name="file_company_intro_image">
                  </div>              
               </div> 

            
          </div>
        </div>
       <!--  <div class="table form panel">
              <div class="tbody">

                  <div>
                        <div class="tr">
                            <div class="td">
                                Introduction
                              </div>
                              <div class="td">
                                <textarea id="txt_company_intro[]" name="txt_company_intro[]"  disabled>skipped</textarea>
                              </div>              
                        </div>
                         <div class="tr">
                            <div class="td">
                                Public Relations
                              </div>
                              <div class="td">
                                <textarea id="txt_company_intro[]" name="txt_company_intro[]"  disabled>skipped</textarea>
                              </div>              
                        </div>
                         <div class="tr">
                            <div class="td">
                                Public Relations (image)
                              </div>
                              <div class="td">                   
                                <input type="file" id="file_company_pr_img[]" name="file_company_pr_img[]">
                            </div>              
                        </div>
                </div>
              </div>
          </div>
      </div> -->
      <div class="btn-wrapper">
        <a href="{{ url('company/profile') }}" class="btn">
          Back
        </a>
        <button type="button" id="btnEditProfile" class="btn">
          Save
        </button>
      </div>
    </div>
    
  </form>

</main>
@endsection