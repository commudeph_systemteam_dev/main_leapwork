<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- FOR AJAX REQUESTS -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <!-- added js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/validator.js')}}"></script>  
  <script type="text/javascript" src="{{url('/js/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment-timezone.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/moment-timezone-with-data.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/js/notification.js')}}"></script>
  <script type="text/javascript" src="{{asset('/js/messages.js')}}"></script>
  <script type="text/javascript" src="{{asset('/js/admin-notification.js')}}"></script>


  <script type="text/javascript" src="{{url('/js/jquery.cookie.min.js')}}"></script>
  
  <script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
  <script src="{{url('/js/common/lib/pace.min.js')}}"></script>
  <script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>
  <!-- Louie: Insert global js for predefined jquery etc. -->
  <script src="{{url('/js/common/global.js')}}"></script> 
  
  <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
    window.pusherKey =  {!! json_encode(config('app.PUSHER_APP_KEY')) !!};
    //setup the ajax headers
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  </script>
  
  <!-- title -->
  <title> @yield('title') LEAP Work</title>
  
  <!-- js included in admin.common.html-head -->
  <!-- page specific js -->
  @stack('scripts')

  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/jquery/jquery-ui.min.css')}}" />
  <link rel="stylesheet" media="print,screen" href="{{url('css/common.css')}}">
  <link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
  <link rel="stylesheet" media="screen and (max-width: 767px)" href="{{url('css/sp.css')}}">

  

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- page specific css -->
  @stack('styles')

</head>
<body>
    <header id="global-header">
        @include('admin.common.header')
        
    </header>
  <div class="content-wrapper">
    <aside id="sidebar">
        @include('admin.common.sidebar')
      
    </aside>
    @if (Session::has('message'))
      <div class="alert__modal">
          <div class="alert__modal--container">
             <p>{!! Session::get('message') !!}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
          </div>
       </div>
     @endif
    
    <!-- Removed main as templace - each main has custom class -->
    <!-- <main id="master-admin-view" class="content-main"> -->
        @yield('content')
    <!-- </main> -->
  </div>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
</body>
</html>