@extends('admin.layouts.app')

@section('title')
  Company Information |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/companies.css')}}" />
  <!-- Post Scripts -->
    <script type="text/javascript" src="{{asset('js/admin/master-admin/common.js')}}"></script>
<!-- Scripts -->

@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/config/constants.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companies.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/companyprofile.js')}}"></script>

@endpush

@section('content')

@if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
      </div>
    </div>
@endif


@if($errors->any())
  <div class="alert__modal_container">
      <div class="alert__modal_content">
          <a class="modal__close" id="btn_alert_close"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
          <div class="alert__modal_dialog">
              <span class="blue">{{ __('messages.admin.company.update') }}</span>
              @foreach($errors->all() as $error)
                <p>{{$error}}</p>
              @endforeach
          </div>
      </div>
  </div> <!-- .alert__modal_container -->
@endif


<main id="notice-view" class="content-main">

  <header class="pan-area">
    <p class="text">
      Company Information 
    </p>
  </header>

  <form method="POST" id="frmAdminCompanyProfile" enctype='multipart/form-data'>
      {{ csrf_field() }}
      <div class="confirm__modal_container">
          <div class="confirm__modal_content">
              <a class="modal__close" id="btn_close"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
              <div class="confirm__modal_dialog">
                  <p><input type="password" id="txt_password" name="txt_password" placeholder="New Password"></p>
                  <p><input type="password" id="txt_password_confirmation" name="txt_password_confirmation" placeholder="Confirm Password"></p>
                  <span id="spPassword" class="help-block required--text">
                      <strong class="text"></strong>
                  </span>
                  <div class="confirm__modal_btns">
                      <button class="spare_user confirm__btn" id="spare_user">BACK</button>
                      <a class="save_user confirm__btn" id="btnSavePassword">SAVE</a>
                  </div>
              </div>
          </div>
      </div> <!-- .confirm__modal_container -->
      <input type="hidden" id="company_id" name="company_id" value="{{ $company->company_id }}" >
      <input type="hidden" id="company_intro_id" name="company_intro_id" value="{{ $companyIntro->company_intro_id }}" >
      <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}" >
      <input type="hidden" id="change_password_flag" name="change_password_flag" >

    <div class="content-inner edit-view">

      <div class="section contact">
        <fieldset id="myFieldset" disabled>
        <div class="table form panel">
          @if($company->company_status == "UNVERIFIED")
          <div class="tbody">
              <div class="tr">
                  <div class="td">
                    {{__('labels.company-name')}}
                  </div>
                  <div class="td">
                     {{ $company->company_name}}
                  </div>
                  <div class="td"></div>
                  <div class="td"></div>
                </div>
                <div class="tr">
                  <div class="td">
                    {{__('labels.company-website')}}
                  </div>
                  <div class="td">
                    {{ $company->company_website}}
                  </div>
                  <div class="td"></div>
                  <div class="td"></div>
                </div>
                <div class="tr">
                  <div class="td">
                    {{__('labels.contact-name')}}
                  </div>
                  <div class="td">
                    {{ $company->company_contact_person}}
                  </div>
                  <div class="td"></div>
                  <div class="td"></div>
                </div>
                <div class="tr">
                  <div class="td">
                    {{__('labels.contact-number')}}
                  </div>
                  <div class="td">
                    {{ $company->company_contact_no }}
                  </div>
                  <div class="td"></div>
                  <div class="td"></div>
                </div>
                <div class="tr">
                  <div class="td">
                    {{__('labels.email-address')}}
                  </div>
                  <div class="td">
                    {{ $company->company_email}}
                  </div>
                  <div class="td"></div>
                  <div class="td"></div>
                </div>
                <div class="tr create__address">
                    <div class="td">
                      {{__('labels.address')}}
                    </div> 
                    <div class="td">
                      {{$company->company_postalcode}}
                      <br>
                      {{$company->company_address1}}
                      <br>
                      {{$company->company_address2}}
                    </div>
                    <div class="td"></div>
                    <div class="td"></div>
                </div>
            <div class="tr">
                <div class="td">
                    {{__('labels.business-permits')}}
                   <br>
                   <span class="sub__text">{{__('labels.upload-business-permit-example')}}</span>
                </div>
                <div class="td">
                    <div class="dv_company_files"  style="display: inline; width: 300px;">
                        <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file1']}}" class="js-file_link">
                          {{ $company["company_file1"] }}
                        </a>
                    </div>
                </div>
                <div class="td">
                    <div class="dv_company_files"  style="display: inline; width: 300px;">
                        <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file2']}}" class="js-file_link">
                          {{ $company["company_file2"] }}
                        </a>
                    </div>
                </div>
                <div class="td">
                    <div class="dv_company_files"  style="display: inline; width: 300px;">
                      <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file3']}}" class="js-file_link">
                        {{ $company["company_file3"] }}
                      </a>
                    </div>
                </div>
            </div>
          </div>
          @else
          <div class="tbody">
            <div class="tr">
              <div class="td">
                {{__('labels.company-name')}} <span class="required">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                 <input type="text" id="txt_company_name" name="txt_company_name" value="{{ $company->company_name}}" >
                 <span id="spCompanyName" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.company-website')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_website" name="txt_company_website" value="{{ $company->company_website}}" >
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.contact-name')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_person" name="txt_company_contact_person" value="{{ $company->company_contact_person}}" >
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.contact-number')}} <span class="required">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_contact_no" name="txt_company_contact_no" value="{{ $company->company_contact_no }}" >
                <span id="spContactNo" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.email-address')}}  <span class="required">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                <input type="text" id="txt_company_email" name="txt_company_email" value="{{ $company->company_email}}" >
                <span id="spEmail" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
              </div>
            </div>
            @if($company->company_status != "ARCHIVED")
            <div class="tr" id="password_pannel" style="display: none;">
              <div class="td">
                {{__('labels.password')}} <span class="required hidden">{{__('labels.required')}}</span>
              </div>
              <div class="td">
                  <a onclick="return changePassword(this.getAttribute('data-href'))" class="btn">{{__('labels.change-password')}}</a>              
                {{--  <div id="dvChangePassword" class="hidden">
                    <span id="btnCancelChangePassword"> X </span>
                        <span id="spPassword" class="help-block required--text">
                     <strong class="text"></strong>
                </span>
                </div>  --}}
              </div>
            </div>              
            @endif
             <div class="tr create__address">
                <div class="td">
                  {{__('labels.address')}} <span class="required">{{__('labels.required')}}</span>
                </div> 
                <div class="td">
                  <input list="codes" value="{{$company->company_postalcode}}" id="txt_company_postalcode" name="txt_company_postalcode" class="short_input ib">
                    <datalist id="codes">
                        @foreach(array_keys($arrPhilippines) as $key)
                            <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                        @endforeach
                    </datalist>
                  <input type="text"  id="txt_company_address1" name="txt_company_address1" value="{{$company->company_address1}}"> 
                  <br><br>
                  <input type="text"  id="txt_company_address2" name="txt_company_address2" value="{{$company->company_address2}}"> 
                  <span class="help-block required--text hide" id="postalCodeErr">
                      <strong>{{__('labels.postal-code')}} is {{__('labels.required')}}</strong>
                  </span>
                  <span class="help-block required--text hide" id="address1Err">
                      <strong>{{__('labels.address-1')}} is {{__('labels.required')}}</strong>
                  </span>
                  <span class="help-block required--text hide" id="address2Err">
                      <strong>{{__('labels.address-2')}} is {{__('labels.required')}}</strong>
                  </span>
                </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.ceo')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_ceo" name="txt_company_ceo" value="{{ $company->company_ceo}}" >
                <span id="spCompanCeo" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.number-of-employees')}}
              </div>
              <div class="td">
                 {{ Form::select('so_employee_range_id', 
                                  $employeeRanges,
                                  $company->company_employee_range_id,
                                  ['placeholder' => __('labels.choose-one')])
                 }}
              </div>
            </div>
            <div class="tr">
              <div class="td">
               {{__('labels.date-founded')}}
              </div>
              <div class="td">
                <input type="text" id="txt_company_date_founded" name="txt_company_date_founded" class="datepicker" value="@if(!is_null($company->company_date_founded)){{\Carbon\Carbon::parse($company->company_date_founded)->format('m/d/Y')}}@endif">
                 <span id="spDateFounded" class="help-block required--text">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.industry')}}
              </div>
              <div class="td">
                @for ($i = 0; $i < count($company->company_industries); $i++)
                  {{ Form::select('so_company_industry_id[]', 
                                  $jobIndustries, 
                                  $company->company_industries[$i]->company_industry_master_job_industry_id,
                                   ['placeholder' => __('labels.choose-one')])
                  }}
                  <br><br>
                @endfor

                @for ($i = count($company->company_industries); $i < 3; $i++)
                  {{ Form::select('so_company_industry_id[]', 
                                   $jobIndustries,
                                   null,
                                   ['placeholder' => __('labels.choose-one')])
                  }}
                  <br><br>
                @endfor

              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.twitter')}}
              </div>
              <div class="td">
               <input type="text" id="txt_company_twitter" name="txt_company_twitter" value=" {{ $company->company_twitter}}" >
              </div>
            </div>
            <div class="tr">
              <div class="td">
                 {{__('labels.facebook')}}
              </div>
              <div class="td">
              <input type="text" id="txt_company_facebook" name="txt_company_facebook" value=" {{ $company->company_facebook}}" >
              </div>
            </div>
            <div class="tr">
              <div class="td">
                {{__('labels.company-logo')}}
                <p class="required--text red" id="image-size-label">(Please upload 1:1)</p>
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_logo_remove_flag" name="txt_company_logo_remove_flag" >
                <span class="js-company_logo-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_logo">
                     @if(!empty($company->company_logo))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_logo")}}'  id="img_company_logo">
                        <a href="#" id="btnRemoveCompanyLogo"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                    @endif
                </div>
                <input type="file" id="file_company_logo" name="file_company_logo">
                <span id="spCompanyLogo" class="help-block required--text js-company_logo-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>
            <div class="tr">
              <div class="td">
                 {{__('labels.company-banner')}}
                 <p class="+ required--text red" id="image-size-label">(Please upload 16:4)</p>
              </div>
              <div class="td">
                <input type="hidden" id="txt_company_banner_remove_flag" name="txt_company_banner_remove_flag" >
                <span class="js-company_banner-error_list" data-error-format="Allowed file types: gif, jpg, jpeg, png. Each File size should be 10MB maximum."/>
                <div id="dv_company_logo">
                    @if(!empty($company->company_banner))
                        <img src='{{url("/storage/uploads/company/company_$company->company_id/$company->company_banner")}}' id="img_company_banner" />
                        <a href="#" id="btnRemoveCompanyBanner"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                    @endif
                </div>
                <input type="file" id="file_company_banner" name="file_company_banner">
                <span id="spCompanyBanner" class="help-block required--text js-company_banner-error">
                  <strong class="text"></strong>
                </span>
              </div>
            </div>

             <div class="tr">
                <div class="td">
                    {{__('labels.introduction')}}
                  </div>
                  <div class="td">
                    <textarea id="txt_company_intro_content" name="txt_company_intro_content" value="{{ $companyIntro->company_intro_content }}" >{{ $companyIntro->company_intro_content }}</textarea>
                  </div>              
             </div>

             <div class="tr">
                  <div class="td">
                      {{__('labels.public-relations')}}
                    </div>
                    <div class="td">
                      <textarea id="txt_company_intro_pr" name="txt_company_intro_pr" value="{{ $companyIntro->company_intro_pr }}">{{ $companyIntro->company_intro_pr }}</textarea>
                    </div>              
              </div>

              <div class="tr">
                    <div class="td">
                         {{__('labels.business-permits')}}
                        <br>
                        <span class="sub__text">{{__('labels.upload-business-permit-example')}}</span>
                    </div>
                    <div class="td">
                      <input type="hidden" id="txt_company_file_remove_flag" name="txt_company_file_remove_flag" > 
                      <span class="js-company_file-error_list" data-error-format="Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum." data-error-required="You are required to add at least one attachment" data-error-duplicate="Cannot upload 2 files of the same name" data-error-filesize="Cannot upload a file greater than 10mb"/>
                         
                      @if($company["company_file1"] != '')
                      <input type="file" id="companyFile1" name="companyFile1" class="js-company_file"  style="display: none; width: 300px;"/>
                          <div class="dv_company_files"  style="display: inline; width: 300px;">
                              <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file1']}}" class="js-file_link">
                                  {{ $company["company_file1"] }}
                              </a>
                              <a href="#" data-id="1" id="companyFile1CloseIcon" class="btnRemoveCompanyFile"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                          </div>
                      @else
                      <input type="file" id="companyFile1" name="companyFile1" class="js-company_file"  style="display: inline; width: 300px;"/>
                      @endif
                      <br/><br/>

                      @if($company["company_file2"] != '')
                      <input type="file" id="companyFile2" name="companyFile2" class="js-company_file" style="display: none; width: 300px;"/>
                           <div class="dv_company_files"  style="display: inline; width: 300px;">
                              <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file2']}}" class="js-file_link">
                                  {{ $company["company_file2"] }}
                              </a>
                              <a href="#" data-id="2" id="companyFile2CloseIcon" class="btnRemoveCompanyFile"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                          </div>
                      @else
                      <input type="file" id="companyFile2" name="companyFile2" class="js-company_file"  style="display: inline; width: 300px;"/>
                      @endif
                      <br/><br/>

                       @if($company["company_file3"] != '')
                      <input type="file" id="companyFile3" name="companyFile3" class="js-company_file" style="display: none; width: 300px;"/>
                          <div class="dv_company_files"  style="display: inline; width: 300px;">
                              <a target="_blank" href="{{url('/storage/uploads/company/company_')}}{{$company->company_id}}/{{$company['company_file3']}}" class="js-file_link">
                                  {{ $company["company_file3"] }}
                              </a>
                              <a href="#" data-id="3" id="companyFile3CloseIcon" class="btnRemoveCompanyFile"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                          </div>
                      @else
                      <input type="file" id="companyFile3" name="companyFile3" class="js-company_file"  style="display: inline; width: 300px;"/>
                      @endif
                       <br/><br/>

                      <br/><br/>
                      <span class="help-block required--text js-company_file-error" id="sp-company_file-error">
                        <strong id="company_file-error_message">
                            <!-- Allowed file types: pdf,doc,docx.Each File size should be 10MB maximum. -->
                        </strong>
                      </span>

                      <!-- <span id="spBusinessPermitMain" class="sub__text">* {{__('labels.max-upload')}}</span> -->
                      <span id="spBusinessPermit" class="help-block required--text">
                        <strong class="text"></strong>
                      </span>
                  </div>            
               </div> 
               @foreach($unissuedClaims as $claim)
                 <div class="tr">
                      <div class="td">
                         {{__('labels.unissued-unpaid-claim')}}
                      </div>
                      <div class="td">
                          <a href="{{url('/admin/billing/detail')}}/{{ $company->company_id}}" class="btn">{{__('labels.view-billing')}}</a>
                      </div>
                  </div>
                 @endforeach
                  
              <div class="tr">
                  <div class="td">
                      {{__('labels.current-plan')}}
                  </div>
                  <div class="td">
                      {{  $company->company_current_plan }}
                  </div>
              </div>
                  
              {{-- <div class="tr">
                    <div class="td">
                        {{__('labels.company-status')}}
                    </div>
                    <div class="td">
                        @if( $company->company_status == "UNVERIFIED" )
                           <a href="#" id="btn-verify" class="btn"> 
                              Verify 
                          </a>
                        @elseif( $company->company_status == "ARCHIVED" )
                            <span class="red">{{ $company->company_status }}</span>
                        @else 
                          {{ Form::select('company_status', 
                                          config('constants.'.Session::get('locale').'_companyStatus'), 
                                          $company->company_status)  
                          }}
                        @endif
                    </div>
                </div>  --}}
          </div>
          @endif
      </div>
    </fieldset>
      <div class="btn-wrapper cf">
        @if($company->company_status == "UNVERIFIED")
          <a id="btn-deny" class="btn alt-btn">
            {{__('labels.deny')}}
          </a>
          <a href="#" class="btn" id="btn-verify">{{__('labels.approve')}}</a>
        @elseif($company->company_status == "ARCHIVED")
          <a href="{{ url('admin/companies') }}" class="btn alt-btn">
            {{__('labels.back')}}
          </a>
        @else
          <a href="{{ url('admin/companies') }}" class="btn alt-btn">
            {{__('labels.back')}}
          </a>
          <a href="#" class="btn" id="btnEditCompany">{{__('labels.edit')}}</a>
          <a href="#" class="btn" id="btnSaveCompany">{{__('labels.save')}}</a>
        @endif
      </div>
      

    </div>
    
  </form>

</main>
@endsection

