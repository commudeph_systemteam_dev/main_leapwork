@include('admin.common.html-head')

<!-- css -->
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
<link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/common.css')}}" />
<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/ranking.css')}}" />
<!-- title -->

<title>Ranking | LEAP Work</title>
                
@section('og-url',url('/job/details'.'/'.$job->job_post_id))
@section('og-title',$job->job_post_title)
@section('og-description',$job->job_post_description)

</head>

<body>
    <header id="global-header">
        @include('admin.common.header')
    </header>

    <div class="content-wrapper">
        <aside id="sidebar">
            @include('admin.common.sidebar')
        </aside>
        <form method="POST" accept="{{url('admin/ranking')}}">
            {{ csrf_field() }}
            <main id="ranking-view" class="content-main">
                <header class="pan-area">
                    <p class="text">
                        Job Post Ranking
                    </p>
                </header>

                <!-- @ if(isset($job->job_post_image1))
                    @ php
                        Image::make('./storage/uploads/job-posts/'.$job->job_post_image1)
                            ->resize(1200,630)
                            ->save('./storage/uploads/job-posts/'.'resized_'.$job->job_post_image1)
                    @ endphp
                    @ section('og-image',url('/storage/uploads/job-posts/'.'resized_'.$job->job_post_image1))
                @ endif -->
                {{ csrf_field() }}
                <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $job_post_id }}">
                <input type="hidden" id="job_post_resume_flag" name="job_post_resume_flag" >

            <div class="content job__details details_size">
                

                 <div class="feature__image clearfix">
                        <div>
                            <img src="{{ !is_null($job->job_post_key_pc) 
                                ?   url('/storage/uploads/job-posts/'.$job->job_post_key_pc)
                                :   asset('images/top_noimage_1200x400.jpg')}}" alt="">
                        </div>
                        {{-- <div>
                            <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_key_sp) }}" alt="">
                        </div> --}}
                    </div> 
                    <div class="jobdetails__content">
                        <h1>{{$job->job_post_title}}</h1>
                        <div class="jobdetails__information">
                            <h3>Business Contents</h3>
                            <p>{{$job->job_post_business_contents}}</p>
                            <h3>Job Description</h3>
                            <p>{{$job->job_post_description}}</p>
                            @if(!is_null($job->job_post_image1) && !is_null($job->job_post_image2))
                            <div class="top_latestjobs_imgs clearfix">
                            <div class="two">
                                <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image1) }}" alt="">
                            </div>
                            <div class="two">
                                <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image2) }}" alt="">
                            </div>
                            </div>
                            @elseif(!is_null($job->job_post_image1) && is_null($job->job_post_image2))
                                <div class="top_latestjobs_imgs clearfix">
                                <div class="one">
                                    <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image1) }}" alt="">
                                </div>
                                </div>
                            @elseif(is_null($job->job_post_image1) && !is_null($job->job_post_image2))
                                <div class="top_latestjobs_imgs clearfix">
                                <div class="one">
                                    <img src="{{ url('/storage/uploads/job-posts/'.$job->job_post_image2) }}" alt="">
                                </div>
                                </div>
                            @else
                                <div class="top_latestjobs_imgs clearfix">
                                </div>
                            @endif
                            {{-- <h3> &nbsp; </h3> --}}
                            <h3>Work Description</h3>
                            <table class="jobdetails__table">
                                <tr>
                                    <td>Overview</td>
                                    <td> {{$job->job_post_overview}}</td>
                                </tr>
                                <tr>
                                    <td>Qualifications Requirements</td>
                                    <td> {{$job->job_post_qualifications}} </td>
                                </tr>
                                <tr>
                                    <td>Employment Category</td>
                                    <td>{{$jobClassification->master_job_classification_name}}</td>
                                </tr>
                                <tr>
                                    <td>Salary</td>
                                    <td>
                                        @if(Auth::user()["user_accounttype"] != null) 
                                            <span>PHP</span>{{ number_format($job->job_post_salary_min, 2, '.', ',') }} - <span>PHP</span>{{ number_format($job->job_post_salary_max, 2, '.', ',') }}
                                        @else
                                            <a href="{{url('/login')}}"><span>Login to view salary</span></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Vacancy</td>
                                    <td>{{$job->job_post_no_of_positions}} openings</td>
                                </tr>
                                <tr>
                                    <td>Selection Process</td>
                                    <td>{{$job->job_post_process}}</td>
                                </tr>
                                <tr>
                                    <td>Work Location</td>

                                    <td>{{ $company->company_address1 }} <br>{{ $company->company_address2 }} <br> {{$companyLocation->master_job_location_name}} , {{$country->master_country_name}}</td>
                                </tr>
                                <tr>
                                    <td>Working Hours</td>
                                    <td>{{ date('g:ia', strtotime($job->job_post_work_timestart))}} - {{ date('g:ia', strtotime($job->job_post_work_timeend))}}</td>
                                </tr>
                                <tr>
                                    <td>Benefits</td>
                                    <td>{{$job->job_post_benefits}}</td>
                                </tr>
                                <tr>
                                    <td>Holiday / Vacation</td>
                                    <td>{{$job->job_post_holiday}}</td>
                                </tr>
                                <tr>
                                    <td>Characteristics</td>
                                    <td>{{$job->job_post_otherinfo}}</td>
                                </tr>
                            </table>
                        </div><!-- .jobdetails__information -->
                    </div><!-- .jobdetails__content -->

                    <div class="btn-wrapper">
                        @if($currentBlade == 'PopularJobs')
                            <a href="{{url('admin/ranking')}}" class="btn">
                                      Back
                            </a>
                        @else
                            <a href="{{url('admin/master-admin')}}" class="btn">
                                      Back
                            </a>
                        @endif
                    </div>

                </div>
          </main>
       </form>
     </div>
@include('admin.common.footer')