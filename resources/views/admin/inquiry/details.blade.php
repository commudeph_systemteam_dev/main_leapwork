@extends('admin.layouts.app')

@section('title')
  {{ __('labels.inquiry') }} |
@stop

@push('styles')
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/common.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/admin/contact.css')}}" />

  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/applicant.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/applicant/home.css')}}" />

@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/admin/inquiry.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/admin-inquiry.js')}}"></script>
@endpush

@section('content')

<main id="master-contact-view" class="content-main">
  <header class="pan-area">
      <p class="text">
          {{ __('labels.inquiry-details') }}
      </p>
  </header>

  @if (Session::has('message'))
    <div class="alert__modal">
      <div class="alert__modal--container">
         <p>{{ Session::get('message') }}</p><span>x</span>
      </div>
    </div>
  @endif

  <form method="POST" action="" id="frmCompanyInquiry">
      
        {{ csrf_field() }}
        <input type="hidden" id="company_inquiry_thread_id" name="company_inquiry_thread_id" value="{{ $threadId['company_inquiry_thread_id'] }}">
        <input type="hidden" id="company_inquiry_conversation_message" name="company_inquiry_conversation_message" >

        <ul class="comment_list">
           <!--  <li class="list_title_wrap cf">
                
            </li> -->
            <li class="list_info">
                <div class="detail_contents detail_contents_chat">
                  
                    <div class="tab_area clearfix">
                        <p class="tab tab_recieve_mail active left">{{ __('labels.conversations') }}</p>
                    </div>

                    <div id="dvInbox" class="dvTab">
                        <ul class="chat_list recieve_mail_area">                           
                            <li data-t_id="{{ $inboxMessage->company_inquiry_thread_id }}" id="tb_{{ $inboxMessage->company_inquiry_thread_id }}" class="cloesed_chat">
                                <p class="clearfix">
                                    <time class="left"> 
                                        {{ date('Y.m.d', strtotime($inboxMessage['lastMessage']->company_inquiry_conversation_datecreated)) }} 
                                    </time>
                                    <span class="text left">
                                        {{ mb_strimwidth($inboxMessage->company_inquiry_thread_title, 0, 50, "") }}
                                        :
                                        {{ mb_strimwidth($inboxMessage['lastMessage']->company_inquiry_conversation_message, 0, 100, "...") }}
                                    </span>
                                </p>
                                <figure class="btn_pulldown">
                                    <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                    <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                </figure>
                            </li>

                            <li id="message_area_{{ $inboxMessage->company_inquiry_thread_id }}" class="opened_chat">
                                <div class="messages_container">
                                    @php
                                        $day="";
                                    @endphp
                                    @foreach($inboxMessage->company_inquiry_conversations as $key => $companyInquiryConversation)
                                    @if(is_null($day) 
                                        || (\Carbon\Carbon::parse($companyInquiryConversation->company_inquiry_conversation_datecreated)->format('m-d-Y')
                                        != $day ))
                                        @php
                                            $day = \Carbon\Carbon::parse($companyInquiryConversation->company_inquiry_conversation_datecreated)->format('m-d-Y');
                                        @endphp
                                        <hr>
                                        <span style="font-size:12px; text-align:center;display:block;">
                                            @if($day == \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m-d-Y'))
                                                Today
                                            @elseif($day == \Carbon\Carbon::parse(\Carbon\Carbon::yesterday())->format('m-d-Y'))
                                                Yesterday
                                            @else
                                                {{ \Carbon\Carbon::parse($companyInquiryConversation->company_inquiry_conversation_datecreated)->format('M j, Y') }}
                                            @endif
                                        </span>
                                    @endif
                                        <div class="opened_chat_inner opened_chat_recieve">
                                            <div class="chat_area layout_2clm clearfix">
                                                <figure class="icon {{ ($companyInquiryConversation->company_inquiry_conversation_user_id != $userId) 
                                                ? 'left'
                                                : 'right'}}">
                                                    <img src="{{ $companyLogo[$key] }}" alt="">
                                                </figure>
                                                  <div class="icon__username">
                                                      {{ $completeUserName[$key] }}
                                                      <span class="message__date"> {{ date('M j, Y h:i A', strtotime($companyInquiryConversation->company_inquiry_conversation_datecreated)) }}</span>
                                                  </div>
                                                <!-- <div> -->
                                                  <div class="message_contents {{ ($companyInquiryConversation->company_inquiry_conversation_user_id != $userId) 
                                                  ? 'left'
                                                  : 'right'}}">
                                                      {{$companyInquiryConversation->company_inquiry_conversation_message}}
                                                  </div>
                                                <!-- </div> -->
                                                  
                                            </div>                                             
                                        </div>
                                    @endforeach    
                                </div>
                                <div class="reply_container">
                                    <div class="opened_chat_inner opened_chat_send">
                                        <div class="form_area">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            <h3 class="sub_title"><!-- {{ __('labels.reply') }} --></h3>
                                                        </th>
                                                        <td>
                                                            <textarea class="ctxtInput" placeholder="Type a Message..."></textarea>
                                                             <span class="help-block required--text spErrorMessage hidden">
                                                                 <strong class="text">Please enter a message</strong>
                                                            </span><br>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <div class="layout_1clm ta_c">
                                                <div class="dvMainButtons">
                                                    <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->company_inquiry_thread_id }}">{{ __('labels.reply') }}</a>
                                                </div>
                                                <div class="dvConfirmButtons hidden" style="display:none;">
                                                    <a class="btnBackReinput btn_submit btn ib">{{ __('labels.back') }}</a>
                                                    <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->company_inquiry_thread_id }}">{{ __('labels.confirm') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- dvInbox -->

            </div><!-- detail_contents --> 
        </li> <!-- list_info -->
      </ul>


      <div class="layout_1clm ta_c">
          <a href="{{ url('/admin/inquiries') }}" class="btn_submit btn ib alt-btn"> {{ __('labels.back') }} </a>
      </div>

  </form>

</main>

<!-- Post Scripts. This should've been global :(-->
  @include('admin.common.footer') 
<!-- Scripts -->

@endsection