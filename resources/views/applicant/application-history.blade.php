@extends('applicant.layouts.app')

@section('title')
  Application History |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/applicant/application-history.js')}}"></script>
@endpush

@section('bodyClass')
  mypageApplications-history
@stop

@section('content')

<main class="content-main right">

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
        </div>
      </div>
    @endif

    <form method="GET" id="frmApplicationHistory" >
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $userId }}">

        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">{{ __('labels.application-history') }}</h1>
                <div class="list_sort">
                    {{ Form::select('so_job_application_status', 
                                     $jobApplicationStatus,
                                     old('so_job_application_status'),
                                    ['id'          => 'so_job_application_status',
                                     'placeholder' =>  __('labels.all') , 
                                      'class'      => 'apphistory__select'])   
                     }}
                </div>
            </li>

             @foreach($applicationHistories as $applicationHistory)

                 <li class="list_info">
                     <a href="{{ url('/applicant/application-history/details') 
                                        . '/' 
                                        . $applicationHistory['job_application_id'] }}" 
                        class="link_post db">
                         <h2 class="list_info_title"> 
                             {{ $applicationHistory['job_post_title'] }} 
                         </h2>

                         <div class="inner cf">
                              <h3 class="office_name"> 
                                  {{ $applicationHistory['job_post_company_name'] }} 
                              </h3>

                              <div class="info">
                                  <p class="info_01 ib">
                                      {{ __('labels.location') }} 
                                      : 
                                      {{ $applicationHistory['job_post_location'] }}
                                  </p>
                                  <p class="info_02 ib">
                                      {{ __('labels.salary') }} 
                                      : 
                                      {{ $applicationHistory['job_post_salary'] }}
                                  </p>
                              </div>

                              @if($applicationHistory['job_post_status'] == 'EXPIRED')
                                <p class="label red ta_c"> 
                                    EXPIRED
                                </p>
                              @else
                                <p class="{{ $applicationHistory['job_application_status_css'] }}"> 
                                    {{ $applicationHistory['job_application_status'] }}
                                </p>
                              
                                <div class="{{ $applicationHistory['job_application_mail_css'] }}"></div>
                              @endif
                              
                          </div>

                        </a>
                  </li>
            @endforeach
          
          <!-- resources/views/vendors/pagination/custom.blade.php -->
          <center>{{ $applicationHistories->links('vendor.pagination.custom') }}</center>

          </ul>
      </form>

      @if(count($applicationHistories) < 1)
          <div class="no-records"><p> {{ __('labels.no-job-applications') }} </p></div>
      @endif

</main><!-- content-main -->    

@endsection