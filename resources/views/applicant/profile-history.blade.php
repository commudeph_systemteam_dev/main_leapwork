@extends('applicant.layouts.app')

@section('title')
  Profile History |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
  <style>
      .required-empty {
        border-color: red !important;
    }
  </style>
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/applicant/applicant.js')}}"></script>
@endpush

@section('bodyClass')
  mypageResume
@stop

@section('content')
    <form method="POST" id="frmProfileHistory" enctype='multipart/form-data'>

          {{ csrf_field() }}
          <input type="hidden" id="user_id" name="user_id" value="{{ $applicantProfile->applicant_profile_user_id }}">
          <input type="hidden" id="applicant_profile_id" name="applicant_profile_id" value="{{ $applicantProfile->applicant_profile_id }}">
          <input type="hidden" id="job_post_id" name="job_post_id" value="{{ $jobPost['jobPostId'] }}">
          <input type="hidden" id="job_post_resume_flag" name="job_post_resume_flag" value="{{  $jobPost['jobPostResumeFlag'] }}">
          <input type="hidden" id="applicant_resume" name="applicant_resume" value="{{ $applicantProfile->applicant_profile_resumepath }}">


          <main class="content-main right">
            @if (Session::has('message'))
              <div class="alert__modal">
                <div class="alert__modal--container">
                   <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
                </div>
              </div>
            @endif
            
              <ul class="comment_list">
                <li class="list_title_wrap cf">
                    <h1 class="list_title">
                         {{ __('labels.profile') }} > {{ __('labels.resume') }}
                    </h1>
                </li>


                <li class="list_info form_area">
                        <fieldset id="myFieldset" disabled>
                    <form>
                        <table>
                            <tbody>
                                    
                                <tr>
                                    <th class="clearfix">{{ __('labels.education') }} <span class="required ib">{{ __('labels.required') }}</span></th>
                                    <td>
                                        <div id="dv_college_input"> 
                                            <input type="hidden" id="applicant_education_id_del" name="applicant_education_id_del" />
                                            @for($i=0; $i<count($applicantProfile->applicant_education); $i++)
                                            <div id="dv_college" class="dv_college_entry cf">
                                                <input type="hidden" id="applicant_education_id[]" name="applicant_education_id[]" value="{{ $applicantProfile->applicant_education[$i]->applicant_education_id }}" />
                                                <div class="dvRemoveCollege"> 
                                                @if($i > 0)
                                                    <button type="button" class="btnRemoveCollegeDiv btnRemove hiddenIcon">X</button> 
                                                @endif
                                                </div>

                                            {{ Form::select('so_applicant_education_year_passed[]', 
                                                        $soYears, 
                                                        $applicantProfile->applicant_education[$i]->applicant_education_year_passed,
                                                        ['placeholder' =>'- ' . __('labels.year') . ' -', 'class' => 'db left three_clm_input'])   }}
                                              {{ Form::select('so_applicant_education_year_month[]', 
                                                        $soMonths, 
                                                        $applicantProfile->applicant_education[$i]->applicant_education_year_month,
                                                        ['placeholder' =>'- ' . __('labels.month') . ' -', 'class' => 'db left three_clm_input'])   }}

                                              {{ Form::select('so_applicant_education_status[]', 
                                                        $soCollegeStatus, 
                                                        $applicantProfile->applicant_education[$i]->applicant_education_status,
                                                        ['placeholder' =>'- ' . __('labels.choose-one') . ' -', 'class' => 'db left three_clm_input'])   }}
                                              <br><br>
                                              <input type="text" id="txt_applicant_education_school[]" name="txt_applicant_education_school[]" placeholder="School Name" value="{{ $applicantProfile->applicant_education[$i]->applicant_education_school }}" class="db left three_clm_input">
                                              
                                               {{ Form::select('so_applicant_education_qualification_id[]', 
                                                        $masterQualifications, 
                                                        $applicantProfile->applicant_education[$i]->applicant_education_qualification_id,
                                                        ['placeholder' =>'- ' . __('labels.choose-one') . ' -', 'class' => 'db left three_clm_input'])   }}

                                              <input type="text"  id="txt_applicant_education_course[]" name="txt_applicant_education_course[]"  placeholder="Course Name" value="{{ $applicantProfile->applicant_education[$i]->applicant_education_course }}" class="db left three_clm_input">

                                              <br><br>

                                      </div>
                                      
                                      @endfor
                                        <span id="spEducation" class="help-block required--text">
                                          <strong class="text"></strong>
                                        </span><br>

                                      <a class="btn__add" id="btnAddCollegeDiv">{{ __('labels.add-more') }}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="clearfix">{{ __('labels.work') }}<br>{{ __('labels.experience') }} <span class="required ib right">{{ __('labels.required') }}</span></th>
                                    <td>
                                       
                                      <div id="dv_workexp_input"> 
                                          <input type="hidden" id="applicant_workexp_id_del" name="applicant_workexp_id_del" />
                                           @for($i=0; $i<count($applicantProfile->applicant_workexperiences); $i++)
                                              <div id="dv_workexp" class="dv_workexp_entry">
                                                <input type="hidden" id="applicant_workexp_id[]" name="applicant_workexp_id[]" value="{{ $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_id }}" />

                                                 <div class="dvRemoveWorkexp"> 
                                                  @if($i > 0)
                                                    <button type="button" class="btnRemoveWorkexpDiv btnRemove hiddenIcon">X</button> 
                                                  @endif
                                                </div>

                                                <table class="inner_table">
                                                  <tbody>
                                                      <tr>
                                                        <th>Company</th>
                                                        <td>
                                                        <input type="text" id="txt_applicant_workexp_company_name[]" name="txt_applicant_workexp_company_name[]" value="{{ $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_company_name }}" class="long_input ib">
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <th>Duration</th>
                                                        <td class="clearfix">
                                                          <div class="left_area left clearfix">
                                                              {{ Form::select('so_applicant_workexp_datefrom_year[]', 
                                                                        $soYears, 
                                                                        date('Y', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_datefrom)),
                                                                        ['placeholder' =>'- ' . __('labels.year') . ' -', 'class' => 'cSoYearFrom left middle_text ta_c'])   }}
                                                    
                                                              {{ Form::select('so_applicant_workexp_datefrom_month[]', 
                                                                        $soMonths, 
                                                                        date('n', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_datefrom)),
                                                                        ['placeholder' =>'- ' . __('labels.month') . ' -', 'class' => 'cSoMonthFrom left middle_text ta_c'])   }}
                                                          </div>
                                                          <div class="left middle_text ta_c in__between">〜</div>
                                                          <div class="right_area right clearfix">
                                                              {{ Form::select('so_applicant_workexp_dateto_year[]', 
                                                                        $soYears, 
                                                                        date('Y', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto)),
                                                                        [
                                                                            'placeholder' =>'- ' . __('labels.year') . ' -', 
                                                                            'class' => $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto === '0000-00-00' 
                                                                                ? 'cSoYearTo left middle_text ta_c hidden'
                                                                                : 'cSoYearTo left middle_text ta_c',
                                                                        ])   
                                                                }}
                                                    
                                                              {{ Form::select('so_applicant_workexp_dateto_month[]', 
                                                                        $soMonths, 
                                                                        date('n', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto)),
                                                                        [
                                                                            'placeholder' =>'- ' . __('labels.month') . ' -', 
                                                                            'class' => $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto === '0000-00-00' 
                                                                                ? 'cSoMonthTo left middle_text ta_c hidden'
                                                                                : 'cSoMonthTo left middle_text ta_c',

                                                                        ])   
                                                                }}

                                                                <input type="hidden" name="present_checkbox[]" id="present_checkbox" value="{{$applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto === '0000-00-00' ? 'on' : 'off' }}">
                                                                <input class="present-checkbox" type="checkbox" id="present_checkbox" onclick="presentButtonClickEvent(this);" {{$applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto === '0000-00-00' ? 'checked' : '' }} >Present
                                                            </div>
                                                        </td>
                                                      </tr>
                                                     <tr>
                                                        <th>Job <br>Description</th>
                                                        <td>
                                                            <textarea id="txt_applicant_workexp_past_job_description[]" name="txt_applicant_workexp_past_job_description[]" cols="30" rows="10">{{ $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_past_job_description }}</textarea>
                                                        </td>
                                                      </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                          @endfor
                                          <span id="spWorkexp" class="help-block required--text">
                                              <strong class="text"></strong>
                                            </span>
                                          <br/>
                                          <a class="btn__add" id="btnAddWorkexpDiv">{{ __('labels.add-more') }}</a>
                                     
                                        </div>   
                                    </td>
                                </tr>
                                <tr>
                                    <th class="clearfix">{{ __('labels.job-career') }}<span class="required ib right"> {{ __('labels.required') }}</span></th>
                                    <td>
                                        <textarea id="txt_applicant_profile_objective" name="txt_applicant_profile_objective" cols="30" rows="10">{{ $applicantProfile->applicant_profile_objective }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th>{{ __('labels.skill') }}</th>
                                    <td>
                                        <table class="inner_table">
                                            <tbody>
                                                <tr>
                                                    <th>
                                                      <div class="skill_language_group">
                                                        <div class="skill_language">
                                                          {{ __('labels.language') }}
                                                          <br>
                                                        </div>
                                                        <div class="skill_qualification">
                                                          {{ __('labels.qualification') }}
                                                        </div>
                                                       
                                                      </div>
                                                    </th>
                                                    <td>
                                                        <div id="dv_communication_input">
                                                            <input type="hidden" id="applicant_communication_id_del" name="applicant_communication_id_del" />
                                                            @for($i=0; $i<count($applicantProfile->applicant_skills_communication); $i++)
                                                                <div id="dv_communication" class="dv_communication_entry">
                                                                
                                                                    <div class="dvRemoveCommunication"> 
                                                                        @if($i > 0)
                                                                        <button type="button" class="btnRemoveCommunicationDiv btnRemove hiddenIcon">X</button>
                                                                        @endif
                                                                    </div>

                                                                    <input type="text" id="txt_applicant_communication_name[]" name="txt_applicant_communication_name[]" value="{{ $applicantProfile->applicant_skills_communication[$i]->applicant_skill_name }}" class="ctxtCommunication long_input ib mt10" placeholder="Language">

                                                                    <input type="text" id="txt_applicant_communication_level[]" name="txt_applicant_communication_level[]" value="{{ $applicantProfile->applicant_skills_communication[$i]->applicant_skill_level }}" class="ctxtQualification long_input ib mt10" placeholder="Qualification" >

                                                                    <input type="hidden" id="applicant_communication_id[]" name="applicant_communication_id[]" value="{{ $applicantProfile->applicant_skills_communication[$i]->applicant_skill_id }}"  />
                                                                        
                                                                </div>
                                                            @endfor
                                                            <span id="spCommunication" class="help-block required--text">
                                                                <strong class="text"></strong>
                                                            </span>
                                                            <br/>
                                                            <a class="btn__add" id="btnAddCommunicationDiv">{{ __('labels.add-more') }}</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>{{ __('labels.technical-experience') }}</th>
                                                    <td>
                                                        <div class="clearfix">
                                                            <div id="dv_skill_input"> 
                                                              <div id="" class="clearfix mt10">
                                                                @for($i=0; $i<9; $i++)
                                                                  
                                                                      @if($i < count($applicantProfile->applicant_skills_technical))
                                                                       <input type="hidden" id="applicant_skill_id[]" name="applicant_skill_id[]" value="{{ $applicantProfile->applicant_skills_technical[$i]->applicant_skill_id }}" />

                                                                        <input type="text" id="txt_applicant_skill_name[]" name="txt_applicant_skill_name[]" value="{{ $applicantProfile->applicant_skills_technical[$i]->applicant_skill_name }}" class="db left three_clm_input ib">
                                                                      @else
                                                                        <input type="hidden" id="applicant_skill_id[]" name="applicant_skill_id[]" class="db left three_clm_input ib">

                                                                        <input type="text" id="txt_applicant_skill_name[]" name="txt_applicant_skill_name[]" class="db left three_clm_input ib">
                                                                      @endif
                                                                      @if($i % 3 == 2)
                                                                        </div>
                                                                        <div id="" class="clearfix mt10">
                                                                      @endif
                                                                @endfor
                                                              </div>
                                                              <span id="spSkill" class="help-block required--text">
                                                                  <strong class="text"></strong>
                                                              </span>
                                                          </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th>{{ __('labels.desired-occupation') }}</th>
                                    <td>
                                        <table class="inner_table_occupation">
                                            <tbody>
                                                <tr>
                                                    <th ><div class="occupation_choice">{{ __('labels.1st-choice') }}</div></th>
                                                    <td class="clearfix">
                                                      <input type="text" id="txt_applicant_profile_desiredjob1" name="txt_applicant_profile_desiredjob1" placeholder="First Choice" value="{{ $applicantProfile->applicant_profile_desiredjob1 }}" class="long_input ib mt10">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th ><div class="occupation_choice">{{ __('labels.2nd-choice') }}</div></th>
                                                    <td class="clearfix">
                                                       <input type="text" id="txt_applicant_profile_desiredjob2" name="txt_applicant_profile_desiredjob2" placeholder="Second Choice" value="{{ $applicantProfile->applicant_profile_desiredjob2 }}" class="long_input ib mt10"> 

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th ><div class="occupation_choice">{{ __('labels.3rd-choice') }}</div></th>
                                                    <td class="clearfix">
                                                        <input type="text" id="txt_applicant_profile_desiredjob3" name="txt_applicant_profile_desiredjob3" placeholder="Third Choice" value="{{ $applicantProfile->applicant_profile_desiredjob3 }}"  class="long_input ib mt10">


                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="clearfix">{{ __('labels.expected') }}<br>{{ __('labels.salary') }} <span class="required ib right">{{ __('labels.required') }}</span></th>
                                    <td class="va_m">
                                       <input type="number" min="0" max="999999"  id="txt_applicant_profile_expected_salary" name="txt_applicant_profile_expected_salary" value="{{ ($applicantProfile->applicant_profile_expected_salary != 0) ? $applicantProfile->applicant_profile_expected_salary : '' }}" class="short_input ib">
                                        <span id="spExpectedSalary" class="help-block required--text">
                                            <strong class="text"></strong>
                                        </span>                                    
                                    </td>
                                </tr>
                                <tr>
                                    <th>{{ __('labels.public-relations') }}</th>
                                    <td>
                                        <textarea  id="txt_applicant_profile_public_relation" name="txt_applicant_profile_public_relation" cols="30" rows="10">{{ $applicantProfile->applicant_profile_public_relation }}</textarea>
                                    </td>
                                </tr>                                
                                <tr>
                                    <th class="clearfix">{{ __('labels.resume') }}<span class="required ib right"> {{ __('labels.required') }}</span></th>
                                    <td>
                                        <a href="#" data-href="{{ url('/applicant/profile/resume/display') }}" id="lnkViewResume"> 
                                            {{ $applicantProfile->applicant_profile_resumepath }}
                                        </a> 
                                       <input type="file" id="file_applicant_profile_resumepath" name="file_applicant_profile_resumepath" style="margin-top: 15px;">
                                        <span id="spResumePath" class="help-block required--text">
                                            <strong class="text"></strong>
                                        </span>
                                    </td>
                                </tr>
                           
                            </tbody>
                        </table>
                    

                    </form>

                </fieldset>
                </li><!-- list_info -->
                <p class="btn_wrap ta_c">
                        <button type="button" id="btnEditProfileHistory" class="btn__save">{{ __('labels.edit') }}</button>
                        <button type="button" id="btnSaveProfileHistory" class="btn__save">{{ __('labels.save') }}</button>
                    <!-- <input type="submit" name="保存" value="保存" class="ib"> -->
                </p>

          </form>
        </main><!-- content-main -->
    </div><!-- content-wrapper -->

    <script type="text/javascript">
        $(window).load(function(){
            $('.btn_pulldown').addClass('profile_lower');
            $('.child_pulldown').addClass('profile_lower');
        });
    </script>

    <div id="limitReacheModal" class="w3-modal">
          <div class="w3-modal-content w3-animate-top w3-card-4" style="width: 500px!important;">
              <header class="billing_modal_header"> 
                  <span style="margin-top: -9px;" onclick="document.getElementById('limitReacheModal').style.display='none'" 
                  class="w3-button w3-display-topright modal_close"><span><img class="applyCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span></span>
              </header>
              <div class="w3-container billing_modal_body">
                <div class="applicant" style="margin: 10px 5px;">
                     <h4> You can only insert 3 college profiles </h4>
                </div>
              </div>
          </div>
    </div>

</body>

<!-- Post Scripts -->
  <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
  </script>
<!-- Scripts -->

@endsection
