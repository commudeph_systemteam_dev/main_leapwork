@extends('applicant.layouts.app')

@section('title')
  Account Information |
@stop

@push('styles')
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/user/profile-account.css')}}" /> -->
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
<script type="text/javascript" src="{{url('/js/applicant/applicant.js')}}"></script>
@endpush

@section('bodyClass')
  mypageAccount-Information
@stop


@section('content')

<main class="content-main right">

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span><img id="applicantCloseIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>
        </div>
      </div>
    @endif

    <form method="POST" id="frmProfileInfo" enctype='multipart/form-data'>

        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $applicantProfile->applicant_profile_user_id }}">
        <input type="hidden" id="applicant_profile_id" name="applicant_profile_id" value="{{ $applicantProfile->applicant_profile_id }}">


        <div class="confirm__modal_container">
            <div class="confirm__modal_content">
                <a class="modal__close" id="btn_close"><img src="{{ url('images/ico_remove.svg') }}" alt=""></a>
                <div class="confirm__modal_dialog">
                    <p><input placeholder="Enter Password" type="password" id="txt_applicant_profile_password" name="txt_applicant_profile_password"></p>
                    <p><input placeholder="Confirm Password" type="password" id="txt_applicant_profile_password_confirmation" name="txt_applicant_profile_password_confirmation"></p>
                    <span id="spPassword" class="help-block required--text">
                        <strong class="text"></strong>
                    </span>
                    <div class="confirm__modal_btns">
                        <button class="spare_user confirm__btn" id="spare_user">BACK</button>
                        <a class="save_user confirm__btn" id="btnSavePassword">SAVE</a>
                    </div>
                </div>
            </div>
        </div> <!-- .confirm__modal_container -->

        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">
                   {{ __('labels.profile') }} > {{ __('labels.account-information') }}
                </h1>
                <br/>
            </li>

            <li class="list_info form_area">
                    <fieldset id="myFieldset" disabled>
                <table>
                        
                    <tbody>
                        
                        <tr>
                            <th class="clearfix">{{ __('labels.full-name') }}<span class="required ib right"> {{ __('labels.required') }} </span></th>
                            <td>
                                <input type="text" id="txt_applicant_profile_firstname" name="txt_applicant_profile_firstname" class="long_input ib mt10" placeholder="First Name" value="{{ $applicantProfile->applicant_profile_firstname }}" style="margin-top: 0px;"> <!-- remove inline -->

                                <input type="text" id="txt_applicant_profile_middlename" name="txt_applicant_profile_middlename" class="long_input ib mt10" placeholder="Middle Name" value="{{ $applicantProfile->applicant_profile_middlename }}">
                                
                                <input type="text" id="txt_applicant_profile_lastname" name="txt_applicant_profile_lastname" class="long_input ib mt10" placeholder="Last Name" value="{{ $applicantProfile->applicant_profile_lastname }}">

                                <span id="spFirstName" class="help-block required--text">
                                    <strong class="text"></strong>
                                </span>
                                <span id="spMiddleName" class="help-block required--text">
                                    <strong class="text"></strong>
                                </span>
                                <span id="spLastName" class="help-block required--text">
                                  <strong class="text"></strong>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th class="clearfix">{{ __('labels.birthdate') }}<span class="required ib right"> {{ __('labels.required') }} </span></th>
                            <td>
                                <input type="text" id="txt_applicant_profile_birthdate" name="txt_applicant_profile_birthdate" class="datepicker" value="{{ $applicantProfile->applicant_profile_birthdate ? date('m/d/Y', strtotime($applicantProfile->applicant_profile_birthdate)) :''}}">
                               <span id="spBirthdate" class="help-block required--text">
                                  <strong class="text"></strong>
                               </span>
                            </td>
                        </tr>

                        <tr>
                            <th class="clearfix">{{ __('labels.change-password') }}</th>
                            <td>
                                    <a onclick="return changePassword(this.getAttribute('data-href'))" class="changepassword">{{__('labels.change-password')}}</a>
                                {{--  <input placeholder="Enter Password" type="password" id="txt_applicant_profile_password" name="txt_applicant_profile_password">
                                <input placeholder="Confirm Password" type="password" id="txt_applicant_profile_password_confirmation" name="txt_applicant_profile_password_confirmation">
                               <span id="spPassword" class="help-block required--text">
                                  <strong class="text"></strong>
                               </span>  --}}
                            </td>
                        </tr>


                        <tr>
                            <th class="clearfix">{{ __('labels.gender') }}<span class="required ib right"> {{ __('labels.required') }} </span></th>
                            <td>
                                {{ Form::radio('rb_applicant_profile_gender', __('labels.male') , $applicantProfile->applicant_profile_gender == 'MALE' ? true: false) }}  
                                <span> {{ __('labels.male') }} </span> <span class="space__gap"></span>
                                {{ Form::radio('rb_applicant_profile_gender', __('labels.female') , $applicantProfile->applicant_profile_gender == 'FEMALE' ? true: false) }} <span> {{ __('labels.female') }} </span>

                                <span id="spGender" class="help-block required--text">
                                  <strong class="text"></strong>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th class="clearfix">{{ __('labels.address') }}<span class="required ib right"> {{ __('labels.required') }} </span></th>
                            <td>
                                <input list="codes" placeholder="Postal Code" id="txt_applicant_profile_postalcode" name="txt_applicant_profile_postalcode" class="short_input ib" value="{{ $applicantProfile->applicant_profile_postalcode }} " >
                                <datalist id="codes">
                                    @foreach(array_keys($arrPhilippines) as $key)
                                        <option value="{{$arrPhilippines[$key]}}"> {{$key}} - {{$arrPhilippines[$key]}}
                                    @endforeach
                                </datalist>
                                <input type="text" id="txt_applicant_profile_address1" name="txt_applicant_profile_address1" class="long_input ib mt10" placeholder="{{ __('labels.address-1') }}" value="{{ $applicantProfile->applicant_profile_address1 }}">
                                <input type="text" id="txt_applicant_profile_address2" name="txt_applicant_profile_address2" class="long_input ib mt10" placeholder="{{ __('labels.address-2') }}" value="{{ $applicantProfile->applicant_profile_address2 }}">

                                 <span id="spPostalCode" class="help-block required--text">
                                     <strong class="text"></strong>
                                 </span>
                                 <span id="spAddress1" class="help-block required--text">
                                     <strong class="text"></strong>
                                 </span>
                            </td>
                        </tr>
                        <tr>
                            <th class="clearfix">{{ __('labels.contact') }}<span class="required ib right"> {{ __('labels.required') }} </span></th>
                            <td>
                                <input type="text" id="txt_applicant_profile_contact_no" name="txt_applicant_profile_contact_no" class="short_input ib" value="{{ $applicantProfile->applicant_profile_contact_no }}">
                                <span id="spContact" class="help-block required--text">
                                   <strong class="text"></strong>
                               </span>
                            </td>
                        </tr>

                        <tr>
                            <th class="clearfix">{{ __('labels.email-address') }}<span class="required ib right"> {{ __('labels.required') }} </span></th>
                            <td>
                               <input type="text" id="txt_user_email" name="txt_user_email" class="long_input ib" value="{{ $applicantProfile->user_email }}">
                               <span id="spUseremail" class="help-block required--text">
                                <strong class="text"></strong>
                               </span>
                            </td>
                        </tr>
                        <tr>
                            <th>{{ __('labels.profile-picture') }}</th>
                            <td class="va_m">
                                <div class="profilepic__container">
                                    <div>
                                        <div class="image__container">
                                            @if(!empty($applicantProfile->applicant_profile_imagepath))
                                                <img src="{{ url('/storage/uploads/applicantProfile/profile_') . 
                                                        $applicantProfile->applicant_profile_id . 
                                                        '/' .
                                                        $applicantProfile->applicant_profile_imagepath }}" alt="" class="eyecatch ib">
                                            @elseif(!is_null(Auth::user()->applicant_profile->applicant_fb_image))
                                                <img src="{{Auth::user()->applicant_profile->applicant_fb_image}}" alt="">
                                            @else
                                                <img src="{{ url('/images/applicant/img-side-icon01.png') }}" alt="" class="eyecatch ib">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="file__container">
                                        <a class="link ib">
                                        <input type="file" id="file_applicant_profile_imagepath" name="file_applicant_profile_imagepath" class="link ib" /> 
                                        </a>
                                        <span id="spProfilePicture" class="help-block required--text">
                                            <strong class="text"></strong>
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    
                    </tbody>

                </table>

            </fieldset>
            </li>
            <p class="btn_wrap ta_c">
                    <button type="button" id="btnEditProfile" class="btn__save">{{ __('labels.edit') }}</button>
                    <button type="button" id="btnSaveProfile" class="btn__save">{{ __('labels.save') }}</button>
            </p>
          </ul>

        

      </form>

</main><!-- content-main -->    

<!-- Post Scripts -->

<!-- Scripts -->

@endsection
