@extends('company.layouts.app')


@section('title')
  Scout Email |
@stop

@push('styles')
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/company/scout.css')}}" />
@endpush
 
@push('scripts')
  <script type="text/javascript" src="{{url('/js/company/scout.js')}}"></script>
@endpush

@section('content')

    <main id="scout-view" class="content-main">
      <header class="pan-area">
        <p class="text">
          Scout - Favorites (User)
        </p>
      </header>
      <div class="content-inner">
        <nav class="local-nav">
          <ul class="menu">
            <li class="item">
              <a href="{{url('company/scout')}}">List</a>
            </li>
            <li class="item">
              <a href="{{url('company/scout/search')}}">Search</a>
            </li>
            <li class="item">
              <a href="{{url('company/scout/favorite-user')}}">Favorites (User)</a>
            </li>
            <li class="item">
              <a href="{{url('company/scout/favorite-company')}}">Favorites (Company)</a>
            </li>
            <li class="item">
              <a href="{{url('company/scout/done')}}">Scouted</a>
            </li>
          </ul>
        </nav>
        <div class="section panel scout-index">
          <div class="table">
            <div class="thead">
              <div class="tr">
                <div class="th">
                </div>
                <div class="th">
                </div>
              </div>
            </div>
            <div class="tbody">
            <div class="tr">
              <div class="td">
              <button id="btnConvo" type="button">
                See Conversation History
              </button>
              </div>
            </div>

            <!-- scout conversation --> 
            <div id="scoutConvo" class="hide">
              @if(count($scoutConvo) == 0)
              <div class="tr">
                <div class="th">
                 {{Lang::get('messages.no-scout-conversation')}}
                </div>
              </div>
              @else
              <div class="tr">
                <div class="th">
                User
                </div>
                <div class="th">
                Message
                </div>
                <div class="th">
                Date
                </div>
              </div>
              @endif
            @foreach($scoutConvo as $convo)
              <div class="tr">
                <div class="td">
                  {{$convo->userProperties($convo->scout_conversation_user_id)["userName"]}}
                </div>
                <div class="td">
                  {{strip_tags($convo->scout_conversation_message)}}
                </div>
                <div class="td">
                  {{$convo->scout_conversation_date_added}}
                </div>
              </div>
            @endforeach
            </div>
            <!-- scout conversation --> 
            
            <div class="tr">
              <div class="td">
              </div>
            </div>

            <form method="POST" action="" id="frmTemplate">
            {{csrf_field()}}
            <!-- tr --> 
            <div class="tr">
              <div class="td">
                <input type="text" name="applicant_profile_id" value="{{$applicant->applicant_profile_id}}" hidden/>
                <input type="text" name="scout_id" value="{{$scoutId->scout_id}}" hidden />
                Scout Email To:
              </div>
                
              <div class="td">
                {{$applicant->userProperties($applicant->applicant_profile_user_id)["userName"]}}
              </div>
            </div>
            <div class="tr">
                <div class="td">
                <span class="required" id="activeJobPostRequired"> required </span>
                  Choose a Template :
                </div>
                <div class="td">
                  <input type="text" name="companyID" value="{{$company->company_id}}" hidden />
                  <input type="text" name="applicantID" value="{{$applicant->applicant_profile_id}}" hidden />
                      {{Form::select('master_mail_status', 
                            config('constants.mailStatus'),
                            ''
                      )}} 
                      {{Form::select('activeJobPosts', 
                            $activeJobPosts,
                            $scoutId->scout_job_post_id
                      )}} 
                      <span class="help-block required--text hide" id="noJobPostErr">
                        <strong>Active Job Post is required.</strong>
                        <a href="{{url('/company/job-posting')}}"> Create here!</a>
                      </span>
                </div>
            </div>
              <div class="tr">
                <div class="td">
                  <span class="required"> required </span>
                  Title : 
                </div>
                <div class="td">
                  <input type="text" name="company_mail_subject" placeholder="Email Subject" value="{{ old('company_mail_subject') }}" />  
                  <span class="help-block required--text">
                      <strong class="{{ $errors->has('company_mail_subject') ? '' : ' hide' }}">Email Title is required</strong>
                  </span>
                </div>
              </div>
              <div class="tr">
                <div class="td">
                  <span class="required"> required </span>
                    Content : 
                </div>
                <div class="td">
                    <textarea rows="50" cols="100" name="company_mail_body" placeholder="Email Body" style="height: 350px;" >{{ old('company_mail_body') }}</textarea>
                    <span class="help-block required--text">
                      <strong class="{{ $errors->has('company_mail_body') ? '' : ' hide' }}">Email Body is required</strong>
                  </span>
                </div>
              </div>
              <div class="tr">
                <div class="td">
                Change Scout Status : {{Form::select('scout_status', 
                                          config('constants.scoutStatus'),
                                          array_search($scoutId->scout_status,config('constants.scoutStatus'))
                                          )
                                      }} 
                </div>
              </div>
            <!-- tr -->
            </div>
          </div>
          <a href="{{url('company/scout/favorite-user')}}" class="btn">
                Back
          </a>
          <button id="btnPreview" name="btnPreview" type="button">Preview</button>    
          <button id="btnSendEmail" name="btnSendEmail" type="button">Send</button>    
          </form>
        </div> <!-- div seciton panel  -->
      </div>
    </main>
@endsection