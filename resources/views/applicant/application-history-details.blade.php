@extends('applicant.layouts.app')

@section('title')
  Application History |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/applicant/application-history.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/messages/applicant-application-history.js')}}"></script>
@endpush

@section('bodyClass')
  mypageApplications-history
@stop

@section('content')

<main class="content-main right">

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span>    
        </div>
      </div>
    @endif

    @if(!empty($message))
      {{$message}}
    @endif

    <form method="POST" id="frmApplicationHistory" class="applicationHistory">
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $userId }}">
        <input type="hidden" id="job_application_id" name="job_application_id" value="{{ $applicationHistory['job_application_id'] }}">
        <input type="hidden" id="job_application_thread_id" name="job_application_thread_id" >
        <input type="hidden" id="job_application_conversation_message" name="job_application_conversation_message" >
        <input type="hidden" id="userIcon" value="{{$userImgSrc}}">
        <input type="hidden" id="companyIcon" value="{{$companyImgSrc}}">
        <input type="hidden" id="userName" value="{{$userName}}">

        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">
                    <a href="{{ url('/applicant/application-history') }}" class="link">{{ __('labels.application-history') }}
                    </a>
                </h1>
                <br/>
            </li>

            <li class="list_info">
                
                    <div class="link_post db cf">
                        <h2 class="list_info_title"> {{ $applicationHistory['job_post_title'] }} </h2>

                        <div class="inner">
                            <h3 class="office_name"> {{ $applicationHistory['job_post_company_name'] }} </h3>

                            <div class="info">
                                <p class="info_01 ib"> {{ __('labels.location') }} : {{ $applicationHistory['job_post_location'] }} </p>
                                <p class="info_02 ib"> {{ __('labels.salary') }} : {{ $applicationHistory['job_post_salary'] }} </p>
                            </div>

                            @if($applicationHistory['job_post_status'] == "EXPIRED")
                            <p class="label red ta_c"> 
                                    EXPIRED
                            </p>
                            @elseif($applicationHistory['job_application_status'] != "WITHDRAW")
                              <select id="so_job_application_detail_status" name="so_job_application_detail_status" class="{{ $applicationHistory['job_application_status_css'] }}"> 
                                  <option value="{{ $applicationHistory['job_application_status'] }}">{{ $applicationHistory['job_application_status'] }}</option>
                                  <option value="WITHDRAW">WITHDRAW</option>
                              </select>
                            @else
                                <p class="label red ta_c"> 
                                    WITHDRAW
                                </p>
                            @endif
                              <div class="icon_mail" id="dvBtnMail" style="cursor: pointer;" > 
                                 
                              </div>

                              <h5 style="position: absolute;
                                  top: 0px;
                                  right: 5px;
                                  font-size: 12px;">
                                  {{ __('labels.new') }}
                              </h5>

                        </div>
                    </div><!-- link_post -->


                    <div class="detail_contents detail_contents_chat">
                        <div class="tab_area clearfix">
                            <p class="tab tab_recieve_mail active left">{{ __('labels.inbox') }}</p>
                            <p class="tab tab_send_mail left">{{ __('labels.sent') }}</p>
                            <p class="tab tab_compose_mail left" style="display: none;">{{ __('labels.new') }}</p>
                        </div>

                        <div id="dvInbox" class="dvTab">
                            @if(count($inboxMessages) < 1 )
                                <ul class="chat_list recieve_mail_area">
                                    <li class="opened_chat" style="display: list-item;background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send noMessage">
                                             {{ Lang::get('messages.no-messages') }}
                                        </div>

                                    </li>
                                </ul>                                
                            @endif

                             <ul class="chat_list recieve_mail_area">
                                @foreach($inboxMessages as $inboxMessage)
                                    <li data-t_id="{{ $inboxMessage->job_application_thread_id }}" id="tb_{{ $inboxMessage->job_application_thread_id }}" class="cloesed_chat">
                                        <p class="clearfix">
                                            <time class="left"> 
                                                {{ date('Y.m.d', strtotime($inboxMessage['lastMessage']->job_application_conversation_datecreated)) }} 
                                            </time>
                                            <span class="text left">
                                                {{ $inboxMessage->job_application_thread_title }}
                                                :
                                                {{ $inboxMessage['lastMessage']->job_application_conversation_message }}
                                            </span>
                                        </p>
                                        <figure class="btn_pulldown">
                                            <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                            <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                        </figure>
                                    </li>

                                    <li id="message_area_{{ $inboxMessage->job_application_thread_id }}" class="opened_chat">
                                        <div class="messages_container">
                                            @php
                                                $day="";
                                            @endphp
                                            @foreach($inboxMessage->job_application_conversations as $jobApplicationConversation)
                                                @if(is_null($day) 
                                                || (\Carbon\Carbon::parse($jobApplicationConversation->job_application_conversation_datecreated)->format('m-d-Y')
                                                != $day ))
                                                @php
                                                    $day = \Carbon\Carbon::parse($jobApplicationConversation->job_application_conversation_datecreated)->format('m-d-Y');
                                                @endphp
                                                <hr>
                                                <span style="font-size:12px; text-align:center;display:block;">
                                                    @if($day == \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m-d-Y'))
                                                        Today
                                                    @elseif($day == \Carbon\Carbon::parse(\Carbon\Carbon::yesterday())->format('m-d-Y'))
                                                        Yesterday
                                                    @else
                                                        {{ \Carbon\Carbon::parse($jobApplicationConversation->job_application_conversation_datecreated)->format('M j, Y') }}
                                                    @endif
                                                </span>
                                            @endif
                                                <div class="opened_chat_inner opened_chat_recieve">
                                                    <div class="chat_area layout_2clm clearfix">
                                                        <figure class="icon {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            <img src="{{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                            ? $companyImgSrc
                                                            : $userImgSrc }}" alt="">
                                                        </figure>
                                                        <div class="icon__username">
                                                            {{ $jobApplicationConversation->userProperties($jobApplicationConversation->job_application_conversation_user_id)["userName"] }}
                                                           
                                                            <span class="message__date"> {{ date('M j, Y h:i A', strtotime($jobApplicationConversation->job_application_conversation_datecreated)) }}</span>
                                                        </div>
                                                        <p class="message_contents {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            {{$jobApplicationConversation->job_application_conversation_message}}
                                                        </p>
                                                    </div>                                             
                                                </div>
                                            @endforeach    
                                        </div>
                                        <div class="reply_container">
                                            <div class="opened_chat_inner opened_chat_send">
                                                <div class="form_area">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <h3 class="sub_title"><!-- {{ __('labels.reply') }} --></h3>
                                                                </th>
                                                                <td>
                                                                    <textarea class="ctxtInput"  placeholder="Type a Message..."></textarea>
                                                                     <span class="help-block required--text spErrorMessage hidden">
                                                                         <strong class="text">Please enter a message</strong>
                                                                    </span><br>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="layout_1clm ta_c">
                                                        <div class="dvMainButtons">
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->job_application_thread_id }}">{{ __('labels.reply') }}</a>
                                                        </div>
                                                        <div class="dvConfirmButtons hidden">
                                                            <a class="btnBackReinput btn_submit alt-btn btn ib">{{ __('labels.back') }}</a>
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $inboxMessage->job_application_thread_id }}">Confirm</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div> <!-- dvInbox -->

                        <!-- default hidden -->
                        <div id="dvSent" style="display:none;" class="dvTab" >
                            @if(count($sentMessages) < 1 )
                                <ul class="chat_list recieve_mail_area">
                                    <li class="opened_chat" style="display: list-item;background: white;">
                                       
                                        <div class="opened_chat_inner opened_chat_send noMessage">
                                             {{ Lang::get('messages.no-messages') }}
                                        </div>

                                    </li>
                                </ul>                                
                            @endif

                            <ul class="chat_list recieve_mail_area">
                                @foreach($sentMessages as $sentMessage)
                                    <li data-t_id="{{ $sentMessage->job_application_thread_id }}" id="tb_{{ $sentMessage->job_application_thread_id }}" class="cloesed_chat">
                                        <p class="clearfix">
                                            <time class="left"> 
                                                {{ date('Y.m.d', strtotime($sentMessage['lastMessage']->job_application_conversation_datecreated)) }} 
                                            </time>
                                            <span class="text left">
                                                {{ $sentMessage->job_application_thread_title }}
                                                :
                                                {{ $sentMessage['lastMessage']->job_application_conversation_message }}
                                            </span>
                                        </p>
                                        <figure class="btn_pulldown">
                                            <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img ">
                                            <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img on_off">
                                        </figure>
                                    </li>

                                    <li id="message_area_{{ $sentMessage->job_application_thread_id }}" class="opened_chat">
                                        <div class="messages_container">
                                            @php
                                                $day="";
                                            @endphp
                                            @foreach($sentMessage->job_application_conversations as $jobApplicationConversation)
                                            @if(is_null($day) 
                                            || (\Carbon\Carbon::parse($jobApplicationConversation->job_application_conversation_datecreated)->format('m-d-Y')
                                            != $day ))
                                            @php
                                                $day = \Carbon\Carbon::parse($jobApplicationConversation->job_application_conversation_datecreated)->format('m-d-Y');
                                            @endphp
                                            <hr>
                                            <span style="font-size:12px; text-align:center;display:block;">
                                                @if($day == \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('m-d-Y'))
                                                    Today
                                                @elseif($day == \Carbon\Carbon::parse(\Carbon\Carbon::yesterday())->format('m-d-Y'))
                                                    Yesterday
                                                @else
                                                    {{ \Carbon\Carbon::parse($jobApplicationConversation->job_application_conversation_datecreated)->format('M j, Y') }}
                                                @endif
                                            </span>
                                            @endif
                                            <div class="opened_chat_inner opened_chat_recieve">
                                                    <div class="chat_area layout_2clm clearfix">
                                                        <figure class="icon {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">
                                                            <img src="{{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                            ? $companyImgSrc
                                                            : $userImgSrc }}" alt="">
                                                        </figure>
                                                        <div class="icon__username">
                                                            {{ $jobApplicationConversation->userProperties($jobApplicationConversation->job_application_conversation_user_id)["userName"] }}
                                                            <span class="message__date"> {{ date('M j, Y h:i A', strtotime($jobApplicationConversation->job_application_conversation_datecreated)) }}</span>
                                                        </div>
                                                        <p class="message_contents {{ ($jobApplicationConversation->job_application_conversation_user_id != $userId) 
                                                        ? 'left'
                                                        : 'right'}}">{{ $jobApplicationConversation->job_application_conversation_message }}</p>
                                                    </div>                                             
                                                </div>
                                            @endforeach    
                                        </div>
                                        <div class="reply_container">
                                            <div class="opened_chat_inner opened_chat_send">
                                                <div class="form_area">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    <h3 class="sub_title">{{ __('labels.reply') }}</h3>
                                                                </th>
                                                                <td>
                                                                    <textarea class="ctxtInput"></textarea>
                                                                     <span class="help-block required--text spErrorMessage hidden">
                                                                         <strong class="text">Please enter a message</strong>
                                                                    </span><br>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="layout_1clm ta_c">
                                                        <div class="dvMainButtons">
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $sentMessage->job_application_thread_id }}">{{ __('labels.reply') }}</a>
                                                        </div>
                                                        <div class="dvConfirmButtons hidden">
                                                            <a class="btnBackReinput btn_submit alt-btn btn ib">{{ __('labels.back') }}</a>
                                                            <a class="btnReplyConversation btn_submit btn ib" data-thread-id="{{ $sentMessage->job_application_thread_id }}">Confirm</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </li>
                                @endforeach
                            </ul>
                        </div>  <!-- dvSent -->
                        
                        <!-- default hidden -->
                        <div id="dvCompose" style="display: none;;" class="dvTab">
                            <ul class="chat_list recieve_mail_area">                           
                                <li class="opened_chat" style="display: list-item;background: white">
                                   
                                    <div class="opened_chat_inner opened_chat_send">
                                        <span style="float:right; cursor: pointer;" id="btnCloseCompose">x</span>
                                        <div class="form_area">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            <h3 class="sub_title">{{ __('labels.title') }}</h3>
                                                        </th>
                                                        <td>
                                                            <input type="text" id="txt_title_new" name="txt_title_new" class="long_input ib">
                                                             <span id="spTitle" class="help-block required--text spErrorMessage hidden">
                                                                 <strong class="text">Please enter a title</strong>
                                                            </span>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                            <h3 class="sub_title">{{ __('labels.message') }}</h3>
                                                        </th>
                                                    <td>
                                                        <textarea class="ctxtInput" id="txt_message_new" name="txt_message_new" placeholder="Type a Message..."></textarea>
                                                            <span id="spMessage" class="help-block required--text spErrorMessage hidden">
                                                                <strong class="text">Please enter a message</strong>
                                                        </span>
                                                    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="layout_1clm ta_c">
                                            <div class="dvMainButtons">
                                                <a class="btnConfirmSend btn_submit btn ib">{{ __('labels.send') }}</a>
                                            </div>
                                            <div class="dvConfirmButtons hidden">
                                                <a class="btnBackReinput btn_submit alt-btn btn ib">{{ __('labels.back') }}</a>
                                                <a id="btnComposeThread" class="btn_submit btn ib"  >{{ __('labels.send') }}</a>
                                            </div>
                                        </div>
                                    </div>

                                </li>
                            </ul>
                        </div>  <!-- dvCompose -->

                    </div><!-- detail_contents -->
                
              </li>

          </ul>
      </form>

</main><!-- content-main -->    

<!-- Post Scripts -->
    

<!-- Scripts -->

@endsection