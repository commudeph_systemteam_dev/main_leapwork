@extends('applicant.layouts.app')

@section('title')
  Comment |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
@endpush

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/corporate/profile.js')}}"></script> -->
@endpush

@section('bodyClass')
  mypageComments
@stop

@section('sidebar-title')
  Comment
@stop

@section('content')
    <main class="content-main right">
        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">Comments</h1>
            </li>
            @foreach($commentJobPosts as $commentJobPost)
            
            
            <li class="list_info">
                <a href="{{url('applicant/comment-details')}}/{{$commentJobPost->first()->job_inquiry_id}}" class="link_post db">
                <!-- <a href="{{url('/job/details/')}}/{{$commentJobPost->first()->job_post->job_post_id}}" class="link_post db"> -->
                    <h2 class="list_info_title">{{$commentJobPost->first()->job_post->job_post_title}}</h2>

                    <div class="inner cf">
                        <h3 class="office_name">{{$commentJobPost->first()->job_post->company->company_name}}</h3>
                        <div class="info">
                            <p class="info_01 ib">Location: {{$commentJobPost->first()->job_post->location->master_job_location_name}}</p>
                            <p class="info_02 ib">Salary: PHP {{number_format($commentJobPost->first()->job_post->job_post_salary_min)}}
            ~ PHP {{number_format($commentJobPost->first()->job_post->job_post_salary_max)}}</p>
                        </div>
                        <p class="comment"><span>{{$commentJobPost->first()->job_inquiry_replies->count()+1}}</span></p>
                    </div>
                </a>
            </li><!-- list_info -->
            @endforeach
             <!-- resources/views/vendors/pagination/custom.blade.php -->
             <center>{{ $commentJobPosts->links('vendor.pagination.custom') }}</center>
        </ul><!-- comment_list -->
    </main><!-- content-main -->
@endsection

