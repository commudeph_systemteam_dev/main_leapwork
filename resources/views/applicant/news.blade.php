@extends('applicant.layouts.app')
   
@section('title')
  News |
@stop

@push('scripts')
  <!-- <script type="text/javascript" src="{{url('/js/applicant/applicant-news.js')}}"></script> -->
@endpush

@section('bodyClass')
  mypageAnnoucements
@stop
@section('content')
<form method="GET" id="applicantNewsForm">
      <main class="content-main right">
      <ul class="comment_list"> 
          <li class="list_title_wrap cf">
                <h1 class="list_title">{{__('labels.news-announcements')}}</h1>
                <div class="list_sort">
                    {{ Form::select('applicant_news_status'
                    , config('constants.announcements-filter')
                    , null
                    , array('class' => 'apphistory__select' , 'onchange' => 'this.form.submit()')) 
                    }}
                </div>
          </li>
          @if($announcements->count() == 0)
                <li class="no-new-announcements"><p>{{Lang::get('messages.notifications-none')}}</p></li>
          @endif
          @foreach($announcements as $announcement)
          <li class="list_info">
              <a href="{{url($announcement->url)}}" class="link_post db">
                  <div class="inner clearfix">
                      <div class="left_box left">
                          <time class="time">{{date('Y.m.d', strtotime($announcement->date))}}</time>
                          <p class="{{$announcement->class}}">{{$announcement->label}}</p>
                      </div>
                      <div class="right_box right">
                          <h2 class="sub_title">{{$announcement->title}}</h2>
                          <p class="text">
                          {{ mb_strimwidth(strip_tags($announcement->details), 0, 300, "...")}}
                          </p>
                      </div>
                  </div>
              </a>
          </li><!-- list_info -->
          @endforeach
          
        <!-- resources/views/vendors/pagination/custom.blade.php -->
      </ul><!-- comment_list -->
       <center>{{ $announcements->links('vendor.pagination.custom') }}</center>

    </main><!-- content-main -->   
    
</form>
   
@endsection