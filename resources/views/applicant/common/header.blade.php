
<div class="inner cf">
	<figure class="logo">
		<a href="{{url('front/top/default')}}"><img src="{{asset('images/logo.svg')}}" alt="LEAPWORK"></a>
	</figure>
	<nav class="navigation__menu">
		<ul class="clearfix">
			<li><a href="{{url('front/searchjobs')}}">{{__('labels.find-jobs')}}</a></li>
			<li><a href="{{url('front/latestjobs')}}">{{__('labels.latest-jobs')}}</a></li>
			<li><a href="{{url('front/popularjobs')}}">{{__('labels.popular-jobs')}}</a></li>
			@if(Auth::user()["user_accounttype"] != null)
				<li><a href="{{url('front/favoritelist')}}">{{__('labels.favorite-list')}}</a></li>
			@endif

		</ul>
	</nav>
	<div class="navigation_features">
		<input type="search" class="search_box" placeholder="Search">
		@if(Auth::user()["user_accounttype"] != null)
			<ul class="navigation__icons clearfix">
				<li>
					<a href="#">
						<img src="{{asset('images/icon_message.svg')}}" alt="">
					</a>
				</li>
				<li>
					<a href="#" class="notif_on js_notifbtn_on">
						<img src="{{asset('images/icon_notification_on.svg')}}" alt="">
					</a>
					<a href="#" class="notif_off js_notifbtn_off">
						<img src="{{asset('images/icon_notification.svg')}}" alt="">
					</a>
					<ul class="nav_notifications">
						
						<li>
							<a href="#">TEST</a>
						</li>
						<li><a href="" class="notifications__button">{{__('labels.view-more')}}</a></li>
					</ul>
				</li>
				<li id="userDropdown" class="">
					<a id="btnUserLogo" href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{asset('images/icon_user.svg')}}" alt="">
					</a>
					<ul id="dropdown" class="dropdown-menu">
						<li class="ac__list"><a href="{{url('/applicant')}}">{{__('labels.profile')}}</a></li><br/>
						<li class="ac__list"><a href="{{url('/logoutUser')}}">{{__('labels.logout')}}</a></li>
					</ul>
				</li>
			</ul>
		@else
			<div class="login_buttons">
				<a href="{{url('/login')}}" class="button_login">{{__('labels.login')}}</a>

			</div>
		@endif
	</div>
	
		
	
</div><!-- .navigation -->

@if(Request::is('front/top'))
	<div class="header_kv">
		<img src="{{asset('images/top_kv.jpg')}}" alt="KV">
	</div><!-- .header_kv -->

@endif