
<nav class="global-nav">
    <ul class="list">
        <li class="side_top_icon">
            <div class="side_wrapper">
                <div class="side_top_icon_img">
                    <!-- TODO: composer view much better implem -->
                    <div class="icon__img">
                    @if( !empty(Auth::user()->applicant_profile->applicant_profile_imagepath) )
                        <img src="{{ url('/storage/uploads/applicantProfile/profile_') .
                                     Auth::user()->applicant_profile->applicant_profile_id . 
                                     '/' .
                                     Auth::user()->applicant_profile->applicant_profile_imagepath }}" alt="">
                    @elseif(!is_null(Auth::user()->applicant_profile->applicant_fb_image))
                        <img src="{{Auth::user()->applicant_profile->applicant_fb_image}}" alt="">
                    @else
                        <img src="{{ url('images/applicant/img-side-icon01.png') }}" alt="">
                    @endif
                    </div>

                </div>
                <p class="caption">
                {{$userName}}
                </p>
            </div>
        </li>

        <li class="item law1">
            <a href="{{ url('/applicant/news') }}">
                <span>{{__('labels.announcements')}}</span>
            </a>
        </li>

        {{-- <li class="item law2">
            <a href="{{ url('/applicant/comments') }}">
                <span>{{__('labels.comments')}}</span>
            </a>
        </li> --}}

        <li class="item law3">
            <a href="{{ url('/applicant/application-history') }}">
                <span>{{__('labels.application-history')}}</span>
            </a>
        </li>

        <li class="item law4">
            <a href="{{ url('/applicant/browsing-history') }}">
                <span>{{__('labels.browsing-history')}}</span>
            </a>
        </li>

        <li class="item law5">
            <a href="{{ url('/applicant/scout') }}">
                <span>{{__('labels.scout')}}</span>
            </a>
        </li>

        <li class="item law6">
            <a href="javascript:void(0)" class="abtn_pulldown">
                <span>{{__('labels.profile')}}</span>
            </a>

            <figure class="btn_pulldown">
                <img src="{{ url('images/applicant/btn_pulldown_open.png') }}" alt="開く" class="btn_img btn_img_open ">
                <img src="{{ url('images/applicant/btn_pulldown_close.png') }}" alt="閉じる" class="btn_img btn_img_close on_off">
            </figure>
        </li>

        <li class="child_pulldown">
            <ul>
                <li class="item law7">
                    <a href="{{ url('/applicant/profile/account') }}">
                        <span>{{__('labels.account-information')}}</span>
                    </a>
                </li>

                <li class="item law8">
                    <a href="{{ url('/applicant/profile/resume') }}">
                        <span>{{__('labels.resume')}}</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>