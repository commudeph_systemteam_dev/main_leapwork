@extends('applicant.layouts.app')

@section('title')
  Profile History |
@stop

@push('styles')
  <!-- <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/company/companies.css')}}" /> -->
  <style>
      .required-empty {
        border-color: red !important;
    }
  </style>
@endpush

@push('scripts')
  <script type="text/javascript" src="{{url('/js/common/svalidator.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/applicant/applicant.js')}}"></script>
@endpush

@section('bodyClass')
  mypageResume
@stop

@section('content')

<main class="content-main right">

    @if (Session::has('message'))
      <div class="alert__modal">
        <div class="alert__modal--container">
           <p>{{ Session::get('message') }}</p><span>x</span>
        </div>
      </div>
    @endif

    <form method="POST" id="frmProfileHistory">
        {{ csrf_field() }}
        <input type="hidden" id="user_id" name="user_id" value="{{ $applicantProfile->applicant_profile_user_id }}">
        <input type="hidden" id="applicant_profile_id" name="applicant_profile_id" value="{{ $applicantProfile->applicant_profile_id }}">


        <ul class="comment_list">
            <li class="list_title_wrap cf">
                <h1 class="list_title">
                   {{ __('labels.profile') }} > {{ __('labels.resume') }}
                </h1>
                <br/>
            </li>

            <li class="list_info form_area">
                <table>
                  <tr>
                    <td>{{ __('labels.college') }}</td>
                    <td colspan="5">
                        <div id="dv_college_input"> 
                            <input type="hidden" id="applicant_education_id_del" name="applicant_education_id_del" />
                            @for($i=0; $i<count($applicantProfile->applicant_education); $i++)                          
                                <div id="dv_college" class="dv_college_entry">
                                    <input type="hidden" id="applicant_education_id[]" name="applicant_education_id[]" value="{{ $applicantProfile->applicant_education[$i]->applicant_education_id }}" />

                                    {{ Form::select('so_applicant_education_year_passed[]', 
                                              $soYears, 
                                              $applicantProfile->applicant_education[$i]->applicant_education_year_passed,
                                              ['placeholder' => '- Choose One -'])   }}

                                    {{ Form::select('so_applicant_education_year_month[]', 
                                              $soMonths, 
                                              $applicantProfile->applicant_education[$i]->applicant_education_year_month,
                                              ['placeholder' => '- Choose One -'])   }}

                                    {{ Form::select('so_applicant_education_status[]', 
                                              $soCollegeStatus, 
                                              $applicantProfile->applicant_education[$i]->applicant_education_status,
                                              ['placeholder' => '- Choose One -'])   }}


                                    <div class="dvRemoveCollege" style="float:right;"> 
                                      @if($i > 0)
                                      <button type="button" class="btnRemoveCollegeDiv">X</button> 
                                      @endif
                                    </div>
                                    <br>
                                    <input type="text" id="txt_applicant_education_school[]" name="txt_applicant_education_school[]" placeholder="School Name" value="{{ $applicantProfile->applicant_education[$i]->applicant_education_school }}">
                                    <br>
                                     {{ Form::select('so_applicant_education_qualification_id[]', 
                                              $masterQualifications, 
                                              $applicantProfile->applicant_education[$i]->applicant_education_qualification_id,
                                              ['placeholder' => '- Choose One -'])   }}

                                    <input type="text"  id="txt_applicant_education_course[]" name="txt_applicant_education_course[]"  placeholder="Course Name" value="{{ $applicantProfile->applicant_education[$i]->applicant_education_course }}">
                                </div>
                            @endfor
                              <span id="spEducation" class="help-block required--text">
                                <strong class="text"></strong>
                              </span>
                            <br/>
                            <a class="btn__add" id="btnAddCollegeDiv">{{ __('labels.add-more') }}</a>
                        </div> <!-- div_college_input -->
                    </td>
                  </tr>
                  <tr>
                    <td>{{ __('labels.work-experience') }}</td>
                    <td colspan="5">
                        <div id="dv_workexp_input"> 
                            <input type="hidden" id="applicant_workexp_id_del" name="applicant_workexp_id_del" />
                             @for($i=0; $i<count($applicantProfile->applicant_workexperiences); $i++)
                                <div id="dv_workexp" class="dv_workexp_entry">
                                   <input type="hidden" id="applicant_workexp_id[]" name="applicant_workexp_id[]" value="{{ $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_id }}" />

                                    {{ __('labels.company-name') }} :  
                                    <div class="dvRemoveWorkexp" style="float:right;"> 
                                      @if($i > 0)
                                      <button type="button" class="btnRemoveWorkexpDiv">X</button> 
                                      @endif
                                    </div>

                                    <br/>
                                    <input type="text" id="txt_applicant_workexp_company_name[]" name="txt_applicant_workexp_company_name[]" value="{{ $applicantProfile->applicant_workexperiences[$i]->applicant_workexp_company_name }}">
                                    <br/>
                                    {{ __('labels.started-finished-date') }}
                                    <br/>
                                    {{ Form::select('so_applicant_workexp_datefrom_year[]', 
                                              $soYears, 
                                              date('Y', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_datefrom)),
                                              ['placeholder' => '- ' . __('labels.choose-oen') . ' -'])   }}
                          
                                    {{ Form::select('so_applicant_workexp_datefrom_month[]', 
                                              $soMonths, 
                                              date('n', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_datefrom)),
                                              ['placeholder' => '- Choose One -'])   }}
                                    
                                    ～

                                     {{ Form::select('so_applicant_workexp_dateto_year[]', 
                                              $soYears, 
                                              date('Y', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto)),
                                              ['placeholder' => '- Choose One -'])   }}
                          
                                    {{ Form::select('so_applicant_workexp_dateto_month[]', 
                                              $soMonths, 
                                              date('n', strtotime($applicantProfile->applicant_workexperiences[$i]->applicant_workexp_dateto)),
                                              ['placeholder' => '- Choose One -'])   }}
                                </div>
                            @endfor
                            <span id="spWorkexp" class="help-block required--text">
                                <strong class="text"></strong>
                              </span>
                            <br/>
                            <a class="btn__add" id="btnAddWorkexpDiv">Add more</a>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Job Career / Objectives</td>
                    <td colspan="5">
                      <textarea id="txt_applicant_profile_objective" name="txt_applicant_profile_objective" cols="30" rows="10">
                      {{ $applicantProfile->applicant_profile_objective }}
                      </textarea>
                    </td>
                  </tr>
                  <tr>
                    <td>Skill</td>
                    <td colspan="5">
                       <div id="dv_communication_input"> 
                            <input type="hidden" id="applicant_communication_id_del" name="applicant_communication_id_del" />
                             @for($i=0; $i<count($applicantProfile->applicant_skills_communication); $i++)
                                <div id="dv_communication" class="dv_communication_entry">
                                   <input type="hidden" id="applicant_communication_id[]" name="applicant_communication_id[]" value="{{ $applicantProfile->applicant_skills_communication[$i]->applicant_skill_id }}" />

                                   Language:
                                    <input type="text" id="txt_applicant_communication_name[]" name="txt_applicant_communication_name[]" class="ctxtCommunication" value="{{ $applicantProfile->applicant_skills_communication[$i]->applicant_skill_name }}">

                                    Qualification:
                                    <input type="text" id="txt_applicant_communication_level[]" name="txt_applicant_communication_level[]" class="ctxtQualification" value="{{ $applicantProfile->applicant_skills_communication[$i]->applicant_skill_level }}">

                                     <div class="dvRemoveCommunication" style="float:right;"> 
                                      @if($i > 0)
                                      <button type="button" class="btnRemoveCommunicationDiv">X</button> 
                                      @endif
                                    </div>

                                    <br/>
                                </div>
                            @endfor
                            <span id="spCommunication" class="help-block required--text">
                                <strong class="text"></strong>
                            </span>
                            <br/>
                             <a class="btn__add" id="btnAddCommunicationDiv">Add more</a>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Skills</td>
                    <!-- Static 8 inputs for skills -->
                    <td colspan="5">
                      <div id="dv_skill_input"> 
                             @for($i=0; $i<count($applicantProfile->applicant_skills_technical); $i++)
                                <div id="dv_skill" class="dv_skill_entry">
                                   <input type="hidden" id="applicant_skill_id[]" name="applicant_skill_id[]" value="{{ $applicantProfile->applicant_skills_technical[$i]->applicant_skill_id }}" />

                                    <input type="text" id="txt_applicant_skill_name[]" name="txt_applicant_skill_name[]" value="{{ $applicantProfile->applicant_skills_technical[$i]->applicant_skill_name }}">

                                </div>
                            @endfor
                            <!-- Loop for remaining rows -->
                            @for($i=0; $i<(8-count($applicantProfile->applicant_skills_technical)); $i++)
                                <div id="dv_skill" class="dv_skill_entry">
                                   <input type="hidden" id="applicant_skill_id[]" name="applicant_skill_id[]" />

                                    <input type="text" id="txt_applicant_skill_name[]" name="txt_applicant_skill_name[]" >

                                </div>
                            @endfor
                            <span id="spSkill" class="help-block required--text">
                                <strong class="text"></strong>
                            </span>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Desired Job Position</td>
                    <td colspan="5">
                      <input type="text" id="txt_applicant_profile_desiredjob1" name="txt_applicant_profile_desiredjob1" placeholder="First Choice" value="{{ $applicantProfile->applicant_profile_desiredjob1 }}"> 
                      <br>
                      <input type="text" id="txt_applicant_profile_desiredjob2" name="txt_applicant_profile_desiredjob2" placeholder="Second Choice" value="{{ $applicantProfile->applicant_profile_desiredjob2 }}"> 
                      <br>
                      <input type="text" id="txt_applicant_profile_desiredjob3" name="txt_applicant_profile_desiredjob3" placeholder="Third Choice" value="{{ $applicantProfile->applicant_profile_desiredjob3 }}"> 
                      <br>
                    </td>
                  </tr>
                  <tr>
                    <td>Expected Salary</td>
                    <td colspan="5">
                      <input type="text" min="0" max="999999"  id="txt_applicant_profile_expected_salary" name="txt_applicant_profile_expected_salary" value="{{ ($applicantProfile->applicant_profile_expected_salary != 0) ? $applicantProfile->applicant_profile_expected_salary : '' }}">
                      <span id="spExpectedSalary" class="help-block required--text">
                          <strong class="text"></strong>
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>Public Relations</td>
                    <td colspan="5">
                      <textarea  id="txt_applicant_profile_public_relation" name="txt_applicant_profile_public_relation" cols="30" rows="10">
                        {{ $applicantProfile->applicant_profile_public_relation }}
                      </textarea>
                    </td>
                  </tr>
                </table><!-- .table__profile -->
                 
                 <p class="btn_wrap ta_c">
                     <button type="button" id="btnSaveProfileHistory" class="btn__save">SAVE</button>
                 </p>
            </li>

          </ul>
      </form>

</main><!-- content-main -->    

<!-- Post Scripts -->

<!-- Scripts -->

@endsection
