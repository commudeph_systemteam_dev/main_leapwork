<!DOCTYPE html>
<html>
<head>
  <!-- <meta name="robots" content="noindex,nofollow"> -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="format-detection" content="telephone=no">
  <!--
  <meta name="description" content="このWebサイトの説明が入ります。">
  <meta name="keywords" content="キーワード,キーワード">
  <meta property="og:title" content="このWebサイトのタイトルが入ります。">
  <meta property="og:type" content="website">
  <meta property="og:description" content="このWebサイトの説明が入ります。">
  <meta property="og:url" content="http://XXXXXXXXXX.com">
  <meta property="og:image" content="http://XXXXXXXXXX.com/images/og.png">
  <meta property="fb:app_id" content="XXXXXXXXXXXXXXXX">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@XXXXX">
  <meta name="twitter:creator" content="@XXXXX">
  <meta name="twitter:image:src" content="http://XXXXXXXXXX.com/images/og.png">
  -->
  @include('front.common.html-head')
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>

  <!-- Louie: Insert global js for predefined jquery etc. -->
  <script type="text/javascript">
      //サイドバーのプルダウン
      $(function(){
          $('.btn_pulldown, .abtn_pulldown').click(function() {
              $('.btn_img').toggleClass('on_off');
              $('.child_pulldown').stop().slideToggle(300);
          });
      });
  </script>

  <script type="text/javascript">
      $(function(){
          $("aside#sidebar div.menu-btn").on("click", function() {
              $(this).toggleClass("active");
              $("aside#sidebar").toggleClass("side-open");
              $("main.content-main").toggleClass("side-open");
          });
      });
  </script>

  <script src="{{url('/js/common/global.js')}}"></script> 
  <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
  </script>

  <!-- title -->
  <title>@yield('title') | LEAP Work </title>
  
  <!-- js included in admin.common.html-head -->
  <!-- page specific js -->
  @stack('scripts')
  <link href="{{url('css/admin/modal.css')}}"     media="screen" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/applicant/home.css')}}" />
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" media="print,screen" href="{{url('css/common.css')}}">

  <link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
  <link rel="stylesheet" media="screen and (max-width: 767px)" href="{{url('css/sp.css')}}">

  <link rel="shortcut icon" href="{{url('/images/common/favicon/favicon.ico')}}">

  <link rel="stylesheet" type="text/css" media="screen" href="{{url('css/jquery/jquery-ui.min.css')}}" />
    
  <!-- Nav -->
  <!-- <link rel="stylesheet" media="print,screen" href="http://localhost:8000/css/admin/index.css"> -->
  


   <!-- page specific css -->
  @stack('styles')
   
  @include('front.common.header')
</head>
<body class="@yield('bodyClass')">

    <header id="global-header">
    
    </header>
    <div class="responsive__container">
        <div class="content-wrapper clearfix">

            <aside id="sidebar" class="clearfix left">
                @include('applicant.common.sidebar')
            </aside>

            @yield('content')

            <!-- page specific post js -->
            @stack('post-scripts')
            
        </div>
    </div>
    
</body>

@include('front.common.footer')