
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <!-- css -->
  <!-- js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>

  <script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
  <script src="{{url('/js/common/lib/pace.min.js')}}"></script>
  <script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>
  <script src="{{url('/js/common/global.js')}}"></script> 
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/login.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/payment.css')}}" />

  <!-- title -->
  <title>Credit Card | LEAP Work</title>
  <main id="login-view">
    <figure class="logo">
        <img src="{{url('images/common/icon/header-logo.svg')}}" alt="">
    </figure>
    <a href="{{url('/')}}" id="main__pageBtn">{{ __('labels.back-to-main-page') }}</a>
    <div class="panel">
      <div class="inner">
            <div class="panel-body creditcard-view">
				<div class="info">
					<h2> Plan payment expired. Please contact LeapWork admin </h2>
				</div>

            </div>        
      </div>
    </div>
  </main>
</body>
</html>