
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <!-- meta -->
  <meta charset="UTF-8" />
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv="Imagetoolbar" content="no" /><![endif]-->

  <!-- favicon -->
  <meta name="msapplication-TileImage" content="{{url('/images/common/favicon/msapplication-TileImage.png')}}" />
  <meta name="msapplication-TileColor" content="#000" />
  <link rel="apple-touch-icon" href="{{url('/images/common/favicon/apple-touch-icon.png')}}" />
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{url('/images/common/favicon/favicon.png')}}" />
  <!-- css -->
  <!-- js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-1.12.0.min.js')}}"></script>
  <script type="text/javascript" src="{{url('/js/common/jquery/jquery-ui.min.js')}}"></script>

  <script src="{{url('/js/common/lib/svgxuse.js')}}"></script>
  <script src="{{url('/js/common/lib/pace.min.js')}}"></script>
  <script src="{{url('/js/common/lib/jquery.matchHeight-min.js')}}"></script>
  <script src="{{url('/js/common/global.js')}}"></script> 
  <!-- css -->
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/login.css')}}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/payment.css')}}" />
  <link rel="stylesheet" media="print,screen and (min-width: 768px)" href="{{url('css/pc.css')}}">
  <link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/admin/modal.css')}}" />
  <!-- title -->
  <title>User Registration | LEAP Work</title>
  <main id="login-view">
    <figure class="logo">
        <img src="{{url('images/common/icon/header-logo.svg')}}" alt="">
    </figure>
    <a href="{{url('/')}}" id="main__pageBtn">{{ __('labels.back-to-main-page') }}</a>
    <div class="panel invoice_details">
      <div class="inner">
            <div class="panel-body register-view">
                   <form action="{{url('payment/payment_complete')}}" method="POST" id="confirmForm" data-stripe-publishable-key="pk_test_MA2xKTYcqx2VJVHD2F3f03rj">
                        {{ csrf_field() }}
                        
                      <div class="">
                            <p class="service-title">
                                {{ __('labels.claim-details') }}
                            </p>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    <table>
                                        <tr>
                                            <td>
                                                 {{ __('labels.claim-code') }}
                                            </td>
                                            <td>
                                                <input type="hidden" id="claimNumConfirm" name="claimNumConfirm" value="{{ isset($_POST['claim_no']) ? $_POST['claim_no'] : '' }}" readonly>
                                                {{ isset($_POST['claim_no']) ? $_POST['claim_no'] : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 {{ __('labels.company-name') }}
                                            </td>
                                            <td>
                                               {{ $claims->company_name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 {{ __('labels.plan') }}
                                            </td>
                                            <td>
                                               {{ $claims->master_plan_name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 {{ __('labels.price') }}
                                            </td>
                                            <td>
                                                PHP {{ number_format($claims->master_plan_price , 2, '.', ',') }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 {{ __('labels.credit-card-no') }}
                                            </td>
                                            <td>
                                                <input type="hidden" id="creditCardConfrim" name="creditCardConfrim"  value="{{ isset($_POST['credit_card_no']) ? $_POST['credit_card_no'] : '' }}" readonly>
                                                {{ isset($_POST['credit_card_no']) ? App\Models\Claim::ccMasking($_POST['credit_card_no']) : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                CVC
                                            </td>
                                            <td>
                                               <input type="hidden" id="cvcConfirm" name="cvcConfirm" value="{{ isset($_POST['cvc']) ? $_POST['cvc'] : ''}}" readonly>
                                               {{ isset($_POST['cvc']) ? $_POST['cvc'] : ''}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Month
                                            </td>
                                            <td>
                                                <input type="hidden" id="monthConfirm" name="monthConfirm" value="{{ isset($_POST['exp_month']) ? $_POST['exp_month'] : '' }}" readonly>
                                                {{ isset($_POST['exp_month']) ? $_POST['exp_month'] : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Year
                                            </td>
                                            <td>
                                                <input type="hidden" id="yearConfirm" name="yearConfirm" value="{{ isset($_POST['exp_year']) ? $_POST['exp_year'] : '' }}" readonly>
                                                {{ isset($_POST['exp_year']) ? $_POST['exp_year'] : '' }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="menu cf confirm_menu">
                                <button type="button" class="back_button" onClick="history.back()">{{ __('labels.back') }}</buton>
                            
                                <button type="button" onclick="stripFunction()">{{ __('labels.confirm') }}</button>
                                <!-- <button type="submit" name="searchInvoice">Search</buton> -->
                            </div>
                            <div id="id01" class="w3-modal">
                                <div class="w3-modal-content w3-animate-top w3-card-4 payment" style="
                                    padding-bottom: 2%;
                                    width: 450px !important;
                                ">
                                    <header> 
                                        <span onclick="document.getElementById('id01').style.display='none'" 
                                        class="w3-button w3-display-topright"><span onClick="history.back()"><img id="closeIcon" src="{{ url('images/ico_remove.svg') }}" alt=""></span></span>
                                        <h2 style="
                                            padding-top: 34px;
                                        ">{{ __('labels.final-payment') }}</h2>
                                    </header>
                                    <div class="w3-container">
                                        <p style="
                                        height: 83px;
                                        padding-top: 0px;">{{ __('labels.we-will-now-charge-amount') }}<span class="amount">PHP {{ number_format($claims->master_plan_price , 2, '.', ',') }} </span></p>
                                        <p style="padding-top: 5px;"><input type="submit" name="paynow" value="{{ __('labels.pay-now') }}"></p>
                                    </div>
                                </div>
                            </div>
                      </div>
                    </form>
            </div>        
      </div>
    </div>
  </main>
</body>
  <script src="https://js.stripe.com/v2/"></script>
  <script src="https://js.stripe.com/v3/"></script>
</html>