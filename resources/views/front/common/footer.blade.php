
<footer class="footer">
	<div class="footer__content">
	@if(Auth::user()["user_accounttype"] != null)
		<a href="{{url('front/top/default')}}" class="footer__logo"><img src="{{ url('images/footer_logo.png') }}" alt=""></a>
	@else
		<a href="./" class="footer__logo"><img src="{{ url('images/footer_logo.png') }}" alt=""></a>
	@endif
	
		<ul class="footer__items clearfix">
			<li><a href="{{url('front/searchjobs')}}">{{Lang::get('labels.find-jobs')}}</a></li>
			<li><a href="{{url('front/latestjobs')}}">{{Lang::get('labels.latest-jobs')}}</a></li>
			<li><a href="{{url('front/popularjobs')}}">{{Lang::get('labels.popular-jobs')}}</a></li>
			@if(Auth::user()["user_accounttype"] != null)
				<li><a href="{{url('front/favoritelist')}}">{{Lang::get('labels.favorite-list')}}</a></li>
			@else
				<li><a href="{{url('login')}}">{{Lang::get('labels.favorite-list')}}</a></li>
			@endif
		</ul>
		<small class="pc">&copy; Commude Philippines Inc.</small>

		<div class="social_icons_top">
			<ul class="clearfix">
				<li><a href="https://www.facebook.com/commudephilippines/" target="_blank"><img src="{{ url('images/common/icon/icon_facebook.png') }}" alt="" class="footer_fb"></a></li>
				<li><a href="https://twitter.com/leapworkph" target="_blank"><img src="{{ url('images/common/icon/icon_twitter.png') }}" alt="" class="footer_twitter"></a></li>
				<li class="sp"><a href="#top"><img src="{{ url('images/common/icon/icon_top.png') }}" alt="" class="footer_top"></a></li>
			</ul>
		</div>
	</div>
	
	<small class="sp">&copy; Commude Philippines Inc.</small>
	<a href="#top" class="footer_totop pc"><img src="{{ url('images/common/icon/icon_top.png') }}" alt="" class="footer_top"></a>
</footer><!-- .footer -->
</div>
</body>
</html>