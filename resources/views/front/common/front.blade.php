﻿<div class="latest__job">
	@if(count($jobs) > 0)
	   	@foreach($jobs as $key => $value)
	   		<form id= "frontForm" method="post" action="{{url(substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'/front')))}}"> <!-- Form Start -->
	   			{{ csrf_field() }}
				<div class="top_jobs">
					<div class="top_latestjobs_item">
						<a href="{{url('job/details')}}/{{$value->job_post_id}}">
						{{--  @if($value->job_post_image1 != null && $value->job_post_image2 != null && $value->job_post_image3 != null)						
							<div class="top_latestjobs_imgs clearfix">
								<div class="three">
									<img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image1) }}" alt="">
								</div>
								<div class="three pc">
									<img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image2) }}" alt="">
								</div>
								<div class="three pc">
									<img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image3) }}" alt="">
								</div>
						@elseif($value->job_post_image1 != null && $value->job_post_image2 != null && $value->job_post_image3 == null)
							<div class="top_latestjobs_imgs clearfix">
								<div class="two">
									<img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image1) }}" alt="">
								</div>
								<div class="two">
									<img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image2) }}" alt="" class="pc">
								</div>
						@elseif($value->job_post_image1 != null && $value->job_post_image2 == null && $value->job_post_image3 == null)
							<div class="top_latestjobs_imgs">
								<div class="one">
									<img src="{{ url('/storage/uploads/job-posts/'.$value->job_post_image1) }}" alt="">
								</div>
						@elseif($value->job_post_image1 == null && $value->job_post_image2 == null && $value->job_post_image3 == null)
							<div class="top_latestjobs_imgs">
								<div class="one default">
									<img src="{{asset('images/top_blankimage.jpg') }}" alt="">
								</div>
						@endif  --}}
							<div class="top_latestjobs_imgs">
								<div class="one default">
									<img src="{{ !is_null($value->job_post_key_pc) 
										?   url('/storage/uploads/job-posts/'.$value->job_post_key_pc)
										:   asset('images/top_noimage_1200x400.jpg')}}" alt="">
								</div>
							</div>
						</a>					

						<div class="top_latestjobs_item_details">
							<div class="top_latestjobs_item_pos">
								<div class="job_position">
									<p><a href="{{url('front/job/details')}}/{{$value->job_post_id}}">{{ $value->job_post_title }}</a></p>
									@if($value->job_post_created > Carbon\Carbon::now()->subDays(3))
										<!-- <a href="javascript:void(0)" class="category_new">NEW</a> -->
										<span class="category_new">NEW</span>
									@endif
								</div>
								
								
								<p class="date_posted">	{{ date('d/m/Y', strtotime($value->job_post_created)) }} </p>
							</div>

							<h3>{{ $value->job_post_overview }}</h3>

							<p class="top_latestjobs_item_description">	{{ mb_strimwidth($value->job_post_description, 0, 300, " ...") }}</p>

							<div class="top_latestjobs_item_sub">
								<p class="salary_range">
									@if(Auth::user()["user_accounttype"] != null) 
										<span>PHP</span>{{ number_format($value->job_post_salary_min, 2, '.', ',') }} - <span>PHP</span>{{ number_format($value->job_post_salary_max, 2, '.', ',') }}
									@else
										<a href="{{url('front/job/details/login_to_apply')}}/{{$value->job_post_id}}"><span>Login to view salary</span></a>
									@endif
								</p>
								<div class="skills_prefecture clearfix">
									<a href="javascript:void(0)" class="skills">{{ $value->job_post_skills }}</a>
									<a target="_blank" href="https://www.google.com/maps/place/{{ $value->master_job_location_name }}/{{ $value->master_country_name }}" class="prefecture">{{ $value->master_job_location_name }}, {{ $value->master_country_name }}</a>
								</div>
							</div>
						</div> <!-- .top_latestjobs_item_details -->
						<input type="hidden" name="job_id" value="{{ $value->job_post_id }}">
						@if(strpos($_SERVER['REQUEST_URI'], '/front/favoritelist' )  !== false)
							<input type="Submit" class="top_latestjobs_item_remove" value="ー" name="remove">
						@endif
						<a href="{{url('front/job/details')}}/{{$value->job_post_id}}" class="job_interested"><input type="button" id="interested" value="{{__('labels.im-interested')}}" name="interested"> </a>
						
					</div><!-- .top_latestjobs_item 1 -->
				</div>
			</form> <!-- Form End -->
		@endforeach
	@include('front.pagination.default', ['paginator' => $jobs])
	@else
	{{--  <div class="no_job_display">
		<p>
			{{__('labels.no-job-available-to-display')}}
		</p>
	</div>  --}}
	@endif
</div>

