
<script type="text/javascript" src="{{url('/js/pusher.min.js')}}"></script>
<script type="text/javascript" src="{{url('/js/front/front.js')}}"></script>
<script type="text/javascript">
  var APP_URL = {!! json_encode(url('/')) !!}
	window.userId    =  {!! json_encode(Auth::user()['id']) !!};
	window.pusherKey =  {!! json_encode(config('app.PUSHER_APP_KEY')) !!};
	window.notificationsLimit =  {!! json_encode(config('constants.notifications.limit')) !!};
</script>

<script type="text/javascript">
  var seenAnnouncements = {{json_encode($seenAnnouncements)}};
</script>

<script type="text/javascript" src="{{url('/js/applicant/header.js')}}"></script>
	<div class="content" id="top">
		<header class="header">
			<div class="navigation">
				<a href="javascript:void(0)" class="sp__navigation sp"><span class="navigation__bar"></span></a>
				<h1 class="logo">
					<a href="{{url('front/top/default')}}"><img src="{{asset('images/logo.svg')}}" alt="LEAPWORK"></a>
				</h1>
				<nav class="navigation__menu">
					<input type="search" class="search_box sp">
					<ul class="clearfix">
						<li><a href="{{url('front/searchjobs')}}">{{__('labels.find-jobs')}}</a></li>
						<li><a href="{{url('front/latestjobs')}}">{{__('labels.latest-jobs')}}</a></li>
						<li><a href="{{url('front/popularjobs')}}">{{__('labels.popular-jobs')}}</a></li>
						@if(Auth::user()["user_accounttype"] != null)
							<li><a href="{{url('front/favoritelist')}}">{{__('labels.favorite-list')}}</a></li>
						@else
							<li><a href="{{url('login')}}">{{__('labels.favorite-list')}}</a></li>
						@endif
					</ul>
				</nav>
				<div class="overlay sp"></div>
				<div class="navigation_features">
					<form method="get" action="{{ url('front/search') }}" onkeypress="search(event)">
						<div class="search__container">
							<button onkeypress="search(event)"></button>
							<input type="search" name="search" class="search_box pc" placeholder="Search">
						</div>
					</form>
					<ul class="function-menu cf">
						@if(Auth::user()["user_accounttype"] != null)
						<li class="function applicant">
      				<span class="icon {{ (count($messageIconNotification)) > 0 ? 'unread': ''}}"></span>
						  <ul class="tooltip">
							  	<div class="tooltip__container">
							  		
								{{--  <li class="item">
			            <span class="user-icon" 
										
											@if(!empty(Auth::user()->applicant_profile->applicant_profile_imagepath))
												style="background-image: url({{ url('/storage/uploads/applicantProfile/profile_') .
                                     Auth::user()->applicant_profile->applicant_profile_id . 
                                     '/' .
                                     Auth::user()->applicant_profile->applicant_profile_imagepath }});"
											@endif
										
									></span>
			            <a href="{{ url('applicant/profile/account') }}">
										<p class="text">
											<b>{{ $userName }}</b>
										</p>
			            </a>
			          </li>  --}}
								@if(count($messageNotifications) == 0)
								<li id="noMessageItem" class="item">
									<p class="text no-notification">
										{{__('messages.messages-none')}}
									</p>
								</li>
								@endif
								@foreach($messageNotifications as $message)
									@php
										//get the very latest message if any
										$userProfile = $message->scout_conversations->last()->user_profile;
										$name = '';

										switch($userProfile->user_accounttype) {
											case 'CORPORATE ADMIN':
											case 'CORPORATE USER':
												$name = $userProfile->company_profile->company_contact_person;
											break;

											default:
											$name = 'User';
										}
									@endphp

									<li id="thread_{{$message->scout_thread_id}}" class="item">
										<span class="user-icon"></span>
										<a href="{{ url($message['notification_url'])}}">
										  @php
												$timezone = 'Asia/Manila';
											@endphp
											@php
												$timezone = 'Asia/Manila';
												$now = \Carbon\Carbon::now($timezone);
												$MessagedeliveryDate = \Carbon\Carbon::parse($message->notification_created_at ,$timezone);
											@endphp
											<p class="text">
												@if($now->diffInHours($MessagedeliveryDate) <= 1)
													<span class="delDateSpan">{{$MessagedeliveryDate->diffForHumans()}}</span>
												@elseif($now->diffInDays($MessagedeliveryDate) < 1)
													<span class="delDateSpan">{{$MessagedeliveryDate->format('h:i A')}}</span>
												@else
													<span class="delDateSpan">{{$MessagedeliveryDate->format('D (m/d)')}}</span>
												@endif
												<b>{{ $name }} </b><br> 
													{{json_decode($message->notification_data)->title}}<br>
													{{ mb_strimwidth(json_decode($message->notification_data)->content, 0, 100, " ...")}}
											</p>
										</a>
									</li>
								@endforeach
							    <li class="read-more">
							      <a href="{{url('/applicant/news')}}">
							        {{__('labels.view-more')}}
							      </a>
							    </li>
							 </div>
						  </ul>
						</li>
						<li class="function notice">
							<span class="icon {{(count(json_decode($bellNotifications)) > 0) || $newAnnouncementsCount > 0 ? 'unread' : ''}}"></span>
							<ul class="tooltip">
							  	<div class="tooltip__container">
							  		{{-- <li data-id="1" data-type="notification" class="item">
						          <!-- <span class="user-icon"></span>
						          <a href="{{ url('applicant/profile/account') }}">
						            <p class="text">
						              <b>{{ $userName }}</b>
						            </p>
						          </a> -->
										</li> --}}
								@if(count($bellListNotifications) == 0 && count($announcements) == 0)
								<li id="noNotif" class="item">
									<p class="text">
										{{__('messages.notifications-none')}}
									</p>
								</li>
								@endif
								@foreach($bellListNotifications as $notification)
								<li class="item">
									<a href="{{ url('/applicant/news').'/'.$notification['notification_url']}}">
										<p class="text">
											<b>{{json_decode($notification->notification_data)->title}}</b>
											<br>
											{{ mb_strimwidth(json_decode($notification->notification_data)->content, 0, 100, " ...")}}
										</p>
									</a>
								</li>
								@endforeach

								<!-- Adjust this depending on the requirements -->

				        
								@foreach($announcements as $announcement)
									@php
										$timezone = 'Asia/Manila';
						        $now = \Carbon\Carbon::now($timezone);
						        $deliveryDate = \Carbon\Carbon::parse($announcement->announcement_deliverydate ,$timezone);
						      @endphp

			          <li data-id="{{$announcement->announcement_id}}" data-type="announcement" class="item {{(!in_array($announcement->announcement_id, $seenAnnouncements)) ? 'unseen' : ''}}">
									<span class="user-icon"><img src="{{ asset('/storage/uploads/master-admin/leapwork_logo.png') }}"></span>
			            <a href="{{ url('/applicant/news').'/'.$announcement->announcement_id}}">
									<p style="text-align: right;">
										@if($now->diffInHours($deliveryDate) <= 1)
										  {{$deliveryDate->diffForHumans()}}
										@elseif($now->diffInDays($deliveryDate) < 1)
										  {{$deliveryDate->format('h:i A')}}
										@else
										  {{$deliveryDate->format('D (m/d)')}}
										@endif
									</p>
			              <p class="text">
			                <b>Announcement: {{$announcement->announcement_title}}</b>
			                <br>
											{{ mb_strimwidth($announcement->announcement_details, 0, 100, " ...")}}
			              </p>
			            </a>
			          </li>
			        @endforeach
							<li class="read-more">
								<a href="{{url('/applicant/news')}}">
									{{__('labels.view-more')}}
								</a>
							</li>
							</div>
						  </ul>
						</li>
						@if(is_null(Auth::user()->applicant_profile))
						<li class="function user">
						  <span class="icon"></span>
						  <ul class="tooltip">
							<div class="tooltip__container">
						    <li class="item profile">
						      <a href="{{ url('applicant/profile/account') }}">
						        {{__('labels.profile')}}
						      </a>
						    </li>
						    <li class="item logout">
						      <a href="{{url('/logoutUser')}}">
						        {{__('labels.logout')}}
						      </a>
						    </li>
						  </ul>
						</li>
						@else
						<li class="function user">
						  <span class="icon">
								<img src="@if(!is_null(Auth::user()->applicant_profile->applicant_profile_imagepath))
									{{ 
										url('/storage/uploads/applicantProfile/profile_') .
										Auth::user()->applicant_profile->applicant_profile_id . 
										'/' .
										Auth::user()->applicant_profile->applicant_profile_imagepath 
									}}
								@elseif(!is_null(Auth::user()->applicant_profile->applicant_fb_image)){{Auth::user()->applicant_profile->applicant_fb_image}}@endif" alt="" srcset=""></span>
						  <ul class="tooltip">
							<div class="tooltip__container">
						    <li class="item profile">
						      <a href="{{ url('applicant/profile/account') }}">
						        {{__('labels.profile')}}
						      </a>
						    </li>
						    <li class="item logout">
						      <a href="{{url('/logoutUser')}}">
						        {{__('labels.logout')}}
						      </a>
						    </li>
						  </ul>
						</li>
						@endif
						
						@else
						<li class="function user defaultLogin">
						  <span class="icon"></span>
						  <ul class="tooltip">
						    <li class="item">
						      <a href="{{url('/login')}}">
						         {{__('labels.login')}}
						      </a>
						    </li>
						  </ul>
						</li>
						@endif
					</div>
						</ul>
				</div>
			</div><!-- .navigation -->

			<input type="hidden" id="detailsUrl" value="/applicant/news/">

			@if(Request::is('front/top'))
				<div class="header_kv">
					<img src="{{asset('images/top_kv.jpg')}}" alt="KV">
				</div><!-- .header_kv -->
			@endif

      <div id="noti-container" class="alert__modal" style="display:none;">
        <div class="alert__modal--container">
           <p id="noti-content"></p><span>x</span>
        </div>
      </div>
		</header>
	</div>