		@include('front.common.html-head')
		@include('front.common.header')
		<link rel="stylesheet" type="text/css" href="{{asset('css/featured.css')}}">

		<div class="header_kv">
			<img src="{{asset('images/top_kv.jpg')}}" alt="KV" class="pc">
			<img src="{{asset('images/top_kv_sp.jpg')}}" alt="KV" class="sp">
		</div><!-- .header_kv -->
		<div class="top">
			<div class="top_jobs">



				<!--  add 'top jobs' 2018.10.05 jotaki -->
				<div class="home-feature">
					<div class="home-feature__search">
						<form action="" class="home-feature__searchForm">
							<p class="home-feature__searchInput"><input type="text" placeholder="keyword (location, job title, category, salary)"></p>
							<p class="home-feature__searchSubmit"><input type="submit" value="Search"></p>
						</form><!-- .home-feature__searchForm -->
					</div><!-- .home-feature__search -->
					<div class="home-feature__main">
						<div class="home-feature__top">
							<div class="home-feature__topList">
								<h3 class="home-feature__ttl">Top Jobs</h3>
								<div class="home-feature__topListBlock">
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer</h4>
											<h5>Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer</h4>
											<h5>Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer</h4>
											<h5>Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer Front End Developer Front End Developer Front End Developer</h4>
											<h5>Commude Philippines Commude Philippines Commude Philippines Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines. Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
								</div><!-- .home-feature__topListBlock -->
								<div class="home-feature__topListSliderWrap">
									<div class="home-feature__topListSlider swiper-container js-top-jobs-slider">
										<ul class="swiper-wrapper">
											<li class="swiper-slide"  style="background-image:url(/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/images/top_kv.jpg)"><a href="#">
											</a></li>
										</ul>
									</div><!-- .home-feature__topListSlider -->
									<div class="swiper-button-prev js-top-jobs-slider-prev"></div>
									<div class="swiper-button-next js-top-jobs-slider-next"></div>
								</div><!-- .home-feature__topListSliderWrap -->
							</div><!-- .home-feature__topList -->
						</div><!-- .home-feature__top -->
						<div class="home-feature__recommend">
							<h3 class="home-feature__ttl">Recommend Jobs</h3>
							<div class="home-feature__recommendListBlock">
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/images/top_kv.jpg)"></p>
									<h4>Front End Developer Front End Developer Front End Developer</h4>
									<h5>Commude Philippines Commude Philippines Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
							</div><!-- .home-feature__recommendListBlock -->
						</div><!-- .home-feature__recommend -->
					</div><!-- .home-feature__main -->
				</div><!-- .home-feature -->
				<!--  add 'top jobs' 2018.10.05 jotaki -->



				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_1featuredjobs">
						<a href="#" class="top_1featuredjobs__imgContainer">
							<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
							<div class="top_featuredJob__text">
								Lorem ipsum dolor sit amet, consectetur
							</div>
						</a>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_2featuredjobs">
						<div class="top_2featuredjobs__list pc">
							<a href="#" class="top_2featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_2featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_3featuredjobs">
						<div class="top_3featuredjobs__list pc">
							<a href="#" class="top_3featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_3featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_3featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_4featuredjobs">
						<div class="top_4featuredjobs__list pc">
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
							<a href="#" class="top_4featuredjobs__list_item">
								<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
								<div class="top_featuredJob__text">
									Lorem ipsum dolor sit amet, consectetur
								</div>
							</a>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_5featuredjobs">
						<div class="top_5featuredjobs__list pc">
							<div class="top_featuredjobs_item_sub01">
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet, consectetur
									</div>
								</a>
							</div>
							<div class="top_featuredjobs_item_sub02">
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
								<a href="#">
									<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_blankimage_1200x400.jpg')}}')"></div>
									<div class="top_featuredJob__text">
										Lorem ipsum dolor sit amet
									</div>
								</a>
							</div>
						</div>
						<div class="swiper-container sp">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
								<div class="swiper-slide">
									<a href="#">
										<div class="slider__imgcontainer">
											<img src="{{asset('images/top_blankimage.jpg')}}" alt="">
										</div>
										<div class="top_featurejobs_item_sub_p">
											<p>Lorem ipsum dolor sit amet, consectetur</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</section>
				</div>

				<div class="top_featuredjobs_container">
					<h2>{{__('labels.featured-jobs')}}</h2>
					<section class="top_0featuredjobs">
						<a href="#" class="top_1featuredjobs__imgContainer">
							<div class="top_featuredJob__img" style="background-image: url('{{asset('images/top_noimage_1200x400.jpg')}}')"></div>
							<img src="{{asset('images/top_noimage_1200x400.jpg')}}" alt="">
						</a>
					</section>
				</div>


				<div class="breadcrumbs_pagetitle">
					<h2>{{__('labels.latest-jobs')}}</h2>
				</div>
				@include('front.common.front')
			</div>
		</div>
		@include('front.common.footer')
