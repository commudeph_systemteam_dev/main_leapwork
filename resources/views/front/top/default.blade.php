		@include('front.common.html-head')
		@include('front.common.header')
		<div class="header_kv">
			<img src="{{asset('images/top_kv.jpg')}}" alt="KV" class="pc">
			<img src="{{asset('images/top_kv_sp.jpg')}}" alt="KV" class="sp">
		</div><!-- .header_kv -->
		<div class="top">
			<div class="top_jobs">

				<!--  add 'top jobs' 2018.10.05 jotaki -->
				<div class="home-feature">
					<div class="home-feature__search">
						<form action="" class="home-feature__searchForm">
							<p class="home-feature__searchInput"><input type="text" placeholder="keyword (location, job title, category, salary)"></p>
							<p class="home-feature__searchSubmit"><input type="submit" value="Search"></p>
						</form><!-- .home-feature__searchForm -->
					</div><!-- .home-feature__search -->
					<div class="home-feature__main">
						<div class="home-feature__top">
							<div class="home-feature__topList">
								<h3 class="home-feature__ttl">Top Jobs</h3>
								<div class="home-feature__topListBlock">
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer</h4>
											<h5>Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer</h4>
											<h5>Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer</h4>
											<h5>Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
									<div class="home-feature__topListGroup">
										<p class="home-feature__topListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p><!-- .home-feature__topListImg -->
										<div class="home-feature__topListTxt">
											<h4>Front End Developer Front End Developer Front End Developer Front End Developer</h4>
											<h5>Commude Philippines Commude Philippines Commude Philippines Commude Philippines</h5>
											<p>Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines. Unit G-2 New Solid Building, 357 Sen. Gil Puyat Avenue, Makati City, Metro Manila 1200, Philippines.</p>
										</div><!-- .home-feature__topListTxt -->
										<div class="home-feature__topListLink">
											<h6>0-24-2108</h6>
											<p><a href="#">APPLY</a></p>
										</div><!-- .home-feature__topListLink -->
									</div><!-- .home-feature__topListGroup -->
								</div><!-- .home-feature__topListBlock -->
								<div class="home-feature__topListSliderWrap">
									<div class="home-feature__topListSlider swiper-container js-top-jobs-slider">
										<ul class="swiper-wrapper">
											<li class="swiper-slide"  style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"><a href="#">
											</a></li>
											<li class="swiper-slide"  style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"><a href="#">
											</a></li>
										</ul>
									</div><!-- .home-feature__topListSlider -->
									<div class="swiper-button-prev js-top-jobs-slider-prev"></div>
									<div class="swiper-button-next js-top-jobs-slider-next"></div>
								</div><!-- .home-feature__topListSliderWrap -->
							</div><!-- .home-feature__topList -->
						</div><!-- .home-feature__top -->
						<div class="home-feature__recommend">
							<h3 class="home-feature__ttl">Recommend Jobs</h3>
							<div class="home-feature__recommendListBlock">
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer</h4>
									<h5>Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer Front End Developer Front End Developer</h4>
									<h5>Commude Philippines Commude Philippines Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer Front End Developer Front End Developer</h4>
									<h5>Commude Philippines Commude Philippines Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer Front End Developer Front End Developer</h4>
									<h5>Commude Philippines Commude Philippines Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
								<div class="home-feature__recommendListGroup">
									<p class="home-feature__recommendListImg" style="background-image:url(/leapwork_test/public/images/top_kv.jpg)"></p>
									<h4>Front End Developer Front End Developer Front End Developer</h4>
									<h5>Commude Philippines Commude Philippines Commude Philippines</h5>
									<p class="home-feature__recommendListLink"><a href="#">See More</a></p>
								</div><!-- .home-feature__recommendListGroup -->
							</div><!-- .home-feature__recommendListBlock -->
						</div><!-- .home-feature__recommend -->
					</div><!-- .home-feature__main -->
				</div><!-- .home-feature -->
				<!--  add 'top jobs' 2018.10.05 jotaki -->
				{{--  //@include('front.common.front')  --}}
			</div>
		</div>
		
		
		@include('front.common.footer')
	
