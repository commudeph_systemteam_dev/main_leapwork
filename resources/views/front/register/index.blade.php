<div align="center">
	<form action="<?php echo SITE_URL . 'top'; ?>" method="post">
		<table>
			<tr>
				<th>{{ __('labels.email-address') }}</th>
				<td><input type="text" id="email_address" name="email_address"/></td>
			</tr>
			<tr>
				<th>{{ __('labels.username') }}</th>
				<td><input type="text" id="username" name="username"/></td>
			</tr>
			<tr>
				<th>{{ __('labels.password') }}</th>
				<td><input type="password" id="password" name="password"/></td>				
			</tr>
		</table>
		<input type="submit" name="submit_btn" id="submit_btn" value="Submit"/>
	</form>
</div>