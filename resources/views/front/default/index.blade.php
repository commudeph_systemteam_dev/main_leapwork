@include('front.common.html-head')
	<!-- css -->
	<link rel="stylesheet" type="text/css" media="screen" href="{{url('/css/login.css')}}" />

	<div class="content login" id="top">

		<header class="header">
			<div class="navigation">

			{{-- @if(config('app.debug'))
				<span>VERSION {{config('app.version')}}</span>
			@endif --}}
				<h1 class="logo">
					<a href="#"><img src="{{url('images/logo.svg')}}" alt="LEAPWORK"></a>

				</h1>

				<nav class="navigation__menu">
				</nav>
				<div class="navigation_features">
				<div class="login_buttons--top">
					<a href="{{url('/login')}}" class="button_facebook">{{Lang::get('labels.login')}}</a><a href="{{url('/register-company')}}" class="button_email">{{Lang::get('labels.click-here-for-employers')}}</a>
				</div>
			</div>

			</div><!-- .navigation -->
		</header><!-- .header -->

		<div class="login_buttons">
			<!-- <a href="{{url('/redirect')}}" class="button_facebook">Login with Facebook</a> -->
			 <!-- <a href="{{url('/auth/facebook')}}" class="button_facebook">Login with Facebook</a>  -->
			<a href="{{url('/front/top')}}" class="button_facebook">{{Lang::get('labels.view-jobs')}}</a>
			<a href="{{url('/register')}}" class="button_email">{{Lang::get('labels.register')}}</a>
		</div>

	</div>

</body>
