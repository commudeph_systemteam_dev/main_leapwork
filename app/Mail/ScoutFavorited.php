<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ApplicantsProfile;
use App\Models\MasterMailTemplate;
use App\Models\Company;
use Auth;

class ScoutFavorited extends Mailable
{
    use Queueable, SerializesModels;

    public $mailTemplate;
    public $applicant;
    /** Currently not used but serves as a template on how to use Laravel Mailables.
     * Create a new message instance.
     * Inject ApplicantsProfile $applicant
     * Inject MasterMailTemplate $mailTemplate
     * @return void
     */
    public function __construct(ApplicantsProfile $applicant, MasterMailTemplate $mailTemplate)
    {
        $this->applicant    = $applicant;
        $this->mailTemplate = $mailTemplate;
    }

    /**
     * Build the message.
     * We will use preg_replace for replacing varibales stored from the master_mail_template
     * Note on the view (mailables.scout.scout-favorited)
     * you must use {!!$body!!} to escape html tags.
     * For e.g. $body = <br><br> <h1> hello world! </h1>
     * using {!!$body!!} will render the br and h1 html tags, not doing so will yield string only.
     * @author Karen Cano
     * @return $this
     */
    public function build()
    {
        $bodyTemplate = $this->mailTemplate->master_mail_body;
        /* pattern to be replaced */
        $bodyPatterns = array();
        $bodyPatterns[0] = '/{{applicantName}}/';
        $bodyPatterns[1] = '/{{companyName}}/';
        $bodyPatterns[2] = '/{{companyAdmin}}/';
        $bodyPatterns[3] = '/{{companyAdminEmail}}/';
        $bodyPatterns[4] = '/{{companyAdminAddress}}/';
        $bodyPatterns[5] = '/{{jobLink}}/';
        $bodyPatterns[6] = '/{{jobPositionName}}/';
        $bodyPatterns[7] = '/{{leapworkLogo}}/';
        
        /* pattern to be inject on that variable */
        
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = $this->applicant->applicant_profile_firstname
                                    .' '.$this->applicant->applicant_profile_middlename
                                    .' '.$this->applicant->applicant_profile_lastname;

        $company = Company::find(Auth::user()["user_company_id"])->first();

        $replaceCompanyPatterns[1] = $company->company_name;
        $replaceCompanyPatterns[2] = $company->company_contact_person;
        $replaceCompanyPatterns[3] = $company->company_email;
        $replaceCompanyPatterns[4] = $company->company_postalcode
                                     ."<br>".$company->company_address1
                                     ."<br>".$company->company_address2;
        $replaceCompanyPatterns[7] = '<img src="' . url('images/leapwork.PNG') .'" alt="">';
                                     
        
        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from('dev-no-reply@leapwork.com')
                    ->subject($this->mailTemplate->master_mail_subject)
                    ->view('mailables.scout.scout-favorited')
                    ->with('body', $body);
    }
}
