<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\MasterMailTemplate;
use App\Models\ApplicantsProfile;
use Auth;

class RegisterUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $mailTemplate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ApplicantsProfile $user, MasterMailTemplate $mailTemplate)
    {
        //
        $this->user         = $user;
        $this->mailTemplate = $mailTemplate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /* pattern to be replaced */
        $bodyPatterns = array();
        $bodyPatterns[0] = '/{{email}}/';
        $bodyPatterns[1] = '/{{name}}/';
        $bodyPatterns[2] = '/{{!!loginLink!!}}/';
        $bodyPatterns[3] = '/{{!!activationLink!!}}/';
        $bodyPatterns[4] = '/{{leapworkLogo}}/';

       $subjectTemplate = $this->mailTemplate->master_mail_subject;

       /* pattern to be replaced */    
       $subjectPatterns = array();
       /* pattern to be inject on that variable */
       $replaceSubjectPatterns = array();
       $replaceSubjectPatterns[0] = ucwords($this->user->applicant_profile_preferred_email);
       $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);

       $applicantUser = User::where('email',$this->user->applicant_profile_preferred_email)->first();

       $bodyTemplate = $this->mailTemplate->master_mail_body;
       /* pattern to be inject on that variable */
       $replaceUserPatterns = array();
       $replaceUserPatterns[0] = ucwords($this->user->applicant_profile_preferred_email);
       $replaceUserPatterns[1] = $this->user->applicant_profile_firstname . ' ' . $this->user->applicant_profile_lastname;
       $replaceUserPatterns[2] = '<a href="'.url('/login').'">Click here</a>';
       $replaceUserPatterns[3] = '<a href="'. url('activate/'.$applicantUser->activation_token) .'">Click here</a>';
       $replaceUserPatterns[4] = '<img src="' . url('images/leapwork.PNG') .'" alt="">';

       $body = preg_replace($bodyPatterns, $replaceUserPatterns, $bodyTemplate);
       
       return $this->from(config('mail.from.address'))
                   ->subject($subject)
                   ->view('mailables.user.newly-registered-user')
                   ->with('body', $body);
    }
}
