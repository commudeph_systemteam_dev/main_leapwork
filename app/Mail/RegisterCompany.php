<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Company;
use App\Models\MasterMailTemplate;
use Auth;

class RegisterCompany extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $mailTemplate;
    
    public function __construct(Company $company,MasterMailTemplate $mailTemplate)
    {
        $this->company    = $company;
        $this->mailTemplate    = $mailTemplate;
    }

    /**
    * @author Karen Cano
     * Build the message.
     * We will use preg_replace for replacing varibales stored from the master_mail_template
     * Note on the view (mailables.scout.scout-favorited)
     * you must use {!!$body!!} to escape html tags.
     * For e.g. $body = <br><br> <h1> hello world! </h1>
     * using {!!$body!!} will render the br and h1 html tags, not doing so will yield string only.
     * @author Ian Sebastian
     * Added every Company Status can send mails.
     * @return $this
     */
    public function build()
    {
        $masterMailTemplate = MasterMailTemplate::where('master_mail_status',$this->mailTemplate)
                                                ->select('master_mail_subject', 'master_mail_body')
                                                ->first();
         /* pattern to be replaced */
         $bodyPatterns = array();
         $bodyPatterns[0] = '/{{companyName}}/';
         $bodyPatterns[1] = '/{{companyAdmin}}/';
         $bodyPatterns[2] = '/{{companyContactName}}/';
         $bodyPatterns[3] = '/{{!!loginLink!!}}/';
         $bodyPatterns[4] = '/{{currentPlan}}/';
         $bodyPatterns[5] = '/{{leapworkLogo}}/';

        $subjectTemplate = $this->mailTemplate->master_mail_subject;

        /* pattern to be replaced */    
        $subjectPatterns = array();
        /* pattern to be inject on that variable */
        $replaceSubjectPatterns = array();
        $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
        $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);

        $bodyTemplate = $this->mailTemplate->master_mail_body;
        /* pattern to be inject on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
        $replaceCompanyPatterns[1] = $this->company->company_email;
        $replaceCompanyPatterns[2] = $this->company->company_contact_person;
        $replaceCompanyPatterns[3] = '<a href="'.url('/login').'">Click here</a>';
        $replaceCompanyPatterns[4] = $this->company->company_current_plan;
        $replaceCompanyPatterns[5] = '<img src="' . url('images/leapwork.PNG') .'" alt="">';

        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from(config('mail.from.address'))
                    ->subject($subject)
                    ->view('mailables.company.newly-registered-company')
                    ->with('body', $body);
    }
}
