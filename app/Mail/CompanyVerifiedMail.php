<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Company;
use App\Models\Claim;
use App\Models\MasterMailTemplate;
use App\Models\MasterPlans;

use Auth;

class CompanyVerifiedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $company;

    public function __construct(Company $company)
    {
        $this->company  = $company;
    }

    /**
     * Main boot for mail content
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @desc    Added Master Plan upon Company Verification
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23 Louie: Initial Release
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    public function build()
    {   
        $masterMailTemplate = MasterMailTemplate::where('master_mail_status','COMPANY VERIFIED')
                                                ->select('master_mail_subject', 'master_mail_body')
                                                ->first();

        $subjectTemplate = $masterMailTemplate->master_mail_subject;
        $bodyTemplate = $masterMailTemplate->master_mail_body;
        

         /* pattern to be replaced */
         $bodyPatterns = array();
         $bodyPatterns[0] = '/{{companyName}}/';
         $bodyPatterns[1] = '/{{companyContactName}}/';
         $bodyPatterns[2] = '/{{appUrl}}/';
         $bodyPatterns[3] = '/{{appEmail}}/';
         $bodyPatterns[4] = '/{{dateSent}}/';
         $bodyPatterns[5] = '/{{planName}}/';
         $bodyPatterns[6] = '/{{planAmount}}/';
         $bodyPatterns[7] = '/{{leapworkLogo}}/';

        $planDetails = MasterPlans::where('master_plan_name',$this->company->company_current_plan)
                        ->first();

        /* pattern to be replaced */    
        $subjectPatterns = array();
        /* pattern to be inject on that variable */
        $replaceSubjectPatterns = array();
        $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
       
        $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);
        
        /* pattern to be inject on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
        $replaceCompanyPatterns[1] = $this->company->company_contact_person;
        $replaceCompanyPatterns[2] = url('/login');
        $replaceCompanyPatterns[3] = config('constants.APP_EMAIL');
        $replaceCompanyPatterns[4] = date("F d, Y h:i:s A",strtotime($this->company->company_date_registered));
        $replaceCompanyPatterns[5] = $planDetails->master_plan_name;
        $replaceCompanyPatterns[6] = ($planDetails->master_plan_price == 0)
                                        ? 'FREE'
                                        : 'PHP ' . number_format($planDetails->master_plan_price, 2, '.', ',');
        $replaceCompanyPatterns[7] = '<img src="' . url('images/leapwork.PNG') .'" alt="">';
       
        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

        return $this->from(config('constants.APP_NO_REPLY_EMAIL'))
                    ->subject($subject)
                    ->view('mailables.company.generic')
                    ->with('body', $body);
    }
    

    /**
     * Content mail builder
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/23 Louie: Initial Release
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    private function createCompanyVerificationMail(string $subjectTemplate, string $bodyTemplate)
    {
        
    }
    

    /**
     * Content mail builder
     * 
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * 
     * @param  string $subjectTemplate
     * @param  string $bodyTemplate
     * 
     * @return void
     */
    private function createCompanyClaimVerification(string $subjectTemplate, string $bodyTemplate)
    {
        /* pattern to be replaced */
        $bodyPatterns = array();
        $bodyPatterns[0] = '/{{companyName}}/';
        $bodyPatterns[1] = '/{{companyAdmin}}/';
        $bodyPatterns[2] = '/{{companyContactName}}/';
        $bodyPatterns[3] = '/{{claimPaymentMethod}}/';
        $bodyPatterns[4] = '/{{claimPlan}}/';
        $bodyPatterns[5] = '/{{claimPlanAmount}}/';
        $bodyPatterns[6] = '/{{bankName}}/';
        $bodyPatterns[7] = '/{{bankAccountName}}/';
        $bodyPatterns[8] = '/{{bankAccountNo}}/';         
        $bodyPatterns[9] = '/{{dateSent}}/';
        $bodyPatterns[10] = '/{{leapworkLogo}}/';

       /* pattern to be replaced */    
       $subjectPatterns = array();
       /* pattern to be inject on that variable */
       $replaceSubjectPatterns = array();
       $replaceSubjectPatterns[0] = ucwords($this->company->company_name);
      
       $subject = preg_replace($bodyPatterns, $replaceSubjectPatterns, $subjectTemplate);

       /* pattern to be inject on that variable */
       $replaceCompanyPatterns = array();
       $replaceCompanyPatterns[0] = ucwords($this->company->company_name);
       $replaceCompanyPatterns[1] = $this->company->company_name;
       $replaceCompanyPatterns[2] = $this->company->company_contact_person;
       $replaceCompanyPatterns[3] = $this->claim->claim_payment_method;
       $replaceCompanyPatterns[4] = ucwords($this->claim->master_plans->master_plan_name);
       $replaceCompanyPatterns[5] = number_format($this->claim->master_plans->master_plan_price,2);
       $replaceCompanyPatterns[6] = config('constants.BANK_TRANSFER_MAIL_PATTERNS')['bankName'];
       $replaceCompanyPatterns[7] = config('constants.BANK_TRANSFER_MAIL_PATTERNS')['bankAccountName'];
       $replaceCompanyPatterns[8] = config('constants.BANK_TRANSFER_MAIL_PATTERNS')['bankAccountNo'];
       $replaceCompanyPatterns[9] = date("F d, Y h:i:s A",strtotime($this->claim->claim_datecreated));
       $replaceCompanyPatterns[10] = '<img src="' . url('images/leapwork.PNG') .'" alt="">';
      
       $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate);

       return $this->from('dev-no-reply@leapwork.com')
                   ->subject($subject)
                   ->view('mailables.company.generic')
                   ->with('body', $body);
    }

    
}
