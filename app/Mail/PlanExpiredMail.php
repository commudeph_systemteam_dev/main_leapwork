<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlanExpiredMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * this variable will automatically available at the mail view
     */
    public $data = 'Your Plan is about to expire.';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@leapwork.com')
                    ->subject('Plan is about to expire.')
                    ->view('mailables.company.plan-expiry');
    }
}
