<?php

namespace App\Listeners;

use App\Events\PusherEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Pusher;

class TriggerPusherEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PusherEvent  $event
     * @return void
     */
    public function handle(PusherEvent $event)
    {
        //
        $pusher = new Pusher(
            config('app.PUSHER_APP_KEY'),
            config('app.PUSHER_APP_SECRET'),
            config('app.PUSHER_APP_ID'),
            [
              'encrypted' => true,
              'cluster'   => config('app.PUSHER_APP_CLUSTER')
            ]
        );



        $out = $pusher->trigger($event->data['channel'], $event->data['event'], $event->data['data'], null, true);

        Log::info($out);
    }
}
