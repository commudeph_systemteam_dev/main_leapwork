<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use App\Models\User;
use Auth;
use App\Models\ScoutThread;
use App\Models\AdminCompanyThread;
use App\Models\JobApplicationThread;
use App\Models\CompanyInquiryThread;

use App\Models\JobInquiryReply;

use Pusher;

/**
 * Model of Notification - notification_data
 * 
 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-22
 */
trait GlobalTrait
{
	  /**
	 * userName
	 * Return the userName based from user_accounttype
     * E.g {{$model->userName($model->model_user_id)}}
     * Sample @comment-details.blade.php
     * {{$reply->userProperties($reply->job_inquiry_reply_user_id)["userName"]}}
	 * @author Karen Irene Cano <karen_cano@commude.ph>
	 * @param  userId
	 * @return string userName
	 */
    public function userProperties($userId,$notification=null)
	{
        $user = User::find($userId);
        $properties = array();

		switch($user["user_accounttype"])
        {
            case 'USER':
                $userName = $user->applicant_profile->applicant_profile_firstname
                                ." ".$user->applicant_profile->applicant_profile_middlename
                                ." ".$user->applicant_profile->applicant_profile_lastname;
                $properties["userName"] = ucwords($userName);
                $properties["admin_notification_url"] = '/applicant/news/';
				        $properties["scout_notification_url"] = '/applicant/scout-detail/';
                $properties["job_application_notification_url"] = '/applicant/application-history/details/';
                $properties["admin_messsage_notification_url"] = '';
				        $properties["scout_url_param1"] = 'scout_id';
                $properties["scout_url_param2"] = 'notification_id';
                $properties["inquiry_notification_url"] = '/applicant/comment-details/';
                $properties["inquiry_url_param1"] = 'job_inquiry_reply_inquiry_id';
                $properties["inquiry_url_param2"] = 'notification_id';
                $properties["job_application_notification_url"] = 'test';
                
                $properties["url"] = '';
                $properties["type"] = '';

                if($notification != null)
                {
                    $properties = $this->userPusherNotificationLinks($notification,$properties);
                }

                return $properties;
            case 'CORPORATE ADMIN':
            case 'CORPORATE USER':
                $userName = !is_null($user->company_profile) 
                    ?  $user->company_profile->company_contact_person 
                    : 'User';
                $properties["userName"] = ucwords($userName);
                $properties["admin_notification_url"] = 'company/notice/details/';
                $properties["scout_notification_url"] = '/company/scout/favorite-user/choosTemplate/';
                $properties["job_application_notification_url"] = '/company/applicant/job-application/';
                $properties["admin_messsage_notification_url"]  = '/company/notice/details/';
                $properties["company_inquiry_notification_url"]  = '/company/inquiry/';
				$properties["scout_url_param1"] = 'scout_applicant_id';
				$properties["scout_url_param2"] = 'notification_id';
                $properties["inquiry_notification_url"] = '/company/applicant/questionaire/detail/';
                $properties["inquiry_url_param1"] = 'job_inquiry_reply_inquiry_id';
                $properties["inquiry_url_param2"] = 'notification_id';

                $properties["url"] = '';
                $properties["type"] = '';

                if($notification != null)
                {
                    $properties = $this->companyPusherNotificationLinks($notification,$properties);
                }
                $properties["new_applicant_notification_url"] = '/company/applicant';

                return $properties;
            case 'ADMIN':
				$adminEmail = explode('@',$user->email);
                $userName = $adminEmail[0];

                $properties["userName"] = ucwords($userName);
                $properties["url"] = '';
                $properties["type"] = '';
                $properties["company_replies_notification_url"]  = '/admin/inquiry/';

                if($notification != null)
                {
                    $properties = $this->adminPusherNotificationLinks($notification,$properties);
                }

                
                return $properties;
        }//end switch
    }

    
    public function adminPusherNotificationLinks($notification, $properties)
    {
        switch($notification->notification_type)
        {
            case 'COMPANY REPLIES':
                $adminApplicationThread = CompanyInquiryThread::find($notification->notification_type_id);
                $properties["url"]      = $properties["company_replies_notification_url"]; // no custom mapping yet - general view only
                $properties["type"]     = 'message';
                
                return $properties;
        }
    }

    /**
     * userPusherNotificationLinks
     * Add properties to be consumed for the CreateNotificationListener.broadcastOn()
     * based from Notification.notification_type for the USER user_accounttype
     * @author    Karen Irene Cano     <karen_cano@commmude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-12-26
     */
    public function userPusherNotificationLinks($notification,$properties)
    {

        switch($notification->notification_type)
        {
            case 'SCOUT':
                $scoutThread       = ScoutThread::find($notification->notification_type_id);
                $properties["url"] = $properties["scout_notification_url"] .
                                     $scoutThread->scout_thread_scout_id .
                                     "/" .
                                     $notification->notification_id;
                return $properties; 
            case 'INQUIRY':
                $jobInquiryReplies = JobInquiryReply::find($notification->notification_type_id);
                $properties["url"] = $properties["inquiry_notification_url"] .
                                     $jobInquiryReplies->job_inquiry_reply_inquiry_id .
                                     "/" .
                                     $notification->notification_id;
                return $properties;
            case 'JOB APPLICATION':
                $jobApplicationThread = JobApplicationThread::find($notification->notification_type_id);
                $properties["url"]    = $properties["job_application_notification_url"] .
                                        $jobApplicationThread->job_application->job_application_id;
                return $properties; 
            case config('constants.notification_type.JOB_APPLICATION_CONVERSATION'):
                $jobApplicationThread = JobApplicationThread::find($notification->notification_type_id);
                $properties["url"]    = $properties["job_application_notification_url"] .
                                        $jobApplicationThread->job_application->job_application_id;
                return $properties; 

        }
    }

    /**
     * Unified template to get the url link for each notification
     * based on the notification type
     * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-12-27
     */
    public function companyPusherNotificationLinks($notification,$properties)
    {
      
        switch($notification->notification_type)
        {
            case 'SCOUT':
                $scoutThread        = ScoutThread::find($notification->notification_type_id);
                $properties["url"]  = $properties["scout_notification_url"] .
                                      $scoutThread->scout->scout_applicant_id .
                                      "/" .
                                      $notification->notification_id;
                $properties["type"] = 'message';
                return $properties; 
            case 'INQUIRY':
                $jobInquiryReplies  = JobInquiryReply::find($notification->notification_type_id);
                $properties["url"]  = $properties["inquiry_notification_url"] .
                                      $jobInquiryReplies->job_inquiry_reply_inquiry_id .
                                      "/" .
                                      $notification->notification_id;
                $properties["type"] = 'message';

                return $properties;
            case 'JOB APPLICATION':
                $jobApplicationThread = JobApplicationThread::find($notification->notification_type_id);
                $properties["url"]    = $properties["job_application_notification_url"] .
                                        $jobApplicationThread->job_application->job_application_id;
                $properties["type"]   = 'message';
                return $properties;
            case 'ADMIN CONTACT':
                $adminApplicationThread = AdminCompanyThread::find($notification->notification_type_id);
                $properties["url"]      = $properties["admin_messsage_notification_url"] .
                                          $adminApplicationThread->admin_company_thread_company_id;
                $properties["type"]     = 'message';
                
                return $properties;
            case 'COMPANY INQUIRY':
                $adminApplicationThread = CompanyInquiryThread::find($notification->notification_type_id);
                $properties["url"]      = $properties["company_inquiry_notification_url"]; // no custom mapping yet - general view only
                $properties["type"]     = 'message';
                
                return $properties;
            //alvin: added default to avoid showing of errors
            default:
                $properties["url"]      = '';
                $properties["type"]     = 'message';

                return $properties;
        }
    }


    /**
    * will create a event on the notification channel
    * 
    * @param Announcements $data: the data to be sent
    * @author Alvin Generalo
    */
    public static function sendNotification($data)
    {
      $channel;

      if($data->announcement_target == 'COMPANY'){
        //anouncement to COMPANY
        if($data->announcement_to == 'ALL'){
          //announcement to ALL COMPANY
          $channel = 'notification-channel-company';
        } else {
          //announcement to SPECIFIC COMPANY
          if($data->announcement_to_user == 'ALL'){
            //announcement to SPECIFIC COMPANY and ALL USERS
            $channel = 'notification-channel-'.$data->announcement_to;
          } else {
            //announcement to SPECIFIC COMPANY and SPECIFC USER
            $channel = 'notification-channel-'.$data->announcement_to.'-'.$data->announcement_user_id;
          }

        }
      } else if($data->announcement_target == 'USER'){
        //announcement to a specific USER
        if($data->announcement_to_user != 'ALL'){
          //announcement to a specific USER
          $channel = 'notification-channel-user-'.$data->announcement_user_id;
        } else {
          //announcement to all COMPANY and USERS
          $channel = 'notification-channel';
        }
      } else {
        //announcement to all COMPANY and USERS
        $channel = 'notification-channel';
      }

      if(self::readyToDeliver($data->announcement_deliverydate, 'Asia/Manila')){
        self::createPusherEvent($data, $channel, 'update');
      }
    }


    /**
    * send a notification through pusher
    *
    *
    * @param array $data : data to be passed through pusher
    * @param string $channel : the channel to be used
    * @param string $event : the event to be triggered
    *
    * @author Alvin Generalo 
    */
    public static function createPusherEvent($data, $channel, $event)
    {
      $pusher = new Pusher(
          config('app.PUSHER_APP_KEY'),
          config('app.PUSHER_APP_SECRET'),
          config('app.PUSHER_APP_ID'),
          [
              'encrypted' => true,
              'cluster'   => config('app.PUSHER_APP_CLUSTER')
          ]
      );

      //NOTE: the trigger function will automatically convert $data to array
      $pusher->trigger($channel, $event, $data);

    }

    
    /**
    * will compare the current date to the input date
    * 
    * @param string $date : the date to compare 
    * @param string $timezone : the timezone to use
    * 
    * @return boolean : TRUE if the date is less than or equal to the current date 
    * @author Alvin Generalo
    */
    public static function dateLessThanToday($date, $timezone = null)
    {
      $dateToday = is_null($timezone) 
                    ? Carbon::now()
                    : Carbon::now($timezone);

      $testDate = is_null($timezone)
                        ? Carbon::parse($date)
                        : Carbon::parse($date, $timezone);

      return $testDate->lte($dateToday);


    }

    /**
    * will determine if the announcement should be sent
    * 
    * @param string $date : the delivery date
    * @param string $timezone : the timezone to use
    * 
    * @return boolean : TRUE if the announcement is ready to be sent
    * @author Alvin Generalo
    */
    public static function readyToDeliver($date, $timezone = null)
    {
      return self::dateLessThanToday($date, $timezone);
    }



}
