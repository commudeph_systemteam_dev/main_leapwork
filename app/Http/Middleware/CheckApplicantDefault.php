<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Scout;
use App\Models\JobInquiryReply;
use App\Models\ApplicantsProfile;

class CheckApplicantDefault
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) 
        {
            if(Auth::user()->user_accounttype == 'USER'){
                /**
                 * Get all notifications for this user
                 */
                Auth::user()["scoutObject"] = Auth::user()->applicant_profile->scouts;
               
                $messageNotifications = array();
                $messageNotifications = ApplicantsProfile::getNotification($messageNotifications);
                Auth::user()["user_message_notifications"] = $messageNotifications;

                $bellNotifications = array();
                
                Auth::user()["user_bell_notifications"] = $bellNotifications;
               
            }
            // Auth::logout($request);
        }
        return $next($request);
    }
}
