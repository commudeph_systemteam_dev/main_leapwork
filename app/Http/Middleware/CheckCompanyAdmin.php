<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class CheckCompanyAdmin
{
    /**
     * Handle an incoming request.
     * @author Alvin Generalo
     *         Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    // protected $redirectTo = '/company';
    
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            /**
             *  Switch added by : Karen Cano <karen_cano@commude.ph>
             *  Let's make it a switch and not company_status != 'ACTIVE' 
             *  I feel there will be module to foreach company_status
             *  @since 11-27-2017
             */

            if(Auth::user()->user_accounttype == "CORPORATE ADMIN")
            {
                if(!is_null(Auth::user()->company_profile)) {
                    switch(Auth::user()->company_profile->company_status){
                        case "ACTIVE":
                        case "VERIFIED":
                            return $next($request);
                        case "INACTIVE" :
                            $this->clearAuthUserCustomAttributes();
                            Auth::logout();
                            // return redirect('/login')->with('errorMessage',"You're account is INACTIVE and is still under review."); 
                            return redirect('/login')->with('errorMessage',"You're account does not exist."); 
                        case "DISABLED" :
                            $this->clearAuthUserCustomAttributes();
                            Auth::logout();
                            return redirect('/login')->with('errorMessage',"You're account has been disabled"); 
                        default :
                            $this->clearAuthUserCustomAttributes();
                            Auth::logout();
                            return redirect('/login')->with('errorMessage',"You're account does not exist."); 
                    }
                } else {
                    return $next($request);
                }
                
            }
           return redirect('/error')->with('errorMessage',"You are not authorized to access this page."); 
        } else {
            return redirect('/login');
        }
    }
    
    public function clearAuthUserCustomAttributes()
    {
        unset(Auth::user()->scoutObject); 
        unset(Auth::user()->user_message_notifications); 
        unset(Auth::user()->user_bell_notifications); 
    }
}
