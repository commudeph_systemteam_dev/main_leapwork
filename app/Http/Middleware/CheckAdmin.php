<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Scout;
use App\Models\JobInquiryReply;
use App\Models\AdminCompanyConversation;
use App\Models\Notification;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     * @author Alvin Generalo
     *         Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) 
        {
            if(Auth::user()->user_accounttype == 'ADMIN'){
                /**
                 * Get all notifications for this user
                 */
                $notifications = array();
                Auth::user()["user_notifications"] = $notifications;

                $messageNotifications = array();
                $messageNotifications = AdminCompanyConversation::getMessageIconAdminNotifications($messageNotifications);
                Auth::user()["user_message_notifications"] = $messageNotifications;

                $bellNotifications = array();
                $bellNotifications = Notification::getAllUnreadByType('NEW_COMPANY', Auth::user()['id']);
                Auth::user()["user_bell_notifications"] = $bellNotifications;
                // dd($messageNotifications);       

                return $next($request);
            }
            // Auth::logout($request);
            return redirect('/error')->with('errorMessage','You are not authorized to access this page.'); 
        } 
        else 
        {
            return redirect('/login');
        }      
    }
}
