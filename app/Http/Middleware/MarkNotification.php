<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\Notification;

class MarkNotification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!is_null($request->notification_id)) {
            Notification::markAsRead($request->notification_id);
        }

        return $next($request);
    }
}
