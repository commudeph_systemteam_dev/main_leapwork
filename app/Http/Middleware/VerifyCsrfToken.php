<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
       // 'home'//endpoint name.you can check your json via postman.
        // disable your middlewares and hard code necessary id's/like user id.-Karen
        '/masterbuild'//do not delete. used for auto build - Karen (12-27-2017)
    ];
}
