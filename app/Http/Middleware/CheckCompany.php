<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use App\Models\Scout;
use App\Models\Company;
use App\Models\JobInquiryReply;

use App\Http\Services\NotificationService;

class CheckCompany
{
    /**
     * Handle an incoming request.
     * @author Alvin Generalo
     *         Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    // protected $redirectTo = '/company';
    
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            
            if(Auth::user()->user_accounttype == "CORPORATE ADMIN" || Auth::user()->user_accounttype == 'CORPORATE USER'){
                /**
                 * Get all notifications for this user
                 */
                

                !is_null(Auth::user()->company_profile) 
                    ? Auth::user()["scoutObject"] = Auth::user()->company_profile->scouts 
                    : false;
                    
                $messageNotifications = array();
                $messageNotifications = Company::getMessageIconCompanyNotifications($messageNotifications);
                Auth::user()["user_message_notifications"] = $messageNotifications;

                $bellNotifications = array();
                $bellNotifications = Company::getBellIconCompanyNotifications($bellNotifications);
                Auth::user()["user_bell_notifications"] = $bellNotifications;
                
                return $next($request);
            }
           return redirect('/error')->with('errorMessage','You are not authorized to access this page.'); 
        } else {
            return redirect('/login');
        }
    }
}
