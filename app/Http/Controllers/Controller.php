<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

use App\Models\User;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
     /**
     * Get redirectBackTo path based from user_accounttype
     * @author    Karen Cano <karen_cano@commude.ph>
     * @return $this->redirectBackTo
     */
    public function redirectBackPath()
    {
        $redirectBackTo = "";
        $accountType = Auth::user()["user_accounttype"];
        switch($accountType)
        {
            case "USER":
                    $redirectBackTo = '/front/top/default';
                break;
            case "CORPORATE ADMIN":
            case "CORPORATE USER":
                $redirectBackTo = '/company/home';
                break;

            case "ADMIN":
                $redirectBackTo = '/admin';
                break;
            default :
                $redirectBackTo = '/';
                break;
        }

        return $redirectBackTo;
    }


    public function seeLogin($fave = null)
    {
        if(isset(Auth::user()["user_accounttype"]))
        { 
            return redirect(self::redirectBackPath());
        }
        // elseif(!is_null($fave)){
        //     return redirect('login');
        // }

        return view('front.default.index');
    }

    public function userActivation($token)
    {
        $user = User::where('activation_token',$token)->first();
        if(!is_null($user) && $user->activation_token == $token){
            $user->user_status = 'ACTIVE';
            $user->save();
        }else{
            return redirect('/');
        }
        return redirect('/login');
    }
    
}
