<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\AnnouncementSeen;
use App\Models\Announcements;
use App\Models\ApplicantsProfile;
use App\Models\ApplicantsEducation;
use App\Models\ApplicantsWorkexp;
use App\Models\ApplicantsSkills;
use App\Models\ScoutConversation;
use App\Models\Scout;
use App\Models\Comment;
use App\Models\JobInquiry;

use App\Models\MasterQualification;
use App\Models\MasterCountry;
use App\Models\User;
use App\Models\Notification;

use Spyc;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Helpers\Utility\DateHelper;
use Carbon\Carbon;
use Response;
use Auth;
use Lang;
use Storage;
use App\Helpers\PaginationHelper;


/**
 * ApplicantNewAnnounceeController 
 * @author    Karen Irene Cano <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-03
 *
 */
class ApplicantNewsAnnounceController extends Controller
{
	/**
     * GET: Render /applicant/news index view (DEFAULT VIEW)
     * You have admin announcements, scout conversation announcements and comments announcements
     * Uses $this->announcementObj() for building a union of the announcements object sortable to date
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano        <karen_cano@commude.ph>
     * @return View user/news.blade.php
     */
    public function newsIndex($announcementLabel=null)
    {
        $announcementLabel = isset($_GET['applicant_news_status']) ? $_GET['applicant_news_status'] : null;

        $dateNow = Carbon::now();
        $properties = [];
        /*admin announcements*/
        $adminAnnouncements = Announcements::join('users', 'announcements.announcement_user_id', '=', 'users.id')
                                            ->select(
                                                        'announcement_id',
                                                        'announcement_title',
                                                        'announcement_details',
                                                        'announcement_deliverydate'
                                                    )
                                            ->where('user_accounttype',config('constants.accountType.0'))
                                            ->where('announcement_target', '=', 'USER')
                                            ->orWhere('announcement_target', '=', 'ALL')
                                            ->where('announcement_deliverydate','<=', $dateNow)
                                            ->where('announcement_status','=','ACTIVE')
                                            ->orderby('announcement_deliverydate','desc')
                                            ->get();
        $announcements = new \ArrayObject();

        foreach($adminAnnouncements as $adminAnn)
        {
            $properties["title"] = $adminAnn->announcement_title;
            $properties["details"] = $adminAnn->announcement_details;
            $properties["date"] = $adminAnn->announcement_deliverydate;
            $properties["class"] = "label label_long green ta_c";
            $properties["value"] = "Admin";
            $properties["label"] = config('constants.announcements-filter')['Admin'];
            $properties["url"] = "applicant/news/".$adminAnn->announcement_id;
            $this->announcementObj($announcements,$properties);
        }//endforeach adminAnnouncements

        /*scout announcements*/

        $scouts = Auth::user()["scoutObject"];

        if($scouts != null)
        {
            foreach($scouts as $scout)//get all scouts of the company
            {
                foreach($scout->scout_threads as $thread)
                {
                    $notifs = $thread->scout_notifications;
                    if($notifs->count() > 0)
                    {
                        $properties["title"] = json_decode($notifs->first()->notification_data)->title;
                        $properties["details"] = json_decode($notifs->first()->notification_data)->content;
                        $properties["date"] = $notifs->first()->notification_created_at;
                        $properties["class"] = "label label_long red ta_c";
                        $properties["value"] = "Scout";
                        $properties["label"] = config('constants.announcements-filter')['Scout'];
                        $properties["url"] = "/applicant/scout-detail/".$scout->scout_id;
                        $this->announcementObj($announcements,$properties);
                    }
                }
            }
        }

        /*comment announcements*/
        $checkInComments = JobInquiry::where('job_inquiry_user_id', Auth::user()["id"])
                                    ->distinct('job_inquiry_post_id')
                                    ->get();
        /**foreach job post id*/                           
        foreach($checkInComments as $comment)
        {  
           $jobPostComments = $comment->job_post->job_post_inquiries;
            
           foreach($jobPostComments as $jobComment)
           {
               $date = ($jobComment->job_inquiry_replies->last() != null)
                        ? $jobComment->job_inquiry_replies->last()->job_inquiry_reply_created
                        : $jobComment->job_inquiry_date;
                $properties["title"] = $jobComment->job_inquiry_title;
                $properties["details"] = $jobComment->job_post->job_post_title;
                $properties["date"] = $date;
                $properties["class"] ="label label_long blue ta_c";
                $properties["value"] ="Comment";
                $properties["label"] = config('constants.announcements-filter')['Comment'];
                $properties["url"] = "/applicant/comment-details/".$comment->job_inquiry_id;
                $this->announcementObj($announcements,$properties);
           }
        }
        
        /** Sort the announcements to descending (latest by date) */
        $announcements = Announcements::filterAnnouncements($announcementLabel,$announcements);
       
        /** Set Pagination */
        $announcements = PaginationHelper::getObjectPagination($announcements, 'news');
        $announcements->setPath(substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], "?")));

        return view('applicant.news')
                ->with('adminAnnouncements', $adminAnnouncements)
                ->with('announcements', $announcements);

    }

    /**
     * announcementObj()
     * Create a new Announcement object to render at news.blade.php
     * @author Karen Cano
     * @return object announcements
     */
    public function announcementObj($announcements,$properties)
    {
        $announcement          = new \stdClass();
        $announcement->title   = $properties["title"];
        $announcement->details = $properties["details"];
        $announcement->date    = $properties["date"];
        $announcement->class   = $properties["class"];
        $announcement->label   = $properties["label"];
        $announcement->value   = $properties["value"];
        $announcement->url     = $properties["url"];
        $announcements->append($announcement);
    }

    public function announcementDetail($announcementId)
    {
        AnnouncementSeen::updateOrCreate(['announcement_id' => $announcementId, 'user_id' => Auth::user()->id], ['seen' => '1']);
        $announcement = Announcements::findOrFail($announcementId);

        return view('applicant.announcement-detail')
                ->with('announcement',$announcement);
    }

}
