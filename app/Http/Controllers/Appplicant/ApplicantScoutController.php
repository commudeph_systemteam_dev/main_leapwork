<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Announcements;
use App\Models\ApplicantsProfile;
use App\Models\ApplicantsEducation;
use App\Models\ApplicantsWorkexp;
use App\Models\ApplicantsSkills;
use App\Models\Scout;
use App\Models\ScoutConversation;

use App\Models\MasterQualification;
use App\Models\MasterCountry;
use App\Models\User;

use Spyc;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Helpers\Utility\DateHelper;

use Response;
use Auth;
use Lang;
use Storage;
use App\Helpers\PaginationHelper;
use App\Events\NotifyEvent;
use App\Models\Notification;
use Carbon\Carbon;
use App\Traits\NotificationData;
use App\Traits\NotificationFunctions;

/**
 * ApplicantScoutController 
 * @author    Karen Irene Cano <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-03
 *
 */
class ApplicantScoutController extends Controller
{
    use NotificationFunctions;
    /**
     * GET: Render /applicant/scout index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since  2017-12-19 Louie: Fixed scout filtering query
     *
     * @return View user/scout.blade.php
     */
    public function scoutIndex($scoutStatus=null)
    {
        $userScouted = Auth::user()
                            ->applicant_profile
                            ->scouts;
                            
        if(!(empty($scoutStatus)))
        {
            // 2017-12-19  
            // $userScouted = $userScouted->where('scout_status',$scoutStatus)->get();
            $userScouted = $userScouted->where('scout_status',$scoutStatus);
            //end 
        }
        $userScoutedCount = $userScouted->count();
        $userScouted = PaginationHelper::getObjectPagination($userScouted, 'scout');
        

        return view('applicant.scout')
                ->with('userScouted',$userScouted)
                ->with('userScoutedCount', $userScoutedCount)
                ->with('scoutStatus',$scoutStatus);
    }

    /**
     *scoutDetail
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return View scout-detail
     */
    public function scoutDetail($scoutDetail, $notificationId=null)
    {
        $scout = Scout::findOrFail($scoutDetail);
        $scoutCompany = Scout::findOrFail($scoutDetail)->scout_company_profile;
        $inboxMessages = Scout::findOrFail($scoutDetail)->scout_threads;
                    //    ->where('scout_conversation_user_id', '!=', Auth: :user()['id']);
                       //show only message directed to the user
        /**Update Notification as read*/               
        if($notificationId!=null)
        {
            Notification::markAsRead($notificationId);
        }
        $defaultImage = url('images/applicant/img-side-icon01.png');
        $userId    = Auth::user()['id'];

        $userImgSrc    = (!empty(Auth::user()->applicant_profile->applicant_profile_imagepath)) 
                       ? url( config('constants.storageDirectory.applicantComplete')  .
                         'profile_' .
                         Auth::user()->applicant_profile->applicant_profile_id . 
                         '/' .
                         Auth::user()->applicant_profile->applicant_profile_imagepath )  
                       : $defaultImage;

       $companyImgSrc = (!empty($scoutCompany->company_logo)) 
                       ? url( config('constants.storageDirectory.companyComplete') .
                         'company_' .
                         $scoutCompany->company_id .
                         '/' .
                         $scoutCompany->company_logo )
                       : $defaultImage;

        return view('applicant.scout-detail', compact('userImgSrc', 'companyImgSrc'))
                ->with('scout', $scout)
                ->with('userId', $userId)
                ->with('inboxMessages', $inboxMessages);
    }

    /**
     *createReplyScoutConversation
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param request,data
     * @return save new entry to scout conversation
     */
    public function createReplyScoutConversation(Request $request)
    {
        $newScoutConvo = ScoutConversation::createScoutConversation($request);
        $scout = new Scout();
        $data = $scout->buildNotification($newScoutConvo);
        event(new NotifyEvent($data));

        $threadId = $request->input('scoutThreadId');
        $message = $request->input('scoutConvoMessage_'.$threadId);
        $scoutId = $newScoutConvo->toArray()['scout_thread']['scout']['scout_applicant_id'];
        $userId = Auth::user()->id;
        $receipientId = ScoutConversation::where('scout_conversation_thread_id', $threadId)->where('scout_conversation_user_id', '!=', $userId)->first()->scout_conversation_user_id;

        //since Applicant, we get the name from the applicants profile
        if(Auth::user()->applicant_profile) {
            $name = Auth::user()->applicant_profile->applicant_profile_firstname.' '.Auth::user()->applicant_profile->applicant_profile_lastname;
        } else {
            $name = 'User';
        }
    
        if($newScoutConvo) {

            $data = [
                'threadId'          => $threadId,
                'id'                => $scoutId,
                'name'              => $name,
                'message'           => $message,
                'receipientId'      => $receipientId,
                'type'              => 'scout-company',
                
            ];

            //TODO: make pusher event here
            self::sendMessage($data, 'scout-company');

        }


        // return back();
        return 'Save';
    }

}
