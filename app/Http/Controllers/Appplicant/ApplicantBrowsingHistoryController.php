<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\JobPostView;
use App\Models\JobPostFavorite;

use App\Helpers\PaginationHelper;

use Response;
use Auth;
use Lang;
use Storage;

/**
 * ApplicantBrowsingHistoryController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-03
 *
 */
class ApplicantBrowsingHistoryController extends Controller
{

	 /**
     * GET: Render /applicant/browsing-history index view
     * Loads browsed jobs by user paginated
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View applicant/browsing-history.blade.php
     */
    public function index()
    {
        $userId       = Auth::user()['id'];
        $jobFavorites = JobPostFavorite::getUserFavorites($userId);
        $jobPostViews = JobPostView::getJobPostViews($userId);
        
        $browsingHistories = array();

        //format view display - ensure no logic in view
        for($i=0; $i < count($jobPostViews); $i++)
        {
            $browsingHistories[$i]['job_post_id'] =  $jobPostViews[$i]->job_post->job_post_id;
            $browsingHistories[$i]['job_post_title']        =  $jobPostViews[$i]->job_post->job_post_title;
            $browsingHistories[$i]['job_post_company_name'] =  $jobPostViews[$i]->job_post->company->company_name;
            $browsingHistories[$i]['job_post_location']     =  $jobPostViews[$i]->job_post->location->master_job_location_name;
            
            //view formats
            $browsingHistories[$i]['job_post_enabled']      = $jobFavorites->contains('job_post_favorite_post_id', $browsingHistories[$i]['job_post_id']);
            $browsingHistories[$i]['job_post_favorite_css'] = $jobPostViews[$i]->historyCSS($browsingHistories[$i]['job_post_enabled'])['favorite'];
            $browsingHistories[$i]['job_post_remove_css']   = $jobPostViews[$i]->historyCSS($browsingHistories[$i]['job_post_enabled'])['remove'];

        	//if not equal. just assume a max value
    		$browsingHistories[$i]['job_post_salary'] = ($jobPostViews[$i]->job_post->job_post_salary_min != $jobPostViews[$i]->job_post->job_post_salary_max)
    								? ' PHP ' . number_format($jobPostViews[$i]->job_post->job_post_salary_min) . ' - PHP ' .  number_format($jobPostViews[$i]->job_post->job_post_salary_max)
    								: ' PHP ' . number_format($jobPostViews[$i]->job_post->job_post_salary_max);

            $browsingHistories[$i]['job_post_link'] = url('/job/details/') .
                                                      '/' .
                                                      $jobPostViews[$i]->job_post->job_post_id;



        }

        $browsingHistories = PaginationHelper::getObjectPagination($browsingHistories, 'browsing-history');

        return view('applicant.browsing-history')->with('browsingHistories', $browsingHistories)
                                                 ->with('userId', $userId);
    }

    /**
     * POST: Add a job post from history to favorites
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Request $request form values
     * @return View applicant/browsing-history.blade.php
     */
    public function addToFavorite(Request $request)
    {   
        $userId    = Auth::user()['id']; 
        $jobPostId = $request->input('selected_job_post_id');
       
        $result = JobPostFavorite::addToFavorite($jobPostId);

        $message = ($result)
                 ? Lang::get('messages.applicant.browsing-history.addToFavorte')
                 : Lang::get('messages.failed');

       return redirect('applicant/browsing-history')->with('message', $message);
    }

    /**
     * POST: Add a job post from history to favorites
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Request $request form values
     * @return View applicant/browsing-history.blade.php
     */
    public function removeFromHistory(Request $request)
    {   
        $userId    = Auth::user()['id']; 
        $jobPostId = $request->input('selected_job_post_id');
        
        $result    = JobPostView::archiveJobPostView(
                                                    array(
                                                         'user_id'     => $userId,
                                                         'job_post_id' => $jobPostId
                                                         )
                                                    );

        $message   = ($result)
                   ? Lang::get('messages.applicant.browsing-history.remove')
                   : Lang::get('messages.failed');

       return redirect('applicant/browsing-history')->with('message', $message);
    }

    /**
     * POST: Add a job post from history to favorites
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Request $request form values
     * @return View applicant/browsing-history.blade.php
     */
    public function removeFromFavorites(Request $request)
    {   
        $userId    = Auth::user()['id']; 
        $jobPostId = $request->input('selected_job_post_id');
        
        $result    = JobPostFavorite::removeFromFavorite($jobPostId);
        
        $message   = ($result)
                   ? Lang::get('messages.applicant.browsing-history.remove')
                   : Lang::get('messages.failed');

       return redirect('applicant/browsing-history')->with('message', $message);
    }
    

}