<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ApplicantsProfile;
use App\Models\SocialProvider;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Facebook;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // private $redirectTo = '/front/top';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     * Authenticate normal registration
     * @author Karen Cano <karen_cano@commude.ph>
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/21 Louie: Add company status in checking login validity
     * @since  2017/11/01 Karen: Initial
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        if (isset(Auth::user()["user_accounttype"])) {
            return redirect(self::redirectPath(Auth::user()["user_accounttype"]));
        } else {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                /*update last logged in*/
                $authUser = User::findorFail(Auth::user()["id"]);

                $isCompanyActive = (!is_null(Auth::user()->company_profile))
                ? Auth::user()->company_profile->company_status
                : "ACTIVE";

                if ($authUser->user_status == 'ACTIVE' && $isCompanyActive == 'ACTIVE') {
                    $authUser->updated_at = Carbon::now();
                    $authUser->save();
                    $accountType = Auth::user()["user_accounttype"];
                    $redirectTo = $this->redirectPath($accountType);
                    if (!is_null(session('job_post_id')) && $accountType == "USER") {
                        return redirect(session('job_post_id'));
                    }

                    return redirect($redirectTo);
                } else {
                    $this->guard()->logout();
                    $request->session()->invalidate();
                    return redirect('inactive');
                }
            } else {
                return view('auth.login')->withErrors('msg', 'Please check credentials');
            }
        }

    }

    /**
     * Get redirectTo path based from user_accounttype
     * @author    Karen Cano <karen_cano@commude.ph>
     * @return $this->redirectTo
     */
    public function redirectPath($accountType)
    {
        switch ($accountType) {
            case "USER":
                $this->redirectTo = '/front/top';
                break;
            case "CORPORATE ADMIN":
                $this->redirectTo = '/company';
                break;
            case "CORPORATE USER":
                $this->redirectTo = '/company';
                break;
            case "ADMIN":
                $this->redirectTo = '/admin';
                break;
            default:
                $this->redirectTo = '/';
                break;
        }

        return $this->redirectTo;
    }
    /**
     * redirectToProvider
     *
     * @author    Alvin Generalo
     *            Karen Cano
     * @copyright 2017 Commude
     * @since     2017-10-10
     */
    public function redirectToProvider($provider)
    {
        // dd(Socialite::driver($provider));
        return Socialite::driver($provider)->redirect();
    } //endof redirectToProvider

    /**
     * handleProviderCallback
     *
     * @author    Karen Cano
     * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @summary   Social Provider handler
     * @copyright 2017 Commude
     * @since     2017
     */
    public function handleProviderCallback($provider)
    {

        try
        {

            switch ($provider) {
                case 'facebook':
                    $socialUser = Socialite::driver($provider)
                        ->fields([
                            'name',
                            'first_name',
                            'last_name',
                            'email',
                        ])->user();
                    break;
                case 'linkedin':
                    $socialUser = Socialite::driver($provider)->user();
                    break;

            }

        } catch (\Exception $e) {
            return redirect('/');
        }

        $socialProvider = SocialProvider::where('provider_id', $socialUser->getId())->first();

        if (!$socialProvider) {

            switch ($provider) {
                case 'facebook':
                    $firstName = $socialUser["first_name"];
                    $lastName = $socialUser["last_name"];
                    break;
                case 'linkedin':
                    $firstName = $socialUser["firstName"];
                    $lastName = $socialUser["lastName"];
                    break;

            }

            if (is_null($socialUser->getEmail())) {
                return redirect('/register')
                    ->with('user_name', [
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                    ])
                    ->with('warning', 'You must register an email address.');
            }

            $user = User::firstOrCreate(
                ['email' => $socialUser->getEmail(),
                    'user_accounttype' => 'USER',
                    'user_status' => 'ACTIVE',
                ]
            );

            //Louie: create basic profile immediately
            $applicantProfile = ApplicantsProfile::firstOrCreate(
                [
                    'applicant_profile_lastname' => $lastName,
                    'applicant_profile_firstname' => $firstName,
                    //  'applicant_profile_gender'      => strtoupper($socialUser["gender"]),
                    'applicant_profile_user_id' => $user->id,
                    'applicant_fb_image' => $socialUser->avatar,
                ]
            );

            //create the provider linked to the user
            $user->socialProviders()->create(
                [
                    'provider_id' => $socialUser->getId(),
                    'provider' => $provider,
                ]);

            switch ($provider) {
                case 'facebook':
                    //pass in the newly created $user
                    return $this->facebookLogin($socialUser, $user);
                    break;

                case 'linkedin':
                    auth()->login($user);
                    $accountType = Auth::user()["user_accounttype"];
                    $this->redirectTo = $this->redirectPath($accountType);
                    return redirect($this->redirectTo);
                    break;
            }

        } else {
            //if socialProvider
            auth()->login($socialProvider->user);
            /*update last logged in*/
            $authUser = User::findorFail(Auth::user()["id"]);
            $authUser->updated_at = Carbon::now();
            $authUser->save();

            $accountType = Auth::user()["user_accounttype"];
            $this->redirectTo = $this->redirectPath($accountType);
            return redirect($this->redirectTo);
        }

    } //endof handleProviderCallback

    /**
     * facebookLogin
     *
     * @author    Karen Cano
     * @summary   Facebook login
     * @copyright 2017 Commude
     * @since     2017
     */
    public function facebookLogin($socialUser, $user)
    {
        try {
            $response = Facebook::get('/me?fields=id,name,email,location,work,education,hometown', $socialUser->token);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
        $userData = $response->getGraphUser(); /*$userData contains all details about FB Graph API*/
        auth()->login($user);
        /*update last logged in*/
        $authUser = User::findorFail(Auth::user()["id"]);
        $authUser->updated_at = Carbon::now();
        $authUser->save();

        $accountType = Auth::user()["user_accounttype"];
        $this->redirectTo = $this->redirectPath($accountType);
        return redirect($this->redirectTo);
    }

    public function username()
    {
        return 'user_email';
    }

    public function authenticated(Request $request)
    {
        //when login is successful
        return redirect($this->redirectTo);
    }

    /**
     * Log the user out of the application.
     * @author    Karen Cano
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/');
    }

    public function reset()
    {
        return view('reset');
    }

    public function inactive()
    {
        return view('auth.inactive');
    }

    public function success()
    {
        return view('auth.register-success');
    }

}
