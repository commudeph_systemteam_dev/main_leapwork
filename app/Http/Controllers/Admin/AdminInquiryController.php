<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Company;
use App\Models\CompanyInquiryThread;
use App\Models\CompanyInquiryConversation;
use App\Models\User;
use App\Models\Notification;
use App\Events\NotifyEvent;

use Carbon\Carbon;

use Auth;
use Lang;

use App\Traits\NotificationFunctions;

/**
 * AdminInquiryController 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 */
class AdminInquiryController extends Controller
{
    use NotificationFunctions;

	/**
     * GET: Render Company Inquiries list
     * 
     * 
     * @author Martin Louie Dela Serna
     * @return View company/inquiry/index.blade.php
     */
    public function index()
    {
        $userId    = Auth::user()['id'];
        $companyInquiries   = CompanyInquiryThread::getAll()
                                                    ->paginate(10);

        Notification::markAsReadIfExists($userId, 'COMPANY REPLIES');

        foreach ($companyInquiries as $key => $value) {
            // print_r($value);
            // echo "<br><br>";
        }
        return view('admin.inquiry.index')->with('companyInquiries', $companyInquiries);
    }

    /**
     * POST: Render Company Inquiries list
     * 
     * 
     * @author Martin Louie Dela Serna
     *
     * @param  array $request form values
     *
     * @return View company/inquiry/index.blade.php
     */
    public function searchInquiry(Request $request)
    {
        $companyName       = $request->input('txt_keyword');
        $companyInquiries   = CompanyInquiryThread::getAll(
                                                       array('company_name' => $companyName)
                                                      )
                                                    ->paginate(10);

        return view('admin.inquiry.index')->with('companyInquiries', $companyInquiries);
    }

    /**
     * GET: Render Company inquiry (contact).
     * 
     * Display company name and contact person as default
     * 
     * @author Martin Louie Dela Serna
     *
     * @param  array $request form values
     *
     * @return View company/inquiry/details.blade.php
     */
    public function inquiryDetails(Request $request)
    {
        $userId        = Auth::user()['id'];
        $companyId     = Auth::user()['user_company_id'];
        $company       = Company::find($companyId);
        
        Notification::markAsReadIfExists($userId, 'COMPANY REPLIES');

        $threadId["company_inquiry_thread_id"]      = $request->input('id');
        $inboxMessage  = CompanyInquiryThread::getInquiry($threadId);

        $defaultImage  = url('/storage/uploads/master-admin/leapwork_logo.png');

        $companyImgSrc = (!empty($company->company_logo)) 
                       ? url( config('constants.storageDirectory.companyComplete') .
                         'company_' .
                         $company->company_id .
                         '/' .
                         $company->company_logo )
                       : '';

        $adminImgSrc   = url('/storage/uploads/master-admin/leapwork_logo.png');
        $inboxMessage['lastMessage'] = CompanyInquiryThread::getLastSent($inboxMessage);
        $completeUserName      = [];
        $companyLogo           = [];

        foreach ($inboxMessage->company_inquiry_conversations as $companyInquiryConversation) {

            $userprofile = User::find($companyInquiryConversation->company_inquiry_conversation_user_id)->company_profile;

            $completeName =  $companyInquiryConversation->company_inquiry_conversation_user_id != Auth::user()->id
                    ?  $userprofile->company_contact_person
                    : "Admin";

            // $completeName = $data == "Admin" ? "Admin" : $data->applicant_profile_firstname . " " . $data->applicant_profile_middlename . " " . $data->applicant_profile_lastname;

            $img = $completeName == "Admin" ? $adminImgSrc : url( config('constants.storageDirectory.companyComplete') .'company_' .$userprofile->company_id . "/".$userprofile->company_logo);


            array_push($companyLogo, $img);
            array_push($completeUserName, $completeName);

        }

        return view('admin.inquiry.details', compact("completeUserName", "companyLogo"))   
                                        ->with('adminImgSrc'    , $adminImgSrc)
                                        ->with('companyImgSrc'  , $companyImgSrc)
                                        ->with('threadId'       , $threadId)
                                        ->with('userId'         , $userId)
                                        ->with('inboxMessage'   , $inboxMessage);
    }

    // /**
    //  * POST: Create/send of company_inquiry
    //  * 
    //  * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    //  *
    //  * @param  array $request form values
    //  *
    //  * @return View company/inquiry/index.blade.php
    //  */
    // public function sendInquiry(Request $request)
    // {
    //     $request->validate([
    //           'txt_company_id'     => 'required',
    //           'txt_contact_person' => 'required',
    //           'txt_email'          => 'required',
    //           'txt_content'        => 'required'
    //       ]);

    //     $company_inquiry['company_inquiry_contact_person'] = $request->input('txt_contact_person');
    //     $company_inquiry['company_inquiry_email']          = $request->input('txt_email');
    //     $company_inquiry['company_inquiry_content']        = $request->input('txt_content');
    //     $company_inquiry['company_inquiry_company_id']     = $request->input('txt_company_id');
    //     $company_inquiry['company_inquiry_datecreated']    = date('Y-m-d H:i:s');

    //     CompanyInquiryThread::saveInquiry($company_inquiry);
        
    //     $request->session()->flash('messsage', 'Company Inquiry submitted!');

    //     return redirect('admin/inquiry');
    // }

    /**
     * POST:   /admin/inquiry/replyConversation
     * Create reply to exiting company_inquiry_thread 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-26
     * 
     * @param  array $request form values
     * 
     * @return View admin/inquiry/index.blade.php
     */
    public function replyConversation(Request $request)
    {
        $userId   = Auth::user()["id"];
        $message  = $request->input('company_inquiry_conversation_message');
        $threadId = $request->input('company_inquiry_thread_id');

        //create 
        $company_inquiry_conversation = array();
        $company_inquiry_conversation['company_inquiry_conversation_message']     = $message;
        $company_inquiry_conversation['company_inquiry_conversation_datecreated'] = Carbon::now();
        $company_inquiry_conversation['company_inquiry_conversation_user_id']     = $userId;
        $company_inquiry_conversation['company_inquiry_conversation_thread_id']   = $threadId;

        $newCompanyInquiryConversation = CompanyInquiryConversation::saveCompanyInquiryConversation($company_inquiry_conversation);
        
        //create event
        event(new NotifyEvent(
                $newCompanyInquiryConversation->buildNotification($newCompanyInquiryConversation)
            )
        );

        $receipientId = CompanyInquiryConversation::where('company_inquiry_conversation_thread_id', $threadId)->where('company_inquiry_conversation_user_id', '!=', $userId)->first()->company_inquiry_conversation_user_id;


        $type = 'admin-company';
        $image = url('/storage/uploads/master-admin/leapwork_logo.png');
        if($newCompanyInquiryConversation) {

            $data = [
                'threadId'          => $threadId,
                'id'                => '',
                'name'              => 'Admin',
                'message'           => $message,
                'receipientId'      => $receipientId,
                'type'              => $type,
                'image'             => $image,
                
            ];

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }


        // $message = ($newCompanyInquiryConversation)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }

}
