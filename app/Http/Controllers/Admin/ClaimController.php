<?php

namespace App\Http\Controllers;

use App\Http\Requests\createRequest;
use App\Http\Controllers\Controller;

use App\Helpers\Utility\DateHelper;

use App\Models\Company;
use App\Models\CompanyPlanHistory;
use App\Models\Billing;
use App\Models\Claim;
use App\Models\JobPosting;
use App\Models\JobPostView;
use App\Models\User;
use App\Models\CompanyIntro;
use App\Models\MasterEmployeeRange;
use App\Models\MasterJobIndustries;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Models\MasterPlans;
use App\Models\MasterMailTemplate;
use Spyc;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendInvoice;
use App\Mail\RegisterCompany;
use App\Mail\CompanyVerifiedMail;

use Carbon\Carbon;


use Auth;

/**
 * 
 * @author  Karen Cano   <karen_cano@commude.ph> 
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-29
 */
class ClaimController extends Controller
{
    /**
     * sendInvoice
     * @author  Ian Gabriel Sebastian  <ian_sebastian@commude.ph> 
     * @desc    Set company to ACTIVE upon Mark as Paid
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * Send the email notification to company email
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function sendInvoice($claimsId)
    {
        $claim = Claim::findOrFail($claimsId);
        $company = $claim->company;
        $claimPlan = $claim->master_plans->master_plan_name;

        switch($claimPlan)
        {
            // @todo: case can be disregarded since trial should not reach this point 
            case config('constants.PLAN_TYPE_DICTIONARY')['TRIAL']:
                Mail::to($company->company_email)
                    ->send(new CompanyVerifiedMail($company));
                break;
            default:
                Mail::to($company->company_email)
                    ->send(new SendInvoice($claim, $company));
                break;
        }

        // 2018/07/13 Louie: Add 7 days limit for paying now. 
        $updateClaimIssued = $claim->update([
            'claim_invoice_status' => 'ISSUED',
            'claim_datecreated' => Carbon::now(),
            'claim_end_date'    => Carbon::now()->addDays(7) 
        ]);

        return back();
    }

    /**
     * viewSentInvoice
     * View the invoice to be sent
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function viewSentInvoice($claimsId)
    {
        $claim = Claim::findOrFail($claimsId);
        $company = $claim->company;
        return new SendInvoice($claim,$company);
    }

    /**
     * renderPaymentMethod
     * Render view based from payment method
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function renderPaymentMethod($paymentMethod,$claimToken)
    {
        $claim = Claim::where('claim_token',$claimToken)
                        ->where('claim_payment_method',$paymentMethod)
                        ->first();
        if($claim)
        {
            $viewToRender = 'front.payment-methods.'.strtolower($paymentMethod);

            return view($viewToRender)
                        ->with('claim',$claim);
        }
        else
        {
            return redirect('/error')->with('errorMessage','Token mismatch.You are not authorized to access this page.'); 
        }

    }

     /**
     * markAsPaid
     * @author  Ian Sebastian  <ian_sebastian@commude.ph> 
     * @desc Add claim_amount upon MARK AS PAID and set company status to ACTIVE
     * @author  Karen Cano   <karen_cano@commude.ph> 
     * @desc Mark as paid a claim entry
     * @copyright 2017 Commude Philippines, Inc.
     * @since     2017-11-29
     */
    public function markAsPaid($claimsId)
    {
        $claim = Claim::findOrFail($claimsId);

        $plan = MasterPlans::where('master_plan_name',$claim->claim_name)->first();

        $company = Company::where('company_id',$claim->claim_company_id)->first();
        $company->company_current_plan = $claim->claim_name;
        $company->company_credits = $plan->master_plan_post_limit;
        $company->company_status = 'ACTIVE';
        $company->save();

        $claim->claim_payment_status = 'PAID';
        $claim->claim_status = 'CLOSED';
        $claim->claim_dateupdated = Carbon::now();
        $claim->claim_amount = MasterPlans::where('master_plan_id',$claim->claim_master_plan_id)
                                        ->first()->master_plan_price;
        $claim->save();

        $user = User::where('user_company_id',$claim->claim_company_id)
                                        ->where('email', $company->company_email)
                                        ->first();
        $user->user_status     = 'ACTIVE';
        $user->save();

        $mailTemplate = MasterMailTemplate::where('master_mail_status','PAID')->first();          
        Mail::to($company->company_email)
                    ->send(new RegisterCompany(
                                    $company,$mailTemplate
                                ));
        return back();
    }

    /**
     * invoiceDetails
     * Render Company billing-invoice in Admin Side
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @since 06/05/2018
     */

    public function invoiceDetails($claim_id)
    {
        $condition[0] = array(['claims.claim_id','=', $claim_id]);
        $claimDetails = Claim::getClaimEntry($condition);
        return view('admin/billing/invoice_details', compact('claimDetails'));
    }

    /**
     * invoiceDetails
     * Render Company billing-invoice in Admin Side
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @since 06/05/2018
     */

    public function publicInvoiceDetails($companyid, $claim_id)
    {
        $condition[0] = array(['claims.claim_id','=', $claim_id]);
        $condition[1] = array(['claims.claim_company_id','=', $companyid]);
        $claimDetails = Claim::getClaimEntry($condition);
        return view('admin/billing/invoice_details', compact('claimDetails'));
    }
}
