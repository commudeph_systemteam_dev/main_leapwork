<?php

namespace App\Http\Controllers;

use App\Http\Requests\createRequest;
use App\Http\Controllers\Controller;

use App\Helpers\Utility\DateHelper;

use App\Models\Company;
use App\Models\CompanyPlanHistory;
use App\Models\Billing;
use App\Models\Claim;
use App\Models\JobPosting;
use App\Models\JobPostView;
use App\Models\User;
use App\Models\CompanyIntro;
use App\Models\MasterEmployeeRange;
use App\Models\MasterJobIndustries;
use App\Models\MasterPlans;
use App\Models\MasterJobLocations;
use App\Models\MasterCountry;
use App\Models\MasterJobClassifications;




use Spyc;
use App\Models\ZipCodes\Philippines; //for Provider

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;



use Auth;


/**
 * Contoller of Admin
 * 
 * @author    Alvin Generalo  <alvin_generalo@commude.ph>
 *            Karen Cano   <karen_cano@commude.ph>      <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-13
 */
class AdminController extends Controller
{
    /**
     * home
     * Render Admin home
     * @author Karen Cano   <karen_cano@commude.ph>
     * @return View
     */
    public function home()
    {
        return view('admin.home');
    } 

    /**
     * companies
     * Render Admin companies
     *
     * @var args - default keys for search query
     *
     * @author Karen Cano   <karen_cano@commude.ph>
     * @author Ken Kasai    <ken_kasai@commude.ph>
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @return View
     */
    public function companies()
    {
        $args = array();
        $args['date_registered'] = '';
        $args['date_lastlogin']  = '';
        $args['company_name']    = '';

        $corpData = Company::getAll();

        return view('admin.companies')->with('corpData', $corpData)
                                      ->with('args', $args);
    }

    /**
     * GET: /admin/companies/new
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/company-new.blade.php
     */
     public function createView()
     {
         return view('admin.company-new');
     }

    /**
     * GET: /admin/companies/edit
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/company-new.blade.php
     */
     public function editView()
     {
         return view('admin.company-edit');
     }

    /**
     * companyDetail
     * Render Company Profile
     * @author Karen Cano   <karen_cano@commude.ph>
     * Adapted from CompanyProfileController.index()
     * @return View
     */
    public function companyDetail($companyId)
    {
        $company   = Company::find($companyId);
        $companyIntro = $company->company_intro ?? new CompanyIntro();

        $employeeRanges = MasterEmployeeRange::all()->pluck( 'range'  ,'master_employee_range_id'); //convert to select option
        $jobIndustries  = MasterJobIndustries::all()->pluck( 'master_job_industry_name'  ,'master_job_industry_id'); //convert to select option

        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        $unissuedClaims = Claim::where('claim_invoice_status','NOT ISSUED')
                            ->orWhere('claim_payment_status','UNPAID')
                            ->where('claim_company_id',$companyId)
                            ->get();
        return view('admin.company-profile')->with('company', $company)
                                           ->with('companyIntro', $companyIntro)
                                           ->with('arrPhilippines', $arrYaml)
                                           ->with('employeeRanges', $employeeRanges)
                                           ->with('jobIndustries', $jobIndustries)
                                           ->with('unissuedClaims', $unissuedClaims);
    }

    /**
     * POST: Index containing list of all master tables
     *
     * @var args - default keys for search query
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @return View admin/master-admin/job.blade.php
     */
    public function searchCompany(Request $request)
    {
        $args = array();

        $args['date_registered'] = (!empty($request->input('txt_date_registered')))
                                 ? DateHelper::createDate($request->input('txt_date_registered'))
                                 : '';
        $args['date_lastlogin']  = (!empty($request->input('txt_date_lastlogin')))
                                 ? DateHelper::createDate($request->input('txt_date_lastlogin'))
                                 : '';
        $args['company_name']    = $request->input('txt_keyword');

        $corpData = Company::getAll($args);

        return view('admin.companies')->with('corpData', $corpData)
                                      ->with('args', $args);
    }

     /**
     * view job details when title is clicked (Popular Jobs, Feature list - Admin User)
     * Render job details
     * @author Arvin Alipio
     * @return View
     */
    public function viewJobDetails($currentBlade, $job_post_id,Request $request)
    {

        $job            = JobPosting::where('job_post_id', $job_post_id)
                                    ->first();
        $company        = Company::where('company_id', $job->job_post_company_id)->first();
        $companyLocation= MasterJobLocations::where('master_job_location_id', $job->job_post_location_id)->first(); 
        $country        = MasterCountry::where('master_country_id', $companyLocation->master_job_country_id)->first();
        $jobClassification = MasterJobClassifications::where('master_job_classification_id', $job->job_post_classification_id)->first();
        $condition[0]   = array(['job_posts.job_post_company_id','=', $job->job_post_company_id]);
        $condition[1]   = array(['job_posts.job_post_id','<>', $job_post_id]);
        $jobsOnComp     = JobPosting::getJob('latest',$condition)
                                        ->limit(3)
                                        ->get();

        $condition[0]   = array(['job_posts.job_post_job_position_id','=', $job->job_post_job_position_id]);
        $condition[1]   = array(['job_posts.job_post_id','<>', $job_post_id]);
        $similarJob     = JobPosting::getJob('latest',$condition)
                                        ->limit(3)
                                        ->get();


        JobPostView::newJobPostView($job_post_id);

        return view('admin.job-posting-details', compact('job', 'company','companyLocation','country', 'jobClassification','jobsOnComp', 'similarJob', 'favorite', 'job_post_id', 'currentBlade'));
    }

    /**
     * billing
     * Render Admin companies
     * @author Karen Cano   <karen_cano@commude.ph> , Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function billing(Request $request)
    {
        $condition = [
            'where' => [],
            'orWhere' => []
        ];
        
        if(isset($_POST['keyword']))
        {
            $condition['orWhere'][] = array(['company_name','like', '%' . $_POST['keyword'] . '%']);
            $condition['orWhere'][] = array(['claim_payment_method','like', '%' . strtoupper($_POST['keyword']) . '%']);

        }
        if(isset($_POST['claimStat']))
        {
            if($_POST['claimStat'] != 'All')
            {
                $condition['where'][] = array(['claim_status','=', strtoupper($_POST['claimStat'])]);
               
            }
            if($_POST['paymentStat'] != 'All')
            {
                $condition['where'][] = array(['claim_payment_status','=', strtoupper($_POST['paymentStat'])]);
            } 
        }

        $claimGroupedCount  = Claim::getGroupedClaims($condition)
                                    ->where('company.company_status','ACTIVE')
                                    ->orWhere('company.company_status','VERIFIED')
                                    ->count();
        $claimGrouped       = Claim::getGroupedClaims($condition)
                                    ->where('company.company_status','ACTIVE')
                                    ->where('company_plan_history.company_plan_type','!=','TRIAL')
                                    ->orWhere('company.company_status','VERIFIED')
                                    ->paginate(10);

        return view('admin.billing', compact('claimGrouped', 'claimGroupedCount'));
    }
    /**
     * billing-detail
     * Render Admin billing-detail
     * @author Ian Sebastian   <ian_sebastian@commude.ph>
     * Disable MARK AS PAID unless ISSUED has been marked.
     * @author Karen Cano   <karen_cano@commude.ph>
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function billingDetail($companyId)
    {
        $companyClaims = Company::findOrFail($companyId);
        // $currentPlan = CompanyPlanHistory::where('company_plan_company_id',$companyId)
        //                                 ->get()
        //                                 ->last();
        
        // return view('admin.billing-detail')
        //             ->with('company',$company)
        //             ->with('currentPlan',$currentPlan);


        $condition[0]       = array(
                                    ['company_id','=', $companyId]
                                ); 

        $companyList        = Company::getCompanyClaimList($condition)
                                    ->get();

        $groupedClaims      = Claim::getGroupedClaimID($condition)
                                    ->get();

        $currentPlan        = CompanyPlanHistory::getCurrentPlanHistory($companyId);

        $companyName    = $groupedClaims[0]->company_name;

        foreach($groupedClaims as $claims)
        {
            $latestPlan =  $claims->claim_name;
            $latestMethod =  $claims->claim_payment_method;
            $latestPlanStatus =  $claims->company_plan_status;
        }

        if(!is_null($currentPlan))
        {
            $latestPlan     = $currentPlan->company_plan_type;
            $latestMethod   = $currentPlan->claim_payment_method;
            $latestPlanStatus =  $currentPlan->company_plan_status;
        }
        $planCount = count($groupedClaims);

        for($i=0; $i < $planCount; $i++)
        {
            $invoiceCounter = '00' .  $planCount - $i;
            // Format claim view custom labels
            $groupedClaims[$i]->claim_label = 'INVOICE_' . $invoiceCounter;
        }

        return view('admin.billing-detail', compact('companyList'
                                                    ,'latestMethod'
                                                    ,'companyName'
                                                    , 'latestPlan' 
                                                    , 'companyClaims'
                                                    , 'groupedClaims'
                                                    ,'latestPlanStatus'));
    }
    /**
     * ranking
     * Render Admin ranking
     * @author Karen Cano           <karen_cano@commude.ph> 
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function ranking()
    {
        $condition = null;
        if(isset($_POST['company']))
        {
            $condition[0] = array(['company.company_name','like','%' . $_POST['company'] . '%' ]);
        }
        
        $pageNumber     = isset($_GET['page']) ? ($_GET['page'] - 1) * 10 : 1;
        $companyList    = Company::getCompanyClaimList(null)
                                                ->groupBy('company_name')
                                                ->get();
        $jobs           = JobPosting::getJob('popullar', $condition )->paginate(10);
        $jobsForPV      = JobPosting::getJob('popullar', $condition )->paginate(10);
        $pv = array();
        foreach ($jobsForPV as $key => $value) {
             array_push($pv, JobPostView::getTotalPageViews($value->job_post_id,'job_post_id'));
        }
       
        return view('admin.ranking', compact('jobs', 'pageNumber','companyList','pv'));
    }

    /**
     * masterAdmin
     * Render Admin master-admin
     * @author Karen Cano   <karen_cano@commude.ph>
     * @return View
     */
    public function masterAdmin()
    {
        return view('admin.master-admin');
    }

    /**
     * masterAdminJob
     * Render Admin master-admin-job
     * @author Karen Cano   <karen_cano@commude.ph>
     * @return View
     */
    public function masterAdminJob()
    {
        return view('admin.master-admin-job');
    }

    /**
     * masterAdminMail
     * Render Admin master-admin-mail
     * @author Karen Cano   <karen_cano@commude.ph>
     * @return View
     */
    public function masterAdminMail()
    {
        return view('admin.master-admin-mail');
    }

    /**
     * masterAdminSetting
     * Render Admin master-admin-mail
     * @author Karen Cano   <karen_cano@commude.ph>
     * @return View
     */
    public function masterAdminSetting()
    {
        return view('admin.master-admin-setting');
    }
    
    /**
     * Analytics - Admin Home
     * Fill up array with complete months has no value
     * @author Ian Gabriel Sebastian   <ian_sebastian@commude.ph>
     * @return Array
     */
    public function getFillMissingArray($value, $col =[])
    {
        $array = [];
        $monthCount = date('n');// from start of year up to now

        for($i=1; $i <= $monthCount; $i++){
            $match = array_column($value, $col[0]);
            $index = array_search($i, $match);
            
            if(!empty($index) || strlen($index) !== 0){
                $monthObj = (array) $value[$index];
                $array[$i] = $monthObj[$col[1]];
            }else{
                $array[$i] = 0;
            }
        }

        return $array;
    }

    /**
     * adminDashboard
     * View Dashboard
     * @author Ian Gabriel Sebastian <ian_sebastian@commmude.ph>
     * @author Ken Kasai <ken_kasai@commmude.ph>
     * @return View
     */
    public function adminDashboard()
    {
        // Month and Year
    	$dateArr         = array('MONTH' => date('m'), 'YEAR' => date('Y'));
        $dateArrLM       = array('MONTH' => date('m', strtotime('-1 months')), 'YEAR' => date('Y', strtotime('-1 months')));
        $dateArrNM       = array('MONTH' => date('m', strtotime('+1 months')), 'YEAR' => date('Y', strtotime('+1 months')));

 		// Registration
    	$newlyRegThisMth = Company::countRegistered($dateArr);
    	$newRegTotal     = Company::countRegistered();

    	// Plans
    	$totalTrialPlan  = CompanyPlanHistory::countPlan(config('constants.planType.0'));
    	$totalStandard   = CompanyPlanHistory::countPlan(config('constants.planType.1'));
    	$totalEnterprise = CompanyPlanHistory::countPlan(config('constants.planType.2'));  

        //Billing Analytics
        $monthlyRevenue = $this->getFillMissingArray(Claim::getMonthlySales()->get()->toArray(),['month','total']);   
        $monthlyDeferredRevenue = $this->getFillMissingArray(Claim::getMonthlyDeferredSales()->get()->toArray(),['month','total']);
        $monthlyEstSales = $this->getFillMissingArray(Claim::getMonthlyEstSales()->get()->toArray(),['month','total']);

        $monthlyData = ['monthlyRevenue'=> $monthlyRevenue,
                        'monthlyDeferredRevenue'=> $monthlyDeferredRevenue,
                        'monthlyEstSales'=> $monthlyEstSales];

        // Billing Amount
        $amountPaidLM    = number_format(Claim::sumClaims($dateArrLM, config('constants.paymentStatus.0')), 2, '.', ',');
        $amountPaidTM    = number_format(Claim::sumClaims($dateArr,   config('constants.paymentStatus.0')), 2, '.', ',');
        $amountPaidNM    = number_format(Claim::sumClaims($dateArrNM, config('constants.paymentStatus.1')), 2, '.', ',');
        $totalUnpaidAmt  = number_format(Claim::sumClaims('',         config('constants.paymentStatus.1')), 2, '.', ',');

    	// Job Posts
    	$totalPosts      = JobPosting::countJobPosts();

        // Job Rankings
        $jobRank         = JobPosting::getJob('popullar', null)
                                                ->take(3)
                                                ->get();
        // PV for top 3
        $pv = array();
        foreach ($jobRank as $key => $value) {
            Array_push($pv,JobPostView::getTotalPageViews($value->job_post_id,'job_post_id'));
        }
   		return view('admin.home')->with('newlyRegThisMth',          $newlyRegThisMth)
   								 ->with('newlyRegTotal',            $newRegTotal)
   								 ->with('totalPosts',               $totalPosts)
   								 ->with('totalTrialPlan',           $totalTrialPlan)
   								 ->with('totalStandard',            $totalStandard)
   								 ->with('totalEnterprise',          $totalEnterprise)
                                 ->with('amountPaidLM',             $amountPaidLM)
                                 ->with('amountPaidTM',             $amountPaidTM)
                                 ->with('amountPaidNM',             $amountPaidNM)
                                 ->with('monthlyData',              $monthlyData)
                                 ->with('totalUnpaidAmt',           $totalUnpaidAmt)
                                 ->with('jobRank',                  $jobRank)
                                 ->with('pv',                       $pv);
    }
}
