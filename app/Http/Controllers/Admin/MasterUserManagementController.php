<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterUserManagement;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\ApplicantsProfile;
use App\Models\CompanyUser;
use App\Models\CompanyUserRights;
use App\Models\MasterMailTemplate;

use App\Mail\RegisterUser;

use App\Helpers\Utility\DateHelper;
use Spyc;
use App\Models\ZipCodes\Philippines; //for Provider
use App\Models\MasterCountry;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;

use Validator; 
use Response;
use Auth;
use Lang;

/**
 * Contoller of Admin - User Management
 * 
 * @author    Karen Cano <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 */

class MasterUserManagementController extends Controller
{
    public $request;

    function __construct(Request $request) {
        $this->request = $request;
    }

    /**
    * getActiveUsers
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @author    Rey Norbert Besmonte  <rey_besmonte@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-11-09
    */
    public function getActiveUsers()
    {
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);
        $companyList = Company::select('company_id', 'company_name')->get();

        $activeUsers = MasterUserManagement::getUser()
                        ->paginate(10);
        $companies = Company::all();

        return view('admin.master-admin.user')
                        ->with('activeUsers',$activeUsers)
                        ->with('companies',$companies)
                        ->with('arrPhilippines',$arrYaml)
                        ->with('companyList',$companyList);
                        
    }

    /**
    * softDeleteUser
    * updates users with user_status = 'INACTIVE'
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-10
    */
    public function softDeleteUser($userId)
    {
        $successMessage = "Delete success : ";
        $errorMessage = "Delete failed : ";
        $errorFlag = false;
        $softDeleteUser = User::findorFail($userId);
        try
        {
            $softDeleteUser->user_status = 'INACTIVE';
            $softDeleteUser->updated_at = Carbon::now();
            $softDeleteUser->save();

            $successMessage .= " ".$softDeleteUser->user_firstname;
            $successMessage .= " ".$softDeleteUser->user_middlename;
            $successMessage .= " ".$softDeleteUser->user_lastname;
        }
        catch(\Exception $e)
        {
            $errorMessage .= $softDeleteUser->user_firstname;
            $errorMessage .= $softDeleteUser->user_middlename;
            $errorMessage .= $softDeleteUser->user_lastname;
            $errorMessage = " :: ".$e->getMessage();
        }

        return Redirect('admin/master-admin/user')
                        ->with('successMessage',$successMessage)
                        ->withError('errorMessage',$errorMessage);
    }

    /**
    * Edit User
    * Allows Admin to edit user (Applicant/Company user) 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-29-11
    */
    public function editUser($user_id, $user_accounttype)
    {
        $companyList = Company::select('company_id', 'company_name')->get();
        $userId = Auth::user()['id'];
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);
        $countries = MasterCountry::get();
        $isUserSaved = session()->get('isUserSaved') ?? null;

        if($user_accounttype == "USER")
        {
          $applicantUser = ApplicantsProfile::select('applicant_profile_id',
                                                     'applicant_profile_firstname',
                                                     'applicant_profile_middlename', 
                                                     'applicant_profile_lastname', 
                                                     'applicant_profile_preferred_email',
                                                     'applicant_profile_gender',
                                                     'applicant_profile_birthdate',
                                                     'applicant_profile_postalcode',
                                                     'applicant_profile_address1',
                                                     'applicant_profile_address2',
                                                     'applicant_profile_contact_no',
                                                     'applicant_profile_user_id'
                                                 )
                                ->where('applicant_profile_user_id', '=', $user_id)
                                ->first();

           $user = User::where('id', '=', $userId)->first();

          return view('admin.master-admin.user-edit-applicant', compact('applicantUser', 'companyList'))
                                                                ->with('arrPhilippines', $arrYaml)
                                                                ->with('isUserSaved', $isUserSaved)
                                                                ->with('userId', $userId)
                                                                ->with('user', $user);
        }
        // Just in case need to edit Corporate Admin User
        if($user_accounttype == "CORPORATE USER")
        {
            $corporateUser = CompanyUser::select('company_user_id',
                                                 'company_user_firstname',
                                                 'company_user_lastname',
                                                 'company_user_contact_no',
                                                 'company_user_designation',
                                                 'company_user_gender',
                                                 'company_user_birthdate',
                                                 'company_user_postalcode',
                                                 'company_user_address1',
                                                 'company_user_address2',
                                                 'company_user_company_id',
                                                 'company_user_user_id')
                             ->where('company_user_user_id', '=', $user_id)
                             ->first();
            $corporateUserEmail = User::select('email')->where('id', '=', $corporateUser->company_user_user_id)->first();
            $corporateUserCompany = Company::select('company_name', 'company_id')->where('company_id', '=', $corporateUser->company_user_company_id)->first();

            $companyUserRightsScout = CompanyUserRights::where('company_user_right_company_user_id', '=', $corporateUser->company_user_id)
                                                        ->where('company_user_right_key', '=', 'SCOUT')->first();

            $companyUserRightsCompanyInformation = CompanyUserRights::where('company_user_right_company_user_id', '=', $corporateUser->company_user_id)
                                                                    ->where('company_user_right_key', '=', 'COMPANY INFORMATION')->first();

            $companyUserRightsJobPost = CompanyUserRights::where('company_user_right_company_user_id', '=', $corporateUser->company_user_id)
                                                        ->where('company_user_right_key', '=', 'JOB POST')->first();
          
           $user = User::where('id', '=', $userId)->first();
           return view('admin.master-admin.user-edit-corporateuser', compact('corporateUser', 'corporateUserEmail', 'companyList', 'corporateUserCompany'))
                                                                    ->with('scoutRight',  $companyUserRightsScout)
                                                                    ->with('companyInfoRight',  $companyUserRightsCompanyInformation)
                                                                    ->with('jobPostRight',  $companyUserRightsJobPost)
                                                                    ->with('arrPhilippines', $arrYaml)
                                                                    ->with('userId', $userId)
                                                                    ->with('user', $user)
                                                                    ->with('isUserSaved', $isUserSaved);
                                                                    
        }

    }

    /**
     * GET: Render master_plans user view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/master-admin/user/create.blade.php
     */
     public function createView()
     {
        $companyList = Company::select('company_id', 'company_name')->where('company_status','ACTIVE')->get();
        $userId = Auth::user()['id'];
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);
        $countries = MasterCountry::get();
        $isUserSaved = null;
        return view('admin.master-admin.user-create', compact('companyList'))
                ->with('arrPhilippines', $arrYaml)
                ->with('isUserSaved', $isUserSaved)
                ->with('userId', $userId);
     }

    /**
     * POST:   /company/admin/companies/createCompany
     * Create/update company controller action
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since  2017-12-05
     *
     * @param  array $request form values
     *
     * @return View company/profile/index.blade.php
     */
    public function saveUserPassword(Request $request)
    {   
        $accountType = $this->request->input('user_account_type');
        $viewToReturn = 'admin.master-admin.user-create';

        switch ($accountType) {
            case 'Applicant User':

                if(!empty($request->input('applicantUserId')))
                {
                    $applicantProfileUserId = $request->input('applicantUserId');
                    $viewToReturn = 'admin.master-admin.user-edit-applicant';
                }else
                {
                    $applicantProfileUserId = '';
                }

                !is_null($request->input('txt_password'))
                    ? $user['password'] = bcrypt($request->input('txt_password'))
                    : ''; //else do nothing

                $user['id']               = $applicantProfileUserId;
                $user['email']            = $request->input('txt_email');

                $user['user_accounttype'] = 'USER';
                $user['user_status']      = 'ACTIVE';
                $user['user_company_id']  = '0';
                
                $resultUser = User::saveUser($user);

                $message = Lang::get('messages.admin.master-settings-users.user-applicant-change-password-success');
            break;
            case 'Corporate User':

                //Creation of Corporate Profile in Users table
                if(!empty($request->input('corporateUserId')))
                {
                    $corporateProfileUserId = $request->input('corporateUserId');
                    $viewToReturn = 'admin.master-admin.user-edit-corporateuser';
                }else
                {
                    $corporateProfileUserId = '';
                }

                !is_null($request->input('txt_password'))
                    ? $user['password'] = bcrypt($request->input('txt_password'))
                    : ''; //else do nothing

                //Creation of Corporate User in Users table
                $user['id']               = $corporateProfileUserId;
                $user['email']            = $request->input('txt_email');
                $user['user_accounttype'] = 'CORPORATE USER';
                $user['user_status']      = 'ACTIVE';
                $user['user_company_id']  =  $request->input('company_name');

                $resultUser = User::saveUser($user);

                $message = Lang::get('messages.admin.master-settings-users.user-corporate-change-password-success');

            break;
            default:
                echo "Default called";
            break;
        }

        $companyList = Company::select('company_id', 'company_name')->get();
        $userId = Auth::user()['id'];
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        if($viewToReturn != 'admin.master-admin.user-create')
        {
            // $request->session()->flash('message', $message);
            return redirect()->back()->with('message', $message);
                               
        }else{
            return view($viewToReturn, compact('companyList'))
                    ->with('arrPhilippines', $arrYaml)
                    ->with('message', $message)
                    ->with('userId', $userId);
        }
    }


    /**
    * Master Admin Add User (User Management)
    * Allows Admin to add new user (Applicant/Company user) 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-29-11
    */
     public function saveNewUser(Request $request)
     {
        $accountType = $this->request->input('user_account_type');
        $viewToReturn = 'admin.master-admin.user-create';
        //added by Alvin
        $customErrorMessages = [
            'txt_email.unique' => 'Email has already been taken.',
            'txt_email.email' => 'Email is not a proper email format.',
            'txt_password.confirmed' => 'Password do not match.',
        ];

        switch ($accountType) {
            case 'Applicant User':

                //VALIDATION OF REQUIRED FIELDS
                $validator = Validator::make($request->all(),[
                        'firstName'         => 'required',
                        'lastName'          => 'required',
                        'gender'            => 'required',
                        'txt_email'         => 'required|email|unique:users,email,'.$request->input('applicantUserId').',id',
                        'txt_password'      => 'nullable|confirmed',
                        'contactNumber'     => 'required',

                ], $customErrorMessages);

                if($validator->fails()){
                    return back()->withErrors($validator)->withInput();
                }

                //Creation of Applicant Profile in Users table
                if(!empty($request->input('applicantUserId')))
                {
                    $applicantProfileUserId = $request->input('applicantUserId');
                    $viewToReturn = 'admin.master-admin.user-edit-applicant';
                }else
                {
                    $applicantProfileUserId = '';
                }
                $user['id']               = $applicantProfileUserId;
                $user['email']            = $request->input('txt_email');
                $user['user_accounttype'] = 'USER';
                $user['user_status']      = 'ACTIVE';
                $user['user_company_id']  = '0';
                $user['activation_token'] = str_random(60);
                
                !is_null($request->input('txt_password')) 
                        ?   $user['password'] = bcrypt($request->input('txt_password'))
                        :   '';
                
                $resultUser = User::saveUser($user);


                //For Edit Applicant profile purposes
                if(!empty($request->input('applicantId')))
                {
                    $applicantProfileId = $request->input('applicantId');
                }else
                {
                    $applicantProfileId = '';
                }

                //Creation of Applicant Profile in Applicants_Profile table
                $applicantProfile['applicant_profile_id']               = $applicantProfileId;
                $applicantProfile['applicant_profile_firstname']        = $request->input('firstName');
                $applicantProfile['applicant_profile_middlename']        = $request->input('middleName');
                $applicantProfile['applicant_profile_lastname']         = $request->input('lastName');
                $applicantProfile['applicant_profile_gender']           = $request->input('gender');
                $applicantProfile['applicant_profile_postalcode']       = $request->input('postalCode');
                $applicantProfile['applicant_profile_address1']         = $request->input('address1');
                $applicantProfile['applicant_profile_address2']         = $request->input('address2');
                $applicantProfile['applicant_profile_contact_no']       = $request->input('contactNumber');
                $applicantProfile['applicant_profile_preferred_email']  = $request->input('txt_email');

                if (is_object($resultUser)){
                     $applicantProfile['applicant_profile_user_id']     = $resultUser->id;
                }else {
                     $applicantProfile['applicant_profile_user_id']     = User::getUpdatedUserObj($applicantProfileUserId)->id;
                     $message = 'User has been updated';
                }

                ////////////////////ADDITIONAL VALIDATION FOR SPECIFIC FIELDS/////////////////
                if( !empty($request->input('birthDate')) )
                 $applicantProfile['applicant_profile_birthdate'] = DateHelper::createDate($request->input('birthDate'));
                ///////////////////////////////////////////////////////////////////////////////

                

                $result = ApplicantsProfile::createProfile($applicantProfile);
                if(empty($message))
                {
                    if($result == 1){

                        $applicant = ApplicantsProfile::where('applicant_profile_preferred_email',$applicantProfile['applicant_profile_preferred_email'])->first();

                        $mailTemplate = MasterMailTemplate::where('master_mail_status','NEW USER')->first();          
                        Mail::to($user['email'])
                                    ->send(new RegisterUser(
                                            $applicant,$mailTemplate
                                                ));
                                                
                        $message = Lang::get('messages.admin.master-settings-users.user-applicant-change-success');
                    }else{
                        $message = Lang::get('messages.admin.master-settings-users.user-applicant-change-fail');
                    }
                }

                break; //End of Applicant User case

            case 'Corporate User':

                //VALIDATION OF REQUIRED FIELDS
                $validator = Validator::make($request->all(),[
                    'firstName'     => 'required',
                    'lastName'      => 'required',
                    'contactNumber' => 'required',
                    'user_designation'    => 'required',
                    'job-posting'   => 'required',
                    'scout'         => 'required',
                    'company_information' => 'required',
                    'txt_email' => 'required|email|unique:users,email,'.$request->input('corporateUserId').',id',
                    'password'         => 'nullable|confirmed',
                ], $customErrorMessages);

                if($validator->fails()){
                    return back()->withErrors($validator)->withInput();
                }

                //Creation of Corporate Profile in Users table
                if(!empty($request->input('corporateUserId')))
                {
                    $corporateProfileUserId = $request->input('corporateUserId');
                    $viewToReturn = 'admin.master-admin.user-edit-corporateuser';
                }else
                {
                    $corporateProfileUserId = '';
                }

                //Creation of Corporate User in Users table
                $user['id']               = $corporateProfileUserId;
                $user['email']            = $request->input('txt_email');
                $user['user_accounttype'] = 'CORPORATE USER';
                $user['user_status']      = 'ACTIVE';
                $user['user_company_id']  =  $request->input('company_name');

                !is_null($request->input('txt_password'))
                    ? $user['password'] = bcrypt($request->input('txt_password'))
                    : ''; //else do nothing

                $resultUser = User::saveUser($user);

                if(!empty($request->input('corporateId')))
                {
                    $corporateProfileId = $request->input('corporateId');
                }else
                {
                    $corporateProfileId = '';
                }

                //Creation of Corporate Profile in company_users table
                $corporateUserId = $corporateProfileId;
                $corporateProfile['company_user_id']           = $corporateUserId;
                $corporateProfile['company_user_firstname']    = $request->input('firstName');
                $corporateProfile['company_user_lastname']     = $request->input('lastName');
                $corporateProfile['company_user_contact_no']   = $request->input('contactNumber');
                $corporateProfile['company_user_designation']  = $request->input('user_designation');
                $corporateProfile['company_user_gender']       = $request->input('gender');
                $corporateProfile['company_user_postalcode']   = $request->input('postalCode');
                $corporateProfile['company_user_address1']     = $request->input('address1');
                $corporateProfile['company_user_address2']     = $request->input('address2');
                $corporateProfile['company_user_company_id']   = $request->input('company_name');
                // $corporateProfile['company_user_user_id']      = $resultUser->id;

                if (is_object($resultUser)){
                     $corporateProfile['company_user_user_id']     = $resultUser->id;
                }else {
                     $corporateProfile['company_user_user_id']     = User::getUpdatedUserObj($corporateProfileUserId)->id;
                     $message = 'User has been updated';
                }


                ////////////////////ADDITIONAL VALIDATION FOR SPECIFIC FIELDS/////////////////
                if( !empty($request->input('birthDate')) )
                  $corporateProfile['company_user_birthdate'] = DateHelper::createDate($request->input('birthDate'));
                //////////////////////////////////////////////////////////////////////////////

                $objectOfUser = CompanyUser::createCompanyUser($corporateProfile);
                $result = !is_null($objectOfUser->company_user_id) ?? false;

                if(empty($request->input('newuser')))
                {
                    $companyRightId = $objectOfUser->company_user_id;
                }else
                {
                    $companyRightId = '';
                }
                $companyUserRightId = $companyRightId;
                $companyUserRightsJobPost['company_user_right_key']             = 'JOB POST';
                $companyUserRightsJobPost['company_user_right_value']           = (int) $request->input('job-posting');
                $companyUserRightsJobPost['company_user_right_company_user_id']      = $objectOfUser->company_user_id;
                $companyUserRightsScout['company_user_right_key']               = 'SCOUT';
                $companyUserRightsScout['company_user_right_value']             = (int) $request->input('scout');
                $companyUserRightsScout['company_user_right_company_user_id']        = $objectOfUser->company_user_id;
                $companyUserRightsInformation['company_user_right_key']         = 'COMPANY INFORMATION';
                $companyUserRightsInformation['company_user_right_value']       = (int) $request->input('company_information');
                $companyUserRightsInformation['company_user_right_company_user_id']  = $objectOfUser->company_user_id;

                CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsJobPost);
                CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsScout);
                CompanyUserRights::createUserRights($companyUserRightId, $companyUserRightsInformation);

                if($result==1)
                    $message = Lang::get('messages.admin.master-settings-users.user-corporate-change-success');
                else
                    $message = Lang::get('messages.admin.master-settings-users.user-corporate-change-fail');
                break;//End of Corporate User case
            
            default:
               echo "Default called";
                break;
            
        }


        $companyList = Company::select('company_id', 'company_name')->get();
        $userId = Auth::user()['id'];
        $strYaml = Philippines::ZipCodes();
        $arrYaml = Spyc::YAMLLoadString($strYaml);

        if($viewToReturn != 'admin.master-admin.user-create')
        {
            return redirect()->back()
                    ->with('isUserSaved', $result);
                               
        }else{
            return view($viewToReturn, compact('companyList'))
                    ->with('arrPhilippines', $arrYaml)
                    ->with('isUserSaved', $result)
                    ->with('userId', $userId);
        }
      
        
       
     }

     /**
    * updateAccountType
    * updates users with user_accounttype = 'USER/ADMIN/COMPANY'
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-10
    */
    public function updateAccountType($userId,$accountType)
    {
        $successMessage = "Update success!";
        $errorMessage = "Update failed!";
        $errorFlag = false;
        $updateAccountUser = User::findorFail($userId);
        try
        {
            $updateAccountUser->user_accounttype = $accountType;
            $updateAccountUser->updated_at = Carbon::now();
            $updateAccountUser->save();

            $successMessage .= " ".$updateAccountUser->user_firstname;
            $successMessage .= " ".$updateAccountUser->user_middlename;
            $successMessage .= " ".$updateAccountUser->user_lastname;
            $successMessage .= ":: ".$updateAccountUser->user_accounttype;
        }
        catch(\Exception $e)
        {
            $errorMessage .= $updateAccountUser->user_firstname;
            $errorMessage .= " ".$updateAccountUser->user_middlename;
            $errorMessage .= " ".$updateAccountUser->user_lastname;
            $errorMessage = " :: ".$e->getMessage();
        }

        return array(
                    'successMessage' => $successMessage,
                    'errorMessage' => $errorMessage
                );
    }

   /**
     * GET(AJAX): Render /company/account/user/create index view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  Reques $request form values
     * @return true if exists
     */
    public function emailCheck(Request $request)
    {   
        $user['id']    = $request->input('userId');
        $user['email'] = $request->input('email');
        
        $exists = User::emailExists($user);

        return Response::json($exists);
    }

}//end of MasterUserManagementController