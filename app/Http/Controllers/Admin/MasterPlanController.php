<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\MasterPlans;

/**
 * MasterPlanController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 */

class MasterPlanController extends Controller
{

   /**
    * GET: Index containing list of all master tables
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @return View admin/master-admin/plan.blade.php
    */
    public function index()
    {
        $masterPlans  = MasterPlans::all();

        return view('admin.master-admin.plan.index')->with('masterPlans', $masterPlans);
    }
    
    /**
     * GET: Render master_plans create view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/master-admin/plan/create.blade.php
     */
    public function createView()
    {
        return view('admin.master-admin.plan.create');
    }

    /**
     * GET: Render master_plans edit view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/master-admin/plan/edit.blade.php
     */
    public function editView(Request $request)
    {
        $masterPlanId = $request->input('master_plan_id');
        $masterPlan   = MasterPlans::find($masterPlanId);

        return view('admin.master-admin.plan.edit')->with('masterPlan', $masterPlan);
    }

    /**
     * POST: Create/update of master_plan
     *
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View admin/master-admin/company.blade.php
     */
    public function savePlan(Request $request)
    {
        $request->validate([
              'txt_name'        => 'required',
              'txt_price'       => 'required',
              'txt_post_limit'  => 'required',
              'txt_expiry_days' => 'required'
          ]);

        $master_plan['master_plan_id']          = $request->input('txt_id');
        $master_plan['master_plan_name']        = $request->input('txt_name');
        $master_plan['master_plan_price']       = $request->input('txt_price');
        $master_plan['master_plan_post_limit']  = $request->input('txt_post_limit');
        $master_plan['master_plan_expiry_days'] = $request->input('txt_expiry_days');
        $master_plan['master_plan_status']      = $request->input('txt_status') 
                                                ?? $request->input('so_status') ; //edit view

        MasterPlans::savePlan($master_plan);
        
        return redirect('/admin/master-admin/plan');
    }


}