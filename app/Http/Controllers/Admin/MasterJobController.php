<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MasterJobClassifications;
use App\Models\MasterJobPositions;
use App\Models\MasterJobIndustries;
use App\Models\MasterCountry;
use App\Models\MasterSalaryRange;
use App\Models\MasterEmployeeRange;
use App\Models\MasterSkills;
use Lang;

/**
 * Contoller of Company / Company
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 */

class MasterJobController extends Controller
{

   /**
    * GET: Index containing list of all master tables
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function index()
    {
        $masterJobClassifications  = MasterJobClassifications::all();
        $masterJobIndustries       = MasterJobIndustries::getAll(array('master_job_industry_status' => 'ACTIVE'));
        $masterJobIndustriesActive = MasterJobIndustries::getAll();
        $masterJobPositions        = MasterJobPositions::getAll(array('status' => 'ACTIVE'));
        $masterJobPositionsActive  = MasterJobPositions::getAll(
                                                                array('status' => 'ACTIVE')
                                                               );
        $masterCountries           = MasterCountry::all();
        $masterSalaryRanges        = MasterSalaryRange::all();
        $masterEmployeeRanges      = MasterEmployeeRange::all();
        $masterSkills              = MasterSkills::getAll();

        return view('admin.master-admin.job')->with('masterJobClassifications', $masterJobClassifications)
                                             ->with('masterJobPositions', $masterJobPositions)
                                             ->with('masterJobIndustries', $masterJobIndustries )
                                             ->with('masterJobIndustriesActive', $masterJobIndustriesActive )
                                             ->with('masterCountries', $masterCountries )
                                             ->with('masterSalaryRanges', $masterSalaryRanges)
                                             ->with('masterEmployeeRanges', $masterEmployeeRanges)
                                             ->with('masterSkills', $masterSkills)
                                             ->with('masterJobPositionsActive', $masterJobPositionsActive);
    }

    /**
    * POST: deleteJobIndustry
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete a job industry by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteJobIndustry($jobIndustryId)
    {
        $jobIndustry = MasterJobIndustries::findOrFail($jobIndustryId);
        $checkCompanyIndustries = $jobIndustry->company_industries->count();
        $checkJobPosts = $jobIndustry->job_posts->count();
        $count = $checkCompanyIndustries + $checkJobPosts; 
        $message = '';
        if($count > 0)
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-delete-has-child');
        }
        else //$count == 0 //no depencies
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.job-industry');
            $jobIndustry->update(['master_job_industry_status' => 'INACTIVE']);
        }
        return back()
                ->with('message',$message);
    }

     /**
    * POST: deleteJobIndustry
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete a job industry by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteJobClassification($jobClassificationId)
    {

        $jobClassification = MasterJobClassifications::findOrFail($jobClassificationId);
        $count = $jobClassification->job_posts->count();
        $message = '';
        if($count > 0)
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-delete-has-child');
        }
        else //$count == 0 //no depencies
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.job-classification');
            $jobClassification->delete();
        }
        return back()
                ->with('message',$message);
    }

     /**
    * POST: deleteJobPosition
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete a job industry by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteJobPosition($jobPositionId)
    {
        $jobPosition = MasterJobPositions::findOrFail($jobPositionId);
        $count = $jobPosition->job_posts->count();
        $message = '';
        if($count > 0)
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-delete-has-child');
        }
        else //$count == 0 //no depencies
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.job-position');
            $jobPosition->update(['master_job_position_status' => 'INACTIVE']);
        }
        return back()
                ->with('message',$message);
    }

      /**
    * POST: deleteCountry
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete a country by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteCountry($countryId)
    {
        $country = MasterCountry::findOrFail($countryId);
        $count = $country->job_locations->count();
        $message = '';
        if($count > 0)
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-delete-has-child');
        }
        else //$count == 0 //no depencies
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.country');
            $country->delete();
        }
        return back()
                ->with('message',$message);
    }

    /**
    * POST: deleteSalaryRange
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete a salary range by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteSalaryRange($salaryId)
    {
        //Karen : no child nodes
        $salaryRange = MasterSalaryRange::findOrFail($salaryId);
        $message = Lang::get('messages.admin.master-settings-job-applications.salary-range');
        $salaryRange->delete();
        return back()
                ->with('message',$message);
    }

    /**
    * POST: deleteEmployeeRange
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete an employee range by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteEmployeeRange($employeeRangeId)
    {
        $employeeRange = MasterEmployeeRange::findOrFail($employeeRangeId);
        $count = $employeeRange->companies->count();
        $message = '';
        if($count > 0)
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-delete-has-child');
        }
        else //$count == 0 //no depencies
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.employee-range');
            $employeeRange->delete();
        }
        return back()
                ->with('message',$message);
    }
    
    /**
    * POST: deleteEmployeeRange
    *
    * @author Karen Irene Cano <karen_cano@commude.ph>
    * @summary delete an skill by its id
    * @return View admin/master-admin/job.blade.php
    * string message 
    */
    public function deleteSkill($skillId)
    {
        //Karen : no child nodes
        $skill = MasterSkills::findOrFail($skillId);
        $message = Lang::get('messages.admin.master-settings-job-applications.skill');
        $skill->delete();
        return back()
                ->with('message',$message);
    }
    


   /**
    * POST: Create/update of master_job_classifcations
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @param  array $request form values
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function saveJobClassification(Request $request)
    {
        $request->validate([
              'txt_master_job_classification_name' => 'required'
          ]);

        //$master_job_classification['master_job_classification_id']   = $request->input('txt_master_job_classification_id');
        $master_job_classification['master_job_classification_name'] = $request->input('txt_master_job_classification_name');

        $res = MasterJobClassifications::saveJobClassification($master_job_classification);
        
        $message = '';
        if($res == true)
            $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
        else
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');
        
            return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

    /**
     * POST: Create/update of master_job_classifcations
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $request form values
     *
     * @return View admin/master-admin/job.blade.php
     */
    public function saveJobIndustry(Request $request)
    {
        $request->validate([
              'txt_master_job_industry_name'   => 'required',
            //   'so_master_job_industry_status' => 'required'
          ]);

        $master_job_classification['master_job_industry_id']     = $request->input('txt_master_job_industry_id');
        $master_job_classification['master_job_industry_name']   = $request->input('txt_master_job_industry_name');
        $master_job_classification['master_job_industry_status'] = 'ACTIVE';

        $res = MasterJobIndustries::saveJobIndustry($master_job_classification);
        
        $message = '';
        if($res == true)
            $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
        else
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');

        return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

   /**
    * POST: Create/update of master_job_postion
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @param  array $request form values
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function saveJobPosition(Request $request)
    {
        $request->validate([
              'txt_master_job_position_name'       => 'required',
            //   'so_master_job_position_status'      => 'required',
              'so_master_job_position_industry_id' => 'required'
          ]);

        $master_job_position['master_job_position_id']          = $request->input('txt_master_job_position_id');
        $master_job_position['master_job_position_name']        = $request->input('txt_master_job_position_name');
        $master_job_position['master_job_position_status']      = "ACTIVE";
        $master_job_position['master_job_position_industry_id'] = $request->input('so_master_job_position_industry_id');

        $res = MasterJobPositions::saveJobPosition($master_job_position);
        
        $message = '';
        if($res == true)
            $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
        else
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');

        return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

    /**
    * POST: Create/update of master_country
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @param  array $request form values
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function saveCountry(Request $request)
    {
        $request->validate([
              'txt_master_country_name' => 'required',
          ]);

        $master_country['master_country_id']   = $request->input('txt_master_country_id');
        $master_country['master_country_name'] = $request->input('txt_master_country_name');

        $res = MasterCountry::saveCountry($master_country);
        
        $message = '';
        if($res == true)
            $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
        else
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');

        return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

    /**
    * POST: Create/update of master_salary_range
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @param  array $request form values
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function saveSalaryRange(Request $request)
    {
        $request->validate([
              'txt_master_salary_range_min' => 'required',
              'txt_master_salary_range_max' => 'required'
          ]);

        $master_salary_range['master_salary_range_id']  = $request->input('txt_master_salary_range_id');
        $master_salary_range['master_salary_range_min'] = $request->input('txt_master_salary_range_min');
        $master_salary_range['master_salary_range_max'] = $request->input('txt_master_salary_range_max');
        
        $message = '';
        
        if($master_salary_range['master_salary_range_min'] <=  $master_salary_range['master_salary_range_max'])
        {
            $res = MasterSalaryRange::saveSalaryRange($master_salary_range);
            
            if($res == true)
                $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
            else
                $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');
        }
        else
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-range');
        }
        
        return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

    /**
    * POST: Create/update of master_employee_range
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @param  array $request form values
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function saveEmployeeRange(Request $request)
    {
        $request->validate([
              'txt_master_employee_range_min' => 'required',
              'txt_master_employee_range_max' => 'required'
          ]);

        $master_employee_range['master_employee_range_id']  = $request->input('txt_master_employee_range_id');
        $master_employee_range['master_employee_range_min'] = $request->input('txt_master_employee_range_min');
        $master_employee_range['master_employee_range_max'] = $request->input('txt_master_employee_range_max');

        

        $message = '';
        if($master_employee_range['master_employee_range_min'] <=  $master_employee_range['master_employee_range_max'])
        {
            $res = MasterEmployeeRange::saveEmployeeRange($master_employee_range);
            if($res == true)
                $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
            else
                $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');
        }
        else
        {
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-range');
        }

        return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

    /**
    * POST: Create/update of master_skills
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    *
    * @param  array $request form values
    *
    * @return View admin/master-admin/job.blade.php
    */
    public function saveSkill(Request $request)
    {
        $request->validate([
              'txt_master_skill_name' => 'required',
              'so_master_skill_job_position_id' => 'required'
          ]);

        $master_skill['master_skill_id']   = $request->input('txt_master_skill_id');
        $master_skill['master_skill_name'] = $request->input('txt_master_skill_name');
        $master_skill['master_skill_job_position_id'] = $request->input('so_master_skill_job_position_id');

        $res = MasterSkills::saveSkill($master_skill);
        
        $message = '';
        if($res == true)
            $message = Lang::get('messages.admin.master-settings-job-applications.success-add');
        else
            $message = Lang::get('messages.admin.master-settings-job-applications.forbidden-existing');
        

        return redirect('/admin/master-admin/job')
                ->with('message',$message);
    }

}
