<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\MasterJobFeatures;
use App\Models\JobPosting;
use \Datetime;
use Carbon\Carbon;
/**
 * Contoller of Company / Company
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 */

class MasterJobFeatureController extends Controller
{

   /**
    * GET: Index containing list of all master tables
    *
    * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
    * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
    * @since  Dec 14, 2017
    * @return View admin/master-admin/feature.blade.php
    */
    public function index()
    {
        $masterJobFeatures  = MasterJobFeatures::getAll(null)
                                                ->get();

        $jobs               = JobPosting::getFeaturedJob('latest', null)
                                                ->get();

        return view('admin.master-admin.feature', compact('jobs'))->with('masterJobFeatures', $masterJobFeatures);
    }

    public function getJobDetailsForFeature($job_id)
    {
        $condition[0] = array(['job_posts.job_post_id','=', $job_id]);
        $jobDetails  = JobPosting::getJob('latest', $condition)
                            ->first();
        // $masterJobFeatures  = MasterJobFeatures::all();
        // $jobs               = JobPosting::getJob('latest', null)
        //                                         ->get();

        return json_encode($jobDetails);
    }
    
    /**
     * POST: Create/update of master_job_classifcations
     *
     * File upload using Laravel file storage. Get filename only using basename
     *
     * @todo check if page is mobile and load from pc or sp
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @param  array $request form values
     * @since  Dec 14, 2017
     * @since Feb 21, 2018
     * @return View admin/master-admin/feature.blade.php
     */
    public function saveJobFeature(Request $request)
    {
        $now = Carbon::parse(Carbon::now()->toDateString());
        $from = Carbon::parse($request->input('txt_job_feature_datefrom'));
        $to = Carbon::parse($request->input('txt_job_feature_dateto'));

        if($from->gte($to)) {
            return redirect('admin/master-admin')->with('message', '"From" Date must not be greater than or equal to the "To" Date!');
        }else if($from->lt($now)){
            return redirect('admin/master-admin')->with('message', '"From" Date must not be less than the current date.');
        }else if(!isset($request->all()['job_title'])) {
            return redirect('admin/master-admin')->with('message', 'Please select a Job Title!');
        } 


        $condition[0] = array(['master_job_features.job_feature_job_post_id','=', $_POST['job_title']]);
        $isRecorded  = MasterJobFeatures::getAll($condition)
                                            ->get();

        $allRecords = MasterJobFeatures::all();

        if(count($isRecorded) > 0) {
            return redirect('/admin/master-admin')->with('message', 'Job already added!');
        } else if(count($allRecords) >= 5) {
            return redirect('/admin/master-admin')->with('message', 'Jobs added are at the 5-post limit! Please remove some to add another.');
        }

        if(isset($_POST['save'])){
            echo "save";
            $format = 'm/d/Y';
            $master_job_feature['job_feature_job_post_id']  = $_POST['job_title'];
            $master_job_feature['job_feature_datefrom']     = DateTime::createFromFormat($format, $_POST['txt_job_feature_datefrom']);
            $master_job_feature['job_feature_dateto']       = DateTime::createFromFormat($format, $_POST['txt_job_feature_dateto']);
            $master_job_feature['job_feature_visibility']   = $_POST['so_job_feature_visibility'];
            MasterJobFeatures::saveJobFeature($master_job_feature);
        }

        return redirect('/admin/master-admin')->with('message', 'Job Post successfully added on the Feature List!');
    }



    /**
     * POST: update of master_job_featured
     *
     * 
     *
     * @todo check if page is mobile and load from pc or sp
     * 
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @param  array $request form values
     * @since  Dec 15, 2017
     * @since  Feb 21, 2017
     * @return View admin/master-admin/feature.blade.php
     */
    public function editJobFeature(Request $request)
    {
        $data = $request->all();
        $now = Carbon::parse(Carbon::now()->toDateString());

        $dateFromFeatureTab = Carbon::parse($request->input('txt_job_feature_datefrom_'.$data['job_feature_id']));
        $dateToFeatureTab = Carbon::parse($request->input('txt_job_feature_dateto_'.$data['job_feature_id']));
        
        if(!isset($data['Remove'])){
            if($dateFromFeatureTab->gte($dateToFeatureTab)) {
                return redirect('admin/master-admin')->with('message', '"From" Date must not be greater than or equal to the "To" Date!');
            }else if($dateFromFeatureTab->lt($now)){
                return redirect('admin/master-admin')->with('message', '"From" Date must not be less than the current date.');
            }else{
                if(isset($data['Save'])){
                    //save the edited
                    $format = 'm/d/Y';
                    $master_job_feature['job_feature_datefrom']     = $dateFromFeatureTab;
                    $master_job_feature['job_feature_dateto']       = $dateToFeatureTab;
                    $res = MasterJobFeatures::where('job_feature_id', $data['job_feature_id'])
                                    ->update($master_job_feature);
                    $message = "Job Post successfully updated on the Feature List!";
                }
            }
        }else {
                //remove (Added by Alvin)
                $jobId = $data['job_feature_id'];
                MasterJobFeatures::find($jobId)->delete();
                $message = "Job Post successfully removed on the Feature List!";
        }

        return redirect('/admin/master-admin')->with('message', $message);
    }

}
