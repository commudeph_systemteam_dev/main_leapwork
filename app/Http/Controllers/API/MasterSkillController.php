<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MasterSkills;

use Carbon\Carbon;

use Auth;
use Lang;
use Response;

/**
 * MasterSkillController
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-04
 *
 */
class MasterSkillController extends Controller
{
     /**
     * GET AJAX: /master-skills/all
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $jobPositionId
     * 
     * @return JSON MasterSkills list
     */
    public function getAllSkills($jobPositionId = NULL)
    {
        // $masterSkills = array(
        //                     "masterSkills" => MasterSkills::getAllSkillNames()->toArray()[0]
        //                 );
         $masterSkills = MasterSkills::getAllSkillNames();

        return response()->json($masterSkills);
    }


}