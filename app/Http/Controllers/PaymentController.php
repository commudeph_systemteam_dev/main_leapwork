<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Claim;
use App\Models\Company;
use App\Models\CompanyPlanHistory;
use App\Models\MasterPlans;
use App\Models\MasterMailTemplate;
use App\Models\JobPosting;
use App\Models\User;
use App\Mail\RegisterCompany;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Stripe;
use StripeCharge;

/**
 * Contoller of Payment
 *
 * @author    Rey Norbert Besmonte <rey_besmonte@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-7-06
 */
class PaymentController extends Controller
{
    protected $stripeApiKey = 'sk_test_3lRuu0TUEPMTggwgdxor93Ub';

    /**
     * index views
     *
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * index views
     *
     * get the creadit card details. Check if the token is valid, and if the payment has already been made
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 7, 2017
     */
    public function creditCard()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $condition[0] = array(['claim_token', '=', $_POST['claim_no']]);
            $claims = Claim::getClaimEntry($condition);
            if ($claims != null) {
                if ($claims->claim_payment_status == "UNPAID") {
                    return view('payment/confirmation', compact('claims'));
                }
                $message = "Payment Already Made. Please check details and try again";
                return view('payment/credit_card', compact('message'));
            }
            $message = "Invalid Claim Code. Please check details and try again";
            return view('payment/credit_card', compact('message'));
        }
        return view('payment/credit_card');
    }

    /**
     * index views
     *
     * get the creadit card details with pre input token. From Admin Claims view. Check if the token is valid, and if the payment has already been made
     * display the details of the claim based from the token
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 7, 2017
     */
    public function creditCardWithCode($code)
    {
        
        $code = isset($_POST['claim_no'])
             ? $_POST['claim_no']
             : $code;

        $condition[0] = array(['claim_token', '=', $code]);
        $claims = Claim::getClaimEntry($condition) ?? null;

        // 2018/07/13 Louie: make sure plan payment is not yet expired
        if(Carbon::parse($claims->claim_end_date)->lte(Carbon::now())){
            return view('payment.expired');
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
           
        
            if ($claims != null) {
                if ($claims->claim_payment_status == "UNPAID") {
                    echo $_SERVER['REQUEST_METHOD'];
                    return view('payment/confirmation', compact('claims'));
                }
                $message = "Payment Already Made. Please check details and try again";
                return view('payment/credit_card', compact('message'));
            }
            $message = "Invalid Claim Code. Please check details and try again";
            return view('payment/credit_card', compact('message'));
        }
        return view('payment/credit_card', compact('code'));
    }

    public function confirmation()
    {
        return redirect('payment/credit_card');
    }

    /**
     * index views
     *
     * get the creadit card details. Check if the token is valid, and if the payment has already been made
     * Save the payment to Stripe
     * Activate the user account
     * change claim status to paid
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Feb 13, 2018
     */

    public function paymentComplete(Request $request)
    {
        if (isset($_POST['claimNumConfirm'])) {
            $condition[0] = array(['claim_token', '=', $_POST['claimNumConfirm']]);
            $code = $_POST['claimNumConfirm'];
            $claims = Claim::getClaimEntry($condition);


            if ($claims != null) {
                if ($claims->claim_payment_status == "UNPAID") {

                    Stripe::setApiKey($this->stripeApiKey);

                    try {
                        $charge = StripeCharge::create(array(
                            "amount" => $claims->master_plan_price . '00',
                            "currency" => "php",
                            "source" => $_POST['stripeToken'],
                            "description" => "Test payment.",
                        ));

                        Claim::updateClaim($claims, 'PAID');

                        $claimInfo = Claim::findFirstCompanyHistory($_POST['claimNumConfirm'], 'token');
                        $plan = MasterPlans::where('master_plan_name',$claimInfo[0]->claim_name)->first();
                        if (count(Claim::findFirstCompanyHistory($claimInfo[0]->company_plan_company_id, 'company_id')) == 1) {
                            $updatedValues['company_plan_datestarted'] = Carbon::now();
                            $updatedValues['company_plan_status'] = "ACTIVE";
                            $updatedValues['company_plan_history_id'] = $claimInfo[0]->company_plan_history_id;
                            CompanyPlanHistory::saveCompanyPlanHistory($updatedValues);
                        }

                        $updateCompany['company_credits'] = $plan->master_plan_post_limit;
                        $updateCompany['company_current_plan'] = $plan->master_plan_name;
                        $updateCompany['company_id'] = $claimInfo[0]->company_plan_company_id;
                        $updateCompany['company_status'] = 'ACTIVE';

                        Company::saveProfile($updateCompany);
                        
                        User::activateCorporateUser($claims);

                        $company = Company::where('company_id',$claimInfo[0]->company_plan_company_id)->first();

                        $mailTemplate = MasterMailTemplate::where('master_mail_status','PAID')->first();          
                                Mail::to($company->company_email)
                                            ->send(new RegisterCompany(
                                                            $company,$mailTemplate
                                                        ));
                        return redirect('payment/payment_complete');

                    } catch (\Stripe\Error\RateLimit $e) {
                        $this->stripeErrorHandler($e);
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        $this->stripeErrorHandler($e);

                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed
                        // (maybe you changed API keys recently)
                        $this->stripeErrorHandler($e);

                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid Request
                        $this->stripeErrorHandler($e);

                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed
                        $this->stripeErrorHandler($e);

                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $this->stripeErrorHandler($e);

                    } catch (\Stripe\Error\Card $e) {
                        $this->stripeErrorHandler($e);

                    } catch (\Exception $e) {
                        $this->stripeErrorHandler($e);
                    }

                    $message = "There seems to be an error when processing the payment. Please make sure that your credit card is valid.";
                    return view('payment/credit_card', compact('message', 'code'));

                }
                $message = "Payment Already Made. Please check details and try again";
                return view('payment/credit_card', compact('message', 'code'));
            }
            $message = "Invalid Claim Code. Please check details and try again";
            return view('payment/credit_card', compact('message', 'code'));
        } else {
            return view('payment/payment_complete');
        }
    }

    protected function stripeErrorHandler($e)
    {
        $body = $e->getJsonBody();
        $err = $body['error'];

        $message = $err['message'];
        return view('payment/credit_card', compact('message', 'code'));
    }

}
