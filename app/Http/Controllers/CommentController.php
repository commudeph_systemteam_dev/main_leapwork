<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;

use Auth;

class CommentController extends Controller
{
    public $request;
    
    function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * createComment
     * Create a comment on a job post
     * @author Karen Cano
     * @return View
     */
    public function createComment()
    {
        $comment = Comment::createComment($this->request);
        $backUrl = $this->redirectBackPath();
        return Redirect()->back()
                ->with('backUrl', $backUrl);
    }
}
