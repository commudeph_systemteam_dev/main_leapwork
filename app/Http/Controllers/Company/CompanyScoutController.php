<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Models\Scout;
use App\Models\ApplicantsProfile;
use App\Models\MasterSalaryRange;
use App\Models\JobPosting;
use App\Models\ApplicantsSkills;
use App\Models\CompanyMailTemplate;
use App\Models\Company;
use App\Models\ScoutConversation;
use App\Models\ScoutThread;
use App\Models\Notification;
use App\Events\NotifyEvent;
use Auth;
use DB;
use Lang;

/**Email Includes */
use App\Mail\ScoutFavorited;
use App\Mail\ScoutPreview;
use Illuminate\Support\Facades\Mail;

use App\Traits\NotificationFunctions;


/**
 * Contoller of Company / Scout
 * 
 * @author    Karen Cano       <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-25
 */
class CompanyScoutController extends Controller
{
    use NotificationFunctions;

    public $request;
    
    function __construct(Request $request) {
        $this->request = $request;
    }


    /**
    * updating the scout status
    *
    *
    * @author Alvin Generalo
    */
    function updateScout(Request $request)
    {
        //validation
        $request->validate([
            'scout_status' => 'required',
            'scout_id'      => 'required'
        ]);

        //get the necessary data
        $scout_id = $request->input('scout_id');
        $scout_status = $request->input('scout_status');

        //get the instances
        $update_scout = Scout::find($scout_id);
        
        //update the necessary column
        $update_scout->scout_status = $scout_status;
        //save the updated column
        $update_scout->save();


        return Redirect()->back();

    }

   /**
     * scout
     * Render Company company.scout
     * Where applicant does not exist yet at the scouts table
     * @author Karen Cano
     * @return View
     */
    public function scout()
    {

        $allWithApplicantProfile = Scout::getApplicantsToFavorite()->paginate(10);
        $jobPosts = JobPosting::groupJobByCompany(null, null)->where('job_post_company_id', '=', Auth::user()->user_company_id)->first();
        $message = null;

        if($jobPosts == null){
            $message = "You need to create a Job Post first before scouting";
        }

        return view('company.scout')
                    ->with('allWithApplicantProfile',$allWithApplicantProfile)
                    ->with('message', $message);
    }

    public function scoutResume($scout_id)
    {
        $scout = ApplicantsProfile::findOrFail($scout_id);
        $currentPath = Route::getFacadeRoot()->current()->uri();

        $isSearchView = ((strpos($currentPath, "search") ? true : false));

        return view('company.scout-resume')
                    ->with('scout',$scout)
                    ->with('isSearchView', $isSearchView);
    }
    
    /**
     * addToFavorite
     * Insert to scouts table the user and scout_status = 'FAVORITE'
     * @author Karen Cano
     * @return View
     */
    public function addToFavorite($applicant_profile_id)
    {
        Scout::addFavorite($applicant_profile_id);

        //just change to favorite status for now

        return Redirect()->back();
    }

    /**
     * addManyFavorite
     * Insert to scouts table the user and scout_status = 'FAVORITE'
     * @author Karen Cano
     * @return View
     */
    public function addManyFavorite()
    {
        $applicantIDs = $this->request->addFavorites;
        foreach($applicantIDs as $applicant_profile_id)
        {
            Scout::addFavorite($applicant_profile_id);
        }
        return Redirect()->back();
    }


    /**
     * scoutFavoriteUser
     * Render Company scout-favorite-user
     * @author Karen Cano
     * @return View
     */
    public function scoutFavoriteUser()
    {
        $favoriteUsers= Scout::getApplicantsFavorited();
        $message = ($favoriteUsers->get()->count() < 1) ? "No Favorite Users" : "" ;

        return view('company.scout-favorite-user')
                    ->with('favoriteUsers', $favoriteUsers->get())
                    ->with('message', $message);
    } 
    
    
	/**
     * scoutSearch
     * Render Company scout-search
     * @author Karen Cano
     * @return View
     */
    public function scoutSearch()
    {
        
        $requestSkillName = $this->request->skills_name;
        $gender = $this->request->gender;
        $age    = $this->request->age;
        $requestSalaryMin = $this->request->salary_min;
        $requestSalaryMax = $this->request->salary_max;
        $selectedSkillName = ($requestSkillName == null) ? '' : $requestSkillName ;
        $selectedSalaryMin = ($requestSalaryMin == null) ? '0' : $requestSalaryMin;
        $selectedSalaryMax = ($requestSalaryMax == null) ? '0' : $requestSalaryMax;


        $salaryMin = MasterSalaryRange::orderBy('master_salary_range_min')
                                ->pluck('master_salary_range_min');

        $salaryMax = MasterSalaryRange::orderBy('master_salary_range_max')
                                ->pluck('master_salary_range_max');
       
        $skill_names = ApplicantsSkills::select('applicant_skill_name')
                                ->distinct()
                                ->get();
        
        /**FILTERS */
        $scoutApplicants = Scout::getApplicantsToFavorite();
        
        if($age != null)
        {
            $ageRange = explode('-',$age);
            $scoutApplicants = $scoutApplicants->agedBetween($ageRange[0], $ageRange[1]);
        }

        if($gender != null) $scoutApplicants = $scoutApplicants->gender($gender);
        if($selectedSkillName != '') $scoutApplicants = $scoutApplicants->skill($selectedSkillName);
        $scoutApplicants = $scoutApplicants->expectedSalaryMin($selectedSalaryMin);
        $scoutApplicants = $scoutApplicants->expectedSalaryMax($selectedSalaryMax);

        return view('company.scout-search')
                        ->with('salaryMin', $salaryMin)
                        ->with('salaryMax', $salaryMax)
                        ->with('gender', $gender)
                        ->with('age', $age)
                        ->with('selectedSalaryMin', $selectedSalaryMin)
                        ->with('selectedSalaryMax', $selectedSalaryMax)
                        ->with('skill_names', $skill_names)
                        ->with('selectedSkillName', $selectedSkillName)
                        ->with('scoutApplicants', $scoutApplicants->paginate(10));
    }
    
    /**
     * sendEmailFavoriteUser
     * Render Preview Email or send
     * @author Karen Cano
     * @return 
     */
    public function sendEmailFavoriteUser($scout_applicant_id)
    {
        $applicantFavorited = ApplicantsProfile::find($scout_applicant_id);
        /*check if there is a preffered email address, if none get the user email address*/

        $email = (empty($applicantFavorited->applicant_profile_preferred_email)
                || is_null($applicantFavorited->applicant_profile_preferred_email)) 
                ? $applicantFavorited->user_applicant->email 
                : $applicantFavorited->applicant_profile_preferred_email;
        
        $mailTemplate = CompanyMailTemplate::where('company_mail_company_id',Auth::user()["user_company_id"])
                                        ->first();

        /**Uncomment below to preview view to email to applicant */            
        // return new ScoutFavorited(
        //                     $applicantFavorited,
        //                     $mailTemplate);

        /**Uncomment below to send email.Note it would be sent to leapwork dev team */            
        // Mail::to($email)
        //             ->cc(config('constants.emailLeapWorkDevTeam'))
        //             ->send(new ScoutFavorited(
        //                             $applicantFavorited,
        //                             $mailTemplate
        //                         ));
        
        $scouted = Scout::where('scout_applicant_id',$scout_applicant_id)
                          ->where('scout_company_id', Auth::user()["user_company_id"])
                          ->first();
        $scouted->scout_status = config('constants.scoutStatus.5');//UNDER REVIEW status
        $scouted->scout_date_scouted = Carbon::now();
        $scouted->scout_applicant_id = $scout_applicant_id;
        $scouted->scout_company_id = Auth::user()["user_company_id"];
        $scouted->save();

        return Redirect()->back();
    }
	
	/**
     * scoutFavoriteCompany
     * Render Company scout-favorite-user
     * @author Karen Cano
     * @return View
     */
    public function scoutFavoriteCompany()
    {
        $jobPostFavorited = JobPosting::jobPostsFavorites()->get();
        return view('company.scout-favorite-company')
                ->with('jobPostFavorited', $jobPostFavorited);
    }
    
    
	
	/**
     * done
     * Render Company scout-done
     * @author Karen Cano
     * @return View
     */
    public function done()
    {
        $scouted = Scout::join('applicants_profile', 'scout_applicant_id', '=', 'applicants_profile.applicant_profile_id')
                        ->join('users', 'applicants_profile.applicant_profile_user_id', '=', 'users.id')
                        ->where('applicants_profile.applicant_profile_user_id','!=', null)
                        ->where('users.user_status', '!=', 'INACTIVE')
                        ->where('scout_company_id', Auth::user()["user_company_id"]) 
                        ->get();
        // $scouted = Scout::where('scout_company_id', Auth::user()["user_company_id"])  //Scout::where('scout_company_id', Auth::user()["user_company_id"])->get();
                    //  ->where('scout_company_id', Auth::user()["user_company_id"])
                    //  ->join('scout_threads', 'scouts.scout_id', '=', 'scout_threads.scout_thread_scout_id')
                    //  ->where('scout_status', '<>', 'FAVORITE')
                    //  ->get();

        return view('company.scout-done')
                ->with('scouted',$scouted);
    }
    
    /**
     * chooseTemplateToEmail
     * Load the scout-choose-template view
     * @author Karen Cano
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-19 Louie: Add option for excluding already applied job
     * @since  2017-12-27 Louie: Add opton to tag notification as read without
     *         notification id, use current id and search notification.
     *
     * @return View company/scout-choose-template.blade.php
     *           or company/scout-new.blade.php 
     */
    public function chooseTemplateToEmail($applicantId,$notificationId=null)
    {
        $jobPost      = array();
        $viewSettings = array(); //view formats
        $company      = Company::find(Auth::user()["user_company_id"]);
        $applicant    = ApplicantsProfile::find($applicantId);
        $nextView     = "";
        //why not just load the scout id?
        $scout = Scout::where('scout_applicant_id',$applicantId)
                      ->where('scout_company_id',Auth::user()["user_company_id"])
                      ->first();

        $scoutThreads = ScoutThread::getThreads(
                            array(
                                 'scout_id' =>  $scout->scout_id
                                 )
                      );

        // 2017-12-19 Louie: Add option for excluding already applied job. If possible move to model
        /**Select list for Job Post that is active*/               
        // $activeJobPosts = JobPosting::where('job_post_company_id', Auth::user()["user_company_id"])
        //                             ->where('job_post_status', 1)
        //                             ->orderby('job_post_created','desc')
        //                             ->pluck('job_post_title','job_post_id');

        $activeJobPosts = JobPosting::leftJoin('job_applications', 'job_applications.job_application_job_post_id', '=', 'job_posts.job_post_id')
                                    ->where('job_post_company_id', Auth::user()["user_company_id"])
                                    ->where('job_posts_status', 'ACTIVE')
                                    ->where('job_post_visibility','<>','PRIVATE')
                                    ->whereNotIn('job_post_id', function($query) use ($applicantId){
                                        $query->select('job_application_job_post_id')
                                              ->from('job_applications')
                                              ->where('job_application_applicant_id', '=', $applicantId);
                                    })//sub query
                                    ->orderby('job_post_created','desc')
                                    ->pluck('job_post_title','job_post_id');
        //end

        $applicant['userName'] = $applicant->userProperties($applicant->applicant_profile_user_id)['userName'];
        $applicant['email']    = $applicant->user_applicant->email;

        $viewSettings['new_scout'] = (empty($scout->scout_job_post_id));

        //get last sent for each thread to set as part of header accordion tab
        for($i=0; $i<count($scoutThreads); $i++)
        {
            $scoutThreads[$i]['lastMessage'] = ScoutThread::getLastSent($scoutThreads[$i]);
        }
       
        $scout['status_css']     = $scout->scoutCSS($scout->scout_status)['label'];
        $scout['default_status'] = ($scout->scout_status != 'FAVORITE')
                                 ? $scout->scout_status
                                 : 'UNDER REVIEW'; //default status for initial mails

        /**Update Notification as read*/              
        if($notificationId!=null)
        {
            Notification::markAsRead($notificationId);
        }
        else // 2017-12-27 Louie: mark as read even if no notification id
        {
            $userId = Auth::user()['id'];
            if($scoutThreads->last() != null)// 2017-12-28 Karen : Allow create new scout
            {
                Notification::markAsReadIfExists($userId, 'SCOUT', $scoutThreads->last()->scout_thread_id);
            }
        }

        //build job post if existing
        if(!$viewSettings['new_scout'])
        {
            $jobPost['job_application_id']     =  $scout->job_application_id;
            $jobPost['job_application_status'] =  $scout->job_application_status;

            $jobPost['job_post_id']            =  $scout->job_post->job_post_id;
            $jobPost['job_post_title']         =  $scout->job_post->job_post_title;
            $jobPost['job_post_company_name']  =  $scout->scout_company_profile->company_name;
            $jobPost['job_post_location']      =  $scout->job_post->location->master_job_location_name;
           
            $jobPost['job_post_salary'] = ($scout->job_post->job_post_salary_min != $scout->job_post->job_post_salary_max)
                                                   ? 'PHP ' . number_format($scout->job_post->job_post_salary_min) . ' - PHP ' .  number_format($scout->job_post->job_post_salary_max)
                                                   : 'PHP ' . number_format($scout->job_post->job_post_salary_max);

            $defaultImage = url('images/applicant/img-side-icon01.png');

            $viewSettings['userImgSrc']    = (!empty($applicant->applicant_profile_imagepath)) 
                                           ? url( config('constants.storageDirectory.applicantComplete')  .
                                             'profile_' .
                                             $applicant->applicant_profile_id . 
                                             '/' .
                                             $applicant->applicant_profile_imagepath )  
                                            : $defaultImage;
            $viewSettings['companyImgSrc'] = (!empty($company->company_logo)) 
                                           ? url( config('constants.storageDirectory.companyComplete') .
                                             'company_' .
                                             $company->company_id .
                                             '/' .
                                             $company->company_logo )
                                           : $defaultImage;

            $nextView = 'company.scout-choose-template'; //existing already
        }
        else
        {
            $nextView = 'company.scout-new';
        }

        return view($nextView)->with('company', $company)
                                                    ->with('applicant', $applicant)
                                                    ->with('scout', $scout)
                                                    ->with('scoutThreads',$scoutThreads)
                                                    ->with('activeJobPosts', $activeJobPosts)
                                                    ->with('viewSettings', $viewSettings)
                                                    ->with('jobPost', $jobPost);
    }

    /**
     * POST:   scout/favorite-user/choosTemplate/replyCoversation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View company/scout-choose-template.blade.php
     */
    public function replyEmailConversation(Request $request)
    {
        $userId  = Auth::user()["id"];
        $scoutId     = $request->input('scout_id');
        $threadId    = $request->input('scout_thread_id');
        $scoutStatus = config('constants.scoutStatus')[$request->input('scout_status')];
        $message     = $request->input('scout_conversation_message');

        //update scout status if necessary
        if($scoutStatus)
        {
            Scout::updateStatus(
                       array(
                             'scout_id'     => $scoutId,
                             'scout_status' => $scoutStatus
                            )
                 );
        }

        $scout_conversation = array();
        $scout_conversation['scout_conversation_message']    = $message;
        $scout_conversation['scout_conversation_date_added'] = Carbon::now();
        $scout_conversation['scout_conversation_user_id']    = $userId;
        $scout_conversation['scout_conversation_thread_id']  = $threadId;

        $result = ScoutConversation::saveScoutConversation($scout_conversation);

        //get the user id of the receiving user
        $receipientId = ScoutConversation::where('scout_conversation_thread_id', $threadId)->where('scout_conversation_user_id', '!=', $userId)->first()->scout_conversation_user_id;

        if($result) {

            $name = Auth::user()->company_profile->company_contact_person ;
            $type = 'company-scout';

            $data = [
                'threadId'          => $threadId,
                'id'                => $scoutId,
                'name'              => $name,
                'message'           => $message,
                'receipientId'      => $receipientId,
                'type'              => $type,
                
            ];

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }

        // $message = ($result)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }

     /**
     * chooseTemplateToEmail
     * AJAX to load template
     * @author Karen Cano
     * @return scout-choose-template View
     */
    public function loadTemplateEmail($companyId, $applicantId,$mailStatus, $jobPostId=0)
    {
        $statusChosen = config('constants.mailStatus')[$mailStatus];

        $bodyTemplate = CompanyMailTemplate::where('company_mail_company_id',$companyId)
                                        ->where('company_mail_type', $statusChosen)
                                        ->first();

        $applicantProfile = ApplicantsProfile::find($applicantId);

        $jobPost = JobPosting::find($jobPostId);
        /* pattern to be replaced */
        $bodyPatterns = array();
        $bodyPatterns[0] = '/{{applicantName}}/';
        $bodyPatterns[1] = '/{{companyName}}/';
        $bodyPatterns[2] = '/{{companyAdmin}}/';
        $bodyPatterns[3] = '/{{companyAdminEmail}}/';
        $bodyPatterns[4] = '/{{companyAdminAddress}}/';
        $bodyPatterns[5] = '/{{jobPositionName}}/';
        $bodyPatterns[6] = '/{{jobPositionLink}}/';
        $bodyPatterns[7] = '/{{leapworkLogo}}/';

        /* value to be injected on that variable */
        $replaceCompanyPatterns = array();
        $replaceCompanyPatterns[0] = $applicantProfile->applicant_profile_firstname
                                    .' '.$applicantProfile->applicant_profile_middlename
                                    .' '.$applicantProfile->applicant_profile_lastname;

        $company = Company::find(Auth::user()["user_company_id"])->first();

        $replaceCompanyPatterns[1] = $company->company_name;
        $replaceCompanyPatterns[2] = $company->company_contact_person;
        $replaceCompanyPatterns[3] = $company->company_email;
        $replaceCompanyPatterns[4] = $company->company_postalcode
                                    ."<br>".$company->company_address1
                                    ."<br>".$company->company_address2;

        /**If Job Post is found */
        if($jobPost && $jobPost->job_post_status == 1)
        {
            $replaceCompanyPatterns[5] = $jobPost->job_post_title;
            $replaceCompanyPatterns[6] = config('constants.app.url').'/job/details/'.$jobPost->job_post_id;
        }
        else{
            $replaceCompanyPatterns[5] = "{{activeJobPositionName}}";
            $replaceCompanyPatterns[6] = "{{activeJobPositionLink}}";
        }

        $replaceCompanyPatterns[7] = '<img src="' . url('images/leapwork.PNG') .'" alt="">';

        $body = preg_replace($bodyPatterns, $replaceCompanyPatterns, $bodyTemplate->company_mail_body);

        return array(
                    'subject' => $bodyTemplate->company_mail_subject,
                    'body'    => $body
                );

    }

     /**
     * previewScoutEmail
     * Preview open to another page the email
     * @author Karen Cano
     * @return Email Message View
     */
    public function previewScoutEmail()
    {
        $body = $this->request->company_mail_body;
        $subject = $this->request->company_mail_subject;
        return new ScoutPreview(
                            $subject,
                            $body);
    }



     /**
     * pushEmailConversationValidator
     * Validate the fields and return old text input
     * On the blade have the name : {{ old('company_mail_subject') }}
     * blade used : scout-choose-template.blade.php
     * @author Karen Cano
     * @return Email Message View
     */
    public function pushEmailConversationValidator()
    {
        
        $validator =  $this->request->validate([
            'company_mail_subject' => 'required',
            'company_mail_body'    => 'required',
            'scout_status'         => 'required',
            'activeJobPosts'       => 'required'//ensure that there is an active position selected
        ]);

        if (!($validator)) {
			return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * pushEmailConversation
     * Send and save to db Scout Conversation
     * Updates scouts.scout_status
     * Creates new entry scout_conversation
     * Send email (uncomment for sending)
     * @author Karen Cano
     * @return Email Message View
     */
    public function pushEmailConversation(Request $request)
    {
        // 11/29/2017 Louie: Remove first, fields have changed.
        // $this->pushEmailConversationValidator($this->request);

        $dateNow = Carbon::now();
        $applicant = ApplicantsProfile::findOrFail($request->applicant_profile_id);
        
        $newScoutConvo = Scout::createScout($request,$dateNow,$applicant);
        $scout = new Scout();
        $data = $scout->buildNotification($newScoutConvo);
        event(new NotifyEvent($data)); 
        
        $email = (empty($applicant->applicant_profile_preferred_email)
                || is_null($applicant->applicant_profile_preferred_email)) 
                ? $applicant->user_applicant->email 
                : $applicant->applicant_profile_preferred_email;
        
        /**Uncomment below to send email.Note it would be sent to leapwork dev team */            
        // Mail::to($email)
        //             ->cc(config('constants.emailLeapWorkDevTeam'))
        //             ->send(new ScoutPreview(
        //                             $this->request->company_mail_subject,
        //                             $this->request->company_mail_body
        //                         ));

        return Redirect('/company/scout/done');
    }

}
