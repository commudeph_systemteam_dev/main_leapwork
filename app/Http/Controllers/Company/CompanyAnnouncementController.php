<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Company;
use App\Models\AdminCompanyThread;
use App\Models\AdminCompanyConversation;

use App\Models\Announcements;
use App\Models\AnnouncementSeen;
use App\Models\Notification;

use App\Helpers\Utility\DateHelper;
use Carbon\Carbon;
use Auth;
use Lang;

use App\Traits\NotificationFunctions;

/**
 * CompanyAnnouncementsController- 
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-18
 *
 */
class CompanyAnnouncementController extends Controller
{
    use NotificationFunctions;

	/**
     * GET: Render announcements view
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/notice/index.blade.php
     */
    public function index()
    {  
        $userId = Auth::user()["id"];
        $companyUserType = Auth::user()["user_accounttype"];
        $companyId = User::where('id', $userId)->first()->user_company_id;

        $adminCompanyMessage = AdminCompanyThread::where('admin_company_thread_company_id', $companyId)->get();
        
        if($companyUserType != "CORPORATE ADMIN")
        {
            return view('company.notice.index')->with('adminCompanyMessage', $adminCompanyMessage)
                                               ->with('companyUserType', $companyUserType);
        }
        $companyId = User::where('id', $userId)->first()->user_company_id;

        return view('company.notice.index')->with('adminCompanyMessage', $adminCompanyMessage)
                                           ->with('companyUserType', $companyUserType);
    }

    /**
     * adminAnnouncements
     * Render admin-announcements blade
     *
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return View admin/notice/admin-announcements.blade.php
     */
    public function adminAnnouncements(Request $request)
    {

        $announcements = Announcements::getAll(
                                    array(
                                            'announcement_status' => 'ACTIVE',
                                            'announcement_target' => 'COMPANY'
                                        )
                                )
                                ->get();
        $data = array();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $collection = new Collection($announcements);
        $per_page = 5;
        $currPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();
        $data['announcements'] = new LengthAwarePaginator($currPageResults, count($collection), $per_page);
        $data['announcements']->setPath($request->url());                               
        
        return view('company.notice.admin-announcements')
                ->with('data', $data);
    }

    public function viewAnnouncements($announcementId)
    {
        AnnouncementSeen::updateOrCreate(['announcement_id' => $announcementId, 'user_id' => Auth::user()->id], ['seen' => '1']);

        $announcement = Announcements::find($announcementId);
        return view('company.notice.view-announcements')->with('announcement', $announcement);
    }

    /**
     * GET: Render announcement view. Sets read flag to 1
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return View admin/notice/details.blade.php
     */
    public function details(Request $request)
    {
        $announcementId = $request->input('id');
        $announcement   = Announcements::getAnnouncement($announcementId);

        /**Karen(11-10-17):  Commenting code below because announcement_read_flag is no longer there */
        // $announcement->announcement_read_flag = 1; //read
        // Announcements::updateReadFlag($announcement);

        $announcement   = Announcements::getAnnouncement(
                                                         array('announcement_id' => $announcementId)
                                                        );
        
        // 11/09/2017 Louie: REMOVED
        // Announcements::updateReadFlag($announcementId);

        return view('company.notice.details')->with('announcement', $announcement);
    }

    /**
     * markAsRead
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return void updates notifications.read_at
     */
    public function markAsRead($notificationId)
    {
        Notification::markAsRead($notificationId);
        return back();
    }

    /**
     * updateBellInstance
     * Update the notification_read_at via AJAX
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return void 
     */
    public function updateIconInstance($iconType)
    {
        Notification::where('notification_type',$iconType)
                    ->where('notification_type_id', Auth::user()["id"])
                    // ->orderBy('notification_created_at', 'DESC')
                    ->update([
                        'notification_read_at' => Carbon::now()
                    ]);
    }


    /**
     * GET: Render Admin Company conversation view for Corporate User
     * 
     * @author    Arvin Alipio <aj_alipio@commude.ph>
     *            Referenced: CompanyApplicantController.php 
     *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $company_id
     * 
     * @return View 
     */
    public function messagesView($company_id)
    {

        $userId = Auth::user()["id"];

        $admin  = User::where('user_accounttype', 'ADMIN')->first();
        // $companyId = Auth::user()["user_company_id"];
        // $messageThread = AdminCompanyThread::find($thread_id);
        $corporateUser = Company::find($company_id);

        //load messages
        $inboxMessages = AdminCompanyThread::getThreads(
                            array(
                                    'user_id'   => $admin->id,
                                    'company_id'=> $company_id,
                                    'type'      => 'sent'
                                 )
                            );



        $sentMessages = AdminCompanyThread::getThreads(
                            array(
                                    'user_id'   => $admin->id,
                                    'company_id'=> $company_id,
                                    'type'      => 'inbox'
                                 )
                            );



        for ($i=0; $i < count($inboxMessages); $i++) { 
            $inboxMessages[$i]['lastMessage'] = AdminCompanyThread::getLastSent($inboxMessages[$i]);
        }

        for ($i=0; $i < count($sentMessages); $i++) { 
            $sentMessages[$i]['lastMessage'] = AdminCompanyThread::getLastSent($sentMessages[$i]);
        }

        $defaultImage = url('/storage/uploads/master-admin/leapwork_logo.png');


        $companyImgSrc= (!empty(Auth::user()->applicant_profile->applicant_profile_imagepath)) 
                       ? url( config('constants.storageDirectory.applicantComplete')  .
                         'profile_' .
                         Auth::user()->applicant_profile->applicant_profile_id . 
                         '/' .
                         Auth::user()->applicant_profile->applicant_profile_imagepath )  
                       : $defaultImage;


        $adminImgSrc= (!empty($corporateUser->company_logo)) 
                                           ? url( config('constants.storageDirectory.companyComplete') .
                                             'company_' .
                                             $corporateUser->company_id .
                                             '/' .
                                             $corporateUser->company_logo )
                                           : ''; // <=== default image for company will go here

        $viewSettings['composeEnabled'] = (count($inboxMessages) < 1);
        
        Notification::markAsReadIfExists($userId, 'ADMIN CONTACT');
                $viewSettings['composeEnabled'] = (count($inboxMessages) < 1);

        $completeUserName      = [];
        $completeUserNameArray = [];
        $companyLogo           = [];
        
        foreach($inboxMessages as $inboxMessage){
            foreach ($inboxMessage->admin_company_conversations as $adminCompanyConversation) {
                
                $completeName =  $adminCompanyConversation->admin_company_conversation_user_id == Auth::user()->id
                        ?  Auth::user()->company_profile->company_contact_person
                        : "Admin";

                // $completeName = $data == "Admin" ? "Admin" : Auth::user()->company_profile->company_contact_person;
                array_push($completeUserName, $completeName);
            }
            array_push($completeUserNameArray, $completeUserName);
        }

        return view('company.notice.message-details', compact("completeUserNameArray"))->with('userId', $userId)
                                                    ->with('companyId', $company_id)
                                                    ->with('adminEmail',  $admin->email)
                                                    ->with('corporateUser', $corporateUser)
                                                    ->with('inboxMessages', $inboxMessages)
                                                    ->with('sentMessages', $sentMessages)
                                                    ->with('adminImgSrc', $adminImgSrc)
                                                    ->with('companyImgSrc', $companyImgSrc)
                                                    ->with('viewSettings',  $viewSettings);
    }

    /**
     * POST:  /notice/details/sendCompanyEmailConversation
     * 
     * send mail to Admin 
     * 
     * @author    Arvin Alipio <aj_alipio@commude.ph>
     *            Referenced: CompanyApplicantController.php 
     *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View /company/notice/details/{companyId}
     */
    public function sendCompanyEmailConversation(Request $request)
    {
        $userId = Auth::user()["id"];
        $title   = $request->input('txt_title_new');
        $message = $request->input('txt_message_new');
        $messageRecipient = $request->input('company_id');
          //start service
        $admin_company_thread = array();
        $admin_company_thread['admin_company_thread_title'] =  $title;
        $admin_company_thread['admin_company_thread_datecreated']    = Carbon::now();
        $admin_company_thread['admin_company_thread_user_id']        = $userId;
        $admin_company_thread['admin_company_thread_company_id'] = $messageRecipient;

        $adminCompanyThreadId = AdminCompanyThread::saveAdminCompanyThread($admin_company_thread);

         //save as conversation
        $admin_company_conversation = array();
        $admin_company_conversation['admin_company_conversation_message']     = $message;
        $admin_company_conversation['admin_company_conversation_datecreated'] = Carbon::now();
        $admin_company_conversation['admin_company_conversation_user_id']     = $userId;
        $admin_company_conversation['admin_company_conversation_thread_id']   = $adminCompanyThreadId;

        $result = AdminCompanyConversation::saveAdminCompanyConversation($admin_company_conversation);

        $message = ($result)
                ? Lang::get('messages.message-sent')
                : Lang::get('messages.failed');


        return back()->with('message', $message);
    }

     /**
     * POST:  /notice/details/replyCompanyConversation
     * 
     * @author    Arvin Alipio <aj_alipio@commude.ph>
     *            Referenced: CompanyApplicantController.php 
     *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $request form values
     * 
     * @return View /company/notice/details/{companyId}
     */
    public function replyCompanyConversation(Request $request)
    {
        $userId  = Auth::user()["id"];
        $adminCompanyThreadId = $request->input('admin_company_thread_id');
        $adminCompanyMessage  = $request->input('admin_company_conversation_message');  

        $admin_company_conversation = array();
        $admin_company_conversation['admin_company_conversation_message']     = $adminCompanyMessage;
        $admin_company_conversation['admin_company_conversation_datecreated'] = Carbon::now();
        $admin_company_conversation['admin_company_conversation_user_id']     = $userId;
        $admin_company_conversation['admin_company_conversation_thread_id']   = $adminCompanyThreadId;

        $result = AdminCompanyConversation::saveAdminCompanyConversation($admin_company_conversation);
        
        $receipientId = AdminCompanyConversation::where('admin_company_conversation_thread_id', $adminCompanyThreadId)->where('admin_company_conversation_user_id', '!=', $userId)->first()->admin_company_conversation_user_id;

        //since company, get the name of the contact person
        $name = Auth::user()->company_profile->company_contact_person ;

        $type = 'company-admin-contact';
    
        if($result) {

            $data = [
                'threadId'          => $adminCompanyThreadId,
                'id'                => Auth::user()->user_company_id,
                'name'              => $name,
                'message'           => $adminCompanyMessage,
                'receipientId'      => $receipientId,
                'type'              => $type,
                
            ];

            if(isset($request->all()['userIcon'])) {
                $data['icon'] = $request->input('userIcon');
            }

            //TODO: make pusher event here
            self::sendMessage($data, $type);

        }

        // $message = ($result)
        //             ? Lang::get('messages.message-sent')
        //             : Lang::get('messages.failed');
        
        // return redirect()->back()->with('message', $message);

        return 'Save';
    }
    
}
