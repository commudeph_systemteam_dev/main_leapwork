<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\JobInquiry;
use App\Models\JobInquiryReply;
use App\Models\JobPosting;
use Auth;
use App\Models\Notification;

/**
 * Contoller of Company / Applicant - Inquiry
 * 
 * @author    Karen Cano       <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-25
 */
class CompanyApplicantInquiryController extends Controller
{
    public $request;
    
    function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * applicantQuestionaire
     * Render Company applicant-questionaire
     * @author Karen Cano
     * @return View
     */
    public function applicantQuestionaire()
    {
        $companyJobPosts = JobPosting::where('job_post_company_id', Auth::user()["user_company_id"])
                                    ->get();
                                       
        return view('company.applicant-questionaire')
                    ->with('companyJobPosts', $companyJobPosts);
    } 
    
     /**
     * hardDeleteInquiry
     * Deletes a row at job_inquiry
     * @author Karen Cano
     * @return View
     */
    public function hardDeleteInquiry($job_inquiry_id)
    {
        $hardDelete = JobInquiry::find($job_inquiry_id);
        $hardDelete->delete();

        return Redirect('/company/applicant/questionaire');
    }

    /**
     * applicantQuestionaireDetail
     * Render Company applicant-questioner-detail
     * @author Karen Cano
     * @return View
     */
    public function applicantQuestionaireDetail($job_inquiry_id, $notificationId=null)
    {
        $inquiry = JobInquiry::find($job_inquiry_id);
        /**Update Notification as read*/               
        if($notificationId!=null)
        {
            Notification::markAsRead($notificationId);
        }
        return view('company.applicant-questionaire-detail')
                    ->with('inquiry', $inquiry);
    } 
    
     /**
     * replyInquiry
     * Create a reply based from job inquiry respective to a user
     * @author Karen Cano
     * @return View
     */
    public function replyInquiry()
    {
        $dateNow = Carbon::now();
        $newReply = new JobInquiryReply;
        $newReply->job_inquiry_reply_details = $this->request->job_inquiry_reply_details;
        $newReply->job_inquiry_reply_inquiry_id = $this->request->job_inquiry_id;
        $newReply->job_inquiry_reply_user_id = Auth::user()["id"];
        $newReply->job_inquiry_reply_created = $dateNow;
        $newReply->save();
        return Redirect()->back();
    }
   
}
