<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Scout;
use App\Models\ApplicantsProfile;
use App\Models\MasterSalaryRange;
use App\Models\JobPosting;
use App\Models\ApplicantsSkills;
use App\Models\CompanyMailTemplate;
use App\Models\Company;
use App\Models\JobPostView;
use App\Models\ScoutConversation;
use App\Models\JobApplications;
use Auth;
use DB;
/**Email Includes */
use App\Mail\ScoutFavorited;
use App\Mail\ScoutPreview;
use Illuminate\Support\Facades\Mail;


/**
 * Contoller of Company / Analytics
 * 
 * @author    Karen Cano       <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-31
 */
class CompanyAnalyticsController extends Controller
{
    public $request;
    
    function __construct(Request $request) {
        $this->request = $request;
    }

	/**
     * analytics
     * Render Company analytics
     * @author Karen Cano
     * @return View
     */
    public function analytics()
    {
        $jobPosts = JobPosting::where('job_post_company_id', Auth::user()["user_company_id"])
                                ->orderby('job_post_created','desc');
  
        $jobPostTitles = $jobPosts->pluck('job_post_title','job_post_id')
                                  ->toArray();
        
        $jobCreatedMonths = $jobPosts->select([
                                          DB::raw('month(job_post_created) as Month'),
                                          DB::raw('DATE_FORMAT(job_post_created, "%M") as MonthName'),
                                        ])
                            ->distinct('job_post_created')
                            ->orderby('job_post_created','asc')
                            ->pluck('MonthName','Month')->toArray();

        $jobPostInitial = JobPosting::where('job_post_company_id', Auth::user()["user_company_id"])
                            ->orderby('job_post_created','desc')
                            ->first();
        return view('company.analytics')
                 ->with('jobPostTitles',$jobPostTitles)
                 ->with('jobCreatedMonths',$jobCreatedMonths)
                 ->with('jobPostInitial',$jobPostInitial);
    } 
    
    /**
     * analyticsOfJobPost
     * Render Company analytics on the Canvas line graph
     * @author Karen Cano
     * @return JSON object about the job post, views and applicants
     */
    public function analyticsOfJobPost($jobPostId, $numOfmonth = 0)
    {
        $dateNow = Carbon::now();
        $dateFrom= Carbon::now()->subMonths($numOfmonth);

        //get Job Post properties
        $jobPost = JobPosting::findOrFail($jobPostId);
        //query for job views
        $jobViewsPerMonth = JobPostView::where('job_post_view_job_post_id', $jobPostId)
                                        ->select([
                                            DB::raw('COUNT(*) as countPageViews'),
                                            DB::raw('month(job_post_view_date_created) as Month'),
                                            DB::raw('DATE_FORMAT(job_post_view_date_created, "%M") as MonthName'),
                                        ])
                                        ->groupby('MonthName')
                                        ->orderby('Month');
                                        // ->get();
        $jobViewsPerMonth = ($numOfmonth != 0)
                            ? $jobViewsPerMonth->whereBetween('job_post_view_date_created', [$dateFrom, $dateNow])->get()
                            : $jobViewsPerMonth->get();

        $months = array();//initialize months array
        foreach($jobViewsPerMonth as $jobView)
        {
            array_push($months,$jobView->MonthName);
        }

        //query for job applications. We can't simply join/left join job applications && job views
        $jobApplicants = JobApplications::where('job_application_job_post_id', $jobPostId)
                                        ->select([
                                            DB::raw('month(job_application_date_applied) as Month'),
                                            DB::raw('DATE_FORMAT(job_application_date_applied, "%M") as MonthName'),
                                        ])
                                        ->groupby('MonthName')
                                        ->orderby('Month');

        //save to the months array the months query from jobApplicants
        foreach($jobApplicants as $jobApplicant)
        {
            if(!(in_array($jobApplicant->MonthName, $months)))//check for month duplicates
            {
                array_push($months,$jobApplicant->MonthName);
            }
        }
        //save to array the job applicants per month
        $jobApplicantsPerMonth = array();
        foreach($months as $month)
        {
            $jobApplicantMonthCount = JobApplications::where('job_application_job_post_id', $jobPostId)
                                            ->select([
                                                DB::raw('COUNT(*) as countApplicants'),
                                                DB::raw('month(job_application_date_applied) as Month'),
                                                DB::raw('DATE_FORMAT(job_application_date_applied, "%M") as MonthName'),
                                            ])
                                            ->groupby('MonthName')
                                            ->orderby('Month')
                                            ->having('MonthName',$month)->first();
            //if there is no applicant for that month,assign 0;
            ($jobApplicantMonthCount)
                        ? array_push($jobApplicantsPerMonth,$jobApplicantMonthCount->countApplicants)
                        : array_push($jobApplicantsPerMonth,0);
            
        }

        $jobPostRanking = JobPosting::getRankingTotalVotes($jobPostId);
        return array(
                        "countJobPostViews"     => $jobPost->job_post_views->count(),
                        "jobViews"              => $jobViewsPerMonth,
                        "jobApplicants"         => $jobApplicantsPerMonth,
                        "jobPostDescription"    => $jobPost->job_post_description,
                        "jobPostTitle"          => $jobPost->job_post_title,
                        "jobPostTotalPV"        => JobPostView::getTotalPageViews($jobPost->job_post_id,'job_post_id'),
                        "jobPostApplicants"     => $jobPost->job_post_applications->count(),
                        "jobPostTotalVotes"     => $jobPost->job_post_vote,
                        "jobPostRanking"        => $jobPostRanking,
                        "months"                => $months,
                        "dateNow"               => $dateNow,
                        "dateFrom"              => $dateFrom
                    );
    }//endof analyticsOfJobPost

}
