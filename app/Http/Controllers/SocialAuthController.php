<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Auth;
use App\Models\User;

class SocialAuthController extends Controller
{
     /**
    * callback
    * 
    * @author    Alvin Generalo
    *            
    * @copyright 2017 Commude
    * @since     2017-10-10
    */
    public function callback() {

    	$user = Socialite::driver('facebook')->stateless()->user();
        $authUser = $this->findOrCreateUser($user, 'facebook');
        Auth::login($authUser, true);

        switch (Auth::user()->user_accounttype) {
           case 'USER':
               return redirect('/front/top');
               break;
            case 'ADMIN':
               return redirect('/admin');
               break;
            case 'CORPORATE':
               return redirect('/company');
               break;
           
           default:
               # code...
               break;
       }
    }

    /**
    * redirect
    * 
    * @author    Alvin Generalo
    *            
    * @copyright 2017 Commude
    * @since     2017-10-10
    */
    public function findOrCreateUser($user, $provider)
    {
        $name = explode(" ", $user->name);
        $firstName = $name[0];
        $lastName = $name[1];

        $authUser = User::where('email', $user->email)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'user_firstname' => $firstName,
            'user_lastname' => $lastName,
            'email'    => $user->email
        ]);
    }

}
