<?php

namespace App\Http\Services;

use App\Models\CompanyInquiryThread;
use App\Models\CompanyInquiryConversation;
use App\Models\User;
use App\Traits\NotificationFunctions;

use \Datetime;
use Carbon\Carbon;

use DB;
use Log;

/**
 * Manage company_inquiry threading
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-19
 *
 */
class CompanyInquiryService{

    use NotificationFunctions;

	/**
     * Check if job post is already under job_applications or scouts
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $companyInquiryThread
     * @param  array $companyInquiryConversation
     *
     * @return type of process if exist (applied or scouted)
     */
	public static function storeInquiry(array $companyInquiryThread, array $companyInquiryConversation)
    {
        $success = false;

        DB::beginTransaction();

        try 
        {   
            $companyInquiryThreadId = CompanyInquiryThread::saveCompanyInquiryThread($companyInquiryThread);

            $companyInquiryConversation['company_inquiry_conversation_thread_id'] = $companyInquiryThreadId;

            CompanyInquiryConversation::saveCompanyInquiryConversation($companyInquiryConversation);
            
            DB::commit();
            $success = true;
            
            $receipientId = User::where('email', 'admin@leapwork.com')->first()->id;
        
            if($success) {
                $type = 'company-admin';
                
                $data = [
                    'threadId'          => $companyInquiryThreadId,
                    'id'                => '',
                    'name'              => $companyInquiryThread['company_inquiry_thread_contact_person'] ,
                    'message'           => $companyInquiryConversation['company_inquiry_conversation_message'],
                    'receipientId'      => $receipientId,
                    'type'              => $type,
                    
                ];

                // if(isset($request->all()['userIcon'])) {
                //     $data['icon'] = $request->input('userIcon');
                // }

                //TODO: make pusher event here
                self::sendMessage($data, $type);

            }
        }
        catch (\Exception $e) 
        {
            dd($e);
            $success = false;
            DB::rollback();
            Log::error($e);
        }

        return $success;
	}


}