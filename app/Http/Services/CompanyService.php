<?php

namespace App\Http\Services;

use App\Models\CompanyPlanHistory;
use App\Models\JobPosting;
use App\Models\MasterPlans;
use App\Models\Claim;
use App\Models\Company;
use App\Models\CompanyIndustry;
use App\Models\CompanyIntro;
use App\Models\ApplicantsProfile;
use App\Models\MasterEmployeeRange;
use App\Models\MasterJobIndustries;
use App\Models\MasterMailTemplate;
use App\Models\User;

use App\Helpers\Utility\DateHelper;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterCompany;

use \Datetime;
use Carbon\Carbon;

use DB;
use Log;
use Storage;

/**
 * Manage company profile related transactions
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-05
 *
 */
class CompanyService{

	/**
     * Create/update service for company
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since   2017-12-05
     *
     * @param  array $company data
     *
     * @return true if successful
     */
	public static function saveCompany($companyData)
    {        
        $user    = $companyData['user'];
        $company = $companyData['company'];
        $companyFiles = $companyData['company_files'];
        $companyIndustries = $companyData['company_industries'];
        $companyIntro      = $companyData['company_intro'];
        $companyIntroFile  = $companyData['company_intro_file'];

        $userId    = $user['id'];
        $companyId =  $company['company_id'];
        $companyDirectory =   $directory =  config('constants.companyDirectory') . 'company_' . $companyId; //storage directory

        //remove if flagged or update if not empty
        if( !empty($companyFiles['logoRemoveFlag']) )
            $company['company_logo']   = '';
        elseif( !empty($companyFiles['logoFile']) )
            $company['company_logo']   = basename($companyFiles['logoFile']->store($directory, 'storage-upload'));

         //delete previous file
        $companyLogoImagePath = Company::getLogo($companyId);

        if(!empty($companyLogoImagePath) &&
                 (!empty($companyFiles['logoFile']) ||
                  !empty($companyFiles['logoRemoveFlag']) ))
        {
            $previousImagePath = $companyDirectory . '/' . $companyLogoImagePath;
            
            Storage::disk('storage-upload')->delete($previousImagePath);
        }

        //remove if flagged or update if not empty
        if( !empty($companyFiles['bannerRemoveFlag']) )
            $company['company_banner'] = '';
        elseif( !empty($companyFiles['bannerFile']) )
            $company['company_banner'] = basename($companyFiles['bannerFile']->store($companyDirectory, 'storage-upload'));

        //delete previous file
        $companyBannerImagePath = Company::getBanner($companyId);
        
        if(!empty($companyBannerImagePath) &&
                 (!empty($companyFiles['bannerFile']) ||
                  !empty($companyFiles['bannerRemoveFlag']) ))
        {
            $previousImagePath = $companyDirectory . '/' . $companyBannerImagePath;
            Storage::disk('storage-upload')->delete($previousImagePath);
        }

        //business permits
        for($i=0; $i<count($companyFiles['businessFiles']['file']); $i++)
        {
            $fileName = 'company_file' . ($i+1);

            if(!empty($companyFiles['businessFiles']['removeFlag'][$i]) )
            {
                $company[$fileName] = '';
            }                
            if(!empty($companyFiles['businessFiles']['file'][$i]) )
            {
                $fileNameOriginal = $companyFiles['businessFiles']['file'][$i]->getClientOriginalName();
                $company[$fileName] = basename($companyFiles['businessFiles']['file'][$i]->storeAs($companyDirectory, $fileNameOriginal, 'storage-upload'));
            }
        }

        //business permits remove flagged items first
        for($i=1; $i<=count($companyFiles['businessFiles']['removeFlag']); $i++)
        {
            $fileName = $companyFiles['businessFiles']['removeFlag'][$i];

            $company[$fileName] = '';

            $companyFilePath = Company::getFilePath(
                                            array(
                                                'company_id'=> $companyId,
                                                'company_file' => $fileName
                                                 )
                                        );            
            if(!empty($companyFilePath))
            {
                $previousImagePath = $companyDirectory . '/' . $companyFilePath;
                Storage::disk('storage-upload')->delete($previousImagePath);
            }
        }

        //add business permits
        for($i=0; $i<count($companyFiles['businessFiles']['file']); $i++)
        {
            $fileName = 'company_file' . ($i+1);
   
            if(!empty($companyFiles['businessFiles']['file'][$i]) )
            {
                $fileNameOriginal = $companyFiles['businessFiles']['file'][$i]->getClientOriginalName();
                $company[$fileName] = basename($companyFiles['businessFiles']['file'][$i]->storeAs($companyDirectory, $fileNameOriginal, 'storage-upload'));
            }
        }

        // save company_industry - static 3 as per design
        // delete all - no need to track
        CompanyIndustry::deleteAll($companyId);

        foreach($companyIndustries as $companyIndustry)
        {
            // $companyIndustry["company_industry_company_id"] = "99";
            CompanyIndustry::updateOrCreate($companyIndustry);
        }

         //remove if flagged or update if not empty
        if( !empty($companyIntroFile['imageRemoveFlag']) )
            $companyIntro['company_intro_image'] = '';
        elseif( !empty($companyIntroFile['imageFile']) )
            $companyIntro['company_intro_image'] = basename($companyIntroFile['imageFile']->store($companyDirectory, 'storage-upload'));

        $companyIntoPrImagePath = CompanyIntro::getImage($companyId);
        //delete for updates
        if(!empty($companyIntoPrImagePath))
        {
            $previousImagePath = $companyDirectory . '/' . $companyIntoPrImagePath;
            
            Storage::disk('storage-upload')->delete($previousImagePath);
        }

        Company::saveProfile($company);       
        CompanyIntro::saveCompanyIntro($companyIntro); 
	}

    /**
     * Create service for company
     * @todo optimize the saveProfile
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since   2017-12-15
     *
     * @param  array $company data
     *
     * @return true if successful
     */
    public static function createCompany($companyData)
    {
        $sucess = false;
        
        DB::beginTransaction();
        try 
        {   
            // load masterplan to set in claims and plan history
            $masterPlan  = MasterPlans::find($companyData['plan_type']);
            
            //update company current play (temp)
            $companyData['company']['company_current_plan'] = $masterPlan->master_plan_name;

            $company   = $companyData['company'];
            $user      = $companyData['user'];

            $companyId = Company::saveProfile($company);         

            $companyData['company']['company_id'] = $companyId;

            //bind new company id to child tables
            //build company_industry data
            for($i=1; $i<=count($companyData['company_industries']); $i++)
            {
                $companyData['company_industries'][$i]['company_industry_company_id'] = $companyId;
            }

            // if(!empty(array_filter($companyData['company_intro'])))
            $companyData['company_intro']['company_intro_company_id']  = $companyId;
            $user['user_company_id'] = $companyId;

            //create nnew user company admin
            User::saveUser($user);            
            CompanyService::saveCompany($companyData);

            // Claim transactions (can be dettached from here)
            ////////////////////////////
            // $isPlanTypePayable = MasterPlans::isPlanTypePayable($request->input('radio-group-plan'));
            // $claim = null;
            
            // // Create claim only for payable
            // if($isPlanTypePayable)
            // {
            //     $claim = Claim::createClaim($company,$request);
            // }


            // $company_history_plan = CompanyPlanHistory::createCompanyPlan($company,$request, $claim);
            ////////////////
            if(!is_null($companyData['payment_method'])){
                $claim       = CompanyService::createClaim($companyId, $companyData['payment_method'], $masterPlan);
                Claim::saveclaim($claim);

                $companyPlan = CompanyService::createCompanyPlanHistory($companyId, $masterPlan->master_plan_name);
                CompanyPlanHistory::saveCompanyPlanHistory($companyPlan);
    
            }else{
                $type = $companyData["company"]["company_current_plan"];
                $comp_id =  $companyData["company"]["company_id"];
                
                $dateNow = Carbon::now();
                $company_plan_history = new CompanyPlanHistory;
                $company_plan_history->company_plan_type = $type;
                $company_plan_history->company_plan_status = "ACTIVE";
                $company_plan_history->company_plan_datestarted = $dateNow;
                $company_plan_history->company_plan_company_id = $comp_id;
                $company_plan_history->company_plan_history_claim_id = NULL;
                              
                $company_plan_history->save();
            }
          
            DB::commit();

            $success = true;
        }
        catch (\Exception $e) 
        {
            dd($e);
            $success = false;
            DB::rollback();
            Log::error($e);
        }
        return $success;
    }

     /**
     * Create service for company
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since   2017-12-15
     *
     * @param  array $company data
     *
     * @return true if successful
     */
    public static function editCompany($companyData)
    {
        $sucess = false;
        
        DB::beginTransaction();
        try 
        {
            $company = $companyData['company'];
            $user    = $companyData['user'];
            // Company::saveProfile($company);

            CompanyService::saveCompany($companyData);

            //update email and password 
            User::saveUser($user);           
             
            DB::commit();
            $success = true;
        }
        catch (\Exception $e) 
        {
            dd($e);
            $success = false;
            DB::rollback();
            Log::error($e);
        }
        return $success;
    }

     /**
     * Claim data model builder
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * Added claim name and claim amount to reflect on the database.
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since   2017-12-19
     *
     * @param  int $companyId
     * @param  string $paymentMethod
     * @param  int $planTypeId
     *
     * @return true if successful
     */
    private static function createClaim($companyId, $paymentMethod, $masterPlan)
    {
        $claim = array();
        $now   = date("Y-m-d H:i:s");

        $claim['claim_name']           = $masterPlan->master_plan_name;
        $claim['claim_amount']         = $masterPlan->master_plan_price;
        $claim['claim_invoice_status'] = 'NOT ISSUED'; 
        $claim['claim_payment_method'] = $paymentMethod;
        $claim['claim_payment_status'] = 'UNPAID';
        $claim['claim_master_plan_id'] = $masterPlan->master_plan_id;
        $claim['claim_datecreated']    = Carbon::parse($now);
        $claim['claim_start_date']     = Carbon::parse($now);
        $claim['claim_end_date']       = Carbon::now()->addDays($masterPlan->master_plan_expiry_days);
        $claim['claim_status']         = 'OPENED';
        $claim['claim_company_id']     = $companyId;
        $claim['claim_token']          = $companyId.uniqid();
    
        return $claim;
    }

    /**
     * CompanyPlanHistory model data builder
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @since   2017-12-19
     *
     * @param  int $companyId
     * @param  string $planTyp
     *
     * @return true if successful
     */
    private static function createCompanyPlanHistory($companyId, $planType)
    {
        $companyPlanHistory = array();
        $claims = Claim::where('claim_company_id', $companyId)->first();
        $now = Carbon::now();
        
        $companyPlanHistory['company_plan_type']   = $planType;
        $companyPlanHistory['company_plan_status'] = 'INACTIVE';
        $companyPlanHistory['company_plan_datestarted'] = $now;
        $companyPlanHistory['company_plan_company_id']  = $companyId;
        $companyPlanHistory['company_plan_history_claim_id']  = $claims->claim_id;

        return $companyPlanHistory;
    }


}