<?php

namespace App\Http\Services;

use App\Models\CompanyPlanHistory;
use App\Models\JobPosting;
use App\Models\JobApplications;
use App\Models\Scout;

use \Datetime;
use Carbon\Carbon;

/**
 * Manage job post transactions
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-19
 *
 */
class JobPostService{

	/**
     * Check if job post is already under job_applications or scouts
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $jobPostId
     * @param  int $applicantId
     *
     * @return type of process if exist (applied or scouted)
     */
	public static function checkProcessExists($applicantId, $jobPostId)
    {
        $applied = false;
        $scouted = false;
        $type    = null; //type to return

        $applied = JobApplications::checkIfApplied($applicantId, $jobPostId);
        
        if(!$applied)
        {
            $scouted = Scout::checkIfScouted($applicantId, $jobPostId);
        }

        if($applied == true)
        {
            $type = "Applied";
        }
        elseif($scouted == true)
        {
            $type = "Scouted";
        }

        return $type;
	}


}