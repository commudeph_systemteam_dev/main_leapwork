<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

//Mail
use App\Jobs\SendPlanExpiredMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\PlanExpiredMail;

//Models
use App\Models\Reminders;
use App\Models\CompanyPlanHistory;

use DB;
use Carbon\Carbon;

use Log;


class CheckPlanExpiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planexpiry:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will check the current registered plan of a user if it is going to expire and will send an email reminding the user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * 
     */
    public function handle()
    {
        Log::info('Check Company Plan History!');

        $companies = $this->getCompanyPlanStatus();
        $companyPlanIdsToAvoid = self::getAlreadySentIds();

        foreach($companies as $company) {
            //check if we should send an email warning the user that his plan is bout to expire
            if($this->shouldEmail($company->company_plan_dateexpiry, $company->company_plan_type) && !in_array($company->company_plan_history_id, $companyPlanIdsToAvoid)) {
                //send the mail to user email

                // if queue jobs become available in the test server use this command for better optimization
                // ===> dispatch(new SendPlanExpiredMail($company));
                // for now we will immediately send the email
                Mail::to($company->email)->send(new PlanExpiredMail());
                Log::info('Email Sent! -> '.$company->email);

                //reminder: in the event that we can use queue on the test serer transfer this function to SendPlanExpiredMail.php
                $this->setReminder($company->company_plan_history_id);
            }

            //check if the plan of the user has already expired
            if($this->isExpired($company->company_plan_dateexpiry)) {
                //set the status column of the company plan history table to EXPIRED

                $plan = CompanyPlanHistory::find($company->company_plan_history_id);

                $plan->company_plan_status = 'EXPIRED';

                $plan->save();

                Log::info('Plan Expired -> '.$company->email);

            }

        }
    }

    /**
     * Will check the input date if it is close to expiring within x days based on the type of company plan
     * 
     * @param {string} $date the date to check
     * @param {string} $type the type of plan the curernt company has
     * 
     * @return boolean TRUE if the current date is within daysbefore a mail should be sent
     */
    protected static function shouldEmail($date, $type) 
    {
        //get the number of days before an email must be sent based on the plan type of the company
        $daysBefore = config('constants.MASTER_PLAN_VALIDITY_DAYS_THRESHOLD')[$type];
        //default timezone
        $timezone = config('app.timezone');

        //convert to Carbon objects
        $now            = Carbon::now($timezone)->startOfDay();
        $expirationDate = Carbon::parse($date, $timezone)->startOfDay();
        //$before         = Carbon::parse($date, $timezone)->subDay($daysBefore)->startOfDay();
        $before         = Carbon::parse($date, $timezone)->subDay(12)->startOfDay();

        return $now->between($before, $expirationDate);
    }

    /**
     * will check if the current time has reached the input date
     * in other words, if it the date has expired
     * 
     * @param string $date the date to check
     * 
     * @return boolean TRUE if the date has expired
     * 
     * @author Alvin Genralo <alvin_generalo@commude.ph>
     */
    protected static function isExpired($date)
    {
        $timezone = config('app.timezone');

        $now            = Carbon::now($timezone)->startOfDay();
        $dateToCheck    = Carbon::parse($date, $timezone)->startOfDay();

        return $now->gte($dateToCheck);
    }

    /**
     * will get the companies with current plan status and are ACTIVE
     * 
     * @author Alvin Generalo <alvin_generalo@commude.ph>
     */
    protected static function getCompanyPlanStatus()
    {
        // $companyPlanIdsToAvoid = self::getAlreadySentIds();

        return DB::table('users')->join('company_plan_history', 'users.user_company_id', 'company_plan_history.company_plan_company_id')
                                 ->where('users.user_accounttype', 'CORPORATE ADMIN')
                                 ->where('users.user_status', 'ACTIVE')
                                 ->where('company_plan_history.company_plan_status', 'ACTIVE')
                                 ->whereNotNull('company_plan_history.company_plan_dateexpiry')
                                //  ->whereNotIn('company_plan_history.company_plan_history_id', $companyPlanIdsToAvoid)
                                 ->select([
                                     'users.email',
                                     'company_plan_history.company_plan_dateexpiry',
                                     'company_plan_history.company_plan_type',
                                     'company_plan_history.company_plan_history_id',
                                 ])
                                 ->get();
    }

    /**
     * will get the ids that has already been mailed
     * 
     * @return array
     * 
     * @author Alvin Generalo <alvin_generalo@commude.ph>
     */
    protected static function getAlreadySentIds()
    {
        $ids = [];

        $reminders = Reminders::where('reminder_type', 'company_plan');

        foreach($reminders->cursor() as $reminder) {
            $ids[] = $reminder->reminder_type_id;
        }

        return $ids;
    }

    /**
     * save an id in the reminders table to avoid it next time
     * 
     * @param string $id 
     * 
     * @author Alvin Generalo <alvin_generalo@commude.ph>
     */
    protected static function setReminder($id)
    {
        Reminders::insert([
            'reminder_type' => 'company_plan',
            'reminder_type_id' => $id,
            'reminder_result' => 1,
        ]);
    }

}
