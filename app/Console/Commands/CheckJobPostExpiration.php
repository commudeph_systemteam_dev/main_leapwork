<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\JobPosting;
use Carbon\Carbon;

class CheckJobPostExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:job_posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will check all the job posts for expired ones.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $timezone = config('app.timezone');
        $activeJobs = JobPosting::where('job_posts_status', 'ACTIVE');
        $now = Carbon::now($timezone);

        foreach ($activeJobs->cursor() as $job_post) {
            $jobExpirationDate = Carbon::parse($job_post->job_post_expiration_date);

            $this->info($jobExpirationDate);
            
            if($now->gte($jobExpirationDate)) {
                $this->info('Job Expired');
                $job_post->job_posts_status = 'EXPIRED';
                $job_post->save();
            }
        }
    }
}
