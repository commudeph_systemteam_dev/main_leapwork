<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Traits\NotificationFunctions;

use App\Models\Announcements;

use Log;

class CheckAnnounements extends Command
{

    use NotificationFunctions;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:announcements';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $undeliveredAnnouncements = Announcements::where(function($q){
            $q->whereNull('announcement_delivered')->orWhere('announcement_delivered', '0');
        })->where('announcement_status', 'ACTIVE');

        // Log::info('bg scheduler running');
        foreach ($undeliveredAnnouncements->cursor() as $announcement) {
            // Log::info('bg scheduler running. sending announcement');
            if(self::readyToDeliver($announcement->announcement_deliverydate, 'Asia/Manila')){
                self::sendAnnouncement($announcement);
                
            }
        }
    }
}
