<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Claim;
use App\Models\CompanyPlanHistory;
use Carbon\Carbon;
class CheckClaimExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:claims';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Claim Dates to set expired date to Expired after 7 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $timezone = config('app.timezone');
        $now = Carbon::now($timezone);

        $claims = Claim::where('claim_payment_method','CREDIT CARD')
                        ->orWhere('claim_invoice_status','NOT ISSUED')
                        ->Where('claim_payment_status','UNPAID');

        foreach ($claims->cursor() as $claim) {
            $claimStartDate = Carbon::parse($claim->claim_start_date);

            $this->info($claimStartDate);
            
            if($now->gte($claimStartDate->addDay(7))) {
                $this->info('Job Expired');
                $claim->claim_dateupdated = $now;
                $claim->claim_end_date = $now;
                $claim->claim_payment_status = 'UNPAID';
                $claim->claim_status = 'CANCELLED';
                $claim->save();
                
            }
        }
    }
}
