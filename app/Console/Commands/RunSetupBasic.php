<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class RunSetupBasic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the following commands: composer dump-autoload, view:clear,
	route:clear, config:cache in one command. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info('Running view:clear ....');
		\Artisan::call('view:clear');
		$this->info('Done view:clear ....');
		$this->info('Running config:cache ....');
		\Artisan::call('config:cache');
		$this->info('Done config:cache ....');
		$this->info('Running route:clear ....');
		\Artisan::call('route:clear');
		$this->info('Done route:clear ....');
	}

	
}
