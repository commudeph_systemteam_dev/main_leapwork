<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * MasterJobPositions
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterJobPositions extends Model
{
	private static $myTable = 'master_job_positions';
	protected $primaryKey   = 'master_job_position_id';
	protected $table = 'master_job_positions';
	protected $fillable     = [
						      'master_job_position_id',
						      'master_job_position_name',
						      'master_job_position_status',
						      'master_job_position_datecreated',
						      'master_job_position_industry_id'
						      ];

 	public $timestamps    = false;

 	/**
	 * Gets all active master_job_industries from db
	 * 
	 * Used in admin/master-admin/job
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $args optional conditions for queries
	 *
	 * @return array list of all master_job_positions
	 */
    public static function getAll($args = array())
    {
    	$query =  DB::table(self::$myTable)
		  		  	->join('master_job_industries', 'master_job_positions.master_job_position_industry_id', '=', 'master_job_industries.master_job_industry_id');

    	if(!empty($args))
    	{
    		if(!empty($args['status']))
    			$query = $query->where('master_job_position_status', '=',  $args['status']);

    	}

		return $query->get();
    }

    /**
     * Create/update of master_job_positions
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     */
    public static function saveJobPosition($data)
    {
		$master_job_position = MasterJobPositions::join('master_job_industries','master_job_position_industry_id','master_job_industry_id')
												 ->where('master_job_position_name', $data['master_job_position_name'])
												 ->where('master_job_position_industry_id', $data['master_job_position_industry_id'])
											     ->first();
		
		if(is_null($master_job_position)){
			$position = new MasterJobPositions();
	    	$position->master_job_position_name        = $data['master_job_position_name'];
	    	$position->master_job_position_status      = $data['master_job_position_status'];
			$position->master_job_position_industry_id = $data['master_job_position_industry_id'];
			$position->master_job_position_datecreated = date('Y-m-d H:i:s');
			
			$res = $position->save();
		}else
			$res = false;

	    return $res;
	}
	
	/**
     * Create new job position if not yet existing
	 * Have the master_job_position_industry_id = 99 indicating "Other"
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  array $newJobPosition instance, $request
     * @return newJobPosition Object
     */
	public static function createFromJobPosting($newJobPosition,$request)
	{
		$newJobPosition->master_job_position_name = ucwords($request->job_post_job_position_id);
		$newJobPosition->master_job_position_status = 'ACTIVE';
		$newJobPosition->master_job_position_datecreated =  date('Y-m-d');
		$newJobPosition->master_job_position_industry_id = 99;//Other industry
		$newJobPosition->save();
		return $newJobPosition;
	}

	/**
     * job_posts
     * @author Karen Irene Cano <karen_cano@commude.ph>
	 * @since 12-19-2017
     * @return Jobposting Object related to MasterJobPositions
     */
	public function job_posts()
	{
		return $this->hasMany('App\Models\JobPosting','job_post_job_position_id','master_job_position_id');
	}

}