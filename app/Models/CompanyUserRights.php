<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
/**
 * CompanyUserRights Model
 * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-29-11
 */
class CompanyUserRights extends Model
{
    protected $table      = 'company_user_rights';
	protected $primaryKey = 'company_user_right_id';
	
	public $timestamps = false;

	protected $fillable = [
        'company_user_right_id',
        'company_user_right_key',
        'company_user_right_value',
        'company_user_right_company_user_id'
    ];

    public static function createUserRights($companyUserRightId, $data)
    {

         if(empty($companyUserRightId)){
            $res = CompanyUserRights::create($data);
           
        }
        else
        {
            CompanyUserRights::where('company_user_right_company_user_id', $companyUserRightId)
                               ->where('company_user_right_key', $data['company_user_right_key'])
                               ->update($data);

            $res = CompanyUserRights::where('company_user_right_company_user_id', $companyUserRightId)
                               ->where('company_user_right_key', $data['company_user_right_key'])->get();
        }

         return $res;
    }


    /**
    * Returns true if Company User Right is successfully made
    * 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-29-11
    */
    public static function getResult($data)
    {

         if(empty($data['company_user_right_id']))
         {
            $res = true;
        }
        else
        {
           $res = false;
        }

        return $res;
    }


    /**
    * Returns the user right that matches the filter
    * 
    * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-29-11
    */

    public static function getUserRight($userId, $rightType)
    {
        $userRight = CompanyUserRights::where('company_user_right_company_user_id', '=', $userId)
                                       ->where('company_user_right_key', '=', $rightType)->first();

        return $userRight;
    }

}