<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CompanyIntro Model
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-16
 */
class CompanyIntro extends Model
{
    protected $table      = 'company_intro';
	protected $primaryKey = 'company_intro_id';
	
	public $timestamps = false;

	protected $fillable = [
        'company_intro_content', 
        'company_intro_pr', 
        'company_intro_image', 
        'company_intro_company_id'
    ];

    /*************************************** LARAVEL PROPERTIES ***************************************/

	/**
     * Get parent company
     * one comment belongs to a user
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return Company company
     */
	public function company()
	{
		return $this->belongsTo('App\Models\Company', 'company_intro_company_id', 'company_id');
	}

     /*************************************** FUNCTIONS ***************************************/

    /**
     * Get company_pr of 1 company (assumed has one relationship)
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $data companyId 
     *
     * @return string company_banner if foun
     */
    public static function getImage($companyId)
    {   
        $res = CompanyIntro::where('company_intro_company_id', $companyId)
                           ->first();
                         
        return $res->company_intro_image ?? null;
    }

    /**
     * Update of company_intro (assumed has one relationship)
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  CompanyIntro $data
     *
     * @return result 1 if successful transaction
     */
    public static function saveCompanyIntro($data)
    {   
        //create if not existing else update
        if( empty($data['company_intro_id']) )
            $res = CompanyIntro::create($data);
        else
            $res = CompanyIntro::where('company_intro_id', $data['company_intro_id'])
                               ->update($data);
        return $res;

    }
}
