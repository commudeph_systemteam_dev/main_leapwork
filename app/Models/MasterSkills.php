<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * MasterSkills
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterSkills extends Model
{
	private static $myTable = 'master_skills';

	protected $table        = 'master_skills';
	protected $primaryKey   = 'master_skill_id';
	protected $fillable     = [
						      'master_skill_id',
						      'master_skill_name',
						      'master_skill_datecreated',
						      'master_skill_job_position_id'
						      ];

 	public $timestamps      = false;

 	/**
	 * Gets all active master_skills from db
	 * 
	 * Used in admin/master-admin/job
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @return array list of all master_skills
	 */
    public static function getAll($data = array())
    {
		 return  DB::table(self::$myTable)
		  		   ->join('master_job_positions', 'master_job_positions.master_job_position_id', '=', 'master_skills.master_skill_job_position_id')
		  		   ->select('master_skills.*', 'master_job_positions.master_job_position_name as master_skill_job_position_name')
				   ->get();
    }

    /**
	 * Gets all active master_skills from db
	 * 
	 * Used in /company/job-posting
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @return array list of all master_skill names
	 */
    public static function getAllSkillNames($data = array())
    {
    	$query = MasterSkills::select('master_skill_name')
		 					 ->get();

		 return $query;
	}

 	/**
     * Create/update of master_job_positions
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     */
    public static function saveSkill($data)
    {
		$master_skill = MasterSkills::join('master_job_positions','master_skill_job_position_id','master_job_position_id')
									->where('master_skill_job_position_id', $data['master_skill_job_position_id'])
									->where('master_skill_name', $data['master_skill_name'])
									->first();
									   
		if(is_null($master_skill)){
			$skill = new MasterSkills();
			$skill->master_skill_name = $data['master_skill_name'];
			$skill->master_skill_job_position_id = $data['master_skill_job_position_id'];
			$skill->master_skill_datecreated = date('Y-m-d H:i:s');

			$res = $skill->save();
		}else
			$res = false;


	    return $res;
	}

}