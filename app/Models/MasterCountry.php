<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * MasterCountry
 * 
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class MasterCountry extends Model
{
	private static $myTable = 'master_country';

	protected $table        = 'master_country';
	protected $primaryKey   = 'master_country_id';
	protected $fillable     = [
						      'master_country_id',
						      'master_counry_name'
						      ];

 	public $timestamps      = false;

 	/**
     * Create/update of master_job_positions
     *
     * Used in admin/master-admin/job
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data properties of master_job_positions
     *
     * @return result 1 if successful transaction
     */
    public static function saveCountry($data)
    {
	    $master_country = MasterCountry::where('master_country_name', $data['master_country_name'])
								       ->first();
		
		if(is_null($master_country)){
			$country = new MasterCountry();
	    	$country->master_country_name = $data['master_country_name'];
			
			$res = $country->save();
		}else
			$res = false;

	    return $res;
	}
	
	
 	/**
     *job_locations
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return MasterJobLocations object related to country
     */
	public function job_locations()
	{
		return $this->hasMany('App\Models\MasterJobLocations','master_job_country_id','master_country_id');
	}
	

}