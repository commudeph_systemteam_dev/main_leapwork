<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Interfaces\NotificationInterface;
use App\Http\Services\NotificationService;

use GlobalTrait;
use Auth;
use DB;

/**
 * AdminCompanyConversation
 * 
 * @author    Arvin Alipio <aj_alipio@commude.ph>
 *            Referenced: JobApplicationConversation.php 
 *            @author Martin Louie Dela Serna <martin_delaserna@commude.ph>  
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-08
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class AdminCompanyConversation extends Model implements NotificationInterface
{
    protected $table        = 'admin_company_conversations';
	  protected $primaryKey   = 'admin_company_conversation_id';
    public $timestamps      = false;

  	protected $fillable     = [
  						        'admin_company_conversation_message',
  						        'admin_company_conversation_datecreated',
  						        'admin_company_conversation_user_id',
  						        'admin_company_conversation_thread_id'
  							  ];
	
	/*************************************** LARAVEL PROPERTIES ***************************************/

    /**
     * Get parent admin_company_thread
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return AdminCompanyThread adminCompanyThread
     */
    public function admin_company_thread()
    {
      return $this->belongsTo('App\Models\AdminCompanyThread', 'admin_company_conversation_thread_id', 'admin_company_thread_id');
    }

    /*************************************** FUNCTIONS ***************************************/

    /**
     * Create/update of admin_company_conversation
     *
     * Used in /admin/companies/contact/{companyId}
     *
     * @author Arvin Alipio <aj_alipio@commude.ph>
     *
     * @param  object $data admin_company_conversation 
     *
     * @return result 1 if successful transaction
     */
	public static function saveAdminCompanyConversation($data)
    {     
      //create if not existing else update
      if( empty($data['admin_company_conversation_id']) )
          $res = AdminCompanyConversation::create($data);
      else
          $res = AdminCompanyConversation::where('admin_company_conversation_id', $data['admin_company_conversation_id'])
                                          ->update($data);

        return $res;
    }

    public static function getMessageIconAdminNotifications($notifications)
	{
		$adminCompanyConversation = new AdminCompanyConversation();
        return $adminCompanyConversation->getNotification($notifications);
	}

    /**
     * NotificationInterface.getNotiification implementation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     * 
     * @since  2017-12-28
     * @param  array $data
     *
     */
    public function getNotification($notifications)
    {
        $functionChecker = 'userPusherNotificationLinks'; //GlobalTrait.method to use for getting links

        $notifications = NotificationService::getAllUnread(
			$notifications, 
			'COMPANY REPLIES', 
			Auth::user()['id'],
			$functionChecker,
			['company_replies_notification_url' => Auth::user()->userProperties(Auth::user()['id'])['company_replies_notification_url']
			]
        );

        foreach($notifications as $notification){
            $notification->notification_url = Auth::user()->userProperties(Auth::user()['id'])['company_replies_notification_url'] . "detail?id=" . $notification->notification_type_id;
        }
        // $notifications[0]->notification_url += 
        // $notifications = Scout::scoutUnread($notifications);
        // $notifications = JobInquiryReply::inquiryUnread($notifications);
        // $notifications = self::getJobApplicationConversation($notifications);//in progress
    //   dd($notifications[0]->notification_url);
        return $notifications;
    }

    /**
     * NotificationInterface.buildNotification implementation
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-28
     *
     * @param  array $newAdminCompanyConversation
     *
     * @return Notification notification
     */
    public function buildNotification($newAdminCompanyConversation)
    {
        /**Prepare data for Notification */
        $notification = new Notification;

        /**NotificationData Trait */
        $notification->title             = $newAdminCompanyConversation->admin_company_thread->admin_company_thread_title;
        $notification->content           = $newAdminCompanyConversation->admin_company_conversation_message;
         
        $notification->notification_data = $notification::setNotificationData(
                                               $notification->title,
                                               $notification->content,
                                               $notification->url
                                            );
        $notification->notification_type           = 'ADMIN CONTACT';
        $notification->notification_type_id        = $newAdminCompanyConversation->admin_company_thread->admin_company_thread_id;
        $notification->notification_target_user_id = $newAdminCompanyConversation->admin_company_thread->company->company_admin->id;

        return $notification;
    }

    public static function getUserName($id) {
        return DB::table('admin_company_conversations')
                ->join('applicants_profile', 'admin_company_conversations.admin_company_conversation_user_id', '=', 'applicants_profile.applicant_profile_user_id')
                ->join('users', 'users.id', '=', 'applicants_profile.applicant_profile_user_id')
                ->join('company', 'company.company_id', '=', 'users.user_company_id')
                ->where('applicants_profile.applicant_profile_user_id', '=', $id)
                ->first();
    }

}
