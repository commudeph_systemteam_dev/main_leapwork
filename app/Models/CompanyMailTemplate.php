<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * CompanyMailtemplate Model
 * 
 * @author    Karen Irene Cano <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-30
 */
class CompanyMailtemplate extends Model
{

	protected $table        = 'company_mail_template';
	protected $primaryKey   = 'company_mail_template_id';
	protected $fillable     = [
								'company_mail_subject',
								'company_mail_body',
								'company_mail_date_created',
								'company_mail_type',
								'company_mail_company_id'
						      ];

 	public $timestamps      = false;

    /*************************************** FUNCTIONS ***************************************/

    /**
     * Gets email templates by company_id
     * 1. users (parent)
     * 
     * Used in admin/companies
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $companyId optional conditions for queries
     *
     * @return object applicants_profile
     */
    public static function getEmailTemplates($companyId)
    {
        $query =  CompanyMailTemplate::where('company_mail_company_id', $companyId);

        return $query->get();
    }

    /**
     * Create/update of company_mail_template
     *
     * Used in company/account/mail-templates/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  CompanyMailtemplate $data 
     *
     * @return result 1 if successful transaction
     */
    public static function saveMailTemplate($data)
    {     
	        //create if not existing else update
      if( empty($data['company_mail_template_id']) )
          $res = CompanyMailTemplate::create($data);
      else
          $res = CompanyMailTemplate::where('company_mail_template_id', $data['company_mail_template_id'])
                           ->update($data);

        return !empty($res);
    }

}