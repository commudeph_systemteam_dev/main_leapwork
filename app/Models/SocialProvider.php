<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialProvider extends Model
{
    /**
    * SocialProvider
    * 
    * @author    Karen Cano <karen_cano@commude.ph>
    *            
    * @copyright 2017 Commude
    * @since     2017
    */
    protected $fillable = ['provider_id','provider'];
    

     /**
    * user
    * 
    * @author    Karen Cano
    * @copyright 2017 Commude
    * @since     2017
    */
    function user()
    {
        return $this->belongsTo(User::class);
    }
}
