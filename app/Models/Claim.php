<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterPlans;
use DB;
use Auth;
/**
 * Models of Claim
 * 
 * @author    Ken Kasai         <ken_kasai@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-16
 * @var       string   $claimTable	table of claim info
 */
class Claim extends Model
{
    private static $claimInfo = 'claims';
    protected $primaryKey     = 'claim_id';
    protected $table          = 'claims';
    
    protected $fillable = [
        'claim_name', 
        'claim_amount', 
        'claim_payment_method', 
        'claim_invoice_status', 
        'claim_payment_status', 
        'claim_status', 
        'claim_transaction_no', 
        'claim_token', 
        'claim_start_date',
        'claim_end_date', 
        'claim_datecreated',
        'claim_dateupdated',
        'claim_master_plan_id', 
        'claim_company_id'
    ];
    public $timestamps = false;
	/**
	 * Count the billing amount
	 * Used in admin/dashboard.
	 *
	 * Ex.     $date['MONTH']=>'10' $date['YEAR']=>'2017'
	 *         Claim::sumClaims($date, $status);
	 * 
	 * @param  array $date the month that will search.
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return int the total amount of claims.
	 */
    public static function sumClaims($date=array(), $status)
    {
    	$query = DB::table(self::$claimInfo);

    	return (!empty($date))
    	      ? $query->whereMonth('claim_datecreated', $date['MONTH'])
            	      ->whereYear('claim_datecreated',  $date['YEAR'])
            	      ->where('claim_payment_status',   $status)
            	      ->sum('claim_amount')
           	  : $query->where('claim_payment_status',   $status)
            	      ->sum('claim_amount');
    }

    /**
     * Get the monthly claims that are paid.
     * 
     * Returns all claims that are paid with details
     * @author  Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @return result
     * @since June 06, 2018
     */      

    public static function getMonthlySales()
    {
        $monthlyClaims = DB::table(self::$claimInfo)
                        ->select(DB::raw('MONTH(claim_datecreated) as month, SUM(claim_amount) as total'))
                        ->whereYear('claim_datecreated', '=', date('Y'))
                        ->where('claim_payment_status', '=', 'PAID')
                        ->groupBy(DB::raw('MONTH(claim_datecreated)'));

        return $monthlyClaims;
    }

    /**
     * Get the monthly claims that are unpaid.
     * 
     * Returns all claims that are paid with details
     * @author  Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @return result
     * @since June 06, 2018
     */      

    public static function getMonthlyDeferredSales()
    {
        $monthlyClaims = DB::table(self::$claimInfo)
                        ->select(DB::raw('MONTH(claim_datecreated) as month, SUM(claim_amount) as total'))
                        ->whereYear('claim_datecreated', '=', date('Y'))
                        ->where('claim_payment_status', '=', 'UNPAID')
                        ->groupBy(DB::raw('MONTH(claim_datecreated)'));

        return $monthlyClaims;
    }

    /**
     * Get the monthly total claims.
     * 
     * Returns all claims that are paid with details
     * @author  Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @return result
     * @since June 06, 2018
     */      

    public static function getMonthlyEstSales()
    {
        $monthlyClaims = DB::table(self::$claimInfo)
                        ->select(DB::raw('MONTH(claim_datecreated) as month, SUM(claim_amount) as total'))
                        ->whereYear('claim_datecreated', '=', date('Y'))
                        ->groupBy(DB::raw('MONTH(claim_datecreated)'));

        return $monthlyClaims;
    }

        /**
     * Count the billing amount
     * Used in admin/billing claims.
     *
     * 
     * 
     * @param get complan's 
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     *
     * @return query of latest claim status
     */
    public static function getGroupedClaimID($condition) 
    {
        $query = DB::table('claims') //static::->join
                 ->join('company', 'claims.claim_company_id', '=', 'company.company_id')
                 ->join('company_plan_history'   , 'company_plan_history.company_plan_company_id',   '=', 'company.company_id')
                 ->orderBy('claim_id', 'DESC')
                 ->groupBy('claim_id');
                //  ->whereRaw('claim_id in (select max(claim_id) from claims group by (claim_company_id))');

        if($condition != null)
        {
            foreach ($condition as $key => $value) 
            {
                $query = $query->orWhere($value[0][0],$value[0][1], $value[0][2]);
            }
        }
    	return $query;
    }

    /**
     * Create new claim
     *
     * 
     * @param
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since 11-28-2017 
     * @return query of latest claim status
     */
    public static function createClaim($data,$request)
    {   
        $planDetails = MasterPlans::where('master_plan_name',$request["radio-group-plan-hidden"])
                                 ->first();

        $dateNow = Carbon::now();
        $dateExpire = Carbon::now()->addDays($planDetails->master_plan_expiry_days);
        $new_claim = new Claim;
        $new_claim->claim_name           = $planDetails->master_plan_name;
        $new_claim->claim_amount         = $planDetails->master_plan_price;
        $new_claim->claim_invoice_status = 'NOT ISSUED';
        $new_claim->claim_payment_method = $request->payment_method;
        $new_claim->claim_payment_status = 'UNPAID';
        $new_claim->claim_master_plan_id = $planDetails->master_plan_id;
        $new_claim->claim_datecreated    = $dateNow;
        $new_claim->claim_start_date     = $dateNow;
        $new_claim->claim_end_date       = $dateExpire;
        $new_claim->claim_status         = 'OPENED';
        $new_claim->claim_company_id     = $data->company_id;
        $new_claim->claim_token          = $data->company_id.uniqid();
       
        $new_claim->save();

        return $new_claim;
    }

        /**
     * Create new claim
     *
     * 
     * @param
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since 11-28-2017 
     * @return query of latest claim status
     */
    public static function changePlanClaim($request)
    {   
        $planDetails = MasterPlans::where('master_plan_name',$request->txt_name)
                                 ->first();

        $condition[0] = array(['company.company_id','=', Auth::user()["user_company_id"]]);
        $claimRecord  = self::getClaimEntry($condition);

        $dateNow = Carbon::now();
        $dateExpire = Carbon::now()->addDays($planDetails->master_plan_expiry_days);

        $new_claim = new Claim;
        $new_claim->claim_invoice_status = 'NOT ISSUED';
        $new_claim->claim_payment_method = $claimRecord->claim_payment_method;
        $new_claim->claim_payment_status = 'UNPAID';
        $new_claim->claim_master_plan_id = $planDetails->master_plan_id;
        $new_claim->claim_datecreated    = $dateNow;
        $new_claim->claim_start_date     = $dateNow;
        $new_claim->claim_end_date       = $dateExpire;
        $new_claim->claim_status         = 'OPENED';
        $new_claim->claim_company_id     = Auth::user()["user_company_id"];
        $new_claim->claim_token          = $request->txt_id.uniqid();
       
        $new_claim->save();
    }

    /**
     * master_plans
     * One claim has one master plan
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since 11-29-2017 
     * @return object MasterPlans
     */
    public function master_plans()
    {
        return $this->belongsTo('App\Models\MasterPlans','claim_master_plan_id','master_plan_id');
    }

    
    /**
     * company
     * One claim belongs to one company
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since 11-29-2017 
     * @return object MasterPlans
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company','claim_company_id','company_id');
    }

    /**
     * paymentMethodLink
     * Return payment method link
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @since 11-29-2017 
     * @return object MasterPlans
     */
    public function paymentMethodLink($claimPaymentMethod)
    {
        //to store methodLink in constants.
        return config('constants.app.url')."/paymentMethod/".strtolower($claimPaymentMethod);
    }

    /**
     * index claim entery
     * Get the first claim entry of the company
     * 
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 8, 2017
     */

    public static function getClaimEntry($condition)
    {
        $query = DB::table('claims')
                ->join('master_plans', 'claims.claim_master_plan_id','=','master_plans.master_plan_id')
                ->join('company', 'company.company_id','=','claims.claim_company_id')
                ->join('company_plan_history', 'company_plan_history.company_plan_history_claim_id','=','claims.claim_id')
                ->orderBy('claim_id', 'desc');

        if($condition != null)
        {
            foreach ($condition as $key => $value) 
            {
                $query = $query->where($value[0][0],$value[0][1], $value[0][2]);

            }
        }

        return $query->first();
    }

    /**
     * index null
     * 
     * update the value of the claim from unpaid to paid
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Dec 8, 2017
     */

    public static function updateClaim($data, $status)
    {
         $dateNow = Carbon::now();
         $updatedValues['claim_dateupdated'] = $dateNow;
         $updatedValues['claim_payment_status'] = $status;
         $updatedValues['claim_status'] = 'CLOSED';
         
          DB::table('claims')
            ->where('claim_id', $data->claim_id)
            ->update($updatedValues);
    }

    /**
     * index null
     * CompanyBillingController - billingInvoice()
     * update the status of the claim to cancel
     * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
     * @return UPDATE
     * @since  June 18 2018
     */

    public static function updateClaimCancel($data, $status, $paymentStatus)
    {
         $dateNow = Carbon::now();
         $updatedValues['claim_dateupdated'] = $dateNow;
         $updatedValues['claim_payment_status'] = $paymentStatus;
         $updatedValues['claim_status'] = $status;
         
          DB::table('claims')
            ->where('claim_id', $data->claim_id)
            ->update($updatedValues);
    }


    public static function getAllClaims($condition)
    {
         $query = DB::table('claims')
                ->join('master_plans', 'claims.claim_master_plan_id','=','master_plans.master_plan_id')

                ->join('company', 'company.company_id','=','claims.claim_company_id')

                ->join('company_plan_history'   , 'company_plan_history.company_plan_history_claim_id',   '=', 'claims.claim_id')
                ->groupBy('claims.claim_id')
                ->orderBy('claims.claim_id', 'desc');

        if($condition != null)
        {
            foreach ($condition as $key => $value) 
            {
                $query = $query->where($value[0][0],$value[0][1], $value[0][2]);
            }
        }
        return $query;
    }


    /**
     * Get the claims that are paid (Used in Company user, Billing Paid By)
     * 
     * Returns all claims that are paid with details
     * @author  Arvin Jude Alipio <aj_alipio@commude.ph>
     * @return View
     * @since Dec 12, 2017
     */      

    public static function getPaidClaims()
    {
        $paidClaims = DB::table('claims')
                        ->join('company', 'company.company_id', '=', 'claims.claim_company_id')
                        ->join('billing_info', 'billing_info.billing_info_claim_id', '=', 'claims.claim_id')
                        ->where('claim_payment_status', '=', 'PAID')
                        ->where('company_id','=',Auth::user()["user_company_id"]);

        return $paidClaims->get();

    }

    /**
     * Save claim
     * 
     * Used in admin/companies/create
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-19
     *
     * @param  array $data
     *
     * @return array list of all company
     */
    public static function saveClaim($data)
    {
       //create if not existing else update
        if( empty($data['claim_id']) )
            $res = Claim::create($data);
        else
            $res = Claim::where('claim_id', $data['claim_id'])
                        ->update($data);

        return $res;
    }

        /**
     * index views
     * 
     * Count company History
     * @author  Rey Norbert Besmonte <rey_besmonte@commude.ph>
     * @return View
     * @since Feb 13, 2018
     */
    public static function findFirstCompanyHistory($data, $process)
    {
        if($process = 'token'){
            return Claim::where('claims.claim_token', $data)
                        ->join('company_plan_history', 'company_plan_history.company_plan_history_claim_id','=','claims.claim_id')
                        ->get();
        } else {
            return Claim::where('company_plan_history.company_plan_company_id', $data)
                        ->join('company_plan_history', 'company_plan_history.company_plan_history_claim_id','=','claims.claim_id')
                        ->get();
        }
    }

    /**
     * Count the billing amount
     * Used in admin/billing claims.
     *
     * 
     * 
     * @param get complan's 
     * @author Rey Norbert Besmonte <rey_besmonte@commude.ph>
     *
     * @return query of latest claim status
     */
    public static function getGroupedClaims($condition) 
    {
        $query = DB::table('claims') //static::->join
                 ->join('company', 'claims.claim_company_id', '=', 'company.company_id')
                 ->join('company_plan_history'   , 'claims.claim_company_id',   '=', 'company_plan_history.company_plan_company_id')
                 ->orderBy('claim_id', 'DESC')
                 ->distinct('company.company_id')
                 ->groupBy('claim_id');

        if($condition["where"] != null || $condition["orWhere"] != null)
        {
            foreach ($condition['where'] as $key => $value) 
            {
                $query = $query->where($value[0][0],$value[0][1], $value[0][2]);
            }
            if($condition['orWhere'][0][0][2] != "%%"){
                foreach ($condition['orWhere'] as $key => $value) 
                {
                    if($key == 0){
                        $query = $query->where($value[0][0],$value[0][1], $value[0][2]);
                    }else{
                        $query = $query->orWhere($value[0][0],$value[0][1], $value[0][2]);
                    }
                }
            }
        }

    	return $query;
    }

    public static function ccMasking($number) {
        $maskingCharacter = 'X';
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
    }

}
