<?php

namespace App\Models;

use App\Traits\GlobalTrait;
use App\Traits\NotificationData;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Model of Notification
 *
 * @author    Karen Irene Cano     <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-10
 */
class Notification extends Model
{
    use NotificationData,
        GlobalTrait; //notifications.notification_data

    protected $table = 'notifications';
    protected $primaryKey = 'notification_id';
    public $timestamps = false;
    protected $fillable = [
        'notification_id',
        'notification_type',
        'notification_type_id',
        'notification_data',
        'notification_read_at',
        'notification_created_at',
        'notification_updated_at',
        'notification_target_user_id',
    ];
    public $notification_url; //Karen(12-28-2017):Please do not delete. We have classes that calls this property.

    /**
     * createNotification
     * Create new entry on notifications table
     * @author    Karen Irene Cano     <karen_cano@commmude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @return    void
     */
    public static function createNotification($event)
    {
        $dateNow = Carbon::now();
        $notification = new Notification;
        $notification->notification_type = $event->data->notification_type;
        $notification->notification_type_id = $event->data->notification_type_id;
        $notification->notification_data = $event->data->notification_data;
        $notification->notification_target_user_id = $event->data->notification_target_user_id;
        $notification->notification_created_at = $dateNow;
        $notification->save();

        $messageIconUser = Notification::where('notification_type', 'MESSAGE_ICON')
            ->where('notification_type_id', $event->data->notification_target_user_id)
            ->update([
                'notification_read_at' => null,
            ]);

        return $notification;
    }

    /**
     * markAsRead
     * update as read the notification entry
     * @author    Karen Irene Cano     <karen_cano@commmude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @return    void
     */
    public static function markAsRead($notification_id)
    {
        $notification = Notification::findOrFail($notification_id);
        $updateRelated = Notification::where('notification_type', $notification->notification_type)
            ->where('notification_type_id', $notification->notification_type_id)
            ->where('notification_target_user_id', $notification->notification_target_user_id)
            ->where('notification_read_at', null)
            ->update(
                [
                    'notification_read_at' => Carbon::now(),
                ]);
    }

    /**
     * userIconNotification
     * Create a new entry for user bell/message icon notifications to track
     * if the Bell Icon from the header has been clicked already
     * @author    Karen Irene Cano     <karen_cano@commmude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @return    object newBellNotif
     */
    public static function userIconNotification($IconNotificationType)
    {
        $newBellNotif = new Notification;
        $newBellNotif->notification_type = $IconNotificationType;
        $newBellNotif->notification_type_id = Auth::user()["id"];
        $newBellNotif->notification_data = $newBellNotif::setNotificationData(
            $IconNotificationType,
            'Notification Type Id is user id'
        );
        $newBellNotif->notification_read_at = null;
        $newBellNotif->notification_created_at = Carbon::now();
        $newBellNotif->save();

        return $newBellNotif; //return newly created notification
    }

    /**
     * findBellUser
     * Find an instance of 'BELL_ICON'/'MESSAGE_ICON' respective of the auth::user()
     * Create or return the User
     * @author    Karen Irene Cano     <karen_cano@commmude.ph>
     * @copyright 2017 Commude Philippines, Inc.
     * @return    object notification
     */
    public static function findIconUser($IconNotificationType)
    {
        $bellUser = Notification::where('notification_type', $IconNotificationType)
            ->where('notification_type_id', Auth::user()["id"])
            ->first();
        if (is_null($bellUser)) {
            $bellUser = Notification::userIconNotification($IconNotificationType);
        }

        return $bellUser;
    }

    /**
     * Check notification exist given a type and update.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $notificationTypeId
     * @param  int $notificationTargetUserId
     *
     * @return  1 if successful
     */
    public static function markAsReadIfExists($targetUserId, $type, $typeId = null)
    {
        $query = Notification::where('notification_type', $type)
            ->where('notification_target_user_id', $targetUserId)
            ->whereNull('notification_read_at');
        if ($typeId) {
            $query = $query->where('notification_type_id', $typeId);
		}
		
        $res = $query->update(['notification_read_at' => Carbon::now()]);

        return $res;
    }

    /**
     * Check notification exist given a type and update
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  int $notificationType
     *
     * @return  Notification notifications
     */
    public static function getAllUnreadByType($notificationType, $userId)
    {

        $res = Notification::join('users', 'users.id', 'notification_type_id')
            ->join('company', 'company.company_email', 'users.email')
            ->where('notification_type', $notificationType)
            ->where('notification_target_user_id', $userId)
            ->whereNull('notification_read_at')
            ->orderBy('created_at', 'desc')
            ->get();

        return $res;
    }

}
