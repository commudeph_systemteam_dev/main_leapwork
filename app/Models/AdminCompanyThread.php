<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * AdminCompanyThread
 *
 * @author    Arvin Alipio <aj_alipio@commude.ph>
 *			  Referenced: JobApplicationThread.php 
 *		      @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-12-12
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class AdminCompanyThread extends Model
{
    protected $table        = 'admin_company_threads';
	protected $primaryKey   = 'admin_company_thread_id';
    public $timestamps      = false;

	protected $fillable     = [
						        'admin_company_thread_title',
						        'admin_company_thread_datecreated',
						        'admin_company_thread_user_id',
						        'admin_company_thread_company_id'
							  ];
	
	/*************************************** LARAVEL PROPERTIES ***************************************/
	/**
     * Gets all child admin_company_conversations
     * @author Arvin Alipio <aj_alipio@commude.ph>
     * @return list AdminCompanyConversation
     */
    public function admin_company_conversations()
    {
        return $this->hasMany('App\Models\AdminCompanyConversation', 'admin_company_conversation_thread_id', 'admin_company_thread_id');
    }
	
	/**
     * Get parent admin_company_thread
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return AdminCompanyThread adminCompanyThread
     */
    public function company()
    {
      return $this->belongsTo('App\Models\Company', 'admin_company_thread_company_id', 'company_id');
    }

	/*************************************** FUNCTIONS ***************************************/

	/**
	 * Get last message for the thread
	 * 
	 * @author Arvin Alipio <aj_alipio@commude.ph>
	 * @param  AdminCompanyThread $adminCompanyThread
	 * 
	 * @return object 
	 */
	public static function getLastSent(AdminCompanyThread $adminCompanyThread)
	{
		return $adminCompanyThread->admin_company_conversations
								  ->sortByDesc('admin_company_conversation_datecreated')
								  ->first() 
			   ?? new AdminCompanyThread();
	}

	/**
	 * Get admin_company_threads dynamic query. Eager loading with 
	 * child job_application_conversations
	 *
	 * @author Arvin Alipio <aj_alipio@commude.ph>
	 *
	 * @param  array $filter
	 *
	 * @return AdminCompanyThread adminCompanyThread
	 */
	public static function getThreads($filter)
	{
		$query = AdminCompanyThread::with('admin_company_conversations');

		if(!empty($filter))
		{
			if(!empty($filter['company_id']))
			{
				$query = $query->where('admin_company_thread_company_id', $filter['company_id']);
			}

			if(!empty($filter['user_id']) && !empty($filter['type']))
			{
				$query = ($filter['type'] == 'inbox') 
				       ? $query->where('admin_company_thread_user_id', '!=', $filter['user_id'])
				       : $query->where('admin_company_thread_user_id', $filter['user_id']);
			}
		}

		return $query->get();


	}



	/**
     * Create/update of job_application_thread
     *
     * Used in applicant/profile/history
     *
     * @author Arvin Alipio <aj_alipio@commude.ph>
     *
     * @param  object 
     *
     * @return adminCompanyThread if succesful
     */
	public static function saveAdminCompanyThread($data)
	{
      if( empty($data['admin_company_thread_id']) )
          $res = AdminCompanyThread::create($data);
      else
          $res = AdminCompanyThread::where('admin_company_thread_id', $data['admin_company_thread_id'])
                                    ->update($data);

      return $res->admin_company_thread_id ?? null;
	}
}
