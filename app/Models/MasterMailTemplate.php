<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\MasterUserMailTemplateController;

use DB;

/**
 * MasterMailTemplate
 * 
 * @author    Karen Irene Cano <karen_cano@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-27
 */
class MasterMailTemplate extends Model
{

	protected $table        = 'master_mail_template';
	protected $primaryKey   = 'master_mail_template_id';
	protected $fillable     = [
							  'master_mail_subject',
							  'master_mail_body',
							  'master_mail_date_created',
							  'master_mail_status'
							  ];

    public $timestamps    = false;

    /*************************************** FUNCTIONS ***************************************/
     /**
     * Gets email templates by company_id
     * 
     * Used in master admin
     * 
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     *
     * @return object applicants_profile
     */
    public static function getMasterEmailTemplates()
    {
        $query =  MasterMailTemplate::all();

        return $query;
    }

    /**
     * Create/update of company_mail_template
     *
     * Used in company/account/mail-templates/edit
     *
     * @author Arvin Jude Alipio <aj_alipio@commude.ph>
     *
     * @param  Master Mail template $data 
     *
     * @return result 1 if successful transaction
     */
    public static function saveMasterMailTemplate($data)
    {     
	        //create if not existing else update
      if( empty($data['master_mail_template_id']) )
          $res = MasterMailTemplate::create($data);
      else
          $res = MasterMailTemplate::where('master_mail_template_id', $data['master_mail_template_id'])
                           ->update($data);

        return !empty($res);
    }

    
}