<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use App\Models\Scout;
use App\Models\ScoutConversation;
use App\Interfaces\NotificationInterface;
use App\Traits\GlobalTrait;

class Scout extends Model implements NotificationInterface
{
    use GlobalTrait;
    /**
    * Scout Model for scouts table
    * @author    Karen Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-26
    */

    protected $table        = 'scouts';
    protected $primaryKey   = 'scout_id';
    public $timestamps      = false;
     protected $fillable = [
         'scout_status',
         'scout_date_scouted',
         'scout_applicant_id',
         'scout_company_id',
         'scout_job_post_id'
    ];

    /**
     * addFavorite
     * Insert to scouts table the user and scout_status = 'FAVORITE'
     * @author Karen Cano
     * @return 
     */
    public static function addFavorite($applicant_profile_id)
    {
        $dateNow = Carbon::now();
        $addFavorite = new Scout;
        $addFavorite->scout_applicant_id = $applicant_profile_id;
        $addFavorite->scout_company_id   = Auth::user()["user_company_id"];
        $addFavorite->scout_date_scouted = $dateNow;
        $addFavorite->scout_status       = 'FAVORITE';
        $addFavorite->save();
    }

    /**
     * getApplicantsToFavorite
     * Where applicant does not exist yet at the scouts table
     * @author Karen Cano
     * @return applicants to favorite
     */
    public static function getApplicantsToFavorite()
    {
        // return ApplicantsProfile::leftJoin('scouts', 'applicants_profile.applicant_profile_id', '=', 'scouts.scout_applicant_id');
        return ApplicantsProfile::join('users', 'applicants_profile.applicant_profile_user_id', '=', 'users.id')
                                 ->where('applicants_profile.applicant_profile_user_id','!=', null)
                                 ->where('users.user_status', '!=', 'INACTIVE');
    }

     /**
     * getApplicantsToFavorite
     * Get all favorited applicants
     * @author Karen Cano
     * @return applicants to favorite
     */
    public static function getApplicantsFavorited()
    {
        return ApplicantsProfile::leftJoin('scouts', 'applicants_profile.applicant_profile_id', '=', 'scouts.scout_applicant_id')
                    ->join('users', 'applicants_profile.applicant_profile_user_id', '=', 'users.id')
                    ->where('applicants_profile.applicant_profile_user_id','!=', null)
                    ->where('users.user_status', '!=', 'INACTIVE')
                    ->where('scout_status','=', 'FAVORITE')
                    ->where('scout_company_id', Auth::user()["user_company_id"]);
    }

    /**
	 * user_inquiring ()
	 * One inquiring user belongs to One Job Post, One Job inquiry
	 * This gets the properties of the user inquiring
	 * Ex. {{$inquiry->user_inquiring->email}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object user
	 */
	public function user_scouted()
    {
		$userInquiringObject = $this->belongsTo('App\Models\ApplicantsProfile','scout_applicant_id','applicant_profile_id');
        return $userInquiringObject;
    }
    
     /**
	 * scoutCSS ($applicant_status)
	 * get the equivalent css class based from scout_status
	 * Ex.     {{$scouted->scoutCSS($applicant->applicant_status)}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return string class name
	 */
	public function scoutCSS($applicant_status)
	{
		switch($applicant_status)
		{
        case "FAVORITE":
        case "UNDER REVIEW":
            return array(
                    "label" => "label blue ta_c",
                    "icon_mail" => ""
                    );
        case "DECLINED":
            return array(
                "label" => "label grey ta_c",
                "icon_mail" => "on_off"
            );
        case "NOT ACCEPTED":
        case "REJECTED":
            return array(
                "label" => "label red ta_c",
                "icon_mail" => "on_off"
            );
        case "INTERVIEW":
        case "ACCEPTED":
            return array(
                "label" => "label orange ta_c",
                "icon_mail" => ""
            );
		}
	}//endof applicantCSS
    
     /**
	 * scout_threads ()
	 * This gets the the scout scout_threads
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object scout_threads
	 */
	public function scout_threads()
    {
        return $this->hasMany('App\Models\ScoutThread', 'scout_thread_scout_id', 'scout_id');
    }

    /**
	 * scout_company_profile ()
	 * This gets the the scout company profile
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return string scout company profile
	 */
    public function scout_company_profile()
    {
        return $this->belongsTo('App\Models\Company', 'scout_company_id', 'company_id');
    }

    /**
	 * job_post ()
	 * This gets the properties of the job post related to the scout entry
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_posts
	 */
    public function job_post()
    {
        return $this->belongsTo('App\Models\JobPosting', 'scout_job_post_id', 'job_post_id');
    }

    

    /***
     * Implement Notifications Interface
	 * buildNotification ()
	 * build the notification model for scout
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object notifications 
	 */
    public function buildNotification($newScoutConvo)
    {
          /**Prepare data for Notification */
          $data = new Notification;
          $dateNow = Carbon::now();
          /**NotificationData Trait */
          $data->title = $newScoutConvo->scout_thread->scout_thread_title;
          $data->content = $newScoutConvo->scout_conversation_message;
         
          $data->notification_data      = $data::setNotificationData(
                                                  $data->title,
                                                  $data->content
                                              );
          $data->notification_type      = 'SCOUT';
          $data->notification_type_id   = $newScoutConvo->scout_thread->scout_thread_id;
          $companyAdminUserId = $newScoutConvo->scout_thread->scout->scout_company_profile->company_admin->id;
          $data->notification_target_user_id   = (Auth::user()["id"] != $companyAdminUserId )
                                                ? $companyAdminUserId
                                                :$newScoutConvo->scout_thread->scout->user_scouted->applicant_profile_user_id;
                                                
          return $data;
    }

    /**
	 * createScout ()
	 * Create scout and scout conversation entry
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return void
	 */
    public static function createScout($request,$dateNow,$applicant)
    {
        // 11/29/2017 Louie: Change logic
        // 12/18/2017 Karen: Thanks, much better!

         $scoutModify = Scout::where('scout_applicant_id',$request->applicant_profile_id)
                            ->where('scout_company_id', Auth::user()["user_company_id"])
                            ->first();

          $statusChosen = $request->scout_status;

          if($statusChosen)
          {
              $scoutModify->scout_status = $statusChosen;
          }

          $scoutModify->scout_date_scouted = $dateNow;
          $scoutModify->scout_job_post_id = $request->job_post_id;
          $scoutModify->save();

          /**Create Child Scout Thread */
          $newScoutThread = new ScoutThread;
          $newScoutThread->scout_thread_title = $request->txt_title_new;
          $newScoutThread->scout_thread_datecreated = $dateNow;
          $newScoutThread->scout_thread_scout_id = $request->scout_id;
          $newScoutThread->save();

          /**Create Child Scout Conversation */
          $newScoutConvo = new ScoutConversation;
          $newScoutConvo->scout_conversation_message = $request->txt_message_new;
          $newScoutConvo->scout_conversation_date_added = $dateNow;
          // $newScoutConvo->scout_conversation_read_flag = 0;
          $newScoutConvo->scout_conversation_user_id = Auth::user()["id"];
          $newScoutConvo->scout_conversation_thread_id = $newScoutThread->scout_thread_id;
          $newScoutConvo->save();

        return $newScoutConvo;
    }
    /**
     * Implement Notifications Interface
     * getNotification : format the reponse to fit notifications table
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  array $notifications where we will append/push the scout notifs
     * @return array $notifications
     */
    public function getNotification($notifications)
    {
        $data = new Notification();
        // // 12/18/2017 Louie: added ternary condition to avoid null
        // Pleace adjust and optimize this since viewing jobs doesn't require logged in users
        // $scouts = Auth::user()->authProperties()["scoutObject"];//scout parent 
        // Karen : (12-28-2017) ScoutObject now in middleware
      
        $scouts = Auth::user()["scoutObject"];

        if($scouts != null)
        {
            foreach($scouts as $scout)//get all scouts of the company
            {
                foreach($scout->scout_threads as $thread)
                {
                    $notifs = $thread->scout_notifications;
                    if($notifs->count() > 0)
                    {
                        $thread["scout_id"] = $scout->scout_id;
                        $thread["scout_applicant_id"] = $scout->scout_applicant_id;
                        $thread["notification_id"] = $scout->scout_applicant_id;
                        $thread["notification_created_at"] = $notifs->first()->notification_created_at;
                        $thread["notification_type"] = $notifs->first()->notification_type;
                        $thread["notification_data"] = $notifs->first()->notification_data;
                        $thread["notification_id"] = $notifs->first()->notification_id;
                        $thread["notification_url"] = $data->userProperties(Auth::user()["id"])["scout_notification_url"]
                        .$thread[$data->userProperties(Auth::user()["id"])["scout_url_param1"]]
                        . '/' . $thread[$data->userProperties(Auth::user()["id"])["scout_url_param2"]];

                        array_push($notifications, $thread);
                        // print_r(json_encode($notifications));
                    }
                }
            }
        }
        return $notifications;
    }

     /**
	 * scoutUnread
	 * Append to notifications array the unread scout notifications of the company logged in
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object notifications where notification_type = 'SCOUT'
	 */
    public static function scoutUnread($notifications)
    {
        $scout = new Scout();
        return $scout->getNotification($notifications);
        
    }//end of scoutUnread

    /**
     * Update stauts of scout
     *
     * Used in company/scout/favorite-user/choosTemplate
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  array $data filter string scout_status
     *                            int    scout_id
     * @return 1 if successful transaction
     */
    public static function updateStatus($data)
    {
          $res = Scout::where('scout_id',  $data['scout_id'])
                      ->update( ['scout_status' => $data['scout_status']] );
      return $res;
    }

    /**
   * Check if applicant has already applied for the possition
   * 
   * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
   * @since  2017-12-19 
   *
   * @param  $applicantId
   * @param  $jobPostID
   * @return true if found
   */
    public static function checkIfScouted($applicantId, $jobPostID)
    {

        return Scout::where('scout_applicant_id', $applicantId)
                    ->where('scout_job_post_id', $jobPostID)
                    ->where('scout_status', '!=', 'FAVORITE')
                    ->count() > 0;
    }
}
