<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Model of Applicant Skills
 * 
 * @author    Karen Irene Cano  <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-24
 * @var       
 */
class ApplicantsEducation extends Model
{

    protected $table        = 'applicants_education';
  	protected $primaryKey   = 'applicant_education_id';

  	protected $fillable     = [
                                 'applicant_education_school', 
                                 'applicant_education_course', 
                                 'applicant_education_year_month', 
                                 'applicant_education_year_passed', 
                                 'applicant_education_status', 
                                 'applicant_education_qualification_id', 
                                 'applicant_education_profile_id'
                                ];

    public $timestamps = false;

    /*************************************** FUNCTIONS ***************************************/
    
     /**
     * qualification ()
     * One education belongs to qualification
     * E.g in scout.blade.php
     *   @foreach($applicant->applicant_education as $school)
     *     {{$school->qualification->master_qualification_name}}
     *   @endforeach
     * Would yield : Mapua University - Bachelors/ Degree (we are interested on the qualification name)
     * @author Karen Irene Cano <karen_cano@commmude.ph>
     * @return object master_qualification
     */
    public function qualification()
    {
        return $this->belongsTo('App\Models\ApplicantsQualification', 'applicant_education_qualification_id', 'master_qualification_id');
    }

    /**
     * Create/update of applicant_education
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data applicants_education 
     *
     * @return result 1 if successful transaction
     */
    public static function saveCollege($data)
    {     
      //create if not existing else update
      if( empty($data['applicant_education_id']) )
          $res = ApplicantsEducation::create($data);
      else
          $res = ApplicantsEducation::where('applicant_education_id', $data['applicant_education_id'])
                                    ->update($data);

        return $res;
    }


}
