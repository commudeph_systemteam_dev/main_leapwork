<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnnouncementSeen extends Model
{
    //
		protected $table = 'announcement_seen';

		public $timestamps = false;

		protected $guarded = [];
}
