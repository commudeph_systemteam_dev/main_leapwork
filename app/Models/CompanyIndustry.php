<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * CompanyIndustry Model :composite
 *
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-27
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class CompanyIndustry extends Model
{
	private static $myTable = 'company_industry';

	protected $table        = 'company_industry';
	protected $primaryKey   = 'company_industry_company_id';
    //protected $primaryKey   = 'master_job_industry_id';
	
	public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_industry_company_id', 
        'company_industry_id',
        'company_industry_master_job_industry_id',
    ];

    /*************************************** LARAVEL PROPERTIES ***************************************/

    /**
     * One company has belongs to one employee range 
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return object master_employee_range
     */
    public function job_industry()
    {
        return $this->belongsTo('App\Models\MasterJobIndustries','company_industry_id','master_job_industry_id')
                    ->select(['master_job_industry_name']);
    }

    /*************************************** FUNCTIONS ***************************************/

	/**
     * Create of company_industry
     *
     * Used in company/profile
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $data int company_industry_company_id
     *                     int company_industry_id
     *
     * @return result 1 if successful transaction
     */
    public static function saveCompanyIndustry($data)
    {
	    $res = CompanyIndustry::updateOrCreate($data);

	    return $res;
    }

    /**
     * HARD DELETE of all company_industry belonging to 1 industry
     *
     * Used in company/profile
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $companyId
     *
     * @return result 1 if successful transaction
     */
    public static function deleteAll($companyId)
    {
        $res = CompanyIndustry::where('company_industry_company_id', $companyId)->delete();

        return $res;
    }

    /**
     * createIndustry
     * Create Industry on company industry table
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return void
     */
    public static function createIndustry($companyId,$industryId)
    {
        $company_industry = new CompanyIndustry;
        $company_industry->company_industry_company_id = $companyId;
        $company_industry->company_industry_master_job_industry_id = $industryId;
        $company_industry->save();
    }

}//end of Company
