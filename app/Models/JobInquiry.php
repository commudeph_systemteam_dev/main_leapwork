<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\MasterJobPositions;
use App\Models\MasterPlans;
use App\Traits\GlobalTrait;


/**
 * Models of Job Inquiry
 * 
 * @author    Karen Cano   <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-25
 */
class JobInquiry extends Model
{
	use GlobalTrait;
    protected $table = 'job_inquiry';
	protected $primaryKey   = 'job_inquiry_id';
	public $timestamps = false;
	protected $fillable = [
        'job_inquiry_id',
        'job_inquiry_title',
        'job_inquiry_date',
        'job_inquiry_post_id',
        'job_inquiry_user_id',
        'job_inquiry_reply_user_id',
    ];

    /*************************************** LARAVEL PROPERTIES ***************************************/

	/**
	 * job_post ()
	 * One inquiry belongs to One Job Post
	 * This gets the properties of the parent Job Post.
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_post
	 */
	public function job_post()
    {
		$jobPostObject = $this->belongsTo('App\Models\JobPosting','job_inquiry_post_id','job_post_id');
        return $jobPostObject;
	}

	/**
	 * user_inquiring ()
	 * One inquiring user belongs to One Job Post, One Job inquiry
	 * This gets the properties of the user inquiring
	 * Ex. {{$inquiry->user_inquiring->email}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object user
	 */
	public function user_inquiring()
    {
		$userInquiringObject = $this->belongsTo('App\Models\User','job_inquiry_user_id','id');
        return $userInquiringObject;
	}

	/**
	 * job_inquiry_replies ()
	 * One Job inquiry has many job inquiry replies. This gets the job inquiry replies
	 * related to the job post.
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object job_inquiry_replies
	 */
	public function job_inquiry_replies()
	{
		return $this->hasMany('App\Models\JobInquiryReply', 'job_inquiry_reply_inquiry_id', 'job_inquiry_id');
	}

	/*************************************** FUNCTIONS ***************************************/

	/**
	 * Get all job inquiries as inbox 
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  array $filter
	 * @return object activeJobPostsApplicants
	 */
	public static function getJobInquiry($filter)
	{
		$query = JobInquiryReply::orderBy('job_inquiry_id');

		if(!empty($filter))
		{
			if(!empty($filter['user_id']))
				$query = $query->where('user_id', $filter['user_id']);

			if(!empty($filter['job_post_id']))
				$query = $query->where('job_post_id', $filter['job_post_id']);
			where('job_inquiry_post_id', $filter['job_post_id']);

		}
		return $query->get();
	}


	
}
