<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Models of Billing
 * 
 * @author    Ken Kasai         <ken_kasai@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-16
 * @var       string   $billingTable	table of billing info
 */
class Billing extends Model
{
    private static $billingTable = 'billing_info';

    protected $primaryKey     = 'billing_info_id';
    protected $table          = 'billing_info';
    
    protected $fillable = [
        'billing_info_id', 
        'billing_info_name', 
        'billing_info_contactperson', 
        'billing_info_postalcode', 
        'billing_info_address1', 
        'billing_info_address2', 
        'billing_info_email', 
        'billing_info_dateadded', 
        'billing_info_company_id',
        'billing_info_claim_id'

    ];
    public $timestamps = false;
	/**
	 * Count the billing amount
	 * Used in admin/dashboard.
	 *
	 * Ex.     $date['MONTH']=>'10' $date['YEAR']=>'2017'
	 *         Company::countBilling($date, $status);
	 * 
	 * @param  array $date the month that will search.
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return int the total number of companies registered.
	 */
    public static function countBilling($date=array(), $status)
    {
    	$query = DB::table(self::$billingTable);

        return  (!empty($date))
            	?  $query->whereMonth('billing_info_dateadded', $date['MONTH'])
            			 ->whereYear('billing_info_dateadded',  $date['YEAR'])
            			 ->where('billing_info_status',         $status)
               			 ->count()
            	:  $query->count();
    }

    public static function ccMasking($number) {
        $maskingCharacter = 'X';
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
    }
}
