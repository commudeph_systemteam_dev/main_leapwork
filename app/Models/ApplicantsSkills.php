<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\JobPosting;
use DB;

/**
 * Model of Applicant Skills
 * 
 * @author    Karen Irene Cano  <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-24
 * @var       
 */
class ApplicantsSkills extends Model
{

	protected $primaryKey   = 'applicant_skill_id';
    protected $table        = 'applicants_skills';
	public $timestamps      = false;

	protected $fillable = [
        'applicant_skill_name', 
        'applicant_skill_type', 
        'applicant_skill_level', 
        'applicant_skill_profile_id'
    ];
    
    /*************************************** FUNCTIONS ***************************************/

     /**
     * Create/update of applicants_skills
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data int applicant_id
     *                      string type 
     *
     * @return result 1 if successful transaction
     */
    public static function getSkills($data)
    {     
        $query = ApplicantsSkills::where('applicant_skill_profile_id', $data['applicant_id'])
                                 ->where('applicant_skill_type' ,  $data['skill_type'])
                                 ->get();


        return $query;
    }

    /**
     * Create/update of applicants_skills
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data applicants_skills 
     *
     * @return result 1 if successful transaction
     */
    public static function saveSkill($data)
    {     
      //create if not existing else update
      if( empty($data['applicant_skill_id']) )
          $res = ApplicantsSkills::create($data);
      else
          $res = ApplicantsSkills::where('applicant_skill_id', $data['applicant_skill_id'])
                           ->update($data);

        return $res;
    }
	
}
