<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

/**
 * Models of Comment
 * @author    Karen Irene Cano      <karen_cano@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-27
 */
class Comment extends Model
{
    protected $table = 'comments';
	protected $primaryKey   = 'comment_id';
	
	public $timestamps = false;

	protected $fillable = [
        'comment_details', 
        'comment_date_added', 
        'comment_job_post_id', 
        'comment_user_id'
    ];

	/**
     * createComment(Request $request)
     * Create a new Comment
     * @author Karen Cano
     * @return void
     */
	public static function createComment(Request $request)
	{
		$dateNow = Carbon::now();
		$newComment = new Comment;
		$newComment->comment_details = $request->comment_details;
		$newComment->comment_date_added = $dateNow;
		$newComment->comment_job_post_id = $request->job_post_id;
		$newComment->comment_user_id = Auth::user()["id"];
		$newComment->save();

	}

	/**
     * user()
     * one comment belongs to a user
     * @author Karen Cano
     * @return object user
     */
	public function user()
	{
		$user = $this->belongsTo('App\Models\User', 'comment_user_id', 'id');
		return $user;
	}

	/**
     * job_post()
     * one comment belongs to a a job post
     * @author Karen Cano
     * @return object job_post
     */
	public function job_post()
	{
		return $this->belongsTo('App\Models\JobPosting', 'comment_job_post_id', 'job_post_id');
	}


}
