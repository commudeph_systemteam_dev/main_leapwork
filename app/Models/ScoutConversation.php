<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use App\Traits\GlobalTrait;
use App\Events\NotifyEvent;


class ScoutConversation extends Model
{
    use GlobalTrait;
    /**
    * ScoutConversation Model for scout_conversation
    * @author    Karen Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-30
    */

    protected $table        = 'scout_conversation';
    protected $primaryKey   = 'scout_conversation_id';
    public $timestamps      = false;
    protected $fillable = [
        'scout_conversation_message',
        'scout_conversation_date_added',
        'scout_conversation_user_id',
        'scout_conversation_thread_id'
    ];
   

    /**
     * addFavorite
     * Insert to scouts table the user and scout_status = 'FAVORITE'
     * @author Karen Cano
     * @return 
     */
    public static function addFavorite($applicant_profile_id)
    {
        $dateNow = Carbon::now();
        $addFavorite = new Scout;
        $addFavorite->scout_applicant_id = $applicant_profile_id;
        $addFavorite->scout_company_id = Auth::user()["user_company_id"];
        $addFavorite->scout_date_scouted = $dateNow;
        $addFavorite->scout_status       = 'FAVORITE';
        $addFavorite->save();
    }

    /**
     * getApplicantsToFavorite
     * Where applicant does not exist yet at the scouts table
     * @author Karen Cano
     * @return applicants to favorite
     */
    public static function getApplicantsToFavorite()
    {
        return ApplicantsProfile::leftJoin('scouts', 'applicants_profile.applicant_profile_id', '=', 'scouts.scout_applicant_id');
                    // ->where('scout_status','=', null);

    }

     /**
     * getApplicantsToFavorite
     * Get all favorited applicants
     * @author Karen Cano
     * @return applicants to favorite
     */
    public static function getApplicantsFavorited()
    {
        return ApplicantsProfile::leftJoin('scouts', 'applicants_profile.applicant_profile_id', '=', 'scouts.scout_applicant_id')
                    ->where('scout_status','=', 'FAVORITE')
                    ->where('scout_company_id', Auth::user()["user_company_id"]);
    }

    /**
	 * user_scouted ()
	 * One scout conversation has one applicant profile
	 * Ex. {{$scoutConvo->user_scouted->applicant_profile_firstname}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object applicant profile
	 */
	public function user_scouted()
    {
		$userInquiringObject = $this->belongsTo('App\Models\ApplicantsProfile','scout_conversation_user_id','applicant_profile_user_id');
        return $userInquiringObject;
    }

    public function user_profile()
    {
		$user_profile = $this->belongsTo('App\Models\User','scout_conversation_user_id','id');
        return $user_profile;
    }
    
     /**
	 * scoutCSS ($applicant_status)
	 * get the equivalent css class based from scout_status
	 * Ex.     {{$scouted->scoutCSS($applicant->applicant_status)}}
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return string class name
	 */
	public function scoutCSS($applicant_status)
	{
		switch($applicant_status)
		{
			case "FAVORITE":
			case "ACCCEPTED":
			case "DECLINED":
			case "REJECTED":
			case "FOR INTERVIEW":
			case "UNDER REVIEW":
				return "adjusting";
		}
	}//endof applicantCSS
    
    /**
     *createReplyScoutConversation
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param request
     * @return save new entry to scout conversation
     */
    public static function createScoutConversation($request)
    {
        $dateNow = Carbon::now();
		$newScoutConversation = new ScoutConversation;
		$newScoutConversation->scout_conversation_message = $request["scoutConvoMessage_".$request->scoutThreadId];
		$newScoutConversation->scout_conversation_date_added = $dateNow;
		$newScoutConversation->scout_conversation_user_id = Auth::user()["id"];
		$newScoutConversation->scout_conversation_thread_id = $request->scoutThreadId;
        $newScoutConversation->save();
        
        return $newScoutConversation;
    }

    /**
     *scout_thread
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @return object ScoutThread
     */
    public function scout_thread()
    {
		return $this->belongsTo('App\Models\ScoutThread','scout_conversation_thread_id','scout_thread_id');
    }

    /**
     * Create/update of job_application_conversation
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data job_application_conversation 
     *
     * @return result 1 if successful transaction
     */
    public static function saveScoutConversation($data)
    {     
      //create if not existing else update
        if( empty($data['scout_conversation_id']) )
        {
            $res = ScoutConversation::create($data);
            self::prepNotification($res);
        }
        else
        {
            $res = ScoutConversation::where('scout_conversation_id', $data['scout_conversation_id'])
                            ->update($data);
            $self::prepNotification($res);
        }

        return $res;
    }

    /**
     *
     * @author Karen Irene Cano <karen_cano@commude.ph>
     * @param  scout convo
     * @return void
     */
    public static function prepNotification($newScoutConvo)
    {
        $scout = new Scout();
        $data = $scout->buildNotification($newScoutConvo);
        event(new NotifyEvent($data));

    }

}
