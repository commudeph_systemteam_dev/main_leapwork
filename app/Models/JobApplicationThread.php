<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

/**
 * JobApplicationThread
 *
 * @author    Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-11-08
 * @var       string   $table name in DB
 * @var       int      $primaryKey name in DB
 * @var       array    $fillable coulmns in DB
 */
class JobApplicationThread extends Model
{
    protected $table        = 'job_application_threads';
	protected $primaryKey   = 'job_application_thread_id';
    public $timestamps      = false;

	protected $fillable     = [
						        'job_application_thread_title',
						        'job_application_thread_datecreated',
						        'job_application_thread_user_id',
						        'job_application_thread_application_id'
							  ];
	
	/*************************************** LARAVEL PROPERTIES ***************************************/

	/**
     * Get parent job_application
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return Jobapplication job_application
     */
	public function job_application()
	{
		return $this->belongsTo('App\Models\JobApplications', 'job_application_thread_application_id', 'job_application_id');
	}

	/**
     * Gets all child job_application_conversations
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return list JobApplicationConversation
     */
    public function job_application_conversations()
    {
        return $this->hasMany('App\Models\JobApplicationConversation', 'job_application_conversation_thread_id', 'job_application_thread_id');
    }

	/*************************************** FUNCTIONS ***************************************/

	/**
	 * Get last message for the thread
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @param  JobApplicationThread $jobApplicationThread
	 * 
	 * @return object activeJobPostsApplicants
	 */
	public static function getLastSent(JobApplicationThread $jobApplicationThread)
	{
		return $jobApplicationThread->job_application_conversations
	                                ->sortByDesc('job_application_conversation_datecreated')
	                                ->first()
                ?? new JobApplicationThread();
	}

	/**
	 * Get job_application_threads dynamic query. Eager loading with 
	 * child job_application_conversations
	 *
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $filter
	 *
	 * @return JobApplicationThread jobApplicationThreads
	 */
	public static function getThreads($filter)
	{
		$query = JobApplicationThread::with('job_application_conversations');

		if(!empty($filter))
		{
			if(!empty($filter['job_application_id']))
				$query = $query->where('job_application_thread_application_id', $filter['job_application_id']);

			if(!empty($filter['user_id']) && !empty($filter['type']))
			{
				$query = ($filter['type'] == 'inbox') 
				       ? $query->where('job_application_thread_user_id', '!=', $filter['user_id'])
				       : $query->where('job_application_thread_user_id', $filter['user_id']);
			}
			
		}
		return $query->get();
	}

	/**
     * Create/update of job_application_thread
     *
     * Used in applicant/profile/history
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  object $data job_application_thread 
     *
     * @return JobApplicationThread if succesful
     */
    public static function saveJobApplicationThread($data)
    {     
      //create if not existing else update
      if( empty($data['job_application_thread_id']) )
          $res = JobApplicationThread::create($data);
      else
          $res = JobApplicationThread::where('job_application_thread_id', $data['job_application_thread_id'])
                                    ->update($data);

        return $res->job_application_thread_id ?? null;
    }

}
