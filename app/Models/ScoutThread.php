<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use App\Traits\GlobalTrait;


class ScoutThread extends Model
{
    use GlobalTrait;
    /**
    * ScoutThread Model for scout_threads
    * @author    Karen Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-11-24
    */

    protected $table        = 'scout_threads';
    protected $primaryKey   = 'scout_thread_id';
    public $timestamps      = false;
    protected $fillable = [
        'scout_thread_title',
        'scout_thread_datecreated',
        'scout_thread_scout_id'
    ];

    /*************************************** LARAVEL PROPERTIES ***************************************/
   
    /**
     * Get scout_threads dynamic query. Eager loading with 
     * child scout_conversations
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  array $filter
     *
     * @return ScoutThread scoutThreads
     */
    public static function getThreads($filter)
    {
        // $query = ScoutThread::with(array('scout_conversations' => function($queryIn) {
        //             $queryIn->orderBy('scout_conversation_date_added', 'DESC');
        //         }));
        $query = ScoutThread::with('scout_conversations');

        if(!empty($filter))
        {
            if(isset($filter['scout_id']))
             {
                $query = $query->where('scout_thread_scout_id', $filter['scout_id']);
            }

        }
        return $query->get();
    }

    /**
    * scout_conversations
    * One Scout Thread has many conversations
    * @author    Karen Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-11-24
    */
    public function scout_conversations()
    {
        return $this->hasMany('App\Models\ScoutConversation', 'scout_conversation_thread_id', 'scout_thread_id');
    }

    /*************************************** FUNCTIONS ***************************************/

    /**
     * Get last message for the thread
     * 
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @param  ScoutThread $scoutThread
     * 
     * @return ScoutConversation scoutConversation
     */
    public static function getLastSent(ScoutThread $scoutThread)
    {
        return $scoutThread->scout_conversations
                           ->sortByDesc('scout_conversation_date_added')
                           ->first()
                ?? new ScoutConversation();
    }

    /**
    * scout
    * One Scout Thread belongs to One Scout
    * @author    Karen Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-12-15
    */
    public function scout()
    {
        return $this->belongsTo('App\Models\Scout', 'scout_thread_scout_id', 'scout_id');
    }

     /**
	 * scout_notifications ()
	 * Get the scout notifications related to the scout id
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object notifications where notification_type = 'SCOUT'
	 */
    public function scout_notifications()
    {
        return $this->hasMany('App\Models\Notification', 'notification_type_id', 'scout_thread_id')
                    ->where('notification_type','SCOUT')
                    ->where('notification_read_at',null)
                    ->where('notification_target_user_id',Auth::user()["id"])
                    ->orderby('notification_id','desc');
    }



}
