<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Claim;
use App\Interfaces\NotificationInterface;
use App\Models\CompanyPlanHistory;
use App\Models\Notification;
use App\Models\Announcements;
use App\Models\MasterPlans;

use App\Http\Services\NotificationService;
use Auth;



/**
 * Models of Company
 * 
 * @author    Ken Kasai         <ken_kasai@commmude.ph>
 * @copyright 2017 Commude Philippines, Inc.
 * @since     2017-10-12
 * @var       string   $corpTable	table of the company
 */
class Company extends Model implements NotificationInterface
{
	private static $corpTable = 'company';
	protected $table = 'company';
	protected $primaryKey   = 'company_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 
        'company_website', 
        'company_postalcode', 
        'company_address1', 
        'company_address2', 
        'company_ceo', 
        'company_date_founded',
        'company_contact_person',
        'company_contact_no',
        'company_email',
        'company_twitter', 
        'company_facebook', 
        'company_logo', 
        'company_banner', 
        'company_introduction', 
		'company_token',
		'company_credits',
		'company_current_plan', 
        'company_status',
        'company_file1',
        'company_file2',
        'company_file3',
        'company_date_registered',
        'company_date_updated',
        'company_employee_range_id'
	];

	/*************************************** LARAVEL PROPERTIES ***************************************/
	
	/**
	 * The name of the "created at" column.
	 *
	 * @var string
	 */
	const CREATED_AT = 'company_date_registered';

	/**
	 * The name of the "updated at" column.
	 *
	 * @var string
	 */
	const UPDATED_AT = 'company_date_updated';


	/**
     * company_date_founded accessor
     *
     * @param  string  $value
     * @return string
     */
    public function getDateFoundedAttribute($value) {
    	return  $this->company_date_founded->format('M d, Y');
	}

	/**
	 * One company has belongs to one employee range 
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @return object master_employee_range
	 */
	public function employee_range()
    {
        return $this->belongsTo('App\Models\MasterEmployeeRange','company_employee_range_id','master_employee_range_id')
							  ->select(['master_employee_range_min', 'master_employee_range_max']);;
	}

	/**
     * One company has many industries it can belong to. Composite
     * link so rely on CompanyIndustry orm to parent Master Job Industry
     *
     * Ex. @foreach($company->company_industry as $company_industry)
     *                <span>{{$company_industry->company_industry_name}}</span>
     *     @endforeach
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return list company_industry
     */
    public function company_industries()
    {
        return $this->hasMany('App\Models\CompanyIndustry', 'company_industry_company_id', 'company_id');
    }

    /**
     * Get child company_intro
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return CompanyIntro company_intro
    */
    public function company_intro()
    {
        return $this->hasOne('App\Models\CompanyIntro', 'company_intro_company_id', 'company_id');
    }

    /**
     * Get child company_intro
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @return CompanyInquiryThreads company_inquiry_threads
    */
    public function company_inquiry_threads()
    {
        return $this->hasOne('App\Models\CompanyInquiryThreads', 'company_inquiry_thread_company_id', 'company_id');
    }

    /*************************************** FUNCTIONS ***************************************/

	/**
	 * Count the number of registered companies. (If $month is empty, it will count the total companies inserted in db.)
	 * 
	 * Used in admin/dashboard.
	 *
	 * Ex.     $date['MONTH']=>'10' $date['YEAR']=>'2017'
	 *         Company::countRegistered($date);
	 * 
	 * @param  array $date the month that will search.
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return int the total number of companies registered.
	 */
    public static function countRegistered($date=array())
    {
    	$query = DB::table(self::$corpTable);

        return  (!empty($date))
            	?  $query->whereMonth('company_date_registered', $date['MONTH'])
            			 ->whereYear('company_date_registered',  $date['YEAR'])
               			 ->count()
            	:  $query->count();
    }

	/**
	 * Count the number of plan.
	 * 
	 * Used in admin/dashboard.
	 *
	 * Ex. Company::countPlan('ENTERPRISE');
	 * 
	 * @param  string $plan the plan name.
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return int the total number of specific plan registered.
	 */
    public static function countPlan($plan)
    {
    	$query = DB::table(self::$corpTable);

        return  (!empty($plan))
            	? $query->where('company_current_plan', $plan)
               			->count()
            	: 0;
	}

	/**
	 * companyCreate()
	 * Create a new company and the user
	 * Ex. Company::countPlan('ENTERPRISE');
	*
	 * @author Karen Cano <karen_cano@commmude.ph>
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @since  2017-12-05
	 *
	 * @param  $request
	 * @return newly created $user
	 */
	public static function companyCreate(Request $request)
	{
		$isPlanTypePayable = MasterPlans::isPlanTypePayable($request->input('radio-group-plan'));

		$dateNow = Carbon::now();
		/* new company object */
		$company = new Company;
		$company->company_name 			  = ucwords($request->company_name);
		$company->company_website         = $request->companyWebsite;
		$company->company_postalcode      = $request->postal_code;
		$company->company_address1        = ucwords($request->address1);
		$company->company_address2        = ucwords($request->address2);
		$company->company_date_registered = $dateNow;
		$company->company_contact_person  = ucwords($request->contactName);
		$company->company_contact_no      = $request->contactNumber;
		$company->company_email           = $request->email;
		$company->company_current_plan    = $request["radio-group-plan-hidden"];
		$company->company_status          = "UNVERIFIED";

		$company->save();
		$company->company_file1 =  (!empty($request->companyFile1))
			? basename($request->companyFile1->storeAs(config('constants.companyDirectory').'/company_'.$company->company_id
				, $request->companyFile1->getClientOriginalName()
				, 'storage-upload'))
			: NULL;

		$company->company_file2 =  (!empty($request->companyFile2))
			? basename($request->companyFile2->storeAs(config('constants.companyDirectory').'/company_'.$company->company_id
				, $request->companyFile2->getClientOriginalName()
				, 'storage-upload'))
			: NULL;

		$company->company_file3 =  (!empty($request->companyFile3))
			? basename($request->companyFile3->storeAs(config('constants.companyDirectory').'/company_'.$company->company_id
				, $request->companyFile3->getClientOriginalName()
				, 'storage-upload'))
			: NULL;

			
		$company->save();//save to company id
		
		//Louie: temporary since this is a mandatory field
		//099 - Others
		// CompanyIndustry::createIndustry($company->company_id,99);
		/* new company_plan_history */
		

		/* new user->company object */
		$user = new User;
		$user = User::createCompanyUser($request,$company);
		
		$isPlanTypePayable = MasterPlans::isPlanTypePayable($request->input('radio-group-plan'));
		$claim = null;
		
		// Create claim only for payable
		if($isPlanTypePayable)
		{
			$claim = Claim::createClaim($company,$request);
		}


		$company_history_plan = CompanyPlanHistory::createCompanyPlan($company,$request, $claim);
	
		return $user;
	}//end of companyCreate

	/**
	 * Get the company name
	 * 
	 * Used in admin/dashboard.
	 *
	 * Ex. Company::countPlan('ENTERPRISE');
	 * 
	 * @param  string $plan the plan name.
	 *
	 * @author Ken Kasai <ken_kasai@commmude.ph>
	 *
	 * @return int the total number of specific plan registered.
	 */
	public static function companyList($companyId='')
	{

    	return ($companyId)
    	       ? DB::table(self::$corpTable)->get()
    	       : DB::table(self::$corpTable)->get();
	}

	/**
	 * @author Karen Cano
	 * One company has many claims
	 */
	public function claims()
	{
		return $this->hasMany('App\Models\Claim','claim_company_id','company_id');
	}

	/**
	 * @author Karen Cano
	 * One company has many claims
	 */
	public function claims_issued()
	{
		return $this->hasMany('App\Models\Claim','claim_company_id','company_id')
		->where('claim_invoice_status','ISSUED');
	}
	/**
    * workingHours
	* Working hours array for select options
	* e.g ["01:00","02:00","03:00"..."23:00","0:00"]
    * @author    Karen Irene Cano <karen_cano@commude.ph>
    * @copyright 2017 Commude
    * @since     2017-10-17
    */
	public static function workingHours()
	{
		$hourFormat = array();
		$dateVariable = Carbon::today();
		$currentDate = Carbon::today();
		$add1Day = $currentDate->addDays(1);
		array_push($hourFormat,"00:00");
		while($dateVariable < $add1Day)
		{
			$dateVariable = $dateVariable->addHours(1);
			$formatted = date_format($dateVariable, 'H:i');//H	24-hour format of an hour with leading zeros
			array_push($hourFormat,$formatted);
		}
		array_pop($hourFormat);
		return $hourFormat;
	}

	/**
	 * Gets all company join: users with dynamic query
	 * 
	 * Used in admin/companies
	 * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
	 * @desc Improved Function
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $args optional conditions for queries
	 *
	 * @return array list of all company
	 */
    public static function getAll($args = array())
    {
    	$query =  DB::table(self::$corpTable)
		  		  	->leftJoin('users', 'users.user_company_id', '=', 'company.company_id')
		  		  	->groupBy('company.company_id')
		  		  	->select('company.*', 'users.updated_at');

    	if(!empty($args))
    	{
    		if(!empty($args['company_id']))
    			$query = $query->where('company.company_id', '=',  $args['company_id']);
    		
    		if(!empty($args['date_registered']))
    			$query = $query->where('company.company_date_registered', 'like', '%' . date_format($args['date_registered'], "Y-m-d") . '%');
    		
    		if(!empty($args['date_lastlogin']))
    			$query = $query->where('users.updated_at', 'like', '%' . date_format($args['date_lastlogin'], "Y-m-d") . '%');
    		
    		if(!empty($args['company_name']))
				$query = $query->where('company.company_name', 'like', '%' . $args['company_name'] . '%')
							   ->orWhere('company.company_current_plan', 'like', '%' . $args['company_name'] . '%');

    		if(!empty($args['company_status']))
    			$query = $query->where('company.company_status', '=', $args['company_status']);
    	}

		return $query;
    }

    /**
	 * Gets all company with dynamic query join: all parent tables
	 * master_employee_range. Let laravel orm handle industry children
	 * 
	 * USE ELOQUENT INSTEAD
	 * 
	 * Used in admin/companies. Left join since industry may be blank
	 * 
	 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 *
	 * @param  array $args optional conditions for queries
	 *
	 * @return $company
	 */
    public static function getCompany($args = array())
    {
    	$query = Company::where('company_id', $args['company_id'])
                        ->leftJoin('master_employee_range', 'master_employee_range.master_employee_range_id', '=', 'company.company_employee_range_id');

		return $query->first();
    }


 	/**
	 * Gets all company List
	 * 
	 * Used in admin/companies
	 * 
	 * @author Rey Norbert Besmonte
	 *
	 * @param  condition gets filter based on selected values by user
	 *
	 * @return $company
	 */
    public static function getCompanyClaimList($condition) 
    {
    	$query =  DB::table(self::$corpTable)
    		 			->join('company_plan_history'	, 'company_plan_history.company_plan_company_id', 	'=', 'company.company_id')
    		 			->join('claims'					, 'claims.claim_company_id', 						'=', 'company.company_id');

    	if($condition != null)
	    {
			foreach ($condition as $key => $value) 
			{
		    	$query = $query->where($value[0][0],$value[0][1], $value[0][2]);
		    }
	    }
    	return $query;
    }

    /**
     * Update of company
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-06

     * @param  object $data company 
     *
     * @return result 1 if successful transaction
     */
    public static function saveProfile($data)
    {	
    	$res = null;

    	$companyId = $data['company_id']; 

    	if(empty($data['company_id'])){
            $res = Company::create($data)->company_id;
        }
        else
        {
            $res = Company::where('company_id',  $companyId)
                          ->update($data);
        }
	    
	    return $res;
	}


	public function scouts()
	{
		return $this->hasMany('App\Models\Scout','scout_company_id','company_id');
	}

	/**
     * Get company_logo
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $data companyId 
     *
     * @return string company_logo if foun
     */
    public static function getLogo($companyId)
    {	
	    $res = Company::where('company_id', $companyId)
    				  ->first();

	    return $res->company_logo ?? null;
	}


	/**
     * Get company_banner
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     *
     * @param  int $data companyId 
     *
     * @return string company_banner if foun
     */
    public static function getBanner($companyId)
    {	
	    $res = Company::where('company_id', $companyId)
	    				 ->first();
	    				 
	    return $res->company_banner ?? null;
	}

	/**
     * Get company_banner
     *
     * Used in company/profile/edit
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2017-12-06
     *
     * @param  array $filter
     *
     * @return string company_banner if foun
     */
    public static function getFilePath($filter)
    {	
	    $company = Company::where('company_id', $filter['company_id'])
    				      ->select($filter['company_file'])
    				      ->first();
			 
	    return $company[$filter['company_file']] ?? null;
	}



    /***
     * Implement Notifications Interface
	 * buildNotification ()
	 * build the notification model for scout
	 * @author Karen Irene Cano <karen_cano@commmude.ph>
	 * @return object notifications 
	 */
    public function buildNotification($company)
    {
          /**Prepare data for Notification */
          $data = new Notification;
          $dateNow = Carbon::now();
          /**NotificationData Trait */
          $data->title = $company->company_name;
          $data->content = "New Company Registered";
         
          $data->notification_data      = $data::setNotificationData(
                                                  $data->title,
                                                  $data->content
                                              );
          $data->notification_type      = 'NEW_COMPANY';
          $data->notification_type_id   = $company->company_id;

          return $data;
	}
	
	public function buildNewCompanyNotification($company)
    {
          /**Prepare data for Notification */
          $data = new Notification;
          $dateNow = Carbon::now();
          /**NotificationData Trait */
          $data->title = $company->company_name;
          $data->content = "New Company Registered";
         
          $data->notification_data      = $data::setNotificationData(
                                                  $data->title,
                                                  $data->content
                                              );
		  $data->notification_type      = 'NEW_COMPANY';
		  $data->notification_target_user_id = "1";
          $data->notification_type_id   = $company->company_id;

          return $data;
    }

    public function getNotification ($notifications)
    {
		/*
		 *Karen :(12-28-2017) Commenting code below. Not being used.Will look into it.
		
		$notifications = Notification::where('notifications.notification_type','NEW_COMPANY')
				->where('notifications.notification_read_at',null)
				->get();

		foreach($notifications as $notification)
		{
		$notification["notification_url"] = $data->userProperties(Auth::user()["id"])["inquiry_notification_url"]
					.$inquiry[$data->userProperties(Auth::user()["id"])["inquiry_url_param1"]]
					.'/'.$inquiry[$data->userProperties(Auth::user()["id"])["inquiry_url_param2"]];
		array_push($notifications, $inquiry);
		}
		 */

		// 2017-12-26 Louie: add job application notifs
		$functionChecker = 'companyPusherNotificationLinks'; //GlobalTrait.method to use for getting links

		$notifications = NotificationService::getAllUnread(
			$notifications, 
			'JOB APPLICATION', 
			Auth::user()['id'],
			$functionChecker,
			['job_application_notification_url' => Auth::user()->userProperties(Auth::user()['id'])['job_application_notification_url']
			]
		);

		// 2017-12-28 Louie: add job application notifs and company inquiries
		$notifications = NotificationService::getAllUnread(
			$notifications, 
			'ADMIN CONTACT', 
			Auth::user()['id'],
			$functionChecker,
			['admin_messsage_notification_url' => Auth::user()->userProperties(Auth::user()['id'])['admin_messsage_notification_url']
			]
		);

		$notifications = NotificationService::getAllUnread(
			$notifications, 
			'COMPANY INQUIRY', 
			Auth::user()['id'],
			$functionChecker,
			['company_inquiry_notification_url' => Auth::user()->userProperties(Auth::user()['id'])['company_inquiry_notification_url']
			]
		);


		$notifications = Scout::scoutUnread($notifications);
		$notifications = JobInquiryReply::inquiryUnread($notifications);

        return $notifications;
	}
	
	public static function getBellIconCompanyNotifications($bellNotifications)
	{
		$bellNotifications = Announcements::adminNotifications($bellNotifications);
		$bellNotifications = self::getNewJobApplicantNotification($bellNotifications);
		
		return $bellNotifications;
	}
	
	/**
	 * getCompanyNotifications
	 * Collect notifications relative to the Company Admin/User
	 *  @author Karen Irene Cano <karen_cano@commude.ph>
	 *  @since 12-17-2017
	 *  @return object job_posting 
	 */
	public static function getMessageIconCompanyNotifications($notifications)
	{
		$company = new Company();
        return $company->getNotification($notifications);
	}

	//Karen : (12-28-2017) Commenting code below not used
    // public static function getAdminNotification ($notifications)
    // {
    // 	$notification = new Notification();
    // 	return $notification->getNotification($notifications);
    // }
	
	public function company_admin()
    {
        return $this->hasOne('App\Models\User','user_company_id','company_id');
	}
	
	/**
	 * job_posts
	 * One company has many job posts
	 *  @author Karen Irene Cano <karen_cano@commude.ph>
	 *  @since 12-17-2017
	 *  @return object job_posting 
	 */
	public function job_posts()
	{
        return $this->hasMany('App\Models\JobPosting', 'job_post_company_id', 'company_id');
	}

	/**
	 * newJobApplicantUnread
	 * Get all new job applicant notifications that are unread relative to the authenticated user.
	 *  @author Karen Irene Cano <karen_cano@commude.ph>
	 *  @since 12-17-2017
	 *  @return object notifications
	 */
	public static function getNewJobApplicantNotification($notifications)
	{
		// $newJobApplicants =  Notification::where('notification_type',config('constants.notification_type.NEW_JOB_APPLICANT'))
		// 									->where('notification_target_user_id', Auth::user()["id"])
		// 									->where('notification_read_at',null)
		// 									->get();
		$newJobApplicants =  Notification::getAllUnreadByType(config('constants.notification_type.NEW_JOB_APPLICANT'),Auth::user()["id"]);
											
		foreach($newJobApplicants as $newJobApplicant)
		{
			$newJobApplicant["notification_url"] = Auth::user()->userProperties(Auth::user()["id"])["new_applicant_notification_url"];
			array_push($notifications,$newJobApplicant);
		}
		return $notifications;
	}

	/**
	 * newJobApplicantUnread
	 * Get all new job applicant notifications that are unread relative to the authenticated user.
	 *  @author Karen Irene Cano <karen_cano@commude.ph>
	 *  @since 12-17-2017
	 *  @return object notifications
	 */
	public static function getNewJobApplicantHistoryNotification($notifications)
	{
		$newJobApplicants =  Notification::where('notification_type',config('constants.notification_type.NEW-JOB-APPLICANT'))
											->where('notification_target_user_id', Auth::user()["id"])
											->where('notification_read_at',null)
											->get();
											
		foreach($newJobApplicants as $newJobApplicant)
		{
			$newJobApplicant->notification_url = Auth::user()->userProperties(Auth::user()["id"])["new_applicant_notification_url"];
			array_push($notifications,$newJobApplicant);
		}
		return $notifications;
	}
	
	 /**
     * Get child company_intro
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since  2018/03/20
	 * 
	 * @param  string $companyName
	 * 
     * @return bool true if exists
     */
    public static function isCompanyNameExists(string $companyName)
    {
		$found = Company::where('company_name', $companyName)->exists();
		
		return $found;
    }


}//end of Company
