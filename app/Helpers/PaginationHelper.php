<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\Route;

/**
 * Pagination Helper for joined tables
 *
 * LengthawarePaginator is utilized to enable pagination
 * when multiple objects/entiries are joined from a DB transaction
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @copyright Commude Philippines Inc. 2017
 */

class PaginationHelper{

	/**
     * Custom Pagination for any object
     *
     *
     * @param $entity joined eloquent models 
     * @param $route_path calling page to handle data
     *
     * @return LengthAwarePaginator instance of the object
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     */
	public static function getObjectPagination($entity, $route_path){
		
		// limit defined in 
		$limit = config('constants.pagination.limit');

		 //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($entity);

        //Define how many items we want to be visible in each page
        $perPage = $limit;

        //Slice the collection to get the items to display in current page
        // $currentPageSearchResults = $collection->slice($currentPage * $perPage, $perPage)->all();
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
    
        //Create our paginator and pass it to the view
        return new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage, $currentPage, [
            'path' => $route_path] );

	}

}