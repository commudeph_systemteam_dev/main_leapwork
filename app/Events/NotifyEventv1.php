<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
/**
 * Just implement the ShouldBroadcast interface and Laravel will automatically 
 * send it to Pusher once we fire it 
**/
class NotifiyEventv1 implements ShouldBroadcast 
{
    use SerializesModels;

    /**
     * Only (!) Public members will be serialized to JSON and sent to Pusher
    **/
    public $message;
    public $channelID;
    public $senderID;
    public $messageID;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message, $channelID, $senderID)
    {
        $this->message = $message;
        $this->channelID = $channelID;
        $this->senderID = $senderID;
        $this->messageID = $messageID;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [$this->channelID];
    }
}