var notificationContainer;
var timezone = 'Asia/Manila';
var bellNotification;
var noNotificationMsg;

$(function () {
    notificationContainer = $('#notificationContainer');
    bellNotification = $('#adminBellNotification');
    noNotificationMsg = $('#noNotificationsMsg');

    makePusherEvent('admin-notification', 'new-company', adminNotificationNewCompanyCallback);
});

const adminNotificationNewCompanyCallback = function (data) {

    var htmlMarkUp = '';

    htmlMarkUp += '<li class="item">';

    htmlMarkUp += ' <p style="text-align: right;">'
    htmlMarkUp += moment(data.created_at.date).tz(timezone).fromNow();
    htmlMarkUp += ' </p>'
    htmlMarkUp += ' <a style="text-align: left; color:black" href="' + APP_URL + '/admin/companies/' + data.company_id + '/?notification_id=' + data.notification_id + '">';
    htmlMarkUp += '     <strong>' + data.company_name + '</strong><br>New Company Registered'
    htmlMarkUp += ' </a>'
    htmlMarkUp += '</li>';

    notificationContainer.prepend(htmlMarkUp);

    bellNotification.addClass('unread');
    noNotificationMsg.hide();
}