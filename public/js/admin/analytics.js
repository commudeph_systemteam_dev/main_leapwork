/**
 * analytics.js
 * Analytics JS functions
 * @author Ian Gabriel Sebastian
 * @return View
 */

/**
* graphAnalytics
* Render line graph by AJAX
* used chart.js
* @author Ian Gabriel Sebastian
* @return View
*/
function graphAnalytics(successData)
{
	const months = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"];
	var monthLabels = [];
	var monthlyData =JSON.parse($('#txt-montly_data').val());

	var m = new Date();

	var monthlyRevenue = [];
	var monthlyDeferredRevenue = [];
	var monthlyEstSales = [];
	
	for(var i = 0; i <= m.getMonth() ; i++)
	{
		monthlyRevenue.push(monthlyData['monthlyRevenue'][i+1]);
		monthlyDeferredRevenue.push(monthlyData['monthlyDeferredRevenue'][i+1]);
		monthlyEstSales.push(monthlyData['monthlyEstSales'][i+1]);
		monthLabels.push(months[i]);
	}

	var ctx = document.getElementById("analyticsChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',//kind of chart
		data: {
			labels: monthLabels,
			datasets: [
			// dataset for Page Views
			{
				label: 'Monthly Estimated Sales',
				data: monthlyEstSales,
				fill: false,
				backgroundColor: [
					'rgb(255,255,255)'
				],
				borderColor: [
					'rgb(0,0,255)'
				],
				borderWidth: 1
            },
            {
				label: 'Monthly Revenue',
				data: monthlyRevenue,
				fill: false,
				backgroundColor: [
					'rgb(255,255,255)'
				],
				borderColor: [
					'rgb(0,255,0)'
				],
				borderWidth: 1
            },
            {
				label: 'Monthly Deferred Revenue',
				data: monthlyDeferredRevenue,
				fill: false,
				backgroundColor: [
					'rgb(255,255,255)'
				],
				borderColor: [
					'rgb(255,0,0)'
				],
				borderWidth: 1
			}
		 
		  ]//datasets
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'Sales Chart'
			},
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					display: true,
				}]
			}
		}
	});
}


$(document).ready(function() {
	$.ajax({
		url: APP_URL+"/admin/home/",
		success: function(successData) {
			graphAnalytics(successData);
		},
		error:function(data) {
		},
		type: 'GET'
	});
});//end document ready