 /**
  * Handlers for Master Admin User Management
  *
  * @summary  Master settings - user management js
  *
  * @author  Karen Irene Cano <karen_cano@commude.ph>
  * @updated 2017-10-17
  * @link     URL
  * @since    2017-10-17
  *
  */

 /**
  * Sets view as Corporate User edit on page load
  * @summary if admin selects Corpo User to edit load corpo edit page not appli
  * user page
  * @since 2017-12-19
  * @author  Arvin Jude Alipio <aj_alipio@commude.ph>
  */
 $(document).ready(function () {
 	var accountType = $("[name='user_account_type']").val();
 	console.log(accountType);
 	if (accountType == 'Corporate User') {
 		$('.company__row').css("display", "table-row");
 	} else {
 		$('.company__row').hide();
 	}

 	$(".user__modal_container").hide();
 });

 function confirmDelete(e) {
	$('#txt_user_id').val($(e).data('id'));
 	$('.confirm__modal_container').fadeIn('slow', function () {
 		$('.confirm__modal_dialog').fadeIn('slow');
 		// $('#delete_user').attr('href', href);
 		$('.spare_user_delete').click(function () {
 			$('.confirm__modal_container').fadeOut('slow');
 			$('.confirm__modal_dialog').fadeOut('slow');
 			return false;
		 });
		$('.btn_close_delete').click(function () {
			$('.confirm__modal_container').fadeOut('slow');
			$('.confirm__modal_dialog').fadeOut('slow');
			return false;
		});
 	});
 }



 function changePassword(href) {
 	$('.confirm__modal_container').fadeIn('slow', function () {
 		$('.confirm__modal_dialog').fadeIn('slow');
 		$('#delete_user').attr('href', href);
 		$('#btn_close').click(function () {
			 $('.confirm__modal_container').fadeOut('slow');
			 $('#spPassword').hide();
 			return false;
 		})
 		$('#spare_user').click(function () {
 			$('.confirm__modal_container').fadeOut('slow');
			 $('.confirm__modal_dialog').fadeOut('slow');
			 $('#spPassword').hide();
 			return false;
 		})
 	});
 }

 function displayUserModal(id) {
 	$('#show_user_modal_' + id).fadeIn('slow', function () {
 		$('.user__modal_dialog').fadeIn('slow');
		  $('#close_modal_applicant, #close_modal_corporate, #close_modal_other ,.userInfoClose').click(function () {
 			$('.user__modal_container').fadeOut('slow');
 			$('.user__modal_dialog').fadeOut('slow');
 			return false;
 		})
 	});
 }

 /**
  * updateAccountStatus
  * @summary onchange of accountType dropdown list
  * @since 2017-10-17
  * @access admin rights
  * @author  Karen Irene Cano<karen_cano@commude.ph>
  * @link URL
  * @fires
  * @listens onchange accountType 
  */
 function updateAccountStatus(newAccountType, userId, urlServer) {
 	$.ajax({
 		url: urlServer + "/admin/master-admin/user/update/" + userId + "/" + newAccountType,
 		success: function (data) {
 			console.log(data);
 			$('#divAccountMessage').addClass('alert alert-info');
 			$('#divAccountMessage').html(data["successMessage"]);
 			$("#divAccountMessage").fadeTo(2000, 900).slideUp(900, function () {
 				$("#divAccountMessage").slideUp(900);
 			});
 		},
 		error: function (data) {
 			$('#divAccountMessage').addClass('alert alert-danger');
 			$('#divAccountMessage').html(data["errorMessage"]);
 			$("#divAccountMessage").fadeTo(2000, 900).slideUp(900, function () {
 				$("#divAccountMessage").slideUp(900);
 			});
 		},
 		type: 'GET'
 	});
 } //endof updateAccountStatus

 function toggleUserType() {
 	$('select.user_account_type').on('change', function () {
 		if (this.value == 'Corporate User') {
 			$('.company__row').css("display", "table-row");
 		} else {
 			$('.company__row').hide();
 		}
 	});
 }

 $(function () {
 	toggleUserType();
 }); // ready


 function validateUpdatePasswordForm() {

 	var passed = true;

 	var $txtPassword = $("#txt_password");
 	var $txtConfirmPassword = $("#txt_password_confirmation");
 	var $spPassword = $('#spPassword');

 	if (!$txtPassword.val() && !$txtConfirmPassword.val()) {
 		passed = false;
 		$spPassword.find('.text').text('Please fill-up password fields.');
 		$spPassword.show();
 		$txtPassword.focus();
 	} else if (!$txtPassword.val() && $txtConfirmPassword.val()) {
 		passed = false;
 		$spPassword.find('.text').text('Please enter password.');
 		$spPassword.show();
 		$txtPassword.focus();
 	} else if ($txtPassword.val() && !$txtConfirmPassword.val()) {
 		passed = false;
 		$spPassword.find('.text').text('Please enter Confirm Password.');
 		$spPassword.show();
 		$txtPassword.focus();
 	} else if ($txtPassword.val() != $txtConfirmPassword.val()) {
 		passed = false;
 		$spPassword.find('.text').text('Password is Mismatch.');
 		$spPassword.show();
 		$txtPassword.focus();
 	} else if (!$txtPassword.val() || $txtPassword.val().length < 6) {
 		passed = false;
 		$spPassword.find('.text').text('Password must be 6 characters');
 		$spPassword.show();
 		$txtPassword.focus();
 	} else
 		$spPassword.hide();

 	return passed;
 }





 /**
  * Add User (Master Admin) validation
  * Validates and prompts Master Admin if there are empty fields
  * @author    Arvin Jude Alipio <aj_alipio@commude.ph>
  * @copyright 2017 Commude
  * @since     2017-29-11
  */
 function validate() {
 	var passed = [12];
 	passed.fill(true);
 	var passedAll = true;
 	var newUser = false;

 	var accountType = $("[name='user_account_type']");
 	var firstName = $("[name='firstName']");
 	var validator_firstName = $('#firstNameErr');
 	var lastName = $("[name='lastName']");
 	var validator_lastName = $('#lastNameErr');
 	var email = $("[name='txt_email']");
 	var validator_email = $('#emailErr');
	var password = $("[name='txt_password']");
	var confirmPassword = $("[name='txt_password_confirmation']");
 	var validator_password = $('#passwordErr');
 	var contactNumber = $("[name='contactNumber']");
 	var validator_contactNo = $('#contactErr');
 	var companyName = $("[name='company_name']");
 	var validator_companyName = $('#companyNameErr');
 	var designation = $("[name='user_designation']");
 	var validator_designation = $('#designationErr');
 	var postalCode = $('#postalCode');
 	var validator_postalCode = $('#postalCodeErr');
 	var address1 = $('#address1');
 	var validator_address1 = $('#address1Err');
 	var address2 = $('#address2');
 	var validator_address2 = $('#address2Err');
 	var $txtChangePasswordFlag = $('#change_password_flag');

 	if ($('#newuser').val() == "new") {
 		newUser = true;
 	}

 	if (firstName.val() != "") {
 		validator_firstName.addClass('hide');
 	} else {
 		validator_firstName.removeClass('hide');
 		firstName.focus();
 		passed[0] = false;
 	}
 	if (lastName.val() != "") {
 		validator_lastName.addClass('hide');
 	} else {
 		validator_lastName.removeClass('hide');
 		lastName.focus();
 		passed[1] = false;
 	}
 	if (email.val() == "") {
 		validator_email.removeClass('hide');
 		$('#spUseremail').hide();
 		email.focus();
 		passed[2] = false;
 	} else if (!isValidEmail(email.val())) {
 		validator_email.addClass('hide');
 		$('#spUseremail .text').text('Invalid email format');
 		$('#spUseremail').show();
 		email.focus();
 		passed[2] = false;
 	} else {
 		validator_email.addClass('hide');
 		$('#spUseremail').hide();
 	}

 	// if ((password.val()).replace(/\s/g, '').length && (password.val()).split('').length < 1) {
 	if (newUser) {
 		if ((password.val()).trim() != "") {
 			validator_password.addClass('hide');
 			if ((password.val()).indexOf(' ') != 0) {
 				validator_password.addClass('hide');
 			} else {
 				validator_password.text('Password can not be and can not contain whitespaces');
 				validator_password.css('font-weight', 'bold');
 				validator_password.removeClass('hide');
 				password.focus();
 				passed[11] = false;
			 }
			 
				if (password.val() != confirmPassword.val()) {
					validator_password.text('Password mismatch');
					validator_password.css('font-weight', 'bold');
					validator_password.removeClass('hide');
					password.focus();
					passed[12] = false;
				}
 		} else {
 			validator_password.text('Password is required');
 			if ((password.val()).indexOf(' ') == 0) {
 				validator_password.text('Password can not be and can not contain whitespaces');
 			}
 			validator_password.css('font-weight', 'bold');
 			validator_password.removeClass('hide');
 			password.focus();
			 passed[10] = false;
			 
			if (password.val() != confirmPassword.val()) {
				validator_password.text('Password mismatch');
				validator_password.css('font-weight', 'bold');
				validator_password.removeClass('hide');
				password.focus();
				passed[12] = false;
			}
		 }
		 
		
 	}


 	// if($txtChangePasswordFlag.val() == 1)
 	// {
 	// 	if(!password.val())
 	// 	{
 	// 		validator_password.removeClass('hide');
 	// 		password.focus();
 	// 		passed[6] = false;
 	// 	}
 	// 	else
 	// 		validator_password.addClass('hide');
 	// }

 	if (postalCode.val() == "") {
 		validator_postalCode.removeClass('hide');
 		postalCode.focus();
 		passed[7] = false;
 	} else {
 		validator_postalCode.addClass('hide');
 	}
 	if (address1.val() == "") {
 		validator_address1.removeClass('hide');
 		address1.focus();
 		passed[8] = false;
 	} else {
 		validator_address1.addClass('hide');
 	}
 	if (address2.val() == "") {
 		validator_address2.removeClass('hide');
 		address2.focus();
 		passed[9] = false;
 	} else {
 		validator_address2.addClass('hide');
 	}
 	// if(contactNumber.val() == ""){
 	// 	validator_contactNo.removeClass('hide');
 	// 	$('#spContact').hide();
 	// 	contactNumber.focus();
 	// 	passed[3] = false;
 	// }else if(!isValidPhone(contactNumber.val())){
 	// 	validator_contactNo.addClass('hide');
 	// 	$('#spContact .text').text('Invalid phone format. Ex. +63##########');
 	// 	$('#spContact').show();
 	// 	contactNumber.focus();
 	// 	passed[3] = false;
 	// }
 	// else{
 	// 	validator_contactNo.addClass('hide');
 	// 	$('#spContact').hide();
 	// }


	 var phoneNumber = true;
	 var phonepattern = /^-?\d+\.?\d*$/;
	 
 	if (contactNumber.val() == "") {
 		validator_contactNo.removeClass('hide');
 		$('#spContact').hide();
 		contactNumber.focus();
 		passed[4] = false;
	  } else if (!phonepattern.test(contactNumber.val())) {
 		validator_contactNo.addClass('hide');
		$('#spContact .text').text('Invalid phone number. Please enter numerical values only.');
 		$('#spContact').show();
 		contactNumber.focus();
 		passed[4] = false;
 	}  


 	if (accountType.val() == "Corporate User") {
 		if (companyName.val() != null) {
 			validator_companyName.addClass('hide');
 		} else {
 			validator_companyName.removeClass('hide');
 			companyName.focus();
 			passed[4] = false;
 		}
 		if (designation.val() != "") {
 			validator_designation.addClass('hide');
 		} else {
 			validator_designation.removeClass('hide');
 			designation.focus();
 			passed[5] = false;
 		}
 	}



 	for (var i = 0; i < passed.length; i++) {
 		if (!passed[i]) {
 			passedAll = false;
 		}
 	}

 	if (passedAll) {
 		return true;
 	} else {
 		return false;
 	}
 }

 function emailExists(userId, email) {
 	$.ajax({
 		type: "GET",
 		url: APP_URL + '/admin/master-admin/user/emailCheck',
 		data: {
 			userId: userId,
 			email: email
 		},
 		success: function (data) {
 			console.log('success: ' + data);

 			if (data == 1) {
 				$('#spUseremailAjax .text').text('Email already exists');
 				$('#spUseremailAjax').show();
 			} else {
 				$('#spUseremailAjax .text').text('');
 				$('#spUseremailAjax').hide();
 			}
 		},
 		error: function (data) {
 			console.log('Error emailExists:', data);
 		}
 	});
 }


 $(function () {

	 $('.remove_user').click(function () {
		 var $this = $(this);
		 var value = APP_URL + '/admin/master-admin/user/delete/' + $('#txt_user_id').val();
		 window.location.href = value;
	 });

 	$('#btnAdd').click(function () {
 		if (validate() == true) {
 			console.log("Success");
 			var method = "post";
 			var enctype = "multipart/form-data";
 			var form = document.getElementById("userCreateForm");
 			form.submit();
 		}
 	});

 	$("#activate").click(function () {

 	});

 	$('#txt_email').blur(function () {
 		var userId = $("#user_id").val();
 		var txtUserEmail = $("#txt_email").val().trim();

 		emailExists(userId, txtUserEmail);
 		console.log("Hi");
 		console.log(userId);
 	});

 	$('#aChangePassword').click(function () {
 		var $this = $(this);
 		var $dvChangePassword = $("#dvChangePassword");
 		var $changePasswordFlag = $('#change_password_flag');

 		$this.toggleClass('hidden');
 		$dvChangePassword.toggleClass('hidden');
 		$changePasswordFlag.val(1);
 	});


 	$('#btnCancelChangePassword').click(function () {
 		var $txtPassword = $("#txt_password");
 		var $dvChangePassword = $("#dvChangePassword");
 		var $aChangePassword = $("#aChangePassword");
 		var $changePasswordFlag = $('#change_password_flag');

 		$dvChangePassword.toggleClass('hidden');
 		$aChangePassword.toggleClass('hidden');
 		$txtPassword.val('');
 		$changePasswordFlag.val(0);
 	});

 	$('#btnSavePassword').click(function () {
 		if (validateUpdatePasswordForm()) {
 			var form = document.getElementById('userCreateForm');
 			form.action = APP_URL + '/admin/master-admin/user/savePassword';
 			form.submit();
 		}
 	});

 });