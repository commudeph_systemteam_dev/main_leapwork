 
 /**
 * @summary Add master feature form form validation. 
 * 
 * @since 2017-11-21
 * @access private
 *
 * @fires svalidator validators
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateForm()
{
	var passed = true;
	
	var $txtTitle    = $("#txt_job_feature_title");
	var $filePc      = $("#file_job_feature_kv_pc");
	var $fileSp      = $("#file_job_feature_kv_sp");
	var $fileEc      = $("#file_job_feature_ec_imagepath");
	var $txtMessage  = $("#txt_job_feature_message");
	var $txtDateFrom = $("#txt_job_feature_datefrom");
	var $txtDateTo   =  $('#txt_job_feature_dateto');

	if(!$txtTitle.val().trim())
	{
		passed = false;
		$('#spTitle .text').text('Title is required');
		$('#spTitle').show();
		$txtTitle.focus();
	}
	else
		$('#spTitle').hide();

	if($filePc.val() && inAllowedImageExtensions($filePc.val()))
	{
		passed = false;
		$('#spPcImagePath .text').text('Invalid file extension.');
		$('#spPcImagePath').show();
		$filePc.focus();
	}
	else
		$('#spPcImagePath').hide();

	if($fileSp.val() && inAllowedImageExtensions($fileSp.val()))
	{
		passed = false;
		$('#spSpImagePath .text').text('Invalid file extension.');
		$('#spSpImagePath').show();
		$fileSp.focus();
	}
	else
		$('#spSpImagePath').hide();
	

	if($fileEc.val() && inAllowedImageExtensions($fileEc.val()))
	{
		passed = false;
		$('#spEcImagePath .text').text('Invalid file extension.');
		$('#spEcImagePath').show();
		$fileEc.focus();
	}
	else
		$('#spEcImagePath').hide();
	
	if(!$txtDateFrom.val().trim() || !$txtDateTo.val().trim())
	{
		passed = false;
		$('#spDateRange .text').text('Period range is required');
		$('#spDateRange').show();
	}
	else if(!isValidDate($txtDateFrom.val().trim()) || 
			!isValidDate($txtDateTo.val().trim()))
	{
		passed = false;
		$('#spDateRange .text').text('Invalid date format');
		$('#spDateRange').show();
		$txtDateFrom.parent().focus();
	}
	else
		$('#spDateRange').hide();

	if(!$txtMessage.val().trim())
	{
		passed = false;
		$('#spMessage .text').text('Last name is required');
		$('#spMessage').show();
		$txtMessage.focus();
	}
	else
		$('#spMessage').hide();


	return passed;
    
}//end 


function getDetails(urlServer) {
	var job_id = $('#job_title').val();

	$.ajax({
		url: urlServer + "/admin/master-admin/searchJobDetails/" + job_id,
		success: function(data) {
			var json_data = JSON.parse(data);
			document.getElementById("overview").innerHTML  			= json_data.job_post_overview;
			document.getElementById("qualifications").innerHTML  	= json_data.job_post_qualifications;
			document.getElementById("category").innerHTML  			= json_data.master_job_classification_name;
			document.getElementById("salary").innerHTML  			= "php " + json_data.job_post_salary_min + " - php " + json_data.job_post_salary_max;
			document.getElementById("vacancy").innerHTML  			= json_data.job_post_no_of_positions;
			document.getElementById("process").innerHTML  			= json_data.job_post_process;
			document.getElementById("location").innerHTML  			= json_data.master_job_location_name + ", " + json_data.master_country_name;
			
			document.getElementById("hours").innerHTML  			= fixTime(json_data.job_post_work_timestart) + " - " + fixTime(json_data.job_post_work_timeend); // json_data.job_post_work_timestart + " to " + json_data.job_post_work_timeend;
			
			document.getElementById("benefits").innerHTML  			= json_data.job_post_benefits;
			document.getElementById("holiday").innerHTML  			= json_data.job_post_holiday;
			$(".tr").removeClass("hidden");
		},
		error:function(data) {   
			console.log(data);
		},
		type: 'GET'
	});
}

function fixTime(value) {

	var newTime = value.substring(0, value.length - 3);
	return newTime;
   
    // alert(strTime);
}

 /**
 * @summary  admin/mastter-admin/feature js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 2017-10-16
 * @link     URL
 * @since    2017-10-16
 * @requires jquery-1.12.0.min.js
 *
 */
 
$(function(){
	
 	/**
	 * @summary UL click event handler for Job Feature.
	 * Handle and prepare custom validations here. Validation done
	 * via form
	 *
	 * @since 2017-10-13
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires
	 * @listens btnSaveJobPosition click
	 *
	 */
	$('#btnSaveJobFeature').click(function () 
	{
		if (validateForm())
		{
	     	var form    = document.getElementById('frmMasterFeature');

			form.action =  APP_URL + '/admin/master-admin/saveJobFeature';
	    	form.submit();			
		}

	});
	
	$("#tab-box1-link").click(function(){
		$("#tab-box2").css("display", "none");
	})
	

});


