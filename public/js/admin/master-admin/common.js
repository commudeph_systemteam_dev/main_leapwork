 /**
 * @summary  master-admin shared js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 2017-10-16
 * @link     URL
 * @since    2017-10-16
 * @requires jquery-1.12.0.min.js
 *
 */
 
$(function(){
	
	/**
	 * @summary Sub content nav bar click event
	 *
	 * @since 2017-10-16
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @link URL
	 * 
	 * @fires
	 * @listens ul.menu click
	 *
	 */
 	$(".tab-menu li a").on("click", function() {
        $(".tab-menu li a").removeClass("active");
        $(this).addClass("active");
        $(".tab-boxes>div.box").hide();
        $($(this).attr("href")).fadeToggle();
	});
	
	$("header#global-header div.inner ul.function-menu li.function span.icon").unbind('click').click(function(e) {
		e.stopPropagation();
	  	$('#tooltipUser').css({"opacity": "1"});
	  	$('#tooltipAlert').css({"opacity": "1"});
	  	
	    $(this).next().fadeToggle('fast');
	    $(this).toggleClass("active");

	  });

 	
});


$('#btnUserLogo').click(function(){
	$('#userDropdown').toggleClass('open');
});