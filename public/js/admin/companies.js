

$(document).ready(function(){

	$("#btnSaveCompany").hide();
	$(".btnRemoveCompanyFile").hide();

	$("#btnRemoveCompanyLogo").hide();
	$("#btnRemoveCompanyBanner").hide();

	$('.alert__modal_container').fadeIn('slow', function() {
		$('.confirm__modal_dialog').fadeIn('slow');
		$('#btn_alert_close').click(function(){
			$('.alert__modal_container').fadeOut('slow');
			return false;
		})
  	});
});

 /**
 * @summary selectPlan()
 * Adds active class on selected plan type
 * @since 2017-10-22
 * @author  Nicole Manalo <nicole_manalo@commude.ph>
 */

function selectPlan() {
    
   $("ul.js-plan-list li").on("click",function() {
		var $this = $(this);
		var selectedPlan = $this.find('input[type="radio"]').prop("checked", true).val();

		if(selectedPlan == 100){
			$('#payment_method').hide();
		}else{
			$('#payment_method').show();
		}
        $this.find('input[type="radio"]').prop("checked", true);
        $('ul.js-plan-list li').removeClass('active');
        $this.addClass('active');
    });
}

 /**
 * @summary Corprorate Profile update form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-12-05
 * @access private
 *
 * @fires svalidator.trimAllInputs()
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * 
 */
function validateUpdatePasswordForm()
{

	var passed = true;

	var $txtPassword        = $("#txt_password");
	var $txtConfirmPassword        = $("#txt_password_confirmation");
	var $spPassword       = $('#spPassword');

	if(!$txtPassword.val() && !$txtConfirmPassword.val())
	{
		passed = false;
		$spPassword.find('.text').text('Please fill-up password fields.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if(!$txtPassword.val() && $txtConfirmPassword.val())
	{
		passed = false;
		$spPassword.find('.text').text('Please enter password.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if($txtPassword.val() && !$txtConfirmPassword.val()){
		passed = false;
		$spPassword.find('.text').text('Please enter Confirm Password.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if($txtPassword.val() != $txtConfirmPassword.val()){
		passed = false;
		$spPassword.find('.text').text('Password is Mismatch.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if(!$txtPassword.val()|| $txtPassword.val().length < 6){
		passed = false;
		$spPassword.find('.text').text('Password must be 6 characters');
		$spPassword.show();
		$txtPassword.focus();
	}
	else
		$spPassword.hide();
	
	return passed;
}


function validateUpdateForm()
{
	var passed = true;
	
	var $txtCompanyName     = $("#txt_company_name");
	var $txtEmail           = $("#txt_company_email");
	var $txtPostalCode      = $("#txt_company_postalcode");
	var $txtContactNo       = $("#txt_company_contact_no");
	var $txtDateFounded     = $("#txt_company_date_founded");

	var $companyFile1 = $('#companyFile1');
	var $companyFile2 = $('#companyFile2');
	var $companyFile3 = $('#companyFile3');

	var trCompanyFilesCount = $('.js-file_link').length; //existing files count

	//flags
	var $txtChangePasswordFlag = $('#change_password_flag');

	//spans
	var $spCompanyName    = $("#spCompanyName");
	var $spPostalCode     = $("#spPostalCode");
	var $spEmail          = $("#spEmail");
	var $spContactNo      = $('#spContactNo');
	
	var $spDateFounded    = $('#spDatefounded');
	var $spBusinessPermit = $('#spBusinessPermit');

	var updLogo_error		   	=	validateFormImages(
		document.getElementById('file_company_logo'),
		'js-company_logo-error',
		'js-company_logo-error_list'
	);

	var updBanner_error			=	validateFormImages(
		document.getElementById('file_company_banner'),
		'js-company_banner-error',
		'js-company_banner-error_list'
	);

	trimAllInputs();

	if(!$txtCompanyName.val())
	{
		passed = false;
		$spCompanyName.find('.text').text('Company name is required');
		$spCompanyName.show();
		$txtCompanyName.focus();
	}
	else
		$spCompanyName.hide();

	if($txtPostalCode.val() && !isValidPostalCode($txtPostalCode.val()))
	{
		passed = false;
		$spPostalCode.find('text').text('Invalid Postal Code. Ex. ####');
		$spPostalCode.show();
		$txtPostalCode.focus();
	}
	else
		$spPostalCode.hide();
	
	if(!$txtEmail.val())
	{
		passed = false;
		$spEmail.find('.text').text('Email address is required');
		$spEmail.show();
		$txtEmail.focus();
	}
	else if(!isValidEmail($txtEmail.val()))
	{
		passed = false;
		$spEmail.find('.text').text('Invalid email format');
		$spEmail.show();
		$txtEmail.focus();
	}
	else if($spEmail.find('.text').text().length > 0 )
	{
		passed = false;
		$txtEmail.focus();
		$spEmail.hide();
	}
	else
	{
		$spEmail.find('.text').text('');
		$spEmail.hide();
	}

	//if filled up

	if($txtDateFounded.val() && !isValidDate($txtDateFounded.val()))
	{
		passed = false;
		$spDateFounded.find('.text').text('Invalid date format');
		$spDateFounded.show();
		$txtDateFounded.focus();
	}
	else
		$spDateFounded.hide();

	if (!$txtContactNo.val()) 
	{
		passed = false;
		$spContactNo.find('.text').text('Contact number is required');
		$spContactNo.show();
		$txtContactNo.focus();
	}
	else if($txtContactNo.val().length < 10) //&& !isValidPhone($txtContactNo.val())
	{
		passed = false;
		$spContactNo.find('.text').text('Invalid phone format.'); // Ex. +63##########
		$spContactNo.show();
		$txtContactNo.focus();
	}
	else
		$spContactNo.hide();

	if($txtChangePasswordFlag.val() == 1)
	{
		if(!$txtPassword.val()|| $txtPassword.val().length < 6)
		{
			passed = false;
			$spPassword.find('.text').text('Password must be 6 characters');
			$spPassword.show();
			$txtPassword.focus();
		}
		else
			$spPassword.hide();
	}

	if(!validateFormFiles())
	{
		passed = false;
	}

	if(!updLogo_error){
		document.getElementById("file_company_logo").focus();
		passed = false;
	}	

	if(!updBanner_error){
		document.getElementById("file_company_banner").focus();
		passed = false;
	}	

	return passed;
    
}


 /**
 * @summary Corprorate Profile create form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-12-15
 * @access private
 *
 * @fires svalidator.trimAllInputs()
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * 
 */
function validateCreateForm()
{
	var passed = true;
	
	var $txtCompanyName  = $("#txt_company_name");
	var $txtEmail        = $("#txt_company_email");
	var $txtPostalCode   = $("#txt_company_postalcode");
	var $txtContactNo    = $("#txt_company_contact_no");
	var $txtDateFounded  = $("#txt_company_date_founded");
	var $soPaymentMethod = $('#so_payment_method');
	var $rbPlanType      = $('input[name="rb_plan_type"]:checked');
	var $fileBusinessPermit = $('.company__file');
	var $txtPassword = $("#txt_password");
	var $txtConfirmPassword = $("#txt_password_confirmation");

	//spans
	var $spPassword = $('#spPassword');
	var $spCompanyName = $("#spCompanyName");
	var $spPostalCode = $("#spPostalCode");
	var $spEmail = $("#spEmail");
	var $spContactNo = $('#spContactNo');
	var $spDateFounded = $('#spDatefounded');
	var $spPlanType = $('#spPlanType');
	var $spPaymentMethod = $('#spPaymentMethod');
	var $spBusinessPermit = $('#spBusinessPermit');

	
	var updLogo_error		   	=	validateFormImages(
		document.getElementById('file_company_logo'),
		'js-company_logo-error',
		'js-company_logo-error_list'
	);

	var updBanner_error			=	validateFormImages(
		document.getElementById('file_company_banner'),
		'js-company_banner-error',
		'js-company_banner-error_list'
	);
	
	trimAllInputs();

	if(!$txtCompanyName.val())
	{
		passed = false;
		$spCompanyName.find('.text').text(view.validations['company']['name']['required']);
		$spCompanyName.show();
		$txtCompanyName.focus();
	}
	else
		$spCompanyName.hide();

	if($txtPostalCode.val() && !isValidPostalCode($txtPostalCode.val()))
	{
		passed = false;
		$spPostalCode.find('text').text('Invalid Postal Code. Ex. ####');
		$spPostalCode.show();
		$txtPostalCode.focus();
	}
	else
		$spPostalCode.hide();
	
	if(!$txtEmail.val())
	{
		passed = false;
		$spEmail.find('.text').text('Email address is required');
		$spEmail.show();
		$txtEmail.focus();
	}
	else if(!isValidEmail($txtEmail.val()))
	{
		passed = false;
		$spEmail.find('.text').text('Invalid email format');
		$spEmail.show();
		$txtEmail.focus();
	}
	else if($spEmail.find('.text').text().length > 0 )
	{
		passed = false;
		$txtEmail.focus();
		$spEmail.hide();
	}
	else
	{
		$spEmail.find('.text').text('');
		$spEmail.hide();
	}

	//if filled up

	if($txtDateFounded.val() && !isValidDate($txtDateFounded.val()))
	{
		passed = false;
		$spDateFounded.find('.text').text('Invalid date format');
		$spDateFounded.show();
		$txtDateFounded.focus();
	}
	else
		$spDateFounded.hide();

	var phonepattern = /^-?\d+\.?\d*$/;
	if (!$txtContactNo.val()) 
	{
		passed = false;
		$spContactNo.find('.text').text('Contact number is required');
		$spContactNo.show();
		$txtContactNo.focus();
	}
	else if (!phonepattern.test($txtContactNo.val())) //&& !isValidPhone($txtContactNo.val()), $txtContactNo.val().length < 10 || 
	{
		passed = false;
		$spContactNo.find('.text').text('Invalid phone format. Please enter numerical values only.'); // Ex. +63##########
		$spContactNo.show();
		$txtContactNo.focus();
	}
	// else if($txtContactNo.val() && !isValidPhone($txtContactNo.val()))
	// {
	// 	passed = false;
	// 	$spContactNo.find('.text').text('Invalid phone format. Ex. +63##########');
	// 	$spContactNo.show();
	// 	$txtContactNo.focus();
	// }
	else
		$spContactNo.hide();

	if(!$rbPlanType.is(':checked'))
	{
		passed = false;
		$spPlanType.find('.text').text('Plan Type is required');
		$spPlanType.show();
		$rbPlanType.focus();
	}
	else
		$spPlanType.hide();

	if(!$soPaymentMethod.val() && $rbPlanType.val() != 100)
	{
		passed = false;
		$spPaymentMethod.find('.text').text(view.validations['company']['payment-method']['required']);
		$spPaymentMethod.show();
		$soPaymentMethod.focus();
	}
	else
		$spPaymentMethod.hide();

	if(!validatePasswordFields())
		passed = false;

	//file first if has content
	if(!$fileBusinessPermit.val())
	{
		passed = false;

		$spBusinessPermit.find('.text').text('At least one business permit should be uploaded');
		$spBusinessPermit.show();
	}
	else if(!validateFormFiles()) // perform uploaded file checks
		passed = false;
	else
		$spBusinessPermit.hide();

	//validate images
	if(!updLogo_error){
		document.getElementById("file_company_logo").focus();
		passed = false;
	}	

	if (!$txtPassword.val() && !$txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Please fill-up password fields.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if (!$txtPassword.val() && $txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Please enter password.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if ($txtPassword.val() && !$txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Please enter Confirm Password.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if ($txtPassword.val() != $txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Password is Mismatch.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if (!$txtPassword.val() || $txtPassword.val().length < 6) {
		passed = false;
		$spPassword.find('.text').text('Password must be 6 characters');
		$spPassword.show();
		$txtPassword.focus();
	}
	else
		$spPassword.hide();

	if(!updBanner_error){
		document.getElementById("file_company_banner").focus();
		passed = false;
	}	

	var fileNames = [];
	var sizeLimit = constants.MAX_FILE_UPLOAD;

	// Loop through files
	for (var i = 1; i <= 3; i++) {

		var companyFile = document.getElementById('companyFile' + i);

		// Proceed to file attribute valiation only if file is uploaded
		if (companyFile.value) {

			// File size check
			if (companyFile.files[0].size > sizeLimit) {
				passed = false;
				$spBusinessPermit.find('.text').text('Cannot upload a file greater than 10mb');
				$spBusinessPermit.show();
			} else{
				$spBusinessPermit.hide();
			}
		}
	}

	return passed;
    
}

/**
 * @summary Password check for reuse
 * 
 * @since 2017-12-15
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validatePasswordFields()
{
	var passed = true;
	var $txtPassword = $("#txt_password");
	var $spPassword  = $('#spPassword');

	if(!$txtPassword.val()|| $txtPassword.val().length < 6)
	{
		passed = false;
		$txtPassword.focus();

		$spPassword.find('.text').text('Password must be 6 characters');
		$spPassword.show();		
	}
	else
		$spPassword.hide();

	return passed;
}

/**
 * @summary Validate business permits 
 * 
 * @since 2017-12-06
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateFormFiles()
{
	var passed = true;
	
	var sizeLimit = constants.MAX_FILE_UPLOAD;
	
	var spCompanyFile = document.getElementsByClassName('js-company_file-error')[0];
	var currentFiles = document.getElementsByClassName('js-file_link');
	var currentFilsCount =  currentFiles.length;
	
	var spFileErrors = document.getElementsByClassName('js-company_file-error_list')[0];

	var fileNames = [];

	// Single file uploads
	if(document.getElementById('companyFile'))
	{
		// Loop through files
		for (var i = 1; i <= 3; i++) {

			var companyFile = document.getElementById('companyFile' + i);

			// Proceed to file attribute valiation only if file is uploaded
			if (companyFile.value) {

				// File size check
				if (companyFile.files[0].size > sizeLimit) {
					passed = false;
					spCompanyFile.style.display = "block";
					spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorFileFormat;
				}

				// File extension check
				// svalidator.inAllowedFileExtensions
				if (!(inAllowedFileExtensions(companyFile.value))) {
					passed = false;
					spCompanyFile.style.display = "block";
					spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorFormat;
				}

				// Duplicate file name check
				// svalidator.isInArray
				if (isInArray(companyFile.files[0].name, fileNames)) {
					passed = false;
					spCompanyFile.style.display = "block";
					spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorDuplicate;
				}

				// Assume file count since value exists
				fileNames.push(companyFile.files[0].name);
			}
			else{
				break;
			}
		}

		if ((fileNames.length < 1) && currentFilsCount < 1)  {
			passed = false;
			spCompanyFile.style.display = "block";
			spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorRequired;
	
			spCompanyFile.focus();
		}
		else if (passed) {
			spCompanyFile.firstElementChild.innerHTML = '';
			spCompanyFile.style.display = "none";
		}
	}
	
	//Multiple File Upload
	var adminCompanyFile = document.getElementById('file_business_permit');
	
	if(adminCompanyFile)
	{
		var $fileBusinessPermit   = $('#file_business_permit');
		//spans
		var $spBusinessPermit     = $('#spBusinessPermit');
		var $spBusinessPermitMain = $('#spBusinessPermitMain');

		var trCompanyFilesCount = $('.trCompanyFiles').length; //existing files count
		var maxFileCountLimit = 3 - trCompanyFilesCount;
		var maxFileSizeLimit = constants.MAX_FILE_UPLOAD;; //default 10MB
		var totalFileSize    = 0;

		$spBusinessPermit.hide();//reset first

		if($fileBusinessPermit[0].files.length > maxFileCountLimit) //upload count check
		{
			passed = false;
			// $spBusinessPermitMain.text('Maximum upload is 3 only.');
			$spBusinessPermitMain.addClass('red');
			$fileBusinessPermit.focus();
			return passed; //exit immediately
		}
		else
		{
			$spBusinessPermitMain.removeClass('red');
		}

		if($fileBusinessPermit.val())
		{
			//check each file
			for(var i=0;i< $fileBusinessPermit.length;i++) {
		        totalFileSize += ($fileBusinessPermit[i].size || $fileBusinessPermit[i].fileSize);

		        if(!inAllowedFileExtensions($fileBusinessPermit[i].value)) //file extension check
				{
					passed = false;

					$spBusinessPermit.find('.text').text('Upload contains invalid file extension');
					$spBusinessPermit.show();
					$fileBusinessPermit.focus();

					return passed;
				}
				else
					$spBusinessPermit.hide();

		    }

		    if(totalFileSize > maxFileSizeLimit)
			{
				passed = false;
				$spBusinessPermit.find('.text').text('Upload file size is too big. 10MB max');
				$spBusinessPermit.show();
				$fileBusinessPermit.focus();
			}
			else
			{
				$spBusinessPermit.hide();
			}
		}
	}

	
	return passed;
}

/**
 * @summary Validate Image Files
 * 
 * @since 2018-12-06
 * @access private
 *
 * @author  Ian Gabriel Sebastian <ian_sebastian@commude.ph>
 *
 */
function validateFormImages(image, errorMessage, errorList)
{
	var passed = true;
	
	var sizeLimit = constants.MAX_FILE_UPLOAD;

	var errorSpan = document.getElementsByClassName(errorMessage)[0];
	var errorSpan_list = document.getElementsByClassName(errorList)[0];

	var filename;

	// Proceed to file attribute valiation only if file is uploaded
	if (image.value) {

		// File size check
		if (image.files.size > sizeLimit) {
			passed = false;
			errorSpan.style.display = "block";
			errorSpan.firstElementChild.innerHTML = errorSpan_list.dataset.errorFileFormat;
		}

		// File extension check
		// svalidator.inAllowedFileExtensions
		if (inAllowedImageExtensions(image.value)) {
			passed = false;
			errorSpan.style.display = "block";
			errorSpan.firstElementChild.innerHTML = errorSpan_list.dataset.errorFormat;
		}

		// Assume file count since value exists
		fileName = image.files.name;
	}
	if (passed) {
		errorSpan.firstElementChild.innerHTML = '';
		errorSpan.style.display = "none";
	}


	return passed;
}

/**
 * @summary AJAX: Email exist validation 
 *
 * @since 2017-10-27
 * @access global
 *
 * @param int $userId
 * @param string $email
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function emailExistsLocal(userId, email) 
{
	var $spEmail = $('#spEmail');

	 $.ajax({
        type: "GET",
        url: APP_URL + '/emailCheck',
        data: {
        		userId: userId,
        		email:   email 
        	  },
        success: function (data) {
    		console.log('success: ' + data);

    		if(data == 1)
    		{
				$spEmail.find('.text').text('Email already exists');
				$spEmail.show();
    		}
    		else
    		{
    			$spEmail.find('.text').text('');
    			validatePasswordFields();
    		}
    		
        },
        error: function (data) {
            console.log('Error emailExists:', data);
        }
    });

}

/**
 * @summary Company list search form validation.
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-11-09
 * @access private
 *
 * @fires svalidator validators
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateSearchForm()
{
	var passed = true;
	var txtDateRegistered = $("#txt_date_registered").val().trim();
	var txtDateLastLogin  = $("#txt_date_lastlogin").val().trim();
	var txtKeyword        = $("#txt_keyword").val().trim(); //title search for now

	if(txtDateRegistered && !isValidDate(txtDateRegistered))
	{
		passed = false;
		$('#txt_date_registered').addClass('required-empty');
	}
	else
		$('#txt_date_registered').removeClass('required-empty');

	if(txtDateLastLogin && !isValidDate(txtDateLastLogin))
	{
		passed = false;
		$('#txt_date_lastlogin').addClass('required-empty');
	}
	else
		$('#txt_date_lastlogin').removeClass('required-empty');
	

	return passed;

}//end validateSearchForm

 /**
 * @summary  admin/notice js handlers and validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated  2017-11-09
 * @link     URL
 * @since    2017-10-23
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){
	
	selectPlan();
	$('.registration__confirmation').css({
        'display': 'none'
    });
	/**
	 * @summary UL click event handler search: companies
	 *
	 * @since 2017-10-23
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnSearch click
	 *
	 */
	$('#btnSearch').click(function () 
	{
		if(validateSearchForm())
		{
			var form    = document.getElementById('frmCompanies');
			form.action = APP_URL + '/admin/companies/search';
			form.submit();
		}
		
	});

	/**
	 * @summary Save company event 
	 * used in /admin/companies/
	 *
	 * @since 2017-12-05
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnSaveCompany click
	 *
	 */
	$('#btnSaveCompany').click(function () 
	{
		if(validateUpdateForm())
		{
			var form    = document.getElementById('frmAdminCompanyProfile');
			form.action = APP_URL + '/admin/companies/editCompany';
			form.submit();
		}

	});

	$('#btnSavePassword').click(function () 
	{
		if(validateUpdatePasswordForm())
		{
			var form    = document.getElementById('frmAdminCompanyProfile');
			form.action = APP_URL + '/admin/companies/editCompanyPassword';
			form.submit();
		}

	});

	$('#btnEditCompany').click(function () 
	{
		$("input[type=text], select, textarea, input[type=number], #txt_company_postalcode").css("background-color", "#fff !important"); //#d9d9d9
		$("input[type=text], select, textarea, input[type=number], #txt_company_postalcode").css("color", "#000 !important");
		$("#btnSaveCompany").show();
		$(".btnRemoveCompanyFile").show();

		$("#btnRemoveCompanyLogo").show();
		$("#btnRemoveCompanyBanner").show();

		document.getElementById("myFieldset").disabled = false;
		$("#password_pannel").show();
		$("#btnEditCompany").hide();
	});

	/**
	 * EVENT: Verify company form submit
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since 2018/03/23
	 *
	 * @return void
	 */
	$('#btn-verify').click(function () {
		var $this = $(this);
		var form = document.getElementById('frmAdminCompanyProfile');
		form.action = APP_URL + '/admin/company/verify';
		form.submit();

		$this.prop('disabled', true);

	});
	$('#btn-deny').click(function () {
		var $this = $(this);
		var form = document.getElementById('frmAdminCompanyProfile');
		form.action = APP_URL + '/admin/company/deny';
		form.submit();

		$this.prop('disabled', true);

	});

    
    /**
	 * @summary Save company event 
	 * used in /admin/companies/create
	 *
	 * @since 2017-12-06
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnSaveCompany click
	 *
	 */
	$('#btnCreateCompany').click(function () 
	{
		if(validateCreateForm())
		{
			loadConfirmView();
		}

	});
	

	$('#btnRegisterCompany').click(function(){

		var $this   = $(this);
		var form    = document.getElementById('frmAdminCompanyProfile');
		form.action = APP_URL + '/admin/companies/createCompany';
		form.submit();
		
		$this.prop('disabled', true); //prevent multiple submit
	});


	
	$('#btnBackToInput').click(function(){
		$('.registration__confirmation').css({
			'display': 'none'
		});
		$('#confirm-company-view').css({
			'display': 'none'
		});
		$('.edit-view').css({
			'display': 'block'
		});
	}); 

	/**
	 * Load confirmation page via hiding the company create page. 
	 * 
	 * @author  Arvin Jude Alipio <aj_alipio@commude.ph>
	 * @since   2018/07/10
	 *
	 * @return  void
	 */
	function loadConfirmView(){
		document.getElementById('companyNameConfirm').value = document.getElementById('txt_company_name').value;
		document.getElementById('companyWebsiteConfirm').value = document.getElementById('txt_company_website').value;
		document.getElementById('contactNameConfirm').value = document.getElementById('txt_company_contact_person').value;
		document.getElementById('contactNumberConfirm').value = document.getElementById('txt_company_contact_no').value;
		document.getElementById('codesValueConfirm').value = document.getElementById('txt_company_postalcode').value;
		document.getElementById('address1Confirm').value = document.getElementById('txt_company_address1').value;
		document.getElementById('address2Confirm').value = document.getElementById('txt_company_address2').value;
		document.getElementById('emailConfirm').value = document.getElementById('txt_company_email').value;
		document.getElementById('payment_methodConfirm').value = document.getElementById('so_payment_method').value;

		var $paymentMethodConfirm = $('.js-table__tr-payment_confirm');
		var selectedPlan =  $('input[name="rb_plan_type"]:checked').val()
		var ulFilesConfirm = document.getElementById('ul-files_confirm');
		ulFilesConfirm.innerHTML = ''; // clear children first

		for (var i = 1; i <= 3; i++) {

			var companyFile = document.getElementById('companyFile' + i);

			// Proceed to file attribute valiation only if file is uploaded
			if (companyFile.value) {
				var li = document.createElement('li');
				var label = i + ". ";
				var liContent = label + companyFile.files[0].name;

				li.textContent = liContent;
				li.value = companyFile.files[0].name;

				ulFilesConfirm.appendChild(li);
			}
		}
		// var numOfFiles = $("#file_business_permit")[0].files.length; 

		// for (var i = 0; i < numOfFiles; i++) {

		// 	var companyFile = $("#file_business_permit")[0].files[i];
			
		// 	// Proceed to file attribute valiation only if file is uploaded
		// 	if (companyFile.name) {
		// 		var li = document.createElement('li');
		// 		var label = i+1  + ". ";
		// 		var liContent = label + companyFile.name;
	
		// 		li.textContent = liContent;
		// 		li.value = companyFile.name;
	
		// 		ulFilesConfirm.appendChild(li);
		// 	} 
		// }

		$("input[name=rb_plan_type_selected][value=" + selectedPlan + "]").attr('checked', 'checked');

		$(".plan").removeClass("active");
		if($("input[name=rb_plan_type_selected][value=" + selectedPlan + "]").is(":checked")){
			$("input[name=rb_plan_type_selected][value=" + selectedPlan + "]").closest('li').addClass("active");
		}

		if(selectedPlan == 100){
			$paymentMethodConfirm.hide();
		}else{
			if(!$paymentMethodConfirm.is(':visible')) {
				$paymentMethodConfirm.show();
			}
		}

		$('.registration__confirmation').css({
			'display': 'block'
		});
		$('#confirm-company-view').css({
			'display': 'block'
		});
		$('.edit-view').css({
			'display': 'none'
		});
	}

	 /**
	 * @summary TXT event handler email checker on change
	 *
	 * @since 2017-12-06
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens txt_user_email onchange
	 *
	 */
	$('#txt_company_email').blur(function () 
	{
		var userId        = $("#user_id").val();
		var txtUserEmail  = $("#txt_company_email").val().trim();
		
		emailExistsLocal(userId, txtUserEmail); //local
	});

	/**
	 * @summary Remove image company logo and flag for deletion
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCompanyLogo click
	 *
	 */
    $('#btnRemoveCompanyLogo').click(function () 
	{
		$('#dv_company_logo').remove();
		$('#txt_company_logo_remove_flag').val(1);
    });

    /**
	 * @summary Remove image company banner and flag for deletion
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCompanyBanner click
	 *
	 */
    $('#btnRemoveCompanyBanner').click(function () 
	{
		$('#dv_company_banner').remove();
		$('#txt_company_banner_remove_flag').val(1);
    });

    /**
	 * @summary Remove image company pr image and flag for deletion
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCompanyBanner click
	 *
	 */
    $('#btnRemoveCompanyIntroImage').click(function () 
	{
		$('#dv_company_intro_image').remove();
		$('#txt_company_intro_image_remove_flag').val(1);
    });
	
	/**
	 * @summary Remove current file
	 *
	 * @since 2017-12-06
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCompanyFile click
	 *
	 */
    $('.btnRemoveCompanyFile').click(function () 
	{
		var $this = $(this);
		var $txtCompanyFileRemoveFlag = $('#txt_company_file_remove_flag');

		var currentFileName =  'company_file' + $this.data('id');
		var newValue = $txtCompanyFileRemoveFlag.val() + ';' + currentFileName;

		// $this.parent().parent().parent().remove(); //remove parent tr
		$this.parent().remove();
		var fileInputField = "#companyFile"+$this.data('id');
		$(fileInputField).css("display", "inline");

		$txtCompanyFileRemoveFlag.val(newValue);
    });


	$('#file_business_permit').change(function ()
	{
		validateFormFiles();
	});

	$('#file_company_logo').change(function ()
	{
		var image = document.getElementById('file_company_logo');
		var errorMessage_name = 'js-company_logo-error';
		var fileError_name = 'js-company_logo-error_list';
		
		validateFormImages(image, errorMessage_name, fileError_name);
	});

	$('#file_company_banner').change(function ()
	{
		var image = document.getElementById('file_company_banner');
		var errorMessage_name = 'js-company_banner-error';
		var fileError_name = 'js-company_banner-error_list';

		validateFormImages(image, errorMessage_name, fileError_name);
	});

	$('#aChangePassword').click(function () 
	{
		var $this = $(this);
		var $dvChangePassword = $("#dvChangePassword");
		var $changePasswordFlag = $('#change_password_flag');

		$this.toggleClass('hidden');
		$dvChangePassword.toggleClass('hidden');
		$changePasswordFlag.val(1);
	});

	
	$('#btnCancelChangePassword').click(function () 
	{
		var $txtPassword        = $("#txt_password");
		var $dvChangePassword   = $("#dvChangePassword");
		var $aChangePassword    = $("#aChangePassword");
		var $changePasswordFlag = $('#change_password_flag');
		
		$dvChangePassword.toggleClass('hidden');
		$aChangePassword.toggleClass('hidden');
		$txtPassword.val('');
		$changePasswordFlag.val(0);
	});

});

function CompanyPlanCheck() {
    if (document.getElementsByName('rb_plan_type').val() == "TRIAL") {
        document.getElementsByName('payment_method').style.display = 'block';
    }
    else document.getElementsByName('payment_method').style.display = 'none';

}

function changePassword() {
	$('.confirm__modal_container').fadeIn('slow', function() {
		$('.confirm__modal_dialog').fadeIn('slow');
		$('#btn_close').click(function(){
			$('.confirm__modal_container').fadeOut('slow');
			$('#spPassword').hide();
			return false;
		})
    	$('#spare_user').click(function(){
    		$('.confirm__modal_container').fadeOut('slow');
			$('.confirm__modal_dialog').fadeOut('slow');
			$('#spPassword').hide();
    		return false;
		})
  	});
}