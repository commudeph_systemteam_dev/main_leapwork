/**
 * @summary form validation (not used yet)
 *
 * @since 2017-10-23
 * @access global
 *
 * @param string $prop id or class of input field
 * @param string $propSpan id or class of span message
 * @param string $msg - conditional message to display
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateRequired(propInput, propSpan, msg = '')
{
	var passed = true;
	
	var txtField = $(propInput).val().trim();

	if(!txtField)
	{
		passed = false;
		$(propSpan).show();
	}
	else
		$(propSpan).hide();
	
	return passed;
}

/**
 * @summary File extension validation in ['gif','png','jpg','jpeg'] 
 *
 * @since 2017-11-10
 * @access global
 *
 * @param int $inputFile
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function inAllowedImageExtensions(inputFile) 
{
	var allowedExtensions = ['gif','png','jpg','jpeg'];
	var fileExtension = inputFile.split('.').pop().toLowerCase();
    return ($.inArray(fileExtension, allowedExtensions) == -1);
}//end inAllowedImageExtensions

function inAllowedFileExtensions(inputFile) {
	var allowedExtensions = ['pdf','doc','docx','jpeg','png','bmp','jpg'];
	var fileExtension = inputFile.split('.').pop().toLowerCase();

    return !($.inArray(fileExtension, allowedExtensions) == -1);
}

/**
 * @summary File size limit check (see default below) 
 *
 * @since 2017-12-01
 * @access global
 *
 * @param int $inputFile
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */

function inAllowedFileSize(inputFile) 
{
	var maxLimit = 5000000; //default 5MB
	var file     = inputFile.files[0];

    return (file.size > maxLimit);
}//end isAllowedFileSize


/**
 * @summary Postal Code validation :regex (4 digits)
 *
 * @since 2017-10-27
 * @access global
 *
 * @param string $postalInput
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isValidPostalCode(postalInput) {
    var regexPattern = /\d{4}$/;

    return regexPattern.test(postalInput);
}

/**
 * @summary Phone validation :regex (+631234567891)
 * Philippine mobile only - fixed +63nnnnnnnnnn format
 *
 * @since 2017-10-26
 * @access global
 *
 * @param string $phoneInput
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isValidPhone(phoneInput) {
    var regexPattern = /[\+]63\d{10}$/;

    return regexPattern.test(phoneInput);
}

function isValidPhoneWithoutChars(phoneInput) {
    var regexPattern = /\d{11}$/;

    return regexPattern.test(phoneInput);
}

/**
 * @summary Telephone validation :regex '123-345-3456';
										'(078)789-8908';
										'(078) 789-8908';
 *
 * @since 2017-10-26
 * @access global
 *
 * @param string $telephoneInput
 *
 * @author  Arvin Jude Alipio <aj_alipio@commude.ph>
 *
 */
function isValidTelephone(telephoneInput){
	var regexPattern = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/;

	return regexPattern.text(telephoneInput);
}

function isValidTelephoneWithoutChars(telephoneInput){
    var regexPattern = /\d{10}$/;

	return regexPattern.text(telephoneInput);
}


/**
 * @summary Number validation :regex (0-9)
 * Philippine mobile only - fixed +63nnnnnnnnnn format
 *
 * @since 2017-11-02
 * @access global
 *
 * @param string $phoneInput
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isNumber(numberInput) {
    var regexPattern = /^\d+$/;

    return regexPattern.test(numberInput);
}

/**
 * @summary Email validation :regex (lorem@ipsum.com)
 * Keep it simple ;). Catch validation using token
 *
 * @since 2017-10-26
 * @access global
 *
 * @param string $emailInput
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isValidEmail(emailInput) {
    var regexPattern =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return regexPattern.test(emailInput);
}

/**
 * @summary Date validation :regex (mm/dd/yyyy)
 *
 * @since 2017-10-26
 * @access global
 *
 * @param string $dateInput dateToCheck
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isValidDate(dateInput) {
    var regexPattern = /^(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$/;

    return regexPattern.test(dateInput);
}

/**
 * @summary Time validation :regex (HH:MM AM|PM)
 *
 * @since 2018-07-19
 * @access global
 *
 * @param string $time timeToCheck
 *
 * @author Ian Gabriel Sebastian <ian_sebastian@commude.ph>
 *
 */
function isValidTime(timeInput) {
    var regexPattern = /^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/;

    return regexPattern.test(timeInput);
}
/**
 * @summary Date validator greater :regex (mm/dd/yyyy)
 *
 * @since 2017-11-09
 * @access global
 *
 * @param string $dateInput dateToCheck
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isDateLater(dateInput) {
    var now = new Date();
    var dateToCompare = Date.parse(dateInput);

    return (dateToCompare.getDate() > now.getDate());
}

/**
 * @summary Compare date from date to string inputs
 *
 * @since 2017-11-09
 * @access global
 *
 * @param string $dateFrom
 * @param string $dateTo
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function compareDateRangeString(dateFrom, dateTo) {
	var d1 = dateFrom.split("-");
	var d2 = dateTo.split("-");

    var dateFromObj = new Date(d1[0], d1[1], d1[2]);
    var dateToObj   = new Date(d2[0], d2[1], d2[2]);
    
    return (dateFromObj <= dateToObj);
}



/**
 * @summary Number type validation :regex 
 *
 * @since 2017-10-26
 * @access global
 *
 * @param int $numberInput
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function isValidNumber(numberInput) {
    var regexPattern = /^\d{0,4}(?:[.]\d{1,3})?$/;

    return regexPattern.test(numberInput);
}

/**
 * @summary AJAX: Email exist validation 
 *
 * @since 2017-10-27
 * @access global
 *
 * @param int $userId
 * @param string $email
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function emailExists(userId, email) 
{
	 $.ajax({
        type: "GET",
        url: APP_URL + '/applicant/profile/account/emailCheck',
        data: {
        		userId: userId,
        		email:   email 
        	  },
        success: function (data) {
    		console.log('success: ' + data);

    		if(data == 1)
    		{
				$('#spUseremail .text').text('Email already exists');
				$('#spUseremail').show();
    		}
    		else
    		{
    			$('#spUseremail .text').text('');
				$('#spUseremail').hide();
    		}
    		
        },
        error: function (data) {
            console.log('Error emailExists:', data);
        }
    });

}

/**
 * @summary Trims all input fields
 *
 * @since 2017-12-06
 * @access global
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function trimAllInputs() 
{
	$("form").children().each(function(){
		this.value=$(this).val().trim();
	})
}

/**
 * Check if value exists in array helper
 * 
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since  2018/03/20
 * 
 * @return void
 */
function isInArray(value, array) {
	return array.indexOf(value) > -1;
}

 /**
 * @summary  Shared and generic validators
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated  2017-10-13
 * @link     URL
 * @since    2017-10-13
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){
	
	/**
	 * @summary UL click event handler for Company Inquiry
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateEmail
	 * @listens btnSendInquiry click
	 *
	 */
	// $('#btnSendInquiry').click(function () 
	// {
	// 	if(validateform())
	// 	{
	// 		var form    = document.getElementById('frmCompanyInquiry');
	// 		form.action = APP_URL + '/company/inquiry/sendInquiry';
	// 		form.submit();
	// 	}
 //    });
 

});