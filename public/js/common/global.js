/**
* Useful for jquery defaults and predefined settings
*
* @summary  Global JS required settings
*
* @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
* @updated  2017-10-18
* @link     URL
* @since    2017-10-18
* @requires jquery-1.12.0.min.js
*
*/

/* NICOLE 112017 */
function smoothScroll() {
    $('a[href^="#"]').click(function () {
        var speed = 600;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').delay(200).animate({ scrollTop: position }, speed, 'swing');
        return false;
    });
}



//datepicker setting
function datePicker() {
    if ($('.datepicker').length)
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:+0"
        });

    if ($('.datepickerPast').length)
        $(".datepickerPast").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:+0",
            maxDate: "0"
        });

    if ($('.datepickerFuture').length)
        $(".datepickerFuture").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:+0",
            maxDate: "+3Y",
            minDate: "0"
        });
}



function modalClose() {
	/* modal close
	 * js for closing modals
	 * @author Nicole Manalo
	 * @return 
	 */
    $('.alert__modal--container span').on('click', function () {
        $('.alert__modal').fadeOut(2000);
    });
    
}

function hideDisplay(className) {
    $(className).css({
        'display': 'none'
    });
}

function showDisplay(className, message) {
    $(className).css({
        'display': 'block'
    });

    if (message !== undefined) {
        $(className + ' div .w3-container').html('<p>' + message + '</p>');
    }
}

function creditCardForm() {
    hideDisplay(".confirm_div");
    showDisplay(".info");
}

function verifyCreditCard() {
    var hasError = 0;
    var currentTime = new Date()
    var year = currentTime.getFullYear()

    hideDisplay(".month_error");
    hideDisplay(".cvc_error");
    hideDisplay(".year_error");
    hideDisplay(".cc_error");
    hideDisplay(".claim_error");

    if ((($('#credit_card_no').val()).length < 1) || /^\d+$/.test($('#credit_card_no').val()) == 0) {
        hasError = 1;
        showDisplay(".cc_error");
    }

    if (($('#claim_no').val()).length < 1) {
        hasError = 1;
        showDisplay(".claim_error");
    }

    if ($('#exp_month').val() > 12 || $('#exp_month').val() < 1 || /^\d+$/.test($('#exp_month').val()) == 0) {
        hasError = 1;
        showDisplay(".month_error");
    }

    if ($('#exp_year').val() < year || /^\d+$/.test($('#exp_year').val()) == 0) {
        hasError = 1;
        showDisplay(".year_error");
    }
    if ((($('#cvc').val()).length !== 3) || /^\d+$/.test($('#cvc').val()) == 0) {
        hasError = 1;
        showDisplay(".cvc_error");
    }

    if (!hasError) {

        document.getElementById('claimNumConfirm').value = document.getElementById('claim_no').value;
        document.getElementById('creditCardConfrim').value = document.getElementById('credit_card_no').value;
        document.getElementById('cvcConfirm').value = document.getElementById('cvc').value;
        document.getElementById('monthConfirm').value = document.getElementById('exp_month').value;
        document.getElementById('yearConfirm').value = document.getElementById('exp_year').value;

        hideDisplay(".info");
        showDisplay(".confirm_div");
    }
}


function stripFunction() {

    event.preventDefault();

    Stripe.setPublishableKey('pk_test_MA2xKTYcqx2VJVHD2F3f03rj');
    Stripe.createToken({
        number: $('#creditCardConfrim').val(),
        cvc: $('#cvcConfirm').val(),
        exp_month: $('#monthConfirm').val(),
        exp_year: $('#yearConfirm').val()
    },
        stripeResponseHandler);
}

function stripeResponseHandler(status, response) {

    if (response.error === undefined) {
        var token = response['id'];
        console.log(response);
        $('#confirmForm').append("<input type='hidden' name='stripeToken' id='tokenField' value='" + token + "'/>");
        // $('#confirmForm').submit();
        showDisplay("#id01");
    } else {
        stripeErrorHandler(response.error);
    }

}

function stripeErrorHandler(err) {
    console.log(err);
    showDisplay("#id01", err.message);
}



function navIconsToggle() {
    $('.function-menu .function').on('click', function () {

        if ($(window).width() <= 767) {
            $('.navigation__menu').hide();
        }

        $('.function-menu .function .tooltip').hide();

        $(this).children('.tooltip').toggle();

    });



}

function footerFixed() {
    var footerId = 'footer';
    // 文字サイズ
    var checkFontSize = function (func) {
        // 判定要素の追加
        $('body').append($('<div></div>').attr('id', 'ff-node-temp').css({ 'position': 'absolute', 'top': '0', 'visibility': 'hidden' }).text('S'));
        var defHeight = $('#ff-node-temp').prop('offsetHeight');
        // 判定関数
        var checkBoxSize = function () {
            if (defHeight != $('#ff-node-temp').prop('offsetHeight')) {
                func();
                defHeight = $('#ff-node-temp').prop('offsetHeight');
            }
        };
        setInterval(checkBoxSize, 1000);
    };
    var footerFixed = function () {
        // ドキュメントの高さ
        var dh = $('body').prop('clientHeight');
        // フッターのtopからの位置
        $('.' + footerId).css('top', '0');
        var ft = $('.' + footerId).prop('offsetTop');
        // フッターの高さ
        var fh = $('.' + footerId).prop('offsetHeight');
        // ウィンドウの高さ
        var wh = $(window).prop('innerHeight');
        if (ft + fh < wh) {
            $('.' + footerId).css({ 'position': 'relative', 'top': (wh - fh - ft) + 'px' });
        }
    };

    $(window).on('load resize', function (e) { footerFixed() });
    $(window).on('load', function (e) { checkFontSize(footerFixed) });

}

$(function () {
    navIconsToggle();
    smoothScroll();
    datePicker();
    modalClose();
    footerFixed();
});

