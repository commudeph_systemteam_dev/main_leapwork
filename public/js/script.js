function device() {
	if ( _ua.Mobile ) {
		alert('mobile');
	} else if ( _ua.Tablet ) {
		alert('tablet');
		document.getElementById('viewport').setAttribute('content','width=1200');
	} else {
		alert('others');
	}
}


// scroll effect on anchor tag to id
function scroll() {
	$('a[href^="#"]').click(function(){
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('html, body').stop().animate({ scrollTop: position }, 500, 'swing');
		return false;
	});
}



// TOGGLE NOTIFICATION
function sub_menu() {
  $("ul.function-menu li.function span.icon").on("click", function() {
    $(this).next().fadeToggle('fast');
    $(this).toggleClass("active");
  });
}



// REPORT INPUT MENU TOGGLE CONTENTS
function tabs() {
	$(".application__inbox--container a").click(function(event) {
		event.preventDefault();
		$(this).parent().addClass("current");
		$(this).parent().siblings().removeClass("current");

		var tab = $(this).attr("href");
		$(".tab-content").not(tab).css("display", "none");
		$(tab).fadeIn();
	});
}


// SLIDER SP
function slidersp() {
	if($('.swiper-container').length){
		var swiper = new Swiper('.swiper-container', {
			spaceBetween: '2.5%',
			slidesPerView: 'auto',
			centeredSlides: true,
			loop: true,
			autoplay: 4000,
			speed: 2000,
			slideToClickedSlide : true,
			scrollbarDraggable: true,
			scrollbarSnapOnRelease: true
		});

	}
}

function apply() {
	$('#apply-no').on('click', function() {
		$('.apply-modal').toggle();
	});
}

// BTN SEARCH 171207
function btnSearch() {
	$('.search_box').on('input',function(e){
		if( $(this).val() == '' ) {
			$(this).removeClass('js-search');
		}else {
			$(this).addClass('js-search');
		}
	});
}

// NAVIGATION MENU NICOLE 171207
function toggleMenu() {
	$('.sp__navigation').on("click", function(){
		$('.function-menu .function .tooltip').hide();
		$('.sp__navigation,.navigation__menu').toggleClass('js-open');
	});
}

// SLIDER FOR Top Jobs
function slidersp() {
	if($('.js-top-jobs-slider').length){
		var swiper = new Swiper('.js-top-jobs-slider', {
			paginationClickable: true,
			spaceBetween: 20,
			slidesPerView: 3,
			loop: true,
			autoplay: 2000,
			nextButton: '.js-top-jobs-slider-next',
			prevButton: '.js-top-jobs-slider-prev',
			breakpoints: {
				780: {
					spaceBetween: 0,
					slidesPerView: 1,
				}
			}
		});
	}
}

$(function(){
	scroll();
	sub_menu();
	tabs();
	slidersp();
	apply();

	// change position on sp
	if ($(window).width() < 767){
		$('.swiper-container .top_featurejobs_item_sub_p').matchHeight();

		$('.skills_prefecture:parent').each(function () {
			$(this).insertBefore($(this).prev('.salary_range'));
		});
	}
}); // ready





$(window).on('load', function(){
}); // load





$(window).on('resize', function(){
}); // resize





$(window).scroll(function(){
}); // scroll





$(window).on('load resize', function(){
	toggleMenu();
	if ($(window).width() < 767){
		btnSearch();
	}
}); // load resize