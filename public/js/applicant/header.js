

// /**
//  * @author Karen Cano <karen_cano@commude.ph>
//  * JS File for front/common/header.blade.php
//  */

//  function updateNotificationIcons(){
//     /**Bell Icon */
//     var $bellIcon = $('.function.notice');
//     $bellIcon.on("click",function(){
//         $.get(APP_URL+"/applicant/notice/updateIconInstance/BELL_ICON");
//     });//bellIcon

//     /**Message Icon */
    
//     var $messageIcon = $('.function.applicant');
//         $messageIcon.on("click",function(){
//                 $.get(APP_URL+"/applicant/notice/updateIconInstance/MESSAGE_ICON");
//         });//messageIcon
//  }
// function displayNotification(data){
//     var $notiContainer = $('#noti-container');
//     var $notiContent =  $('#noti-content');
//     $notiContainer.show();
//     $notiContent.html(
//             '<b>'+data.title+'</b>'+
//             data.message+'<br>'
//         );
// }

// function appendMessagesNotification(data){

//     // var $ulMessage        = $('.function.applicant');
//     // var $ulToolTip        = $ulMessage.find('.tooltip');
//     // var $pEmpty           = $ulMessage.find('.tooltip .item-empty');
//     // var $notificationList = $ulMessage.find('.tooltip .item'); 

//     // //remove empty message if necessary
//     // if($pEmpty.length > 0 )
//     //     $pEmpty.parent().remove(); //remove whole li

//     // $ulToolTip.find('.icon').addClass('unread');

//     var $notificationList = $('.function.applicant .tooltip .item');

//     $('.function.applicant .icon').addClass('unread');

//     //ensure 5 notifications only, remove first in list
//     if($notificationList.length > 5)
//         $notificationList.last().remove();

//     $notificationList.first().before(
//         `
//         <li class="item">
//             <span class="user-icon"></span>
//                 <a href="`+data.url+`">
//                     <p class="text">
//                         <b>`+data.title+`</b><br>
//                         `+data.message+`
//                     </p>
//                 </a>
//         </li>
//         `
//     );

//     //automatically remove dummy empty message if necessary
//     if( $('.function.applicant .tooltip .item-empty').length > 0 )
//         $('.function.applicant .tooltip .item-empty').parent().remove();


// }


// function pusherInit(){
//     // window.notificationsLimit;
//     // var pusher = new Pusher(window.pusherKey, {
//     //     cluster: 'ap1',
//     //     encrypted: true
//     // });


//     //pusher variable is initialized in js/notification.js
//     var channel = pusher.subscribe('App.User.'+ window.userId);
//     channel.bind('notification-created-event', function(data) {
//         displayNotification(data);
//         appendMessagesNotification(data);
//     });
// }

// $(function(){
//     updateNotificationIcons();
//     pusherInit();
    
// });