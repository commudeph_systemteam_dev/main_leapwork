 /**
 * @summary  Handlers for applicant/browsing-history
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since    2017-11-03
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){

	/**
	 * @summary Add job_post from browsing history to job_favorites
	 *
	 * @since 2017-11-06
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnAddToFavorite click
	 *
	 */
	$('.btnAddToFavorite').click(function () 
	{	
		var $this = $(this);
		var currentJobPostId = $this.parent().parent().parent().find('input:hidden').val();
		var selectedJobPostId = $('#selected_job_post_id');

		selectedJobPostId.val(currentJobPostId);

		var form    = document.getElementById('frmBrowsingHistory');
		form.action = APP_URL + '/applicant/browsing-history/addToFavorite';
		form.submit();

    });

	/**
	 * @summary Add job_post from browsing history to job_favorites
	 *
	 * @since 2017-11-06
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnAddToFavorite clic
	 *
	 */
	$('.icon_delete').click(function () 
	{	
		var $this = $(this);
		var currentJobPostId = $this.parent().parent().find('input:hidden').val();
		var selectedJobPostId = $('#selected_job_post_id');
		
		selectedJobPostId.val(currentJobPostId);

		var form    = document.getElementById('frmBrowsingHistory');
		form.action = APP_URL + '/applicant/browsing-history/removeFromFavorites';
		form.submit();

    });

});