
/**
* @summary Applicant Profile Information update form validation. 
* Custom messages set here. span sp"Object" for error messages
* 
* @since 2017-10-24
* @access private
*
* @fires svalidator validators
* @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
*
*/
function validateProfileInformationForm() {
	var passed = true;

	var userId = $("#user_id").val();
	var txtUserEmail = $("#txt_user_email").val().trim();
	var txtFirstName = $("#txt_applicant_profile_firstname").val().trim();
	var txtMiddleName = $("#txt_applicant_profile_middlename").val().trim();
	var txtLastName = $("#txt_applicant_profile_lastname").val().trim();
	var txtBirthdate = $("#txt_applicant_profile_birthdate").val().trim();
	var $rbGender = $("input[name=rb_applicant_profile_gender]");
	var txtPostalCode = $("#txt_applicant_profile_postalcode").val().trim();
	var txtAddress1 = $("#txt_applicant_profile_address1").val().trim();
	// var txtAddress2   = $("#txt_applicant_profile_address2").val().trim();
	var txtContact = $("#txt_applicant_profile_contact_no").val().trim();
	var fileProfilePicture = $('#file_applicant_profile_imagepath').val();

	if (!txtUserEmail) {
		passed = false;
		$('#spUseremail .text').text('Email address is required');
		$('#spUseremail').show();
	}
	else if (!isValidEmail(txtUserEmail)) {
		passed = false;
		$('#spUseremail .text').text('Invalid email format');
		$('#spUseremail').show();
	}
	else if ($('#spUseremail .text').text().length > 0) {
		passed = false;
		// $('#spUseremail .text').text('Email already exists');
		// $('#spUseremail').show();
	}
	else
		$('#spUseremail').hide();

	if (!txtFirstName) {
		passed = false;
		$('#spFirstName .text').text('First name is required');
		$('#spFirstName').show();
	}
	else
		$('#spFirstName').hide();

	if (!txtMiddleName) {
		passed = false;
		$('#spMiddleName .text').text('Middle name is required');
		$('#spMiddleName').show();
	}
	else
		$('#spMiddleName').hide();


	if (!txtLastName) {
		passed = false;
		$('#spLastName .text').text('Last name is required');
		$('#spLastName').show();
	}
	else
		$('#spLastName').hide();

	if (!txtBirthdate) {
		passed = false;
		$('#spBirthdate .text').text('Birthdate is required');
		$('#spBirthdate').show();
	}
	else if (!isValidDate(txtBirthdate)) {
		passed = false;
		$('#spBirthdate .text').text('Invalid date format');
		$('#spBirthdate').show();
	}
	else
		$('#spBirthdate').hide();

	if (!$rbGender.is(':checked')) {
		passed = false;
		$('#spGender .text').text('Gender is required');
		$('#spGender').show();
	}
	else
		$('#spGender').hide();

	if (!txtPostalCode) {
		passed = false;
		$('#spPostalCode .text').text('Postal Code is required');
		$('#spPostalCode').show();
	}
	else if (!isValidPostalCode(txtPostalCode)) {
		passed = false;
		$('#spPostalCode .text').text('Invalid Postal Code. Ex. ####');
		$('#spPostalCode').show();
	}
	else
		$('#spPostalCode').hide();

	if (!txtAddress1) {
		passed = false;
		$('#spAddress1 .text').text('Address 1 is required');
		$('#spAddress1').show();
	}
	else
		$('#spAddress1').hide();

	var phonepattern = /^-?\d+\.?\d*$/;
	if (!txtContact) {
		passed = false;
		$('#spContact .text').text('Contact no required');
		$('#spContact').show();
	}
	else if (!phonepattern.test(txtContact)) {
		passed = false;
		$('#spContact .text').text('Invalid phone format. Please enter numerical values only.');
		$('#spContact').show();
	}
	else
		$('#spContact').hide();

	if (fileProfilePicture && inAllowedImageExtensions(fileProfilePicture)) {
		passed = false;
		$('#spProfilePicture .text').text('Invalid file extension.');
		$('#spProfilePicture').show();
	}
	else
		$('#spProfilePicture').hide();

	return passed;

}//end validateProfileInfomrationForm

/**
 * @summary Applicant Profile History update form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-10-24
 * @access private
 *
 * @fires svalidator validators
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateProfileHistoryForm() {
	var passed = true;
	var fileResumePath = document.getElementById('file_applicant_profile_resumepath');
	var applicantResume = document.getElementById('applicant_resume');

	//**College fields
	$('#dv_college_input .dv_college_entry').each(function (index) {
		var $this = $(this); //current div.dv_college_entry
		var inputItems = $this.find('select, input').not(':hidden').filter(function () {
			return $.trim($(this).val()).length
		}).length;

		//validate only once an input has been entered per grouping
		if (inputItems > 0) {
			//select options
			$this.find('select, input').not(':hidden').filter(function () {

				if ($.trim($(this).val()).length == 0)
					passed = false;

				return ($.trim($(this).val()).length == 0)
					? $(this).addClass('required-empty')
					: $(this).removeClass('required-empty')
			});
		}

	});

	//div specific span trigger
	if ($('#dv_college_input .required-empty').length > 0) {
		$('#spEducation .text').text('Please fill up the highlighted items');
		$('#spEducation').show();
	}
	else
		$('#spEducation').hide();

	//**Workexp fields
	$('#dv_workexp_input .dv_workexp_entry').each(function (index) {
		var $this = $(this); //current div.dv_workexp_entry
		var inputItems = $this.find('select, input').not(':hidden').filter(function () {
			return $.trim($(this).val()).length
		}).length;

		//validate only once an input has been entered per grouping
		if (inputItems > 0) {
			var $yearFrom = $this.find(".cSoYearFrom");
			var $yearTo = $this.find(".cSoYearTo");
			var $monthFrom = $this.find(".cSoMonthFrom");
			var $monthTo = $this.find(".cSoMonthTo");
			var $presentCheckbox = $this.find('.present-checkbox');

			var dateFrom = $yearFrom.val() + '-' + $monthFrom.val() + '-01';

			var dateTo = $yearTo.val() + '-' + $monthTo.val() + '-01';
			//select options, required check
			$this.find('select, input').not(':hidden').not('[type="checkbox"]').filter(function () {

				if ($.trim($(this).val()).length == 0)
					passed = false;

				return ($.trim($(this).val()).length == 0)
					? $(this).addClass('required-empty')
					: $(this).removeClass('required-empty')
			});

			if (!compareDateRangeString(dateFrom, dateTo) && !$presentCheckbox.is(':checked')) {
				passed = false;
				$yearFrom.addClass('required-empty');
				$yearTo.addClass('required-empty');
			}
			else {
				$yearFrom.removeClass('required-empty');
				$yearTo.removeClass('required-empty');
			}

		}
	});

	if ($('#dv_workexp_input .required-empty').length > 0) {
		$('#spWorkexp .text').text('Please correct the highlighted items');
		$('#spWorkexp').show();
	}
	else
		$('#spWorkexp').hide();

	//**Applicant Skill: Communication fields
	$('#dv_communication_input .dv_communication_entry').each(function (index) {
		var $this = $(this); //current div.dv_workexp_entry
		var inputItems = $this.find('.ctxtQualification').filter(function () {
			return $.trim($(this).val()).length
		}).length;
		//validate only once an input has been entered per grouping
		if (inputItems > 0) {
			//select options
			$this.find('.ctxtCommunication').filter(function () {

				if ($.trim($(this).val()).length == 0)
					passed = false;

				return ($.trim($(this).val()).length == 0)
					? $(this).addClass('required-empty')
					: $(this).removeClass('required-empty')
			});
		}
	});

	if ($('#dv_communication_input .required-empty').length > 0) {
		$('#spCommunication .text').text('Please fill up the highlighted items');
		$('#spCommunication').show();
	}
	else
		$('#spCommunication').hide();

	//**Profile specific
	var txtExpectedSalary = $('#txt_applicant_profile_expected_salary').val().trim();

	if (!txtExpectedSalary || !isNumber(txtExpectedSalary) &&
		txtExpectedSalary < 0) {
		passed = false;
		$('#spExpectedSalary .text').text('Please input a number.');
		$('#spExpectedSalary').show();
	}
	else
		$('#spExpectedSalary').hide();


	if (applicantResume.value) {
		$('#spResumePath').hide();
	}
	else if (!fileResumePath.value) {
		passed = false;
		$('#spResumePath .text').text('Please upload your resume.');
		$('#spResumePath').show();
	} else {
		if (!inAllowedFileExtensions(fileResumePath.value)) {
			passed = false;
			$('#spResumePath .text').text('Invalid file. Only .doc .docx .pdf files are allowed.');
			$('#spResumePath').show();
		}
		else if (inAllowedFileSize(fileResumePath)) {
			passed = false;
			$('#spResumePath .text').text('File size limit is 5MB.');
			$('#spResumePath').show();
		}
		else
			$('#spResumePath').hide();
	}


	return passed;

}//end validateProfileHistoryForm


/**
* @summary  Handles and validators for user module
*
* @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
* @updated  2017-10-26
* @link     URL
* @since    2017-10-26
* @requires jquery-1.12.0.min.js
*
*/
$(function () {

	$("input[type=text], select, textarea, input[type=number], #txt_applicant_profile_postalcode").css("background-color", "#cccccc"); //#d9d9d9
	$("#btnAddCommunicationDiv, #btnAddWorkexpDiv, #btnAddCollegeDiv").css("opacity", "0.5");
	// , input[type = select], input[type = textarea], input[type = number]

	$('#btnSavePassword').click(function () {
		if (validateUpdatePasswordForm()) {
			var form = document.getElementById('frmProfileInfo');
			form.action = APP_URL + '/applicant/profile/account/savePassword';
			form.submit();
		}
	});
	//trigger sidebar collapsed profile display
	$(".btn_pulldown")[0].click();

	//hide spans by default
	$('span.help-block').hide();

	$("#btnSaveProfile").hide();
	$("#btnSaveProfileHistory").hide();

	$('#btnEditProfile').click(function () {
		$("#btnSaveProfile").show();
		document.getElementById("myFieldset").disabled = false;
		$("input[type=text], select, textarea, input[type=number], #txt_applicant_profile_postalcode").css("background-color", "#fff"); //#d9d9d9

		$("#btnEditProfile").hide();
	});

	$('#btnEditProfileHistory').click(function () {
		$("#btnSaveProfileHistory").show();
		document.getElementById("myFieldset").disabled = false;
		$("input[type=text], select, textarea, input[type=number]").css("background-color", "#fff"); //#d9d9d9
		$("#btnAddCommunicationDiv, #btnAddWorkexpDiv, #btnAddCollegeDiv").css("opacity", "1");
		$(".btnRemoveCollegeDiv, .btnRemoveCommunicationDiv, .btnRemoveWorkexpDiv").removeClass("hiddenIcon");
		$("#btnEditProfileHistory").hide();
	});
    /**
	 * @summary clear span errors of removed items
	 *
	 * @since 2017-11-21
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateProfileHistoryForm
	 * @listens btnRemove click
	 *
	 */
	$(document).on('click', '.btnRemove', function () {
		var $this = $(this);
		$this.parent().parent().parent().find('.help-block .text').text('');

	});

    /**
	 * @summary UL click event handler for Applicant Profile update
	 *
	 * @since 2017-10-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateProfileInformationForm
	 * @listens btnSaveProfile click
	 *
	 */
	$('#btnSaveProfile').click(function () {
		if (validateProfileInformationForm()) {
			var form = document.getElementById('frmProfileInfo');
			form.action = APP_URL + '/applicant/profile/account/saveProfile';
			form.submit();
		}
	});

    /**
	 * @summary TXT event handler email checker on change
	 *
	 * @since 2017-10-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires svalidator.validateAccountform
	 * @listens txt_user_email onchange
	 *
	 */
	$('#txt_user_email').blur(function () {
		var userId = $("#user_id").val();
		var txtUserEmail = $("#txt_user_email").val().trim();

		emailExists(userId, txtUserEmail);
	});

	/************************ HISTORY ************************/

	/**
	 * @summary UL click event handler for Applicant History update
	 *
	 * @since 2017-10-27
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateAccount
	 * @listens btnSaveProfileHistory click
	 *
	 */
	$('#btnSaveProfileHistory').click(function () {
		if (validateProfileHistoryForm()) {
			$("#PhysicalTooltip a").attr("target", "");
			var $this = $(this);
			var form = document.getElementById('frmProfileHistory');
			form.action = APP_URL + '/applicant/profile/history/saveProfileHistory';
			form.removeAttribute("target");
			form.submit();

			$this.prop('disabled', true); //prevent multiple submit
		}
	});


	/**
	 * @summary Add education input fields max up to 3
	 *
	 * @since 2017-10-30
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnAddCollegeDiv click
	 *
	 */
	$('#btnAddCollegeDiv').click(function () {
		var dvCollegeEntryCount = $('.dv_college_entry').length;

		if (dvCollegeEntryCount < 3) {
			var $dvCollege = $("#dv_college");
			var $dvCollegeNew = $dvCollege.clone();

			$dvCollegeNew.find("input").val("").removeClass('required-empty').end();//clear all inputs
			$dvCollegeNew.find("select").val("").removeClass('required-empty').end();//clear all select options

			$dvCollegeNew.find('.dvRemoveCollege')
				.append(
					$('<button type="button" class="btnRemoveCollegeDiv btnRemove">X</button>')
				);

			//append to last element :id^
			$dvCollegeNew.insertAfter($('[id^="dv_college"]').last());
		} else {
			$('#limitReacheModal').show();
		}

	});

	/**
	 * @summary Remove education dynamic div. Dynamic listener
	 *
	 * @since 2017-10-30
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCollegeDiv click
	 *
	 */
	$(document).on('click', '.btnRemoveCollegeDiv', function () {

		var $this = $(this);
		var $applicantEducationIds = $("#applicant_education_id_del");
		//delimiter ';' to track items for deletion
		var currentEducationEntry = $this.parent().parent().find('input:hidden').val();

		if (currentEducationEntry) {
			currentEducationEntry = currentEducationEntry + ";";
			$applicantEducationIds.val($applicantEducationIds.val() + currentEducationEntry);
		}

		$this.parent().parent().remove();

	});

    /**
	 * @summary Add workexp input fields
	 *
	 * @since 2017-10-30
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnAddWorkexpDiv click
	 *
	 */
	$('#btnAddWorkexpDiv').click(function () {
		var dvWorkexpEntryCount = $('.dv_workexp_entry').length;

		var $dvWorkexp = $("#dv_workexp");
		var $dvWorkexpNew = $dvWorkexp.clone();

		var $currentPresentCheck = $dvWorkexp.find('.present-checkbox');

		$dvWorkexpNew.find('input[type="hidden"]').val('off');

		if ($currentPresentCheck.is(':checked')) {
			$dvWorkexpNew.find('.cSoYearTo').removeClass('hidden');
			$dvWorkexpNew.find('.cSoMonthTo').removeClass('hidden');
			$dvWorkexpNew.find('.present-checkbox').prop('checked', false).prop('disabled', true);
		}

		$dvWorkexpNew.find('.cSoYearTo').prop('disabled', false);
		$dvWorkexpNew.find('.cSoMonthTo').prop('disabled', false);

		$dvWorkexpNew.find("input").val("").removeClass('required-empty').end();//clear all inputs
		$dvWorkexpNew.find("select").val("").removeClass('required-empty').end();//clear all select options
		$dvWorkexpNew.find("textarea").val("").removeClass('required-empty').end();//clear all textarea options

		$dvWorkexpNew.find('.dvRemoveWorkexp')
			.append(
				$('<button type="button" class="btnRemoveWorkexpDiv btnRemove">X</button>')
			);

		//append to last element :id^
		$dvWorkexpNew.insertAfter($('[id^="dv_workexp"]').last());

	});

	/**
	 * @summary Remove workexp dynamic div. Dynamic listener
	 *
	 * @since 2017-10-30
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCollegeDiv click
	 *
	 */
	$(document).on('click', '.btnRemoveWorkexpDiv', function () {
		var $this = $(this);
		var $applicantWorkexpIds = $("#applicant_workexp_id_del");
		//delimiter ';' to track items for deletion
		var currentWorkexpEntry = $this.parent().parent().find('input:hidden').val().trim();

		if (currentWorkexpEntry) {
			currentWorkexpEntry = currentWorkexpEntry + ";";
			$applicantWorkexpIds.val($applicantWorkexpIds.val() + currentWorkexpEntry);
		}

		$this.parent().parent().remove();

	});

	/**
	 * @summary Add applicant_skills communication input fields
	 *
	 * @since 2017-11-02
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnAddCommunicationDiv click
	 *
	 */
	$('#btnAddCommunicationDiv').click(function () {
		var dvCommunicationEntryCount = $('.dv_communication_entry').length;

		var $dvCommunication = $("#dv_communication");
		var $dvCommunicationNew = $dvCommunication.clone();

		$dvCommunicationNew.find("input").val("").removeClass('required-empty').end();//clear all inputs
		$dvCommunicationNew.find("select").val("").removeClass('required-empty').end();//clear all select options

		$dvCommunicationNew.find('.dvRemoveCommunication')
			.append(
				$('<button type="button" class="btnRemoveCommunicationDiv btnRemove">X</button>')
			);

		//append to last element :id^
		$dvCommunicationNew.insertAfter($('[id^="dv_communication"]').last());

	});

	/**
	 * @summary Remove workexp dynamic div. Dynamic listener
	 *
	 * @since   2017-11-02
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCommunicationDiv click
	 *
	 */
	$(document).on('click', '.btnRemoveCommunicationDiv', function () {

		var $this = $(this);
		var $applicantCommunicationIds = $("#applicant_communication_id_del");
		//delimiter ';' to track items for deletion
		var currentCommunicationEntry = $this.parent().parent().find('input:hidden').val().trim();

		if (currentCommunicationEntry) {
			currentCommunicationEntry = currentCommunicationEntry + ";";
			$applicantCommunicationIds.val($applicantCommunicationIds.val() + currentCommunicationEntry);
		}

		$this.parent().parent().remove();

	});

    /**
	 * @summary Make sure all inputs are number only
	 *
	 * @since 2017-11-22
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens txt_applicant_profile_expected_salary
	 *
	 */
	$('#txt_applicant_profile_expected_salary').on('input', function () {
		var $this = $(this);

		$this.val($this.val().replace(/[^0-9.]/g, ''));
	});

	/**
	 * @summary Display resume file and load in another tab
	 *
	 * @since 2017-12-01
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens lnkViewResume click
	 *
	 */
	$('#lnkViewResume').click(function () {
		var form = document.getElementById('frmProfileHistory');
		form.action = APP_URL + '/applicant/profile/resume/display';
		form.target = "_blank";
		form.submit();
	});

});

/**
* @summary Corprorate Profile update form validation. 
* Custom messages set here. span sp"Object" for error messages
* 
* @since 2017-12-05
* @access private
*
* @fires svalidator.trimAllInputs()
* @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
* 
*/
function validateUpdatePasswordForm() {

	var passed = true;

	var $txtPassword = $("#txt_applicant_profile_password");
	var $txtConfirmPassword = $("#txt_applicant_profile_password_confirmation");
	var $spPassword = $('#spPassword');

	if (!$txtPassword.val() && !$txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Please fill-up password fields.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if (!$txtPassword.val() && $txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Please enter password.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if ($txtPassword.val() && !$txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Please enter Confirm Password.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if ($txtPassword.val() != $txtConfirmPassword.val()) {
		passed = false;
		$spPassword.find('.text').text('Password is Mismatch.');
		$spPassword.show();
		$txtPassword.focus();
	}
	else if (!$txtPassword.val() || $txtPassword.val().length < 6) {
		passed = false;
		$spPassword.find('.text').text('Password must be 6 characters');
		$spPassword.show();
		$txtPassword.focus();
	}
	else
		$spPassword.hide();

	return passed;
}

$('#btnSavePassword').click(function () {
	if (validateUpdatePasswordForm()) {
		var form = document.getElementById('frmAdminCompanyProfile');
		form.action = APP_URL + '/admin/companies/editCompanyPassword';
		form.submit();
	}

});

function changePassword(href) {
	$('.confirm__modal_container').fadeIn('slow', function () {
		$('.confirm__modal_dialog').fadeIn('slow');
		$('#delete_user').attr('href', href);
		$('#btn_close').click(function () {
			$('.confirm__modal_container').fadeOut('slow');
			$('#spPassword').hide();
			return false;
		})
		$('#spare_user').click(function () {
			$('.confirm__modal_container').fadeOut('slow');
			$('.confirm__modal_dialog').fadeOut('slow');
			$('#spPassword').hide();
			return false;
		})
	});
}


function presentButtonClickEvent(e) {
	if ($(e).is(':checked')) {
		$(e).parent().find('.cSoYearTo, .cSoMonthTo').prop('disabled', true).addClass('hidden');
		$('.dv_workexp_entry').find('input.present-checkbox:not(:checked)').prop('disabled', true).prop('checked', false);
		$(e).prev().val('on');

	} else {
		$(e).parent().find('.cSoYearTo, .cSoMonthTo').removeClass('hidden').prop('disabled', false);
		$('.dv_workexp_entry').find('.present-checkbox').prop('disabled', false);
		$(e).prev().val('off');

	}


}

$(function () {
	if ($('.dv_workexp_entry').find('input.present-checkbox:checked').length > 0)
		$('.dv_workexp_entry').find('input.present-checkbox:not(:checked)').prop('disabled', true).prop('checked', false);
});