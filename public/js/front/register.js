function validate()
 {
 	var passed = [5];
 	passed.fill(true);
 	var passedAll = true;
    var firstName 	  = $("[name='firstName']");
    var middleName 	  = $("[name='middleName']");
 	var lastName 	  = $("[name='lastName']");
 	var email 		  = $("[name='email']");
    var password 	  = $("[name='password']");
    var password_confirm 	  = $("[name='password_confirmation']");
 	
 	if(firstName.val() != ""){
	}else{
		firstName.focus();
		passed[0] = false;
    }

    if(middleName.val() != ""){
	}else{

		lastName.focus();
		passed[1] = false;
    }
    
	if(lastName.val() != ""){
	}else{

		lastName.focus();
		passed[2] = false;
    }
    
	if(email.val() == ""){	
        passed[3] = false;
	}else if(!isValidEmail(email.val())){
		email.focus();
		passed[3] = false;
    }
    
	if(password.val() != ""){
        if(password.val() != password_confirm.val()){
            passed[4] = false;
        }
	}else{
		password.focus();
		passed[4] = false;
    }
    
    if(password_confirm.val() != ""){
	}else{
		password_confirm.focus();
		passed[5] = false;
	}

	for (var i = 0; i < passed.length; i++) {
		if(!passed[i]){
			passedAll = false;
		}
    }
    
    
	if (passedAll){
		return true;
	}else{
		return false;
	}
 }

$(document).ready(function() {
    // var modal = document.getElementById('successModal');
    // // When the user clicks anywhere outside of the modal, close it
    // window.onclick = function(event) {
    //     if (event.target == modal) {
    //         modal.style.display = "none";
    //     }
    // }
});

function submitForm(){
    if(validate()){
            var $this   = $(this);
			var form    = document.getElementById('frmRegisterUser');
			if(form.submit())
			{
				$this.prop('disabled', true); //prevent multiple submit
				$("#successModal").show();
			}
    }
}

function isValidEmail(emailInput) {
    var regexPattern =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return regexPattern.test(emailInput);
}