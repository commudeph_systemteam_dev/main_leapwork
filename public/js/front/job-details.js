/**
 * @summary  Handlers for front/detail/obs/job-details/blade.php
 *
 * @author   Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since    2017-11-28
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){

	/**
	 * @summary Drop down change event to load entries with status
	 *
	 * @since 2017-11-28
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnApply click
	 *
	 */
	$('#btnApplyConfirm').click(function () 
	{	
		var $this   = $(this);
		var target  = $this.data('target');
		var form    = document.getElementById('frmJobDetails');
		var $modalDisplay = "";
		
		
		switch(target)
		{
			case 'apply':
				$modalDisplay =  $('#mdlApply');
				break;
			case 'resume':
				$modalDisplay =  $('#mdlResume');
				break;			
		}

		$modalDisplay.show();

    });

    /**
	 * @summary Drop down change event to load entries with status
	 *
	 * @since 2017-12-01
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnApply click
	 *
	 */
	$('#btnApply').click(function () 
	{	
		var form    = document.getElementById('frmJobDetails');
		form.action = APP_URL + '/job/apply';
		form.method = "POST";
		
		form.submit();
		
		$this.prop('disabled', true);//disable top revent multi submit

    });

     /**
	 * @summary Drop down change event to load entries with status
	 *
	 * @since 2017-12-01
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnApply click
	 *
	 */
	$('#btnUpdateResume').click(function () 
	{	
		var $jobPostResumeFlag = $('#job_post_resume_flag');
		var form = document.getElementById('frmJobDetails');
		
		$jobPostResumeFlag.val(1);

		form.action = APP_URL + '/applicant/profile/resume';
		form.method = "POST";
		
		form.submit();
		
		$this.prop('disabled', true);//disable top revent multi submit

    });

    /**
	 * @summary Drop down change event to load entries with status
	 *
	 * @since 2017-11-28
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnApply click
	 *
	 */
	$('.btnCancel').click(function () 
	{	
		var $this   = $(this);
		var $mdlDiv  = $this.parent().parent().parent(); //current modal div
		var form    = document.getElementById('frmJobDetails');

		$mdlDiv.hide();
    });



});