$(function() {
    if(userId) {
        makePusherEvent('applicant-scout-'+userId, 'message', appendToMessageArea);
    }
});