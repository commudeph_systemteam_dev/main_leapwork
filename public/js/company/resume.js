$(function() {
    $(".jobDescSeeMore").on("click", function(){
        $(this).hide();
        $("#seeMoreContents_" + $(this).data("id")).removeAttr("hidden");
    })

    $(".jobDescSeeLess").on("click", function () {
        $("#jobDescContents_" + $(this).data("id")).show();
        $(".jobDescSeeMore").show();
        $("#seeMoreContents_" + $(this).data("id")).attr("hidden", true);
    })
})