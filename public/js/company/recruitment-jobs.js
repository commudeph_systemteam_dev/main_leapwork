/**
 * recruitment-jobs.js
 * Posts the select_job_post_visibility to filter private/public visibility
 * @author Karen Cano
 * @return View
 */
$(document).ready(function() {
    $("#select_job_post_visibility").change(function(){
        var visibilitySelected = $("#select_job_post_visibility option:selected").text();
        var method = "post";
        var enctype = "multipart/form-data";
        var form = document.getElementById("recruitmentForm");
          form.submit();
    });

});//end document ready

$(function() {
    var modal = document.getElementById('checkLimit');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    
      $('#limitReachedModalBtn').click(function() {
        $("#checkLimit").show();
      });

     

      $('#limitandExpiredModalBtn').click(function() {
        $("#checkLimitAndExpired").show();
      });

      $('.limitReachedModalBtnRepost').click(function() {
        $("#checkLimit").show();
      });

      $('.limitandExpiredModalBtnRepost').click(function() {
        $("#checkLimitAndExpired").show();
      });

      $('#btnDirectJobPost').click(function() {
        
        window.location= APP_URL + "/company/job-posting";
      });

});

function repostJob(href) {
  $('.confirm__modal_container').fadeIn('slow', function() {
    $('.confirm__modal_dialog').fadeIn('slow');
    $('#delete_user').attr('href', href);
		$('#btn_close_conf').click(function(){
			$('.confirm__modal_container').fadeOut('slow');
			return false;
		})
      $('#spare_user').click(function(){
        $('.confirm__modal_container').fadeOut('slow');
        $('.confirm__modal_dialog').fadeOut('slow');
        return false;
    })
    });
}

function closeModalIcon() {
  $('.confirm__modal_container').fadeOut('slow');
  return false;
}

function closeModal() {
  $('.confirm__modal_container').fadeOut('slow');
  $('.confirm__modal_dialog').fadeOut('slow');
  return false;
}