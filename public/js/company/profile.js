 /**
 * @summary Corprorate Profile update form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-10-24
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateform()
{
	var passed = true;
	
	var txtCompanyName 	= 	$("#txt_company_name").val().trim();
	var txtPostalCode  	= 	$("#txt_company_postalcode").val().trim();
	var txtAddress1    	= 	$("#txt_company_address1").val().trim();
	var image = document.getElementById('file_company_intro_image');
	var errorMessage = 'js-public_relation-error';
	var fileError = 'js-public_relation-error_list';
	
	var updLogo_error		   	=	validateFormImages(
		document.getElementById('file_company_logo'),
		'js-company_logo-error',
		'js-company_logo-error_list'
	);

	var updBanner_error			=	validateFormImages(
		document.getElementById('file_company_banner'),
		'js-company_banner-error',
		'js-company_banner-error_list'
	);

	var updPubRelation_error	=	validateFormImages(
		document.getElementById('file_company_intro_image'),
		'js-public_relation-error',
		'js-public_relation-error_list'
	);

	if(!txtCompanyName)
	{
		passed = false;
		$('#spCompanyName .text').text('Company name is required');
		$('#spCompanyName').show();
	}
	else
		$('#spCompanyName').hide();

	if(!txtPostalCode)
	{
		passed = false;
		$('#spPostalCode .text').text('Postal Code is required');
		$('#spPostalCode').show();
	}
	else if (!isValidPostalCode(txtPostalCode))
	{
		passed = false;
		$('#spPostalCode .text').text('Invalid Postal Code. Ex. ####');
		$('#spPostalCode').show();
	}
	else
		$('#spPostalCode').hide();
	
	if(!txtAddress1)
	{
		passed = false;
		$('#spAddress1 .text').text('Address 1 is required');
		$('#spAddress1').show();
	}
	else
		$('#spAddress1').hide();

	if(!validateFormFiles()){
		passed = false;
	}
	
	//validate images
	if(!updLogo_error){
		document.getElementById("file_company_logo").focus();
		passed = false;
	}	

	if(!updBanner_error){
		document.getElementById("file_company_banner").focus();
		passed = false;
	}	

	if(!updPubRelation_error){
		document.getElementById("file_company_intro_image").focus();
		passed = false;
	}	
	return passed;
    
}

 /**
 * @summary  company/inquiry js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 2017-10-24
 * @link     URL
 * @since    2017-10-24
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){
	
	$("#btnEditProfile").hide();
	$("input[type=text], select, textarea, input[type=number], #txt_company_postalcode").css("background-color", "#eee");
	$("input[type=text], select, textarea, input[type=number], #txt_company_postalcode").css("color", "#999");
	 //#d9d9d9
	//hide spans by default
	if ($('span.help-block').length)
		$('span.help-block').hide();

	/**
	 * @summary UL click event handler for Company Profile
	 *
	 * @since 2017-10-23
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateEmail
	 * @listens btnSendInquiry click
	 *
	 */
	$('#btnEditProfile').click(function () 
	{
		if(validateform())
		{
			var form    = document.getElementById('frmCompanyProfile');
			form.action = APP_URL + '/company/profile/editProfile';
			form.submit();
		}
	});
	
	$('#btnEditCompany').click(function ()
	{
		$("#btnEditProfile").show();
		$("input[type=text], select, textarea, input[type=number], #txt_company_postalcode").css("background-color", "#fff !important"); //#d9d9d9
		$("input[type=text], select, textarea, input[type=number], #txt_company_postalcode").css("color", "#000 !important");
		document.getElementById("myFieldset").disabled = false;
		$("#btnEditCompany").hide();
	});

	/**
	 * @summary Remove image company logo and flag for deletion
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCompanyLogo click
	 *
	 */
    $('#btnRemoveCompanyLogo').click(function () 
	{
		$('#dv_company_logo').remove();
		$('#txt_company_logo_remove_flag').val(1);
    });

    /**
	 * @summary Remove image company banner and flag for deletion
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Ian Gabriel Sebastian <ian_sebastian@commude.ph>
	 * 
	 * @listens btnRemoveCompanyBanner click
	 *
	 */
    $('#btnRemoveCompanyBanner').click(function () 
	{
		$('#dv_company_banner').remove();
		$('#txt_company_banner_remove_flag').val(1);
    });

    /**
	 * @summary Remove image company pr image and flag for deletion
	 *
	 * @since 2017-11-17
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @listens btnRemoveCompanyBanner click
	 *
	 */
    $('#btnRemoveCompanyIntroImage').click(function () 
	{
		$('#dv_company_intro_image').remove();
		$('#txt_company_intro_image_remove_flag').val(1);
	});
	
	$('#file_company_logo').change(function ()
	{
		var image = document.getElementById('file_company_logo');
		var errorMessage_name = 'js-company_logo-error';
		var fileError_name = 'js-company_logo-error_list';
		
		validateFormImages(image, errorMessage_name, fileError_name);
	});

	$('#file_company_banner').change(function ()
	{
		var image = document.getElementById('file_company_banner');
		var errorMessage_name = 'js-company_banner-error';
		var fileError_name = 'js-company_banner-error_list';

		validateFormImages(image, errorMessage_name, fileError_name);
	});

	$('#file_company_intro_image').change(function ()
	{
		var image = document.getElementById('file_company_intro_image');
		var errorMessage_name = 'js-public_relation-error';
		var fileError_name = 'js-public_relation-error_list';
		
		validateFormImages(image, errorMessage_name, fileError_name);
	});

	$('#aChangePassword').click(function () 
	{
		var $this = $(this);
		var $dvChangePassword = $("#dvChangePassword");
		var $changePasswordFlag = $('#change_password_flag');

		$this.toggleClass('hidden');
		$dvChangePassword.toggleClass('hidden');
		$changePasswordFlag.val(1);
	});

	$('#btnCancelChangePassword').click(function () 
	{
		var $txtPassword        = $("#txt_password");
		var $dvChangePassword   = $("#dvChangePassword");
		var $aChangePassword    = $("#aChangePassword");
		var $changePasswordFlag = $('#change_password_flag');
		
		$dvChangePassword.toggleClass('hidden');
		$aChangePassword.toggleClass('hidden');
		$txtPassword.val('');
		$changePasswordFlag.val(0);
	});

	
});

/**
 * @summary Validate business permits 
 * 
 * @since 2017-12-06
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateFormFiles()
{
	var passed = true;
	
	var sizeLimit = constants.MAX_FILE_UPLOAD;
	
	var spCompanyFile = document.getElementsByClassName('js-company_file-error')[0];
	var currentFiles = document.getElementsByClassName('js-file_link');
	var currentFilsCount =  currentFiles.length;
	
	var spFileErrors = document.getElementsByClassName('js-company_file-error_list')[0];

	var fileNames = [];

	// Single file uploads
	if(document.getElementById('companyFile'))
	{
		// Loop through files
		for (var i = 1; i <= 3; i++) {

			var companyFile = document.getElementById('companyFile' + i);

			// Proceed to file attribute valiation only if file is uploaded
			if (companyFile.value) {

				// File size check
				if (companyFile.files[0].size > sizeLimit) {
					passed = false;
					spCompanyFile.style.display = "block";
					spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorFileFormat;
				}

				// File extension check
				// svalidator.inAllowedFileExtensions
				if (!(inAllowedFileExtensions(companyFile.value))) {
					passed = false;
					spCompanyFile.style.display = "block";
					spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorFormat;
				}

				// Duplicate file name check
				// svalidator.isInArray
				if (isInArray(companyFile.files[0].name, fileNames)) {
					passed = false;
					spCompanyFile.style.display = "block";
					spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorDuplicate;
				}

				// Assume file count since value exists
				fileNames.push(companyFile.files[0].name);
			}
			else{
				break;
			}
		}

		if ((fileNames.length < 1) && currentFilsCount < 1)  {
			passed = false;
			spCompanyFile.style.display = "block";
			spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorRequired;
	
			spCompanyFile.focus();
		}
		else if (passed) {
			spCompanyFile.firstElementChild.innerHTML = '';
			spCompanyFile.style.display = "none";
		}
	}
	
	//Multiple File Upload
	var adminCompanyFile = document.getElementById('file_business_permit');
	
	if(adminCompanyFile)
	{
		var $fileBusinessPermit   = $('#file_business_permit');
		//spans
		var $spBusinessPermit     = $('#spBusinessPermit');
		var $spBusinessPermitMain = $('#spBusinessPermitMain');

		var trCompanyFilesCount = $('.trCompanyFiles').length; //existing files count
		var maxFileCountLimit = 3 - trCompanyFilesCount;
		var maxFileSizeLimit = constants.MAX_FILE_UPLOAD;; //default 10MB
		var totalFileSize    = 0;

		$spBusinessPermit.hide();//reset first

		if($fileBusinessPermit[0].files.length > maxFileCountLimit) //upload count check
		{
			passed = false;
			// $spBusinessPermitMain.text('Maximum upload is 3 only.');
			$spBusinessPermitMain.addClass('red');
			$fileBusinessPermit.focus();
			return passed; //exit immediately
		}
		else
		{
			$spBusinessPermitMain.removeClass('red');
		}

		if($fileBusinessPermit.val())
		{
			//check each file
			for(var i=0;i< $fileBusinessPermit.length;i++) {
		        totalFileSize += ($fileBusinessPermit[i].size || $fileBusinessPermit[i].fileSize);

		        if(!inAllowedFileExtensions($fileBusinessPermit[i].value)) //file extension check
				{
					passed = false;

					$spBusinessPermit.find('.text').text('Upload contains invalid file extension');
					$spBusinessPermit.show();
					$fileBusinessPermit.focus();

					return passed;
				}
				else
					$spBusinessPermit.hide();

		    }

		    if(totalFileSize > maxFileSizeLimit)
			{
				passed = false;
				$spBusinessPermit.find('.text').text('Upload file size is too big. 10MB max');
				$spBusinessPermit.show();
				$fileBusinessPermit.focus();
			}
			else
			{
				$spBusinessPermit.hide();
			}
		}
	}

	
	return passed;
}

/**
 * @summary Validate Image Files
 * 
 * @since 2018-12-06
 * @access private
 *
 * @author  Ian Gabriel Sebastian <ian_sebastian@commude.ph>
 *
 */
function validateFormImages(image, errorMessage, errorList)
{
	var passed = true;
	
	var sizeLimit = constants.MAX_FILE_UPLOAD;

	var errorSpan = document.getElementsByClassName(errorMessage)[0];
	var errorSpan_list = document.getElementsByClassName(errorList)[0];

	var filename;

	// Proceed to file attribute valiation only if file is uploaded
	if (image.value) {

		// File size check
		if (image.files.size > sizeLimit) {
			passed = false;
			errorSpan.style.display = "block";
			errorSpan.firstElementChild.innerHTML = errorSpan_list.dataset.errorFileFormat;
		}

		// File extension check
		// svalidator.inAllowedFileExtensions
		if (inAllowedImageExtensions(image.value)) {
			passed = false;
			errorSpan.style.display = "block";
			errorSpan.firstElementChild.innerHTML = errorSpan_list.dataset.errorFormat;
		}

		// Assume file count since value exists
		fileName = image.files.name;
	}
	if (passed) {
		errorSpan.firstElementChild.innerHTML = '';
		errorSpan.style.display = "none";
	}


	return passed;
}