//チャットのプルダウン
function btn_pulldown() {
  $('.cloesed_chat').click(function() {

      var $this = $(this);
      var id = $(this).data('t_id');
      $this.next().stop().slideToggle(300, function(){
        scrollBottom('#message_area_'+id+' div.messages_container');
    });
      $this.find('.btn_img').stop().toggleClass('on_off');

  });
}

 /**
 * @summary Corprorate Inquiry form validation. 
 * Custom messages set here. span sp"Object" for error messages
 * 
 * @since 2017-10-23
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateForm()
{
	var passed = true;
	
	var $txtContactPerson  = $("#txt_contact_person");
	var $txtTitle          = $('#txt_title');
    var $txtInquiry        = $("#txt_content");

    //spans
    var $spContactPerson = $('#spContactPerson');
    var $spTitle         = $('#spTitle');
    var $spInquiry       = $('#spInquiry');

    trimAllInputs(); //svalidator

	if(!$txtContactPerson.val())
	{
		passed = false;
		$spContactPerson.removeClass('hidden');
        $txtContactPerson.focus();
	}
	else
		$spContactPerson.addClass('hidden');

    if(!$txtTitle.val())
    {
        passed = false;
        $spTitle.removeClass('hidden');
        $txtTitle.focus();
    }
    else
        $spTitle.addClass('hidden');


	if(!$txtInquiry.val())
	{
		passed = false;
		$spInquiry.removeClass('hidden');
        $txtInquiry.focus();
	}
	else
		$spInquiry.addClass('hidden');

	return passed;
    
}

/**
 * @summary Reply conversation form validation
 * 
 * @since 2017-12-26
 * @access private
 * 
 * @param dom $src div of button
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateReplyForm($src)
{
    var passed = true;
    var $srcDiv = $src.parent().parent();

    $srcDiv.each( function(index){
        var $this = $(this); //current div.dv_college_entry
        
        $this.find('input, textarea').filter(function () {

            if($.trim($(this).val().trim()).length == 0)
            {
                $this.find('.spErrorMessage').removeClass('hidden');
                passed = false;
            }
            else
                $this.find('.spErrorMessage').addClass('hidden');

        });
    });
    
    return passed;
}


 /**
 * @summary  company/inquiry js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 2017-10-13
 * @link     URL
 * @since    2017-10-13
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){

	btn_pulldown();
	
	/**
	 * @summary UL click event handler for Company Inquiry
	 *
	 * @since 2017-10-18
	 * @access private
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * 
	 * @fires validateEmail
	 * @listens btnSendInquiry click
	 *
	 */
	$('#btnSendInquiry').click(function () 
	{
		if(validateForm())
		{
			var $this = $(this);
			var form    = document.getElementById('frmCompanyInquiry');
			form.action = APP_URL + '/company/inquiry/sendInquiry';
			form.submit();

			$this.prop('disabled', true); //prevent multiple submit
		}
    });

	/**
     * @summary Dispaly compose pane
     *
     * @since 2017-12-21
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens dvBtnMail click
     *
     */
    $('#btnNewInquiry').click(function () 
    {
        var $tabComposeMail = $('.tab_compose_mail');
        //hide all tabs
        $('.tab').hide();
        $('.dvTab').hide();

        //show compose panes    
        $('#dvCompose').show();

        $tabComposeMail.addClass('active');
        $tabComposeMail.show();
    });

     /**
     * @summary Close compose pane
     *
     * @since 2017-12-21
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnCloseCompose click
     *txt_message_new
     */
    $('#btnCloseCompose').click(function () 
    {
        var $tabComposeMail = $('.tab_compose_mail');

        $tabComposeMail.removeClass('active');

        $('.tab').show();
        $('.dvTab').show();

        $('#dvCompose').hide();
        $tabComposeMail.hide();
    });

    /**
     * @summary Reply to existing threads/conversation
     *
     * @since 2017-12-22
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnReplyConversation click
     *
     */
    $('.btnReplyConversation').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            var currentThreadId = $this.data('thread-id');
            var currentMessage  = $inputDiv.find('.ctxtInput').val();

            var data = {
                'company_inquiry_thread_id' :  currentThreadId,
                'company_inquiry_conversation_message' : currentMessage,
                'userIcon' : $('#userIcon').val(),
            };

            sendAjax('post', "/company/inquiry/replyConversation", data);

            appendMessage(currentThreadId, currentMessage, $('#userIcon').val(), 'right', $('#userName').val());

            $inputDiv.find('.ctxtInput').val('');

            //scrollBottom('#job-application', currentThreadId);


            // var $threadId       = $('#company_inquiry_thread_id');
            // var $message        = $('#company_inquiry_conversation_message');
            // var form            = document.getElementById("frmCompanyInquiry");
            
            // $threadId.val(currentThreadId);
            // $message.val(currentMessage);
            
            // form.action = APP_URL+ "/company/inquiry/replyConversation";
            // form.submit();
            
            // $this.prop('disabled', true); //prevent multiple submit
        }

    });

    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmReply click
     *
     */
    $('.btnConfirmReply').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            $inputDiv.find(':input').prop('readonly', true);
            $inputDiv.find(':input').addClass('matched');

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });


    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-12-21
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmSend click
     *
     */
    $('.btnConfirmSend').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateForm())
        {
            $inputDiv.find(':input').prop('readonly', true);
            $inputDiv.find('select').prop('disabled', true);

            $inputDiv.find(':input').addClass('disabled');

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });

    /**
     * @summary Hide confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnBackReinput click
     *
     */
    $('.btnBackReinput').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        $inputDiv.find(':input').prop('readonly', false);
        $inputDiv.find('select').removeAttr('disabled');

        $inputDiv.find(':input').removeClass('disabled');
        $inputDiv.find(':input').removeClass('matched');
        
        $this.parent().addClass('hidden');
        $this.parent().parent().find('.dvMainButtons').show();

    });

    /**
     * @summary Compose/create new thread as an inquiry
     *
     * @since 2017-12-21
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnComposeThread click
     *
     */
    $('#btnComposeThread').click(function()
    {
        var $this = $(this);

        if(validateForm())
        {
            var $this = $(this);
			var form    = document.getElementById('frmCompanyInquiry');
			form.action = APP_URL + '/company/inquiry/sendInquiry';
			form.submit();

			$this.prop('disabled', true); //prevent multiple submit
        }

    });

});

