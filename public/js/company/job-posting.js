/**
 * validate
 * validate function for job posting
 * @author Karen Cano
 * @return true/false on submit
 */


function validate()
{
  var passed = [11];
  passed.fill(true);
  var passedAll = true;


  var job_post_title = $("[name='job_post_title']");
  var validator_job_post_title =  $('#titleErr');
  var job_post_position = $("[name='job_post_job_position_id']");
  var validator_job_post_position =  $('#positionErr');
  // var job_post_classification_id = $("[name='job_post_classification_id']");
  var job_post_classification_id =$('select[name=job_post_classification_id]');
  var validator_job_post_classification_id =  $('#classificationErr');
  var job_post_industry_id = $("[name='job_post_industry_id']");
  var validator_job_post_industry_id = $('#industryErr');
  var job_post_industry_other_id = $("[name='job_post_others']");
  var validator_job_post_industry_other_id = $('#industryOtherErr');
  var job_post_description = $("[name='job_post_description']");
  var validator_job_post_description = $('#descriptionErr');
  var job_post_salary_max = $("[name='job_post_salary_max']");
  var validator_job_post_salary_max = $('#salaryMaxErr');
  var job_post_salary_min = $("[name='job_post_salary_min']");
  var validator_job_post_salary_min = $('#salaryRangeErr');
  var job_post_no_of_positions = $("[name='job_post_no_of_positions']");
  var validator_job_post_no_of_positions = $('#numEmployeesErr');
  var job_post_location_id = $("[name='job_post_location_id']");
  var validator_job_post_location_id = $('#workLocation');

    /* job post title */
    if(job_post_title.val() != ""){ 
      validator_job_post_title.addClass('hide');
    }else{
      validator_job_post_title.removeClass('hide');
      job_post_title.focus();
      passed[0] = false;
    }
    /* job_post_position */
    if(job_post_position.val() != ""){
      validator_job_post_position.addClass('hide');
    }else{
      validator_job_post_position.removeClass('hide');
      job_post_position.focus();
      passed[1] = false;
    }

    /* job description */
    if(job_post_description.val() != ""){
      validator_job_post_description.addClass('hide');
    }else{
      validator_job_post_description.removeClass('hide');
      job_post_description.focus();
      passed[2] = false;
    }

    /* job industry */
    if(job_post_industry_id.val() != "" && (job_post_industry_id.val() != "Other" || job_post_industry_other_id.val() != "")){
      validator_job_post_industry_id.addClass('hide');
      validator_job_post_industry_other_id.addClass('hide');
    }else{
          /* job industry others */
      if(job_post_industry_id.val() == "Other" && job_post_industry_other_id.val() == ""){
        validator_job_post_industry_id.addClass('hide');
        validator_job_post_industry_other_id.removeClass('hide');
        job_post_industry_id.focus();
        passed[11] = false;
      }else{
        validator_job_post_industry_other_id.addClass('hide');
        validator_job_post_industry_id.removeClass('hide');
        job_post_industry_id.focus();
        passed[3] = false;
      }

    }



     /* job classification */
     if(job_post_classification_id.val() != ""){
      validator_job_post_classification_id.addClass('hide');
    }else{
      validator_job_post_classification_id.removeClass('hide');
      job_post_classification_id.focus();
      passed[4] = false;
    }

    /* job salary min */
    if(job_post_salary_min.val() != ""){
      validator_job_post_salary_min.addClass('hide');
    }else{
      validator_job_post_salary_min.removeClass('hide');
      job_post_salary_min.focus();
      passed[5] = false;
    }

    /* job salary max */
    if(job_post_salary_max.val() != ""){
      validator_job_post_salary_max.addClass('hide');
    }else{
      validator_job_post_salary_max.removeClass('hide');
      job_post_salary_max.focus();
      passed[6] = false;
    }


    /* job no of positions */
    if(job_post_no_of_positions.val() != ""){
        validator_job_post_no_of_positions.addClass('hide');
      }else{
        validator_job_post_no_of_positions.removeClass('hide');
        job_post_no_of_positions.focus();
        passed[7] = false;
    }

     /* work location */
     if(job_post_location_id.val() != ""){
      validator_job_post_location_id.addClass('hide');
    }else{
      validator_job_post_location_id.removeClass('hide');
      job_post_location_id.focus();
      passed[8] = false;
    }

   

    // if(job_post_salary_min.val() > job_post_salary_max.val()){
    //   passed[9] = false;
    //   $('#salaryMaxErr').removeClass('hide');
    //   salaryMaxValue.focus();
    // }else{
    //   $('#salaryMaxErr').addClass('hide');
    // }

     for (var i = 0; i < passed.length; i++) {
      if(!passed[i]){
        passedAll = false;
      }
    }

    var $tag_job_post_skill = $('#tag_job_post_skills');
    
     if($tag_job_post_skill.val().trim()){
      $('#skillErr').addClass('hide');
    }else{
      $('#skillErr').removeClass('hide');
      $('.bootstrap-tagsinput').focus();
      passedAll = false;
    }

    if(passedAll == true){
      return true;
    }else{
      return false;
    }
}

 /**
 * @summary Tag input binding (suggestions are eager loaded)
 * 
 * @since 2017-12-04
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function initializeTagInput()
{
    var skills = new Bloodhound({

     datumTokenizer: function (datum) {
                         return Bloodhound.tokenizers.whitespace(datum.name);
                    },
     queryTokenizer: Bloodhound.tokenizers.whitespace,
     prefetch: {
         url: APP_URL + '/master-skills/all',
         cache: false,
                  transform: function(response) {

                      return $.map(response, function (name) {
                          return {
                              name: name.master_skill_name
                          }
                      });
                  }
    }

  });

  skills.initialize();

  $(".dvTag input").typeahead({
      hint: true,
      highlight: true,
      minLength: 1
  }, {
      source: skills.ttAdapter(),

      // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
      name: 'name',

      // override to custom if necessary
      // // the key from the array we want to display (name,id,email,etc...)
      // templates: {
      //         empty: [
      //             ''
      //         ],
      //         header: [
      //             ''
      //         ],
      //         suggestion: function (data) {
      //             return '<span>' +  data.name + '</span>'
      //   }
      // },
      display: 'name'
  });

}//end initializeTagInput

var imgDimensions = [];

/**
 * job-posting.s
 * js for job-posting-create
 * @author Karen Cano
 * @return true/false on submit
 * @since -3/19/2018 
 */
$(function() {



  initializeTagInput();

  $('#limitReachedModalBtn').click(function() {
    var modalId = $(this).data('modalid');

    var modal = $('#' + modalId);

    modal.html('');

    var modalHtml = '';

    modalHtml += '<div class="alert__modal">';
    modalHtml +=     '<div class="alert__modal--container">';
    modalHtml += '<p class="update__message">You have insufficient credits for posting a job.<br>Please buy a credit first before posting.</p><span><img id="closeIcon" onclick="$(\'.alert__modal--container\').fadeOut(3000);" src="http://localhost:8000/images/ico_remove.svg" alt=""></span>';
    modalHtml +=     '</div>';
    modalHtml += '</div>';

    modal.append(modalHtml);

    //for closing the modal
    $('#' + modalId+' div div span').click(function()  {
      $('.alert__modal--container').fadeOut(3000);
      setTimeout(() => {
        modal.html('');
      }, 3000);
    });

  });

  // Get the modal
  var modal = document.getElementById('how-to');
    
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
  }


  // $('#file_job_post_key_pc').on('change', function(){
  //   var file = document.getElementById('file_job_post_key_pc').files[0];
  //   img = new Image();
  //   img.src = window.URL.createObjectURL(file);
  //   var reqImgWidth = 1100;
  //   var reqImgHeight = 287;
  //   var validator_job_post_image = $('#imageErr');

  //   img.onload = function () {
  //     var imgWidth = this.width;
  //     var imgHeight = this.height;

  //     // alert(imgWidth + " " + reqImgWidth + " " + imgHeight + " " + reqImgHeight);
  //     if (imgWidth != reqImgWidth || imgHeight != reqImgHeight) {
  //       validator_job_post_image.removeClass('hide');
  //       $('#file_job_post_key_pc').focus();
  //       imgDimensions[0] =  false;
  //     }else{
  //       validator_job_post_image.addClass('hide');
  //       imgDimensions[0] = true;
  //     }
  //   };
  // });

  // $('#file_job_post_image1').on('change', function () {
  //   var file = document.getElementById('file_job_post_image1').files[0];
  //   img = new Image();
  //   img.src = window.URL.createObjectURL(file);
  //   var reqImgWidth = 539;
  //   var reqImgHeight = 290;
  //   var validator_job_post_image = $('#jobPostImage1Err');

  //   img.onload = function () {
  //     var imgWidth = this.width;
  //     var imgHeight = this.height;

  //     // alert(imgWidth + " " + reqImgWidth + " " + imgHeight + " " + reqImgHeight);
  //     if (imgWidth != reqImgWidth || imgHeight != reqImgHeight) {
  //       validator_job_post_image.removeClass('hide');
  //       $('#file_job_post_image1').focus();
  //       imgDimensions[1] = false;
  //     } else {
  //       validator_job_post_image.addClass('hide');
  //       imgDimensions[1] = true;
  //     }
  //   };
  // })

  // $('#file_job_post_image2').on('change', function () {
  //   var file = document.getElementById('file_job_post_image2').files[0];
  //   img = new Image();
  //   img.src = window.URL.createObjectURL(file);
  //   var reqImgWidth = 539;
  //   var reqImgHeight = 290;
  //   var validator_job_post_image = $('#jobPostImage2Err');

  //   img.onload = function () {
  //     var imgWidth = this.width;
  //     var imgHeight = this.height;

  //     // alert(imgWidth + " " + reqImgWidth + " " + imgHeight + " " + reqImgHeight);
  //     if (imgWidth != reqImgWidth || imgHeight != reqImgHeight) {
  //       validator_job_post_image.removeClass('hide');
  //       $('#file_job_post_image2').focus();
  //       imgDimensions[2] = false;
  //     } else {
  //       validator_job_post_image.addClass('hide');
  //       imgDimensions[2] = true;
  //     }
  //   };
  // })
  

  var salaryMinValue = $('input[name=job_post_salary_min]');
  var salaryMaxValue = $('input[name=job_post_salary_max]');

  $('#job-create-preview').click(function (e) {
    var isCorrectSalaryRange;
    // var isCorrectImgDimensions = true;
    
    if(parseInt(salaryMinValue.val()) > parseInt(salaryMaxValue.val())){
      $('#salaryMaxErr2').removeClass('hide');
      $('#salaryMaxErr').removeClass('hide');
      salaryMaxValue.focus();
      isCorrectSalaryRange = false;
    }else{
      $('#salaryMaxErr2, #salaryMaxErr').addClass('hide');
      isCorrectSalaryRange = true;
    }

    previewJobPost();

    // imgDimensions.forEach(function(field) {
    //   if(!field){
    //     isCorrectImgDimensions = false;
    //   }
    // });

    if (validate() == true && isCorrectSalaryRange) { //&& isCorrectImgDimensions
      $('#formJobPost').hide();
      $('#previewPost').show();
    }

  });
    
  $('#job-create-back').click(function (e) {
    $('#previewPost').hide();
    $('#formJobPost').show();
  });


  $('#btnJobPost').click(function (e) {
      var method = "post";
      var enctype = "multipart/form-data";
      var form = document.getElementById("companyForm");
      form.submit();
    
  });
  
  salaryMinValue.change(function(){
    salaryMaxValue.attr('min',salaryMinValue.val());
  });

  $("#file_job_post_key_pc").change(function() {
    readURL(this);
  });

  $("#file_job_post_key_sp").change(function() {
    readURL(this);
  });

  $("#file_job_post_image1").change(function() {
    readURL(this);
  });

  $("#file_job_post_image2").change(function() {
    readURL(this);
  });

  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
  
      reader.onload = function(e) {
        switch(input.attributes.id.value){
          case 'file_job_post_key_pc': $('#img_header_container').attr('src', e.target.result);
          break;
          case 'file_job_post_key_sp': $('#img_header_sp_container').attr('src', e.target.result);
          break;
          case 'file_job_post_image1': $('#img_job1_container').attr('src', e.target.result);
          break;
          case 'file_job_post_image2': $('#img_job2_container').attr('src', e.target.result);
          break;
          default:
          break;
        }
      }
  
      reader.readAsDataURL(input.files[0]);
    }
  }



  /*check salary range salary min < salary max*/
  salaryMaxValue.keyup(function() {
    if(salaryMaxValue.val() < salaryMinValue.val()){ // || salaryMaxValue.val() != ""
        $('#salaryMaxErr').removeClass('hide');
        salaryMaxValue.focus();
    }
    else{
        $('#salaryMaxErr').addClass('hide');
    }
  });

 /*change the number of images to upload*/
    var numOfImagesToUpload = $('select[name="numOfImages"]');

    numOfImagesToUpload.change(function(){
      if($(this).val() == 1){
        $('input[name=file_job_post_image2]').addClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image2]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image2]').val('');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(this).val() == 2){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(this).val() == 3){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('[name=image3]').removeClass('hide');
      }
  });


//set number of images to upload in page load in sync to number of file inputs
$(document).ready(function(){

  $('#previewPost').hide();

      if($(numOfImagesToUpload).val() == 1){
        $('input[name=file_job_post_image2]').addClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image2]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image2]').val('');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(numOfImagesToUpload).val() == 2){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').addClass('hide');
        $('[name=image3]').addClass('hide');
        $('input[name=file_job_post_image3]').val('');
      }
      if($(numOfImagesToUpload).val() == 3){
        $('input[name=file_job_post_image2]').removeClass('hide');
        $('input[name=file_job_post_image3]').removeClass('hide');
        $('[name=image2]').removeClass('hide');
        $('[name=image3]').removeClass('hide');
      }


  if ($('#job_post_industry_id').val() == "Other"){
    document.getElementById('div1').show();
  } else {
    document.getElementById('div1').hide();
  }

});
 
});

function showfield(name){
  if(name=='Other')document.getElementById('div1').innerHTML='<br>Please specify: <input type="text" name="job_post_others" />';
  else {
    document.getElementById('div1').innerHTML='';
  }

}



function previewJobPost(){
  var reader = new FileReader();
  var classifications = document.getElementById('job_post_classification_id');
  var locations = document.getElementById('job_post_location_id');
  var TimeStart = document.getElementById('job_post_work_timestart');
  var TimeEnd = document.getElementById('job_post_work_timeend');

  var defheaderimg = document.getElementById('header-image-default').value;
  var defjobimg = document.getElementById('job-image-default').value;

  var imgheaderContainer  = document.getElementById('img_header_container').src;
  var imgJob1Container    = document.getElementById('img_job1_container').src;
  var imgJob2Container    = document.getElementById('img_job2_container').src;

  if(imgheaderContainer)
    document.getElementById('header_image').src = imgheaderContainer;
  else
    document.getElementById('header_image').src = defheaderimg;

  if(imgJob1Container)
    document.getElementById('job_image1').src = imgJob1Container;
  else
    document.getElementById('job_image1').src = defjobimg;

  if(imgJob2Container)
    document.getElementById('job_image2').src = imgJob2Container;
  else
    document.getElementById('job_image2').src = defjobimg;


  
  document.getElementById("job_title").innerHTML            = null ? '' : document.getElementById('job_post_title').value;
  document.getElementById('job_position').innerHTML        = null ? '' : document.getElementById('job_post_job_position_id').value;
  document.getElementById('job_business').innerHTML         = null ? '' : document.getElementById('job_post_business_contents').value;
  document.getElementById('job_description').innerHTML      = null ? '' : document.getElementById('job_post_description').value;
  document.getElementById('job_overview').innerHTML         = null ? '' : document.getElementById('job_post_overview').value;
  document.getElementById('job_requirements').innerHTML     = null ? '' : document.getElementById('job_post_qualifications').value;
  document.getElementById('job_classification').innerHTML   = null ? '' : classifications.options[classifications.selectedIndex].text;
  document.getElementById('job_vacancy').innerHTML          = null ? '' : document.getElementById('job_post_no_of_positions').value;
  document.getElementById('job_min').innerHTML              = null ? '' : 'PHP ' + document.getElementById('job_post_salary_min').value + '.00';
  document.getElementById('job_max').innerHTML              = null ? '' : 'PHP ' + document.getElementById('job_post_salary_max').value + '.00';
  document.getElementById('job_process').innerHTML          = null ? '' : document.getElementById('job_post_process').value;
  document.getElementById('job_location').value             = null ? '' : locations.options[locations.selectedIndex].text;
  document.getElementById('job_start').innerHTML            = null ? '' : TimeStart.options[TimeStart.selectedIndex].text;
  document.getElementById('job_end').innerHTML              = null ? '' : TimeEnd.options[TimeEnd.selectedIndex].text;
  document.getElementById('job_benefits').innerHTML         = null ? '' : document.getElementById('job_post_benefits').value;
  document.getElementById('job_holiday').innerHTML          = null ? '' : document.getElementById('job_post_holiday').value;
  document.getElementById('job_characteristics').innerHTML  = null ? '' : document.getElementById('job_post_otherinfo').value;
  
	// var $paymentMethodConfirm = $('.js-table__tr-payment_confirm');
	// var selectedPlan =  $('input[name="rb_plan_type"]:checked').val()
	// var ulFilesConfirm = document.getElementById('ul-files_confirm');
	// ulFilesConfirm.innerHTML = ''; 

		// for (var i = 1; i <= 3; i++) {

		// 	var companyFile = document.getElementById('companyFile' + i);

		// 	if (companyFile.value) {
		// 		var li = document.createElement('li');
		// 		var label = i + ". ";
		// 		var liContent = label + companyFile.files[0].name;

		// 		li.textContent = liContent;
		// 		li.value = companyFile.files[0].name;

		// 		ulFilesConfirm.appendChild(li);
		// 	}
		// }

		// $("input[name=rb_plan_type_selected][value=" + selectedPlan + "]").attr('checked', 'checked');

		// $(".plan").removeClass("active");
		// if($("input[name=rb_plan_type_selected][value=" + selectedPlan + "]").is(":checked")){
		// 	$("input[name=rb_plan_type_selected][value=" + selectedPlan + "]").closest('li').addClass("active");
		// }

		// if(selectedPlan == 100){
		// 	$paymentMethodConfirm.hide();
		// }else{
		// 	if(!$paymentMethodConfirm.is(':visible')) {
		// 		$paymentMethodConfirm.show();
		// 	}
		// }

		// $('.registration__confirmation').css({
		// 	'display': 'block'
		// });
		// $('#confirm-company-view').css({
		// 	'display': 'block'
		// });
		// $('.edit-view').css({
		// 	'display': 'none'
		// });
}



