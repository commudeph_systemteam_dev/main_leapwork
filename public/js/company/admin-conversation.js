

 /////
 /**
 * 
 * @summary js for company-contact.blade.php
 * @since 2017-12-13
 * @author  Karen Irene Cano<karen_cano@commude.ph>
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @author  Arvin Alipio <aj_alipio@commude.ph>
 * @link url('company/scout')
 */



//チャットのプルダウン
function btn_pulldown() {
  $('.cloesed_chat').click(function() {

      var $this = $(this);
      var id = $(this).data('t_id');
      $this.next().stop().slideToggle(300, function(){
        scrollBottom('#message_area_'+id+' div.messages_container');
    });
      $this.find('.btn_img').stop().toggleClass('on_off');

  });
}

//受信・送信ボックス切り替えタブ
function tab() {
  $('.tab_send_mail').click(function(){
        changeTab($(this), 'dvSent');
    });

  $('.tab_recieve_mail').click(function() {
        changeTab($(this), 'dvInbox');
  });

  $('.tab_compose_mail').click(function() {
        changeTab($(this), 'dvCompose');
  });

}

/**
 * @summary Change tab event handler (generic)
 *
 * @since 2017-11-24
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * 
 * @listens btnReplyConversation click
 *
 */
function changeTab($src, targetDiv)
{
    var $this = $src;
    var $dvTarget = $('#' + targetDiv);
    var $dvTabs   = $('.dvTab');

    $('.tab').removeClass('active');
    $dvTabs.stop().hide();

    $this.stop().addClass('active');
    $dvTarget.stop().show();
  
    $dvTarget.find('.opened_chat_send').stop().removeClass('on_off');
    $dvTarget.find('.btn_reply').stop().hide();
}

 /**
 * @summary Reply conversation form validation
 * 
 * @since 2017-11-16
 * @access private
 * 
 * @param dom $src div of button
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateReplyForm($src)
{
    var passed = true;
    var $srcDiv = $src.parent().parent();

    $srcDiv.each( function(index){
        var $this = $(this); //current div.dv_college_entry
        
        $this.find('input, textarea').filter(function () {

            if($.trim($(this).val().trim()).length == 0)
            {
                $this.find('.spErrorMessage').removeClass('hidden');
                passed = false;
            }
            else
                $this.find('.spErrorMessage').addClass('hidden');

        });
    });

    // static validation    
    // var passed = true;
    // var $srcDiv = $src.parent().parent();
    // var txtConversationMessage = $srcDiv.find('.ctxtInput').val().trim();

    // if(!txtConversationMessage)
    // {
    //  passed = false;

    //  $srcDiv.find('.spConversationMessage .text').text('Please enter a message');
    //  $srcDiv.find('.spConversationMessage').show();
    // }
    // else
    //  $srcDiv.find('.spConversationMessage').hide();
    
    return passed;
}

 /**
 * @summary Reply conversation form validation
 * 
 * @since 2017-11-16
 * @access private
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 *
 */
function validateComposeForm()
{
    var passed = true;
    var $txtTitleNew   = $('#txt_title_new');
    var $txtMessageNew = $('#txt_message_new');

    if(!$txtTitleNew.val().trim())
    {
        passed = false;

        $('#spTitle .text').text('Please enter title');
        $('#spTitle').removeClass('hidden');

        $txtTitleNew.focus();

    }
    else
        $('#spTitle').addClass('hidden');
    
    if(!$txtMessageNew.val().trim())
    {
        passed = false;

        $('#spMessage .text').text('Please enter a message');
        $('#spMessage').removeClass('hidden');

        $txtMessageNew.focus();

    }
    else
        $('#spMessage').addClass('hidden');
    
    return passed;
}

 /**
 * @summary Company job application handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @updated 
 * @link     URL
 * @since    2017-11-10
 * @requires jquery-1.12.0.min.js
 *
 */
$(function(){

    btn_pulldown();//チャットのプルダウン
    tab();//受信・送信ボックス切り替えタブ

    $('#btnPreview').click(function () 
    {
        var form    = document.getElementById('frmCompanies');
        form.action = APP_URL + '/company/applicant/job-application/message-preview';
        form.target = "_blank";
        form.submit();

    });


    $('#btnSendEmail').click(function () 
    {
        if(validateform())
        {
            var form    = document.getElementById('frmCompanies');
            form.action = APP_URL + '/company/notice/details/sendCompanyEmailConversation';
            form.target = "_self";
            form.submit();
        }
    });

    $("#btnConvo").click(function(){
        $("#dvConversation").toggle();
    });

    $('#so_company_template').change(function() 
    {
        var $this = $(this);
        var template = $this.val();
        
        renderTemplate(template);

    });

    $('#master_mail_status').change(function() 
    {
        renderTemplate();

    });
    /**
     * @summary Dispaly compose pane
     *
     * @since 2017-12-01
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens dvBtnMail click
     *
     */
    $('#btnNewMail').click(function () 
    {
        var $tabComposeMail = $('.tab_compose_mail');
        //hide all tabs
        $('.tab').hide();
        $('.dvTab').hide();

        //show compose panes    
        $('#dvCompose').show();

        $tabComposeMail.addClass('active');
        $tabComposeMail.show();
    });


    /**
     * @summary Close compose pane
     *
     * @since 2017-12-01
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnCloseCompose click
     *txt_message_new
     */
    $('#btnCloseCompose').click(function () 
    {
        var $tabComposeMail = $('.tab_compose_mail');

        $tabComposeMail.removeClass('active');

        $('.tab').show();
        $('.active').click(); //reactive last tab
        
        $('#dvCompose').hide();
        $tabComposeMail.hide();
    });

    /**
     * @summary Reply to existing threads/conversation
     *
     * @since 2017-11-24
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnReplyConversation click
     *
     */
    $('.btnReplyConversation').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            var currentThreadId = $this.data('thread-id');
            var currentMessage  = $inputDiv.find('.ctxtInput').val();
            var icon = $('#userIcon').length
                        ? $('#userIcon').val()
                        : null;

            var data = {
                'admin_company_thread_id' :  currentThreadId,
                'admin_company_conversation_message' : currentMessage,
                'userIcon' : icon
            };

            sendAjax('post', "/company/notice/details/replyCompanyConversation", data);

            appendMessage(currentThreadId, currentMessage, icon, 'right', $('#userName').val());

            $inputDiv.find('.ctxtInput').val('');

            // scrollBottom('#master-contact-view', currentThreadId);


            // var $adminCompanyThreadId = $('#admin_company_thread_id');
            // var $jadminCompmanyConversationMessage  = $('#admin_company_conversation_message');
            // var form = document.getElementById("frmCompanies");
            
            // $adminCompanyThreadId.val(currentThreadId);
            // $jadminCompmanyConversationMessage.val(currentMessage);
            
            // form.action = APP_URL+ "/company/notice/details/replyCompanyConversation";
            // form.submit();
        }

    });

    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmReply click
     *
     */
    $('.btnConfirmReply').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateReplyForm($inputDiv))
        {
            $inputDiv.find(':input').prop('readonly', true);
            $inputDiv.find(':input').addClass('matched');

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });

    /**
     * @summary Hide confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnBackReinput click
     *
     */
    $('.btnBackReinput').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        $inputDiv.find(':input').prop('readonly', false);
        $inputDiv.find('select').removeAttr('disabled');

        $inputDiv.find(':input').removeClass('disabled');
        $inputDiv.find(':input').removeClass('matched');
        
        $this.parent().addClass('hidden');
        $this.parent().parent().find('.dvMainButtons').show();

    });

    /**
     * @summary Toggle confirmation pane
     *
     * @since 2017-11-27
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnConfirmSend click
     *
     */
    $('.btnConfirmSend').click(function()
    {
        var $this = $(this);
        var $inputDiv = $this.parent().parent().parent();

        if(validateComposeForm())
        {
            $inputDiv.find(':input').prop('readonly', true);
            $inputDiv.find('select').prop('disabled', true);

            $inputDiv.find(':input').addClass('disabled');

            $this.parent().hide();
            $this.parent().parent().find('.dvConfirmButtons').removeClass('hidden');
        }

    });


    /**
     * @summary Compose/create new thread as an inquiry
     *
     * @since 2017-11-24
     * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * 
     * @listens btnComposeThread click
     *
     */
    $('#btnComposeThread').click(function()
    {
        var $this = $(this);

        if(validateComposeForm())
        {
            var form = document.getElementById("frmCompanies");

            form.action = APP_URL+ "/company/notice/details/sendCompanyEmailConversation";
            form.submit();
        }

    });

});//end document ready

