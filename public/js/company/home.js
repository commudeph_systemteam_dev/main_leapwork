
/**
 * home.js
 * js for company/home
 * @author Karen Cano
 * @return 
 */

$(document).ready(function() {
    $('.alert__modal--container span').on('click', function(){
        $('.alert__modal').fadeOut();
    });
    
    $("#activateModal").show();


    
});

function activatePlan() {
    var modal = document.getElementById('activateModal');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}


