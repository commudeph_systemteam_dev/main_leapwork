/**
 * @summary JS for auth.js
 * @since 2017-10-22
 * @author  Nicole Manalo <nicole_manalo@commude.ph>
 * @author  Karen Irene Cano <karen_cano@commude.ph>
 */

 /**
 * @summary selectPlan()
 * Adds active class on selected plan type
 * @since 2017-10-22
 * @author  Nicole Manalo <nicole_manalo@commude.ph>
 */
function selectPlan() {
    
   $("ul.plan-list li").on("click",function() {
        var isDisabled = $(this).find('input[type="radio"]').prop('disabled');
        if($(this).find('input[type="radio"]').is(':checked') && isDisabled == false) { 
            $('ul.plan-list li').removeClass('active');
            $('ul.plan-list li label').removeAttr('disabled');
            $('ul.plan-list li input').removeAttr('disabled');
            $(this).addClass('active');
            $(this).find('label').prop('disabled', true);
            // $(this).find('input').prop('disabled', true);
        }
    });
}

 /**
 * 
 * Adds active class on selected plan type
 * 
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @author Karen Irene Cano <karen_cano@commude.ph>
 * @since  2018/03/20 Louie: Add company name validation and revise file inputs.
 *         Cleaned up variables as well.
 * @since  2017/10/23 Karen: Initial release
 * 
 * @fires  svalidator.isValidEmail
 * 
 * @return void
 */
function validateRegisterForm() {
    
    // LOAD constants
    var sizeLimit   = constants.MAX_FILE_UPLOAD;
    
    // Get fields to validate
    var companyName = document.getElementById('companyName');
    var password    = document.getElementById('password');
    var passwordConfirm = document.getElementById('password-confirm');
    var email = document.getElementById('email');
    var contactNumber = document.getElementById('contactNumber');
    var termsAndConditions = document.getElementById("termsAndConditions");
    var postalCode = document.getElementById('codeValue');
    var address1 = document.getElementById('address1');
    // var companyfile1  = document.getElementById('companyFile1');
    // var companyfile2  = document.getElementById('companyFile2');
    // var companyfile3  = document.getElementById('companyFile3');

    // Span errors
    var spCompanyFile = document.getElementsByClassName('js-company_file-error')[0];
    var spEmail = document.getElementsByClassName('js-email_error')[0];
    var spCompanyNameError = document.getElementsByClassName('js-company_name-error')[0];
    var spPassword = document.getElementsByClassName('js-password_error')[0];
    var spPasswordConfirm = document.getElementsByClassName('js-password_confirm_error')[0];
    var spPostalCode = document.getElementsByClassName('js-postal_code_error')[0];
    var spAddress1 = document.getElementsByClassName('js-address1_error')[0];
    
    // Span error containers
    var spEmailErrors = document.getElementsByClassName('js-email-error_list')[0];
    var spFileErrors = document.getElementsByClassName('js-company_file-error_list')[0];
    
    var passed = true;

    if (!postalCode.value.trim()){
        passed = false;
        spPostalCode.firstElementChild.innerHTML = postalCode.dataset.errorRequired;
        spPostalCode.style.display = 'block';
        postalCode.focus();
    }
    else{
        spPostalCode.firstElementChild.innerHTML = '';
        spPostalCode.style.display = 'none';
    }

    if (!address1.value.trim()) {
        passed = false;
        spAddress1.firstElementChild.innerHTML = address1.dataset.errorRequired;
        spAddress1.style.display = 'block';
        address1.focus();
    }
    else {
        spAddress1.firstElementChild.innerHTML = '';
        spAddress1.style.display = 'none';
    }
    
    if (!companyName.value.trim()) {
        passed = false;
        spCompanyNameError.innerHTML = companyName.dataset.errorRequired;
        spCompanyNameError.style.display = 'block';
        companyName.focus();
    }
    else {
        spCompanyNameError.style.display = 'none';
    }
    
    if (!email.value) {
        passed = false;
        spEmail.innerHTML = spEmailErrors.dataset.errorRequired;
        spEmail.style.display = 'block';
        email.focus();
    } 
    else if (email.value && (!isValidEmail(email.value)) ){
        passed = false;
        spEmail.innerHTML = spEmailErrors.dataset.errorFormat;
        spEmail.style.display = 'block';
        email.focus();
    }
    else {
        spEmail.innerHTML = '';
        spEmail.style.display = 'none';
    }

    // var phonepattern = /[09]\d{9}$/;
    var phonepattern = /^-?\d+\.?\d*$/;
    if (!contactNumber.value) {
        passed = false;
        $('.phone_error').css({
            'display': 'block'  
        });

        contactNumber.focus();
    } else if (!phonepattern.test(contactNumber.value)){
        passed = false;
        $('.phone_format_error').css({
            'display': 'block'  
        });

        contactNumber.focus();
    } else {
        $('.phone_error, .phone_format_error').css({
            'display': 'none'
        });
    }
    // }else if(contactNumber.value.length != 11 || !phonepattern.test(contactNumber.value)){
    //     passed = false;
    //     $('.phone_format_error').css({
    //         'display': 'block'  
    //     });

    //     contactNumber.focus();
    // 

    



    if (!password.value) {
        passed = false;
        $('.password_error').css({
            'display': 'block'
        });
        password.focus();
    } else if (password.value.length < 6) {
        passed = false;
        spPassword.firstElementChild.innerHTML = password.dataset.errorMin;
        spPassword.style.display = 'block';
    } else {
        $('.password_error').css({
            'display': 'none'
        });
    }

    if (!passwordConfirm.value || passwordConfirm.value.trim() != password.value.trim()) {
        passed = false;
        $('.password_error_missmatch').css({
            'display': 'block'
        });
        passwordConfirm.focus();    
    } else if (passwordConfirm.value.length < 6 && passwordConfirm.value.trim() == password.value.trim()) {
        passed = false;
        spPasswordConfirm.firstElementChild.innerHTML = passwordConfirm.dataset.errorMatch;
        spPasswordConfirm.style.display = 'block';

        spPasswordConfirm.focus();
    } else {
        $('.password_error_missmatch').css({
            'display': 'none'
        });
    }

    // Temporarily use the disabled functionality to assume that the radio group have been selected
    // var planSelected = $('input[name=radio-group-plan]').prop('disabled');

    // // true is disabled
    // if (planSelected == true) {
    //     passed = false;
    //     $('#planTypeJS').addClass('hide');

    //     $('input[name="radio-group-plan"]').focus();
    // } else {
    //     $('#planTypeJS').removeClass('hide');
    //     $('#planTypeJS').addClass('has-error');
    // }

    var fileNames = [];

    // Loop through files
    for (var i = 1; i <= 3; i++) {

        var companyFile = document.getElementById('companyFile' + i);
        
        // Proceed to file attribute valiation only if file is uploaded
        if (companyFile.value) {
            
            // File size check
            if (companyFile.files[0].size > sizeLimit) {
                passed = false;
                spCompanyFile.style.display = "block";
                spCompanyFile.firstElementChild.innerHTML = "Cannot upload a file greater than 10mb";
            } 

            // File extension check
            // svalidator.inAllowedFileExtensions
            if (!(inAllowedFileExtensions(companyFile.value)) ) {
                passed = false;
                spCompanyFile.style.display = "block";
                spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorFormat;
            }
            
            // Duplicate file name check
            // svalidator.isInArray
            // if (isInArray(companyFile.files[0].name, fileNames))
            // {
            //     passed = false;
            //     spCompanyFile.style.display = "block";
            //     spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorDuplicate;
            // }
            
            // Assume file count since value exists
            fileNames.push(companyFile.files[0].name);
        }        
    }

    if (fileNames.length < 1){
        passed = false;
        spCompanyFile.style.display = "block";
        spCompanyFile.firstElementChild.innerHTML = spFileErrors.dataset.errorRequired;

        spCompanyFile.focus();
    }
    else if(passed)
    {
        spCompanyFile.firstElementChild.innerHTML = '';
        spCompanyFile.style.display = "none";
    }

    if (!termsAndConditions.checked) {
        passed = false;
        $('.conditions_error').css({
            'display': 'block'
        });

    } else {
        $('.conditions_error').css({
            'display': 'none'
        });
    }

    // Plan check - original
    if ($('input[name="radio-group-plan"]').is(':checked') == true) {
        // $('#planTypeJS').removeClass('hide');
        $('#planTypeJS').addClass('hide');

        $('input[name="radio-group-plan"]').focus();
    } else {
        passed = false;

        $('#planTypeJS').removeClass('hide');
        $('#planTypeJS').addClass('has-error');
    }

    return passed;
}

/**
 * Append ajax promises
 *
 * @param    Validator $validator
 * @modifies $validator
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since  2018/03/21
 * 
 * @return void
 */
function addCustomPromises(validator) {

    var emailObj = document.getElementById('email');
    var apiPath = APP_URL
                + '/email/'
                + emailObj.value;
    
    var companyNameObj = document.getElementById('companyName');
    var companyNameApiPath = APP_URL
        + '/company/name/'
        + companyNameObj.value;
    
    // Do not validate empty fields
    if(emailObj.value.trim())
    {
        validator.validators.emailValidationPromise = {

            message: emailObj.dataset.errorExists,
            ok: function (value) {
                var $deferred = $.Deferred();
                $.get(apiPath)
                    .done(function (data) {

                        // Exists and cause error
                        if (data == true) {
                            $deferred.reject();
                        }
                        else {
                            $deferred.resolve(data);
                        }

                    })
                    .fail(function () {
                        console.log("emailValidationPromise ajax fail");
                        $deferred.reject();
                    });
                return $deferred.promise();
            }
        };
    }

   
    if (companyNameObj.value.trim())
    {
        validator.validators.companyNameValidationPromise = {

            message: companyNameObj.dataset.errorExists,
            ok: function (value) {
                var $deferred = $.Deferred();
                $.get(companyNameApiPath)
                    .done(function (data) {

                        // Exists and cause error
                        if (data == true) {
                            $deferred.reject();
                        }
                        else {
                            $deferred.resolve(data);
                        }

                    })
                    .fail(function () {
                        console.log("companyNameValidationPromise ajax fail");
                        $deferred.reject();
                    });
                return $deferred.promise();
            }
        };
    }
    
    validator.rules = [{
            id: "#email",
            name: "email",
            checks: ["emailValidationPromise"]                
        }, {
            id: "#companyName",
            name: "companyName",
            checks: ["companyNameValidationPromise"]
        }
    ];

}

function backToCompanyInput() {
    $('.registration__confirmation').css({
        'display': 'none'
    });
    $('.registration__input').css({
        'display': 'block'
    });

}

/**
 * AJAX: Company name exist validation
 *
 * @param string $companyName
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since  2018/03/20
 * 
 * @return void
 */
function isCompanyNameExists(companyName) 
{
    var companyNameObj = document.getElementById('companyName');
    var spCompanyNameError = document.getElementsByClassName('js-company_name-error')[0];
    var apiPath = APP_URL 
                + '/company/name/' 
                + companyName;
    
    spCompanyNameError.style.display = 'none'; // Clear immediately
    
    $.ajax({
        type: "GET",
        url: apiPath,
        success: function (data) {
            console.log(arguments.callee.name, 'success');

            if(data == true){
                spCompanyNameError.innerHTML = companyNameObj.dataset.errorExists;
                spCompanyNameError.style.display = 'block';
            }
            else if(companyName){
                spCompanyNameError.style.display = 'none';
            }
        },
        error: function (data) {
            console.log(arguments.callee.name, data);
        }
    });
}

/**
 * AJAX: Email exist validation 
 *
 * @param string $email
 *
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since 2018/03/21
 * 
 * @return ajax response
 *
 */
function isEmailExists(email) {

    var emailObj = document.getElementById('email');
    var spEmailError = document.getElementsByClassName('js-email_error')[0];
    var apiPath = APP_URL
                + '/email/'
                + email;

    spEmailError.style.display = 'none'; // Clear immediately

    $.ajax({
        type: "GET",
        url: apiPath,
        success: function (data) {
            console.log(arguments.callee.name, 'success');
            
            if (data == true) {
                spEmailError.innerHTML = emailObj.dataset.errorExists;
                spEmailError.style.display = 'block';
            }
            else if(email) {
                spEmailError.style.display = 'none';
            }

        },
        error: function (data) {
            console.log(arguments.callee.name, data);
        }
    });

}

 /**
 * @summary validateRegisterCompany()
 * Validates the page before submitting on POST to retain the file input
 * 
 * @author Karen Irene Cano <karen_cano@commude.ph>
 * @author Martin Louie Dela Serna <martin_delaserna@commude.ph> 
 * @since  2018/03/20 Louie: Changed input file from multiple to 3 individual
 * @since  2017/10/23 Karen: Initial Release
 * 
 * @return void
 */
function validateRegisterCompany(){
    
    var passed = false;
    
    /**Plan check */
    if($('input[name="radio-group-plan"]').is(':checked')){
        $('#planTypeJS').removeClass('hide');
        $('#planTypeJS').addClass('hide');
         passed = true;
    }else{
        $('#planTypeJS').removeClass('hide');
        $('#planTypeJS').addClass('has-error');
         passed = false;
    }

    var sizeLimit     = constants.MAX_FILE_UPLOAD;
    var companyfile1  = document.getElementById('companyFile1');
    var companyfile2  = document.getElementById('companyFile2');
    var companyfile3  = document.getElementById('companyFile3');
    var spCompanyFile = document.getElementById('company_file-error_message');
     
     // Loop through files
     for (var i=1; i <= 3; i++){
         
         var companyFile = document.getElementById('companyFile' + i);
         
         /**File size check */
         if (passed = true && companyFile.size > sizeLimit)
         {
            spCompanyFile.style.display = "block";
            passed = false;
         }else{
            spCompanyFile.style.display = "none";
            passed = true;
         }

         /**File extension check */ 
         // svalidator.inAllowedFileExtensions
         if (passed = true && !(inAllowedFileExtensions(companyFile.value))){
            spCompanyFile.style.display = "block";
            passed = false;
         }
         else{
            spCompanyFile.style.display = "none";
            passed = true;
         }
     }

    /**Terms and Conditions check */
    var $termsAndConditions = $('#termsAndConditions');
    if(passed = true && !($termsAndConditions.is(':checked'))){
        $('#termsAndConditionsJS').removeClass('hide');
        passed = false;
    }
    else{
        $('#termsAndConditionsJS').removeClass('hide');
        $('#termsAndConditionsJS').addClass('hide');
        passed = true;
    }

    return passed;
}

/**
 * Load confirmation page via hiding the register page. Reusing
 * the legacy and just moved to a function for convenience.
 * 
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since   2018/03/20
 *
 * @return  void
 */
function loadConfirmView()
{

    document.getElementById('companyNameConfirm').value = document.getElementById('companyName').value;
    document.getElementById('companyWebsiteConfirm').value = document.getElementById('companyWebsite').value;
    document.getElementById('contactNameConfirm').value = document.getElementById('contactName').value;
    document.getElementById('contactNumberConfirm').value = document.getElementById('contactNumber').value;
    document.getElementById('codesValueConfirm').value = document.getElementById('codeValue').value;
    document.getElementById('address1Confirm').value = document.getElementById('address1').value;
    document.getElementById('address2Confirm').value = document.getElementById('address2').value;
    document.getElementById('payment_methodConfirm').value = document.getElementById('payment_method').value;
    document.getElementById('emailConfirm').value = document.getElementById('email').value;

    // TODO: get new input types
    // var files = document.getElementById("businesspermit").files;
    // var permitFiles = files[0].name;

    // for (var i = 1; i < files.length; i++)
    // {
    //   permitFiles =   permitFiles + " / " + files[i].name; 
    // }

    var fileNames = '';
    var ulFilesConfirm = document.getElementById('ul-files_confirm');
    ulFilesConfirm.innerHTML = ''; // clear children first
    
     // Loop through files
    for (var i = 1; i <= 3; i++) {

        var companyFile = document.getElementById('companyFile' + i);
        
        // Proceed to file attribute valiation only if file is uploaded
        if (companyFile.value) {
            var li = document.createElement('li');
            var label = i  + ". ";
            var liContent = label + companyFile.files[0].name;

            li.textContent = liContent;
            li.value = companyFile.files[0].name;

            ulFilesConfirm.appendChild(li);
        } 
    }

    var selectedPlan = $('input[name="radio-group-plan"]:checked').val();
    var $paymentMethodConfirm = $('.js-table__tr-payment_confirm');

    if (selectedPlan == "TRIAL") {
        $paymentMethodConfirm.hide();
    } else {
        if(!$paymentMethodConfirm.is(':visible')) {
            $paymentMethodConfirm.show();
        }
    }

    $('.registration__confirmation').css({
        'display': 'block'
    });
    $('.registration__input').css({
        'display': 'none'
    });

    var planSelected = $('#radio-group-plan-hidden');

    if ($('input[name=radio-group-plan]:checked').val() == 'TRIAL') {
        $('.selected li:nth-child(1)').addClass('active');
        planSelected.val('TRIAL');
    } else if ($('input[name=radio-group-plan]:checked').val() == 'STANDARD') {
        $('.selected li:nth-child(2)').addClass('active');
        planSelected.val('STANDARD');
    } else if ($('input[name=radio-group-plan]:checked').val() == 'ENTERPRISE') {
        $('.selected li:nth-child(3').addClass('active');
        planSelected.val('ENTERPRISE');

    }


    window.location.hash = "top";
}

$(window).on('load', function () {
}); // load


$(window).on('resize', function () {
}); // resize

$(window).scroll(function () {
}); // scroll

$(window).on('load resize', function () {
}); // load resize


/**
 * Main boot loader
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since   2018/03/20
 *
 */
$(function(){

    selectPlan();

    /**
     * EVENT: Set hidden field to bypass disabled form value.
     * Handles toggle of payment method option.
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/20
     *
     * @fires  this.isCompanyNameExists
     *      
     */
    $('input[name=rb_plan_type]').change(function (){
        var $this = $(this);
        var radioGroupPlanHidden = document.getElementById('rb_plan_type');
        var $trPayment = $('.js-table__tr-payment');

        radioGroupPlanHidden.value = $this[0].value;

        if($this.val() == "TRIAL")
        {
            $trPayment.hide();
        }
        else
        {
            if(!$trPayment.is(':visible'))
            {
                $trPayment.show();
            }
        }
    });

    $('input[name=radio-group-plan]').on('change', function (){
        var $trPayment = $('.js-table__tr-payment');

        if($('input[name=radio-group-plan]:checked').val() == "TRIAL")
        {
            $trPayment.hide();
        }
        else
        {
            if(!$trPayment.is(':visible'))
            {
                $trPayment.show();
            }
        }
    });
    
    /**
     * EVENT: Check company name exists on blur
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/20
     *
     * @fires  this.isCompanyNameExists
     *      
     */
    $("#companyName").blur(function () {
        
        companyName = document.getElementById('companyName');

        if(companyName.value.trim())
        {
            // isCompanyNameExists(companyName.value);
        }        
    });

    /**
     * EVENT: Check email exists on blur
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/21
     *
     * @fires  this.isEmailExists
     *      
     */
    $("#email").blur(function () {

        email = document.getElementById('email');

        if (email.value.trim()) {
            // isEmailExists(email.value);
        }
    });
    
     /**
     * EVENT: Validate register form before proceeding to confirm
     *
     * @author Martin Louie Dela Serna <martin_delaserna@commude.ph>
     * @since  2018/03/20
     *
     * @fires  this.validateRegisterForm
     * @fires  this.loadConfirmView
     * @fires  addCustomPromises
     * @fires  validatorUtility (validatory_utility.js)
     *      
     * @return void
     */
    $('#btn_confirm').click(function (event) {

        var valid = validateRegisterForm()

        // validator_utility.formValidator
        var validator = new FormValidator();

        addCustomPromises(validator);

        event.preventDefault();

        validator.validate()
            .done(function () {
                console.log("passed");
                
                if(valid) { 
                    validatorUtility.clearErrors();
                    
                    loadConfirmView();
                }
            })
            .fail(function (errors) {
                console.log("fail");
                validatorUtility.clearErrors();
                validatorUtility.showErrors(errors);
            });

        

    });

    $("#frmRegisterCompany").on('submit',function(){
        var $this = $(this);
        var selectedPlan = $('input[name="radio-group-plan"]:checked').val();
        var $paymentMethod = $('#payment_methodConfirm');

        if (selectedPlan == "TRIAL")
        {
            $paymentMethod.val(""); 
        }
        
        $this.prop('disabled', true);
    });

}); // ready
