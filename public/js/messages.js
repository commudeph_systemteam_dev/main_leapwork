



/**
 * append the message to the message area
 *
 * @param {string} id
 * @param {string} message
 * @param {string} icon
 * @param {string} position
 * @param {string} name
 * @param {string} date
 * @param {boolean} isScout
 *
 * @author Alvin Generalo <alvin_generalo@commude.ph>
 */
function appendMessage(id, message, icon, position, name = 'Admin', date = moment().format('lll'), isScout = false)
{
    var messageAreaContainer = !isScout
                    ? '#message_area_'+id+' div.messages_container'
                    : '#message_area_'+id+' div div.messages_container';


    var messageArea = $(messageAreaContainer);

    var htmlMessage = '';

    htmlMessage += !isScout ? '<div class="opened_chat_inner opened_chat_recieve">' : '';
    htmlMessage +=    ' <div class="chat_area layout_2clm clearfix">';
    htmlMessage +=        '<figure class="icon '+ position +'">';
    htmlMessage +=         ' <img src="'+icon+'" alt="">';
    htmlMessage +=    '</figure>';
    htmlMessage +=       '<div class="icon__username">';
    htmlMessage +=            name;
    htmlMessage +=           '<span class="message__date">'+ date +'</span>';
    htmlMessage +=      '</div>';
    htmlMessage +=       '<p class="message_contents '+ position +'">';
    htmlMessage +=          validator.escape(message);
    htmlMessage +=       '</p>';
    htmlMessage +=   '</div> ';
    htmlMessage += !isScout ? '</div>' : '';

    messageArea.append(htmlMessage);

    scrollBottom(messageAreaContainer);

}

function scrollBottom(id)
{
    // var bottom = $(id).position().top + $(id).outerHeight(true);

    // $(id).scrollTop(bottom);

    $(id).animate({scrollTop: $(id).prop('scrollHeight')});
}


//=====================================================================================================================================================


var messageIcon;

var messageNotifArea;

var messageArea;

/**
 * this function will set global variables declared in messages.js
 *
 * @author Alvin Generalo <alvin_generalo@commude.ph>
 */
function initializeGlobalVarMessage()
{
    messageIcon = $('li.function.applicant span');

    messageNotifArea = $('li.function.applicant ul.tooltip div.tooltip__container li.item').last();
}

/**
 * this function will set events for necessary variables
 *
 * @author Alvin Generalo <alvin_generalo@commude.ph>
 */
function setEvents()
{
    messageIcon.click(messageIconClickEvent);

    //userId is declared in notification.js
    if(userId) {
        //makePusherEvent() function is declared in notification.js
        makePusherEvent('message-notif-'+userId, 'message', showMessageNotification);
    }
}

/**
 * will remove the dot icon on the message icon
 *
 * @author Alvin Generalo <alvin_generalo@commude.ph>
 */
function messageIconClickEvent()
{
    if($(this).hasClass('unread')) {
        $(this).removeClass('unread');
    }
}


$(function() {

    //initialize global variables
    initializeGlobalVarMessage();

    //set necessary events functions
    setEvents();


});

/**
 * append a notification in the message notification
 *
 * @param {array} data
 *
 * @author Alvin Generalo <alvin_generalo@commude.ph>
 */
function showMessageNotification(data)
{
    console.log(data);

    var messageNotifHtml = '';

    var url = getMessageNotifUrl(data);

    if($('#thread_'+data.threadId).length) {
        //if the notification exists
        //just update it

        var messageNotif = $('#thread_'+data.threadId);

        messageNotifHtml += '<b>'+ data.name +'</b><br>';
        messageNotifHtml += data.message;

        messageNotif.find('a p').html(messageNotifHtml);

    } else {

        //if the notification does not exists
        //crete a new one

        messageNotifHtml += '<li id="thread_'+ data.threadId +'" class="item">';
        messageNotifHtml += '<p>' + moment(data.date).format("ddd, hh:mm A"); + '</p><br> ';
        messageNotifHtml +=     '<span class="user-icon">';
        messageNotifHtml +=     '<img src="' + data.image + '" alt="" srcset="">';
        messageNotifHtml +=     '</span>';
        messageNotifHtml +=     '<a href="'+ url +'">';
        messageNotifHtml +=         '<p class="text">';
        messageNotifHtml +=             '<b>'+ data.name +'</b><br>';
        messageNotifHtml +=              data.message + '<br>';
        messageNotifHtml +=        '</p>';
        messageNotifHtml +=    '</a>';
        messageNotifHtml += '</li>';

        messageNotifArea.after(messageNotifHtml);

        $('#noMessageItem').hide();

    }

    //show the dot
    messageIcon.addClass('unread');

}

/**
 * return the url for the recevied message
 *
 * @param {array} data
 *
 * @author Alvin Generalo
 */
function getMessageNotifUrl(data)
{
    var url = '';
    switch(data.type) {
        case 'company-scout':
            url = APP_URL + '/applicant/scout-detail/' + data.id;
        break;

        case 'company-applicants':
            url = APP_URL + '/applicant/application-history/details/' + data.id;
        break;

        case 'scout-company':
            url = APP_URL + '/company/scout/favorite-user/choosTemplate/' + data.id;
        break;

        case 'applicants-company':
            url = APP_URL + '/company/applicant/job-application/' + data.id;
        break;

        case 'admin-company':
            url = APP_URL + '/company/inquiry';
        break;

        case 'admin-list-company':
            url = APP_URL + '/company/notice/details/' + data.id;
        break;

        case 'company-admin':
            url = APP_URL + '/admin/inquiry/detail?id='  + data.threadId;
        break;

        case 'company-list-admin':
        url = APP_URL + '/admin/inquiries/details/' + data.id;
        break;

        case 'company-admin-contact':
            url = APP_URL + '/admin/companies/contact/' + data.id;
        break;

        default:
    }

    return url;
}

/**
 * will return the url of the icon of the user
 *
 * @param {array} data
 *
 * @author Alvin Generalo
 */
function getMessageIcon(data)
{
    var icon = '';
    switch(data.type) {
        case 'company-scout':
        case 'company-applicants':
            icon = $('#companyIcon').val();
        break;

        case 'scout-company':
        case 'applicants-company':
            icon = $('#userIcon').val();
        break;

        case 'admin-company':
        case 'admin-list-company':
            icon = APP_URL + '/storage/uploads/master-admin/leapwork_logo.png';
        break;

        case 'company-admin':
        case 'company-admin-contact':
            icon = data.icon;
        break;

        default:
    }

    return icon;
}

/**
 * this will append the received message to the message area if the current page has it
 *
 * @param {arrray} data
 *
 * @author Alvin Generalo
 */
function appendToMessageArea(data)
{
    if($('#message_area_'+data.threadId).length) {
        appendMessage(data.threadId, data.message, getMessageIcon(data), 'left', data.name, moment().format('lll'), $('#isScout').length);
    }
}