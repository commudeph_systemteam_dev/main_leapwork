/**
 * @summary  master-admin shared js handlers and validators
 *
 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
 * @since    2018-03-20
 *
 */
$(function () {

	/**
	 * @summary Sub content nav bar click event
	 *
	 * @author  Martin Louie Dela Serna <martin_delaserna@commude.ph>
	 * @since   2017-10-16
	 * 
	 * @return  void     
	 */


});