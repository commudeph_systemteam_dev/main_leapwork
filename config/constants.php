<?php 
return [
	'MASTER_PLAN_VALIDITY_DAYS_THRESHOLD' => [
		//in days
		'TRIAL' 		=> '7',
		'STANDARD' 		=> '30',
		'ENTERPRISE'	=> '30',
	],

	'MASTER_PLAN_VALIDITY_MONTHS' => [
		//in months
		'TRIAL' 		=> '1',
		'STANDARD' 		=> '12',
		'ENTERPRISE'	=> '12',
	],

	'APP_EMAIL' => 'admin@leapwork.com',
	'APP_NO_REPLY_EMAIL' => 'no-reply@leapwork.com',
	
	'planType' => [
		'TRIAL',
		'STANDARD',
		'ENTERPRISE'
	],
	
	'PLAN_TYPE_DICTIONARY' => [
		'TRIAL' => 'TRIAL',
		'STANDARD' => 'STANDARD',
		'ENTERPRISE' => 'ENTERPRISE'
	],

	'paymentMethod' => [
		'BANK TRANSFER' => 'Bank Transfer',
		// 'PAYPAL' => 'Paypal',
		'CREDIT CARD' => 'Credit Card'
	],

	'paymentMethodLinks' => [
		'BANK TRANSFER' => '',
		// 'PAYPAL' => 'Paypal',
		'CREDIT CARD' => '/payment/credit_card/'
	],

	'BANK_TRANSFER_MAIL_PATTERNS' => [
		'bankName' => 'BDO Savings Account',
		'bankAccountName' => 'Leapwork Co. Savings',
		'bankAccountNo' => '9806720035814'
	],

	'paymentStatus' => [
		'PAID',
		'UNPAID',
		'CANCELLED'
	],

	'pagination' => [
        'limit'  => '5'
	],
	
	'accountType' => [
        'ADMIN',//0  
        'USER', //1 
        'CORPORATE ADMIN', //2
        'CORPORATE USER',  //3
	],

	'scoutStatus' => [
	      'DEFAULT' => 'Select',
        'ACCEPTED' => 'Accepted',    
        'INTERVIEW' => 'Interview',  
        'UNDER REVIEW' => 'Under Review',
        'NOT ACCEPTED' => 'Not Accepted',
        'FAVORITE'		=> 'Favorite',
        'WITHDRAW'		=> 'Withdraw',
        'JOB OFFER'		=> 'Job Offer',
        'PENDING'		=> 'Pending',
	],

	'applicantScoutStatus' => [
		'DEFAULT' => 'Select',
	  'ACCEPTED' => 'Accepted',    
	  'INTERVIEW' => 'Interview',  
	  'UNDER REVIEW' => 'Under Review',
	  'NOT ACCEPTED' => 'Not Accepted',
	  'WITHDRAW'		=> 'Withdraw',
	  'JOB OFFER'		=> 'Job Offer',
	  'PENDING'		=> 'Pending',
  ],

	/**ADMIN to COMPANY change status */
	'en_companyStatus' => [
        'ACTIVE'    => 'Active',  
        'INACTIVE'  => 'Inactive',  
        'DISABLED'  => 'Disabled',  
        'UNVERIFIED'=> 'Unverified'  
        // 'VERIFIED'  => 'Verified'
	],
	/**ADMIN to COMPANY change status */
	'jp_companyStatus' => [
		'ACTIVE'    => 'ACT',  
		'INACTIVE'  => '非ACT',  
		'DISABLED'  => 'Disabled',  
		'UNVERIFIED'=> 'Unverified',  
		'VERIFIED'  => 'Verified'
		],
	/**Applicant NEWS/ANNOUNCEMENTS */
	'announcements-filter' => [
	    ''			=> 'All',  
        'Admin'     => 'Admin',  
        'Scout'     => 'Scout Mail',  
        // 'Comment'	=> 'Comment' 
	],

	'applicantScoutStatus' => [
        ''				=> 'All',  
        'ACCEPTED'      => 'Accepted',  
        'DECLINED'      => 'Declined',  
        'REJECTED'		=> 'Rejected',  
        'INTERVIEW' 	=> 'Interview',  
        'UNDER REVIEW'  => 'Under Review'
	],

	'mailStatus' => [
        'ACCEPTED' => 'Accept',  
        'DECLINED' => 'Decline',  
        'REJECTED' => 'Reject',  
        'INTERVIEW' => 'Interview',  
        'UNDER REVIEW' => 'Under Review'
	],
	
	// MAIL Type
	'mailTypes' => [
        '0' => 'Accepted',  
        '1' => 'Declined',  
        '2' => 'Rejected',  
        '3' => 'For interview',  
        '4' => 'Under review'
	],

    //************ MONTHS ************/
    'months' => [
        '1' => 'January',
        '2' => 'February',
        '3' => 'March',
        '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
],

    //************ JOB APPLICATION STATUS ************/

    'jobApplicationStatus' => [
	    "INTERVIEW"    	=> "Interview",
	    "PENDING"      	=> "Pending" ,
	    "JOB OFFER"    	=> "Job Offer",
	    "ACCEPTED"     	=> "Accepted", 
	    "UNDER REVIEW" 	=> "Under Review",
	    "NOT ACCEPTED" 	=> "Not Accepted",
		"CANCELLED"    	=> "Cancelled",
		"WITHDRAW"		=> "Withdraw"
	],
	
	'jobApplicationStatus_Corp' => [
	    "INTERVIEW"    	=> "Interview",
	    "PENDING"      	=> "Pending" ,
	    "JOB OFFER"    	=> "Job Offer",
	    "ACCEPTED"     	=> "Accepted", 
	    "UNDER REVIEW" 	=> "Under Review",
	    "NOT ACCEPTED" 	=> "Not Accepted",
		"CANCELLED"    	=> "Cancelled"
    ],

    //************ COMPANY MAIL TEMPLATEs ************/
    'companyMailTemplateTypes' => [
        'ACCEPTED',
        'DECLINED',
        'REJECTED',
        'FOR INTERVIEW',
        'UNDER REVIEW'
	],
    //************ COMPANY MAIL DYNAMIC HOLDERS ************/
	'companyMailDynamicHolders' => [
		'en' => [
	        '{{applicantName}}' 	 => "Displays the applicant scout name",  
	        '{{companyName}}'		 => "Displays your company name",    
	        '{{companyAdmin}}'		 => "Displays your company admin's name",    
	        '{{companyAdminEmail}}'  => "Displays your company admin's email",  
	        '{{companyAdminAddress}}'=> "Displays your company's address",  
	        '{{jobPositionName}}'	 => "Displays the Job Position Name selected",
	        '{{jobPositionLink}}'    => "Displays the Job Position Link"
        ],
        'jp' => [
	        '{{applicantName}}' 	 => "Displays the applicant scout name",  
	        '{{companyName}}'		 => "Displays your company name",    
	        '{{companyAdmin}}'		 => "Displays your company admin's name",    
	        '{{companyAdminEmail}}'  => "Displays your company admin's email",  
	        '{{companyAdminAddress}}'=> "Displays your company's address",  
	        '{{jobPositionName}}'	 => "Displays the Job Position Name selected",
	        '{{jobPositionLink}}'    => "Displays the Job Position Link"
        ]
	],

    //************ applicants_education STATUS ************/
    'collegeStatus' => [
    	'GRADUATED' => 'Graduated',
    	'ON GOING'  => 'On-going'
    ],

    /************ FILE DIRECTORIES ************/
    'fileDirectory' =>[
		'applicant' => '/public/storage/uploads/applicantProfile/'
	],


    /************ DIRECTORIES / STORAGE DIRECTORIES ************/
	'storageDirectory' =>[
		'applicantComplete' => '/storage/uploads/applicantProfile/',
		'companyComplete'   => '/storage/uploads/company/'

	],

    /************ COMPANY ************/
	'companyDirectory' => 'uploads/company/',
	
	/************ COMPANY Job Post Create ************/
	'en_numberOfImagesToUpload' => 
	[
		'1' => 'Upload 1 Image',
		'2' => 'Upload 2 Images'
	],
	'jp_numberOfImagesToUpload' => 
	[
		'1' => 'Upload 1 Image',
		'2' => 'Upload 2 Images'
	],

    /************ USER ************/
	'applicantProfileDirectory' => '/uploads/applicantProfile/',

	/************ MAILABLES ************/
	'emailLeapWorkDevTeam' => 
				[
					// 'karenirenecano.commude@gmail.com',
					'alvin_generalo@commude.ph',
					'ken_kasai@commude.ph',
					'martin_delaserna@commude.ph',
					// 'nicole_manalo@commude.ph',
					// 'rey_besmonte@commude.ph',
					'aj_alipio@commude.ph'
				],
	/************ ANALYTICS ************/
	'timeframe' => 
	[
		'0' => '',
		'1'  => 'Last Month',
		'2'  => 'Last 2 Months' 
	],

	/************ APP URL ************/
	'app' => 
	[
		'locales' => 'jp',
		'url' => env('APP_URL')
		
	],

	/**Header Notifications */
	'notifications' =>
	[
        'limit'  => '3'
	],

	'locales' => [
		'en' => 'en',
		'jp' => 'jp'
	],
	'en_select_job_post_visibility' => [
		'' => 'All',
		'PUBLIC' => 'Public',
		'PRIVATE' => 'Private',
	],
	'jp_select_job_post_visibility' => [
		'' => 'All',
		'PUBLIC' => 'Public',
		'PRIVATE' => 'Private',
	],
	'notification_type' => [
		'BELL_ICON'						=> 'BELL_ICON',
		'MESSAGE_ICON'					=> 'MESSAGE_ICON',
		'NEW_JOB_APPLICANT' 			=> 'NEW_JOB_APPLICANT',
		'SCOUT'							=> 'SCOUT',
		'INQUIRY'						=>'INQUIRY',
		'JOB_APPLICATION_CONVERSATION'	=>'JOB_APPLICATION_CONVERSATION',
		
	],
];