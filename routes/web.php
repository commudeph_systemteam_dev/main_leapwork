<?php

Route::get('/testing', 'Controller@test');
Route::post('setLocale/{selectedLocale}', 'LocalizationController@localize');
// Route::any('masterbuild','AutoBuildController@autoBuild'); TEST PUSH

Route::get('', 'Controller@seeLogin');
Route::get('login', 'Controller@seeLogin');
Route::get('activate/{token}', 'Controller@userActivation');

//Try for password reset link
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
// Password Reset Routes...
Auth::routes();

Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/activatePlan/{claimsId}', 'ClaimController@emailActivatePlan');

Route::group(['middleware' => ['web','auth']], function(){
   
    Route::post ('/job/comment',           'CommentController@createComment');
    /* Routes for USER */


    /* Routes for ADMIN */
    Route::group(['middleware' => ['checkAdmin'], 'prefix' => 'admin'], function(){
        Route::get('', 'AdminController@adminDashboard');

        //Company List
        Route::get('companies',                      'AdminCompanyController@companies');
        Route::get('companies/create',               'AdminCompanyController@createView');
        Route::get('companies/edit',                 'AdminCompanyController@editView');
        Route::post('companies/createCompany',         'AdminCompanyController@createCompany');
        Route::post('companies/editCompany',         'AdminCompanyController@editCompany');  
        Route::post('companies/editCompanyPassword',         'AdminCompanyController@editCompanyPassword');      
        Route::post('companies/companyStatusChange', 'AdminCompanyController@companyStatusChange');
        Route::post('company/verify',                'AdminCompanyController@verifyCompany');
        Route::post('company/deny',                'AdminCompanyController@denyCompany');
        
        Route::get('companies/{companyid}',          'AdminCompanyController@companyDetail');
        Route::post('companies/search',              'AdminCompanyController@searchCompany');
        
        Route::get('companies/contact/{companyId}',  'AdminCompanyController@contactCompanyView');
        Route::post('companies/contact/sendCompanyEmailConversation', 'AdminCompanyController@sendCompanyEmailConversation');
        Route::post('companies/contact/replyCompanyConversation',     'AdminCompanyController@replyCompanyConversation');
        
        Route::get ('home',              'AdminController@adminDashboard');

        Route::get ('dashboard',         'AdminController@adminDashboard');
        Route::get ('billing',           'AdminController@billing');
        Route::post ('billing',          'AdminController@billing');
        Route::get('billing/invoice_details/{claim_id}', 'ClaimController@invoiceDetails');

        Route::get('billing/detail/{id}',       'AdminController@billingDetail');
        Route::get('view/billing/{claimsId}',       'ClaimController@viewSentInvoice');
        Route::get('issue/billing/{claimsId}',       'ClaimController@sendInvoice');
        Route::get('markAsPaid/billing/{claimsId}',       'ClaimController@markAsPaid');
        Route::get('ranking',                   'AdminController@ranking');
        Route::Post('ranking',                  'AdminController@ranking');

        Route::get('ranking/viewJobDetails/{blade}/{id}', 'AdminController@viewJobDetails');

        // Admin Announcements
        Route::get('notice',        'AdminAnnouncementController@index');
        Route::get('notice/create', 'AdminAnnouncementController@createView');
        Route::post('notice/ajax',  'AjaxController@getUserAjax');
        Route::get('notice/edit',   'AdminAnnouncementController@editView');

        Route::post('notice/search',             'AdminAnnouncementController@searchAnnouncement');
        Route::post('notice/createAnnouncement', 'AdminAnnouncementController@saveAnnouncement');
        Route::post('notice/editAnnouncement',   'AdminAnnouncementController@saveAnnouncement');
        Route::post('notice/deleteAnnouncement', 'AdminAnnouncementController@archiveAnnouncement');        
        
        // Company Inquiries
        Route::get('inquiries',                 'AdminInquiryController@index');
        Route::get('inquiry/detail',            'AdminInquiryController@inquiryDetails');
        
        Route::post('inquiry/replyConversation', 'AdminInquiryController@replyConversation');
        Route::post('inquiries/search',          'AdminInquiryController@searchInquiry');

        /****************** MASTER SETTINGS ******************/
        // Master Features
        Route::get('master-admin',                          'MasterJobFeatureController@index');
        Route::post('master-admin/saveJobFeature',          'MasterJobFeatureController@saveJobFeature');
        Route::post('master-admin/editJobFeature',          'MasterJobFeatureController@editJobFeature');
        Route::get('master-admin/searchJobDetails/{job_id}','MasterJobFeatureController@getJobDetailsForFeature');
        // Master Jobs
        Route::get('master-admin/job', 'MasterJobController@index');
        Route::post('master-admin/job/delete/jobIndustry/{id}', 'MasterJobController@deleteJobIndustry');
        Route::post('master-admin/job/saveJobClassification', 'MasterJobController@saveJobClassification');
        Route::post('master-admin/job/delete/jobClassification/{id}', 'MasterJobController@deleteJobClassification');
        Route::post('master-admin/job/saveJobIndustry',       'MasterJobController@saveJobIndustry');
        Route::post('master-admin/job/saveJobPosition',       'MasterJobController@saveJobPosition');
        Route::post('master-admin/job/delete/jobPosition/{id}', 'MasterJobController@deleteJobPosition');
        Route::post('master-admin/job/saveCountry',           'MasterJobController@saveCountry');
        Route::post('master-admin/job/delete/country/{id}', 'MasterJobController@deleteCountry');
        Route::post('master-admin/job/saveSalaryRange',       'MasterJobController@saveSalaryRange');
        Route::post('master-admin/job/delete/salaryRange/{id}', 'MasterJobController@deleteSalaryRange');
        Route::post('master-admin/job/saveEmployeeRange',     'MasterJobController@saveEmployeeRange');
        Route::post('master-admin/job/delete/employeeRange/{id}', 'MasterJobController@deleteEmployeeRange');
        Route::post('master-admin/job/saveSkill',             'MasterJobController@saveSkill');
        Route::post('master-admin/job/delete/skill/{id}', 'MasterJobController@deleteSkill');
        
         // Master Plans
        Route::get('master-admin/plan',        'MasterPlanController@index');
        Route::get('master-admin/plan/create', 'MasterPlanController@createView');
        Route::get('master-admin/plan/edit',   'MasterPlanController@editView');

        Route::post('master-admin/plan/createPlan', 'MasterPlanController@savePlan');
        Route::post('master-admin/plan/editPlan',   'MasterPlanController@savePlan');

        // Route::get('master-admin/mail', function () {
        //     return view('admin.master-admin.mail');
        // });

        Route::get('master-admin/mail', 'MasterUserMailTemplateController@index');
        Route::get('master-admin/user-edit-mail/{mailTemplateId}', 'MasterUserMailTemplateController@editView');
        Route::post('master-admin/mail/saveTemplate', 'MasterUserMailTemplateController@saveCompanyMailTemplate');

        /*master user management*/
        Route::get('master-admin/user','MasterUserManagementController@getActiveUsers');
        Route::get('master-admin/user/create', 'MasterUserManagementController@createView');
        Route::post('master-admin/user/save',  'MasterUserManagementController@saveNewUser');
        Route::post('master-admin/user/savePassword',  'MasterUserManagementController@saveUserPassword');
        Route::get('master-admin/user/emailCheck', 'MasterUserManagementController@emailCheck');
        
        Route::get('master-admin/user/delete/{userId}','MasterUserManagementController@softDeleteUser');
        Route::get('master-admin/user/edit/{userId}/{accountType}','MasterUserManagementController@editUser');
        
        Route::get('master-admin/user/update/{userId}/{accountType}','MasterUserManagementController@updateAccountType');
        Route::get('master-admin/user/emailCheck',  'MasterUserManagementController@emailCheck'); 

        Route::get('master-admin/viewJobDetails/{blade}/{id}', 'AdminController@viewJobDetails');

    });

     /* Routes for CORPORATE */
    Route::group(['middleware' => ['checkCompany'], 'prefix' => 'company'], function(){

        

        Route::get('', 'CompanyController@home');
        Route::get('home', 'CompanyController@home');

        Route::get('paymentMethod/{paymentMethod}/{claimToken}','ClaimController@renderPaymentMethod');
        Route::get ('payment/credit_card',      'PaymentController@creditCard');    
        Route::post('payment/credit_card',      'PaymentController@creditCard');
            
        Route::get ('payment/payment_complete',  'PaymentController@paymentComplete');
        Route::post('payment/payment_complete',  'PaymentController@paymentComplete');
            
        Route::get ('payment/confirmation',  'PaymentController@confirmation');
        Route::post('payment/confirmation',  'PaymentController@confirmation');
            
        Route::get ('payment/credit_card/{code}',  'PaymentController@creditCardWithCode');

        // Company Profilenew
        Route::group(['middleware' => ['checkCompanyCompanyInfo']], function()
        {

            Route::get('profile', 'CompanyProfileController@index');
            Route::get('profile/howto/write/company-info', 'CompanyProfileController@howToWriteCompanyProfile');
            Route::get('profile/edit', 'CompanyProfileController@editView');
            Route::post('profile/editProfile', 'CompanyProfileController@saveProfile');
        });

        /* company account management  module */
        Route::get('account',                           'CompanyAccountUserController@users');
        Route::get('account/user/create',               'CompanyAccountUserController@createView');
        Route::get('account/user/edit',                 'CompanyAccountUserController@editCorporateView');
        Route::get('account/user/edit/{id}',            'CompanyAccountUserController@editView');
        Route::post('account/user/saveUser',            'CompanyAccountUserController@saveUser');
        Route::post('account/user/editUser',            'CompanyAccountUserController@editUser');
        Route::post('account/user/editUserPassword',    'CompanyAccountUserController@editUserPassword');
        Route::post('account/user/archiveUser',         'CompanyAccountUserController@saveUserStatus');
        Route::post('account/user/activate',            'CompanyAccountUserController@saveUserStatus');
        Route::post('account/user/deactivate',          'CompanyAccountUserController@saveUserStatus');
        
        Route::get('account/user/emailCheck',  'CompanyAccountUserController@emailCheck'); 

        Route::get('account/mail-templates',  'CompanyMailTemplateController@index');
        
        Route::get('account/mail-templates/display/{id}',       'CompanyMailTemplateController@details');
        Route::get('account/mail-templates/edit/{type}/{id?}',  'CompanyMailTemplateController@editView');
        Route::post('account/mail-templates/saveMailTemplate', 'CompanyMailTemplateController@saveMailTemplate');


        Route::get('account/delete/{userId}','CompanyController@softDeleteUser');
        // Route::get('account/update/{userId}/{accountType}','CompanyController@updateAccountType');

        /* aplicant management */
        Route::get('applicant', 'CompanyController@applicant');
        Route::get('applicant/status/{id}', 'CompanyController@applicantStatus');
        Route::get('applicant/resume/{id}', 'CompanyController@applicantResume');
        Route::post('applicant/edit', 'CompanyController@applicantEdit');
        
        Route::get('applicant/job-application/{jobApplicationId}', 'CompanyApplicantController@jobApplicattionView');

        Route::post('applicant/job-application/sendEmailConversation', 'CompanyApplicantController@sendEmailConversation');
        Route::post('applicant/job-application/replyConversation',     'CompanyApplicantController@replyConversation');
        Route::post('applicant/job-application/changeStatus',     'CompanyApplicantController@changeJobApplicationStatus');
        
        Route::get('applicant/questionaire', 'CompanyApplicantInquiryController@applicantQuestionaire');
        Route::get('applicant/questionaire/detail/{id}/{notificationId?}', 'CompanyApplicantInquiryController@applicantQuestionaireDetail');
        Route::get('applicant/inquiry/delete/{id}', 'CompanyApplicantInquiryController@hardDeleteInquiry');
        Route::post('applicant/inquiry/reply', 'ApplicantInquiriesController@createJobInquiryReply');
        
        Route::get('recruitment-jobs',  'CompanyController@recruitmentJobs');
        Route::post('recruitment-jobs',  'CompanyController@recruitmentJobs');
        Route::get('recruitment-jobs/{id}', 'CompanyController@updateJobPost');
        Route::get('recruitment-jobs/viewJobDetails/{blade}/{id}',  'CompanyController@viewJobDetails');

        Route::get('job-posting', 'CompanyController@jobPosting');
        Route::post('job-posting', 'CompanyController@jobPosting');
        Route::get('job-posting-preview', 'CompanyController@jobPosting');
        Route::get('job-posting/edit/{id}', 'CompanyController@editJobPosting');
        Route::post('job-posting/save/{id}', 'CompanyController@saveJobPosting');
        Route::get('job-posting/howto/write', 'CompanyController@writeJobPostGuide');
        Route::get('job-posting/howto/edit/{id}', 'CompanyController@editJobPostGuide');

        Route::group(['middleware' => ['checkCompanyScout']], function()
        {
            Route::get('scout', 'CompanyScoutController@scout');
            Route::get('scout/resume/{id}', 'CompanyScoutController@scoutResume');
            Route::get('scout/favorite/add/{id}', 'CompanyScoutController@addToFavorite');
            Route::post('scout/favorite/addMany', 'CompanyScoutController@addManyFavorite');

            Route::get('scout/search', 'CompanyScoutController@scoutSearch');
            Route::post('scout/search', 'CompanyScoutController@scoutSearch');
            Route::get('scout/search/resume/{id}', 'CompanyScoutController@scoutResume');

            Route::get('scout/favorite-user', 'CompanyScoutController@scoutFavoriteUser');


            Route::get('scout/favorite-user/choosTemplate/{id}/{notificationId?}', 'CompanyScoutController@chooseTemplateToEmail');

            Route::post('scout/favorite-user/choosTemplate/update', 'CompanyScoutController@updateScout');


            Route::get('scout/favorite-user/choosTemplate/status/{companyId}/{applicantId}/{mailStatus}/{jobPostId?}', 'CompanyScoutController@loadTemplateEmail');
            Route::post('scout/favorite-user/choosTemplate/preview', 'CompanyScoutController@previewScoutEmail');
            Route::post('scout/favorite-user/choosTemplate/sendEmailConversation', 'CompanyScoutController@pushEmailConversation');
            Route::post('scout/favorite-user/choosTemplate/replyConversation', 'CompanyScoutController@replyEmailConversation');
            
            Route::get('scout/favorite-company', 'CompanyScoutController@scoutFavoriteCompany');
            Route::get('scout/done', 'CompanyScoutController@done');
        });

        // Company Announcements
        Route::get('notice',         'CompanyAnnouncementController@index');
        Route::get('notice/announcements',         'CompanyAnnouncementController@adminAnnouncements');

        Route::get('notice/announcements/details/{announcementId}', 'CompanyAnnouncementController@viewAnnouncements');

        Route::get('notice/details', 'CompanyAnnouncementController@details');
        Route::get('notice/markAsRead/{notificationId}', 'CompanyAnnouncementController@markAsRead');
        Route::get('notice/updateIconInstance/{iconType}', 'CompanyAnnouncementController@updateIconInstance');
        
        Route::get('/notice/details/{companyId}', 'CompanyAnnouncementController@messagesView');
        Route::post('/notice/details/sendCompanyEmailConversation', 'CompanyAnnouncementController@sendCompanyEmailConversation');
        Route::post('/notice/details/replyCompanyConversation', 'CompanyAnnouncementController@replyCompanyConversation');

        // Company Inquiries
        Route::get('inquiry',                    'CompanyInquiryController@index');
        Route::post('inquiry/sendInquiry',       'CompanyInquiryController@sendInquiry');
        Route::post('inquiry/replyConversation', 'CompanyInquiryController@replyConversation');

        
        //CORPORATE ADMIN only routes
        Route::group(['middleware' => ['checkCompanyAdmin']], function(){
            // Billing
            Route::get('billing',             'CompanyBillingController@index');
            Route::post('billing',            'CompanyBillingController@index');
            Route::get('billing/plan',        'CompanyBillingController@index');
            Route::get('billing/plan/change', 'CompanyBillingController@changePlanView');
            Route::get('billing/detail',      'CompanyBillingController@billingDetail');
            Route::get('billing/detail/{planID}',      'CompanyBillingController@billingDetail');
            Route::post('billing/detail',     'CompanyBillingController@saveBillingDetail');
            Route::get('billing/invoice',     'CompanyBillingController@billingInvoice');
            Route::post('billing/invoice',    'CompanyBillingController@billingInvoice');

            Route::get('billing/payment',     'CompanyBillingController@billingPayment');
            Route::post('billing/change-plan','CompanyBillingController@changePlan');

            Route::get('analytics', 'CompanyAnalyticsController@analytics');
            Route::get('analytics/{id}', 'CompanyAnalyticsController@analyticsOfJobPost');
            Route::get('analytics/{id}/{numOfmonth}', 'CompanyAnalyticsController@analyticsOfJobPost');

            Route::get('billing/invoice_details/{claim_id}', 'CompanyBillingController@invoiceDetails');
        });

    });

});


Auth::routes();

// Readied routes for password reset routes for corporate account (issuance of password via reset) if necessary
// Route::get('account/password/reset',  'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('account/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/account/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/account/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/account/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
Route::post('/account/password/reset', 'Auth\ResetPasswordController@reset');

Route::get ('home',                         'HomeController@index')->name('home');
/*social oauth */
Route::get ('auth/{provider}',              'Auth\LoginController@redirectToProvider');
Route::get ('auth/{provider}/callback',     'Auth\LoginController@handleProviderCallback');
/*normal login*/

Route::post('loginUser',                    'Auth\LoginController@authenticate');
Route::get ('logoutUser',                   'Auth\LoginController@logout');


Route::get ('reset',                        'Auth\LoginController@reset');


Route::post('back',                         'FrontController@redirectBackPath');
Route::get ('/job/details/{id}',            'FrontController@jobDetails');

Route::get ('/jobs/{id}',          'FrontController@companyJobs');

// Route::get ('/job/details/added_to_favorite/{id}',    'FrontController@addToFavorite');
// Route::get ('/job/details/remove_from_favorite/{id}',  'FrontController@removeFromFavorite');

// Route::get ('/job/details/added_to_like/{id}'    ,'FrontController@addToLike');
// Route::get ('/job/details/remove_from_like/{id}',  'FrontController@removeFromLike');
// Route::get ('/job/details/login_to_apply/{id}'    ,'FrontController@loginToApply');

Route::post('/job/apply', 'FrontController@jobApply');

Route::group(['prefix' => 'front'], function(){
    Route::get('/', 'FrontController@defaultIndex');
    Route::get('search', 'FrontController@globalSearchJob');

    Route::post('top'    ,   'FrontController@topIndex');
    Route::get ('top'    ,   'FrontController@topIndex');

    Route::post('top/default'    ,   'FrontController@topDefault')->middleware('checkApplicantDefault');
    Route::get ('top/default'    ,   'FrontController@topDefault')->middleware('checkApplicantDefault');
    Route::get ('top/default_rev'    ,   'FrontController@topDefault_rev');
    // Route::get ('top/default_rev'    ,  function() {
    //     return view('front.top.default_rev');
    // } );

    Route::get ('latestjobs', 'FrontController@latestJobsIndex');
    Route::post('latestjobs', 'FrontController@latestJobsIndex');

    Route::get ('popularjobs', 'FrontController@popularJobsIndex');
    Route::post('popularjobs', 'FrontController@popularJobsIndex');

    Route::get ('favoritelist', 'FrontController@favoriteListIndex');
    Route::post('favoritelist', 'FrontController@favoriteListIndex');

    Route::post('searchjobs', 'FrontController@searchJobsIndex');
    Route::get ('searchjobs', 'FrontController@searchJobsIndex');

    Route::post('searchjobs/results',   'FrontController@searchJobResults');
    Route::get ('searchjobs/results',   'FrontController@searchJobResults');
    

    Route::get ('/job/details/{id}',            'FrontController@jobDetails');

    Route::get ('/jobs/{id}',          'FrontController@companyJobs');

    Route::get ('/job/details/added_to_favorite/{id}',    'FrontController@addToFavorite');
    Route::get ('/job/details/remove_from_favorite/{id}',  'FrontController@removeFromFavorite');

    Route::get ('/job/details/added_to_like/{id}'    ,'FrontController@addToLike');
    Route::get ('/job/details/remove_from_like/{id}',  'FrontController@removeFromLike');
    Route::get ('/job/details/login_to_apply/{id}'    ,'FrontController@loginToApply');
});
    // Route::get('applicant/news/{announcementId}',     'ApplicantNewsAnnounceController@announcementDetail');



Route::group(['middleware' => 'checkApplicant','prefix' => 'applicant'], function(){

    Route::get('/', 'ApplicantNewsAnnounceController@newsIndex');
    Route::get('news/{announcementId}',     'ApplicantNewsAnnounceController@announcementDetail');
    Route::get('news/{announcementLabel?}',     'ApplicantNewsAnnounceController@newsIndex');
    
    Route::get('comments', 'ApplicantInquiriesController@commentsIndex');
    Route::get('comment-details/{jobPostId}/{notificationId?}', 'ApplicantInquiriesController@commentDetails');
    Route::post('comment-details/reply', 'ApplicantInquiriesController@createJobInquiryReply');
  
    Route::get('application-history',                                   'ApplicantApplicantionHistoryController@index');
    Route::get('application-history/details/{jobApplicationThreadId}',  'ApplicantApplicantionHistoryController@details');
    Route::post('application-history/details/replyConversation',        'ApplicantApplicantionHistoryController@sendEmailConversation');
    Route::post('application-history/details/newInquiry',        'ApplicantApplicantionHistoryController@newInquiry');
    Route::post('application-history/changeJobApplicationStatus', 'ApplicantApplicantionHistoryController@changeJobApplicationStatus');
    
    Route::get('browsing-history',                    'ApplicantBrowsingHistoryController@index');
    Route::post('browsing-history/addToFavorite',     'ApplicantBrowsingHistoryController@addToFavorite');
    Route::post('browsing-history/removeFromHistory', 'ApplicantBrowsingHistoryController@removeFromHistory');
    Route::post('browsing-history/removeFromFavorites', 'ApplicantBrowsingHistoryController@removeFromFavorites');
    
    Route::get('scout', 'ApplicantScoutController@scoutIndex');
    Route::get('scout/{scoutStatus}', 'ApplicantScoutController@scoutIndex');
    Route::get('scout-detail/{scoutId}/{id?}', 'ApplicantScoutController@scoutDetail');
    Route::post('scout-detail/replyScoutConversation', 'ApplicantScoutController@createReplyScoutConversation');
    

    Route::get('profile/account',              'ApplicantProfileController@profileAccountIndex');
    Route::get('profile/account/emailCheck',   'ApplicantProfileController@profileEmailCheck');
    Route::post('profile/account/saveProfile', 'ApplicantProfileController@saveProfileAccount');
    Route::post('profile/account/savePassword', 'ApplicantProfileController@saveProfilePassword');

    // refractor "history" to resume as per new design layout
    Route::get('profile/resume',               'ApplicantProfileController@profileHistoryIndex'); 
    Route::post('profile/resume',               'ApplicantProfileController@profileHistoryIndex'); //post for page segues

    Route::post('profile/resume/display',       'ApplicantProfileController@displayResumeDocument'); 
    
    Route::post('profile/history/saveProfileHistory', 'ApplicantProfileController@saveProfileHistory');


    Route::get('notice/updateIconInstance/{iconType}', 'CompanyAnnouncementController@updateIconInstance');
   
});


Route::get ('register-company',             'CompanyController@createCompany');
Route::post('register-company',             'CompanyController@createCompany');

Route::get('register-complete', function(){
    return view('auth.register-complete');
});

Route::get  ('inactive',               'Auth\LoginController@inactive');
Route::post ('inactive',              'Auth\LoginController@inactive');
Route::get('permission-denied', function(){
    return view('error');
});

Route::get  ('register/success',               'Auth\LoginController@success');
Route::post ('register/success',              'Auth\LoginController@success');

Route::get('invoice/{company_id}/{id}', 'ClaimController@publicInvoiceDetails');

Route::get('paymentMethod/{paymentMethod}/{claimToken}','ClaimController@renderPaymentMethod');
Route::get ('payment/credit_card',      'PaymentController@creditCard');    
Route::post('payment/credit_card',      'PaymentController@creditCard');

Route::get ('payment/payment_complete',  'PaymentController@paymentComplete');
Route::post('payment/payment_complete',  'PaymentController@paymentComplete');

Route::get ('payment/confirmation',  'PaymentController@confirmation');
Route::post('payment/confirmation',  'PaymentController@confirmation');

Route::get ('payment/credit_card/{code}',  'PaymentController@creditCardWithCode');

 //Services - ADD MIDDLEWARE!!
// Termed as services calls since this is not binded in a single controller - reusable functions 
Route::get('master-skills/all/{jobPositionId?}', 'API\MasterSkillController@getAllSkills');
Route::get('emailCheck', 'API\UserAPIController@emailCheck');

Route::post('announcement/seen', 'AdminAnnouncementController@seen');
Route::post('announcement/get', 'AdminAnnouncementController@get');

Route::get('email/{email}',               'API\UserAPIController@isEmailExists');
Route::get('company/name/{companyName}',  'API\CompanyAPIController@isCompanyNameExists');
